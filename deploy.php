<?php

namespace Deployer;

require_once 'recipe/common.php';
require_once 'tasks/lohasit.php';

set('application', 'beta3-site-ocean');

set('git_tty', true);

// Shared files/dirs between deploys
// set('shared_files', []);
// set('shared_dirs', []);

// Writable dirs by web server
// set('writable_dirs', [
// ]);

set('keep_releases', 1);

// 測試站
set('base_domain', 'mtk.demo.lohaslife.cc');
set('base_db', 'demo_lohaslife_mtk');
set('base_db_user', 'demo_mtk');
set('base_db_password', 'X68DnBR8cpym5ZnJYMBS898j');

// 正式站
set('domain', 'DOMAIN'); // DOMAIN改正式站域名
set('db', 'DB_NAME'); // DB_NAME改正式站資料庫名稱
set('db_user', 'DB_USER'); // DB_USER改正式站資料庫使用者
set('db_password', 'DB_PASSWORD'); // DB_PASSWORD改正式站資料庫密碼

// 遠端
host('REMOTE_ADDR')->stage('production') // REMOTE_ADDR改遠端主機ip或域名
    ->user('SSH_USER') // SSH_USER改遠端主機ssh帳號
    ->set('repository', 'BITBUCK_URL') // BITBUCK_URL改butbucket倉庫網址
    ->set('branch', 'master')
    ->set('deploy_path', 'DEPLOY_PATH') // DEPLOY_PATH改遠端主機網站路徑不要有httpdocs
    ->set('dump_path', '{{deploy_path}}/data/db_dumps')
    ->set('backup_path', '{{deploy_path}}/data/backups')
    ->set('http_user', 'www');

// 本地
localhost()
    ->stage('development')
    ->set('repository', 'https://bitbucket.org/lohasit/beta3-site-ocean.git')
    ->set('branch', 'master')
    ->set('deploy_path', '/data/wwwroot/mtk.demo.lohaslife.cc')
    ->set('dump_path', '{{deploy_path}}/data/db_dumps')
    ->set('backup_path', '{{deploy_path}}/data/backups')
    ->set('document_root', __DIR__)
    ->set('http_user', 'www');

localhost('test.demo')
    ->stage('testing')
    ->set('child_theme', 'oceanwp-child')
    ->set('plugin_source_dir', getcwd() . '/wp-content/plugins/')
    ->set('theme_source_dir', getcwd() . '/wp-content/themes/{{child_theme}}/')
    ->set('destination_dir', '/data/wwwroot/test.demo.lohaslife.cc/httpdocs');

// Tasks
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'lohasit:backup_uploads',
    'deploy:release',
    'deploy:update_code',
    // 'deploy:shared',
    // 'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'lohasit:symlink',
    'deploy:unlock',
    'cleanup',
    'lohasit:deploy_wordpress',
])->desc('Deploy WordPress Site');

// If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
