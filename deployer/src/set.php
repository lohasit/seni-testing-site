<?php

namespace Lohasit\Deployer;

require_once 'utils/wp-cli.php';

use function Deployer\get;
use function Deployer\run;
use function Deployer\set;
use function Deployer\writeln;
use function Lohasit\Deployer\Utils\getWPCLIBinary;
use function Lohasit\Deployer\Utils\installWPCLI;

set('current_path', function () {
    $link = run('readlink {{deploy_path}}/httpdocs');
    return substr($link, 0, 1) === '/' ? $link : get('deploy_path') . '/' . $link;
});

set('release_name', function () {
    return date('YmdHis');
});

set('bin/wp', function () {
    $installPath = realpath(__DIR__ . '/../');

    if ($path = getWPCLIBinary($installPath)) {
        return $path;
    }

    $binaryFile = 'wp-cli.phar';

    writeln("WP-CLI binary wasn't found. Installing latest wp-cli to \"$installPath/$binaryFile\".");

    installWPCLI($installPath, $binaryFile, true);
});
