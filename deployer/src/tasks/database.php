<?php

namespace Lohasit\Deployer\Tasks;

require_once 'utils/localhost.php';
require_once 'utils/files.php';

use function Deployer\get;
use function Deployer\runLocally;
use function Deployer\set;
use function Deployer\task;
use function Deployer\writeln;
use function Lohasit\Deployer\Utils\getLocalHostConfig;

task('lohasit:db:local:backup', function () {
    $localWp       = getLocalhostConfig('bin/wp');
    $localDumpPath = getLocalhostConfig('dump_path');
    $now           = date('Y-m-d_H-i', time());
    set('dump_file', "{{base_db}}-$now.sql");
    set('dump_filepath', "$localDumpPath/{{dump_file}}");

    runLocally("mkdir -p $localDumpPath");
    runLocally("$localWp db export $localDumpPath/{{dump_file}} --add-drop-table");
    $uploaded = '';
    if ('production' === get('stage')) {
        runLocally('sed -i "s/{{base_domain}}/{{domain}}/g" ' . $localDumpPath . '/{{dump_file}}');
        run("mkdir -p {{deploy_path}}/{{dump_path}}");
        upload(
            "$localDumpPath/{{dump_file}}",
            '{{deploy_path}}/{{dump_filepath}}'
        );
        $remotePath = getRemotePath();
        run("cd $remotePath && {{bin/wp}} db import {{deploy_path}}/{{dump_filepath}}");
        $uploaded = '＆上傳匯入';
    }
    writeln("➤ LOHASIT 備份本地資料庫$uploaded完成 $localDumpPath/{{dump_file}}");
})->desc('備份本地資料庫＆上傳匯入');
