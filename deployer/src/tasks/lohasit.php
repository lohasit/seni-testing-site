<?php

namespace Lohasit\Deployer\Tasks;

require_once 'set.php';
require_once 'tasks/database.php';

use function Deployer\get;
use function Deployer\run;
use function Deployer\task;
use function Deployer\test;
use function Deployer\upload;
use function Deployer\writeln;
use function Lohasit\Deployer\Utils\getLocalHostConfig;

task('lohasit:success', function () {
    writeln('➤ LOHASIT 網站部署完成 {{domain}}');
})->desc('網站部署完成');

task('lohasit:symlink', function () {
    if (test('[ -d {{deploy_path}}/httpdocs ]')) {
        run('sudo rm -rf {{deploy_path}}/httpdocs');
    }
    run('sudo mv -T {{deploy_path}}/current {{deploy_path}}/httpdocs');
    writeln('➤ LOHASIT 移動軟連結current => httpdocs完成');

})->desc('移動軟連結current=>httpdocs');

task('lohasit:copy_uploads', function () {
    if (test('[ ! -d {{current_path}}/wp-content/uploads ]')) {
        $documentRoot = getLocalhostConfig('document_root');
        if ('production' === get('stage')) {
            upload("$documentRoot/wp-content/uploads/", "{{current_path}}/wp-content/uploads/");
        } else {
            run('sudo mkdir {{release_path}}/wp-content/uploads');
            run('sudo cp -avpf wp-content/uploads/* {{release_path}}/wp-content/uploads');
        }
        writeln('➤ LOHASIT 複製wp-content/uploads完成');
    } else {
        writeln('➤ LOHASIT uploads已經存在(第一次部屬的時候已經上傳)');
    }
})->desc('複製WordPress上傳資料夾');

task('lohasit:backup_uploads', function () {
    if (test('[ -d {{deploy_path}}/httpdocs/wp-content/uploads ]')) {
        run('sudo mv {{deploy_path}}/httpdocs/wp-content/uploads {{deploy_path}}');
        writeln('➤ LOHASIT 備份wp-content/uploads完成');
    }
})->desc('備份WordPress上傳資料夾');

task('lohasit:restore_uploads', function () {
    if (test('[ -d {{deploy_path}}/uploads ]')) {
        run('sudo mv {{deploy_path}}/uploads {{deploy_path}}/httpdocs/wp-content');
        writeln('➤ LOHASIT 還原wp-content/uploads完成');
    }
})->desc('還原WordPress上傳資料夾');

task('lohasit:permission', function () {
    run('sudo chown {{http_user}}:{{http_user}} {{release_path}} -R');
    run('sudo chown {{http_user}}:{{http_user}} {{release_path}}/* -R');
    run('cd {{release_path}}');
    run('sudo find $1 -type d -print0 | sudo xargs -0 chmod 0755');
    run('sudo find $1 -type f -print0 | sudo xargs -0 chmod 0644');
    run('sudo chmod 777 {{current_path}}/wp-content/uploads -R');
    writeln('➤ LOHASIT 修改檔案權限完成');
})->desc('修改檔案權限');

task('lohasit:modify_wp_config', function () {
    // run('cp wp-config.php {{release_path}}');
    upload('wp-config.php', '{{release_path}}');
    run('sudo sed -i "s/{{base_domain}}/{{domain}}/g" {{release_path}}/wp-config.php');
    run('sudo sed -i "s/{{base_db}}/{{db}}/g" {{release_path}}/wp-config.php');
    run('sudo sed -i "s/{{base_db_user}}/{{db_user}}/g" {{release_path}}/wp-config.php');
    run('sudo sed -i "s/{{base_db_password}}/{{db_password}}/g" {{release_path}}/wp-config.php');
    writeln('➤ LOHASIT 修改wp-config.php完成');
})->desc('修改WordPress設定檔');

task('lohasit:sync', function () {
    writeln('➤ LOHASIT 同步plugins....');
    run('rsync -av {{plugin_source_dir}} {{destination_dir}}/wp-content/plugins');
    writeln('➤ LOHASIT 同步{{child_theme}}....');
    run('rsync -avh {{theme_source_dir}} {{destination_dir}}/wp-content/themes/{{child_theme}}');
    writeln('➤ LOHASIT 測試站{{hostname}}同步完成');
})->desc('同步測試站外掛和子佈景');

task('lohasit:deploy_wordpress', [
    'lohasit:restore_uploads',
    'lohasit:copy_uploads',
    'lohasit:modify_wp_config',
    'lohasit:permission',
    // 'lohasit:db:local:backup',
    'lohasit:success',
])->desc('WordPress網站部屬');

task('lohasit:test', function () {
    writeln('{{backup_path}}');
});
