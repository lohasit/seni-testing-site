<?php

namespace Lohasit\Deployer\Utils;

use function Deployer\download;
use function Deployer\get;
use function Deployer\run;
use function Deployer\upload;
function getRemotePath(): string
{
    $remotePath  = '{{deploy_path}}/httpdocs';
    $releasePath = get('release_path');
    if ($releasePath) {
        $remotePath = $releasePath;
    }
    return $remotePath;
}

function pushFiles(string $source, string $destination, array $rsyncOptions)
{
    $localPath  = getLocalhostConfig('document_root');
    $remotePath = getRemotePath();
    upload("$localPath/$source/", "$remotePath/$destination/", ['options' => $rsyncOptions]);
}

function pullFiles(string $source, string $destination, array $rsyncOptions)
{
    $localPath  = getLocalhostConfig('document_root');
    $remotePath = getRemotePath();
    download("$remotePath/$source/", "$localPath/$destination/", ['options' => $rsyncOptions]);
}

function zipFiles(string $dir, string $backupDir, string $filename): string
{

    $backupFilename = $filename . '_' . date('Y-m-d_H-i-s') . '.zip';
    $backupPath     = "$backupDir/$backupFilename";
    run("mkdir -p $backupDir");

    if (substr($dir, -1) == '/') {
        run("cd {$dir} && zip -r {$backupFilename} . {{zip_options}} && mv $backupFilename $backupPath");
    } else {
        $parentDir = dirname($dir);
        $dir       = basename($dir);
        run("cd {$parentDir} && zip -r {$backupFilename} {$dir} {{zip_options}} && mv $backupFilename $backupPath");
    }

    return $backupPath;
}
