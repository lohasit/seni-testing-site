<?php

namespace Lohasit\Deployer\Utils;

use Deployer\Deployer;
use Deployer\Host\Host;

function getLocalHost(): Host
{
    return Deployer::get()->hosts->get('localhost');
}

function getLocalHostConfig(string $key)
{
    $localhost = getLocalHost();
    $config    = $localhost->getConfig();
    return $config->get($key);
}
