<?php

namespace Lohasit\Deployer\Utils;

use function Deployer\commandExist;
use function Deployer\locateBinaryPath;
use function Deployer\runLocally;
use function Deployer\testLocally;

const INSTALLER_DOWNLOAD = 'https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar';

function getWPCLIBinary($path = 'deployer/', $binaryFile = 'wp-cli.phar')
{
    if (commandExist('wp')) {
        return locateBinaryPath('wp') . ' --allow-root';
    }

    if (testLocally("[ -f $path/$binaryFile ]")) {
        return "php $path/$binaryFile --allow-root";
    }

    return false;
}

function installWPCLI($installPath, $binaryName = 'wp-cli.phar', $sudo = false)
{
    $sudoCommand = $sudo ? 'sudo ' : '';

    runLocally("mkdir -p $installPath");
    runLocally("cd $installPath && curl -sS -O " . INSTALLER_DOWNLOAD . " && chmod +x wp-cli.phar");
    if ($binaryName !== 'wp-cli.phar') {
        runLocally("$sudoCommand mv $installPath/wp-cli.phar $installPath/$binaryName");
    }

    return "$installPath/$binaryName";
}
