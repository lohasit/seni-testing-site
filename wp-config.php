<?php

/**
 * Lohas IT 樂活壓板系統
 */
if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    $autoload = require __DIR__ . '/vendor/autoload.php';
} else {
    exit(__DIR__ . '/vendor folder not exists. please run compser install via command line.');
}

/** WordPress 啟用https **/
$_SERVER['HTTPS'] = 'on';

/** WordPress 網址設定 **/
$url = 'https://seni.demo.lohaslife.cc';
define('WP_HOME', $url);
define('WP_SITEURL', $url);

/** WordPress 的資料庫名稱 */
define('DB_NAME', 'demo_lohaslife_seni');

/** 資料庫帳號 */
define('DB_USER', 'demo_seni');
/** 資料庫密碼 */
define('DB_PASSWORD', 'tdmMkB80GYwDnc4B10SiYDty');
/** 資料庫位置 */
define('DB_HOST', 'mariadb');
/** 資料庫編碼*/
define('DB_CHARSET', 'utf8mb4');
/** 資料庫編碼類型*/
define('DB_COLLATE', '');
/** 資料表前綴 */
define('DB_PREFIX', 'lohasit_');
/** 強制後台使用HTTPS連線 */
define('FORCE_SSL_ADMIN', true);
/** WordPress記憶體限制 */
define('WP_MEMORY_LIMIT', '256M');
/** 取消 */
define('ALLOW_UNFILTERED_UPLOADS', true);
/** Keys */
define('AUTH_KEY', 'BEF^Y0;Taooa,?@Q$?Sqd#jDhF1NeLG_2Pxx_fVIClggE+<G_`y6i{BM!<I*Y#nl');
define('SECURE_AUTH_KEY', 'HAPO!5y-_v$amF/DAa}HjlgZO*q)zkNd//,,U9i)}Le~Hwn.8_A8N}B1dgG@%=Os');
define('LOGGED_IN_KEY', ':s?{XMg>&.-+ Z#3WC96%f6Z(5]0+!rsNG.XJ&acqu0R+;Lo&:O$;W360,F9aE1$');
define('NONCE_KEY', 'FR8U4hdhO0+_TUz=0>CKu^-8C .iWdGWG@!kaB(KuS2S_G3M_.<8-5i}FJOG^3Q;');
define('AUTH_SALT', 'B|u}LMKSI(xLgORsp_BXWWLx[ ]jcbp.{%NerT]E>,i,xc.=s$ju~{j%MQ0C6XSE');
define('SECURE_AUTH_SALT', '{I7y>wk[B#<kW^F65z^(MVa$&:{6w&6q<YcM`~/[abSU [`?t#s6tFGz{4S:y`Kv');
define('LOGGED_IN_SALT', 'l5:#dv2c8TBToaSRCW:PKyqNIvuSb@SR[ lwWCS2wCTxZKEn^5X${<u`:o (:+-V');
define('NONCE_SALT', 'LwfL/x8s?bDbtg)?YU.WYSh#ooSu=jFxB)@)>pD58UQQPcz1Jc1a9c@68J_IIpeF');
/** WordPress 資料表前綴 */
$table_prefix = DB_PREFIX;

/** WordPress 自動儲存間隔，預設為60秒 */
//define('AUTOSAVE_INTERVAL', 60 );
/** WordPress 文章版本設定 */
define('WP_POST_REVISIONS', 5);
/**
 * 開發人員用： WordPress 偵錯模式。
 */
/** 啟用 SAVEQUERIES 後，系統便會追蹤及顯示 MySQL 查詢。 */
define('SAVEQUERIES', false);
/** 開啟WordPress偵錯功能 */
define('WP_DEBUG', false);
/** 產生錯誤記錄檔，產生於wp-content/debug.log */
define('WP_DEBUG_LOG', false);
/** 顯示錯誤訊息於html上 */
define('WP_DEBUG_DISPLAY', false);
/** 腳本偵錯功能，如設為true，則載入非minified的js */
define('SCRIPT_DEBUG', false);
/** 停用WordPress核心的自動更新*/
define('WP_AUTO_UPDATE_CORE', false);

/** 同伺服器多網站Memcached記憶體快取分離 */
define('WP_CACHE_KEY_SALT', DB_NAME); //
/** Memcached 伺服器 **/

$memcached_servers =
    [
        ['172.20.0.13', 11211]
    ];


/** WordPress 目錄的絕對路徑。 */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}
/** 設定 WordPress 變數和包含的檔案。 */
require_once ABSPATH . 'wp-settings.php';
