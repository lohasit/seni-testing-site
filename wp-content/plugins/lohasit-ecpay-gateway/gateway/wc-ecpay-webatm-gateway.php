<?php

/**
 * 綠界網路ATM付款方式
 */
class WC_Ecpay_Webatm_Gateway extends WC_Ecpay_Gateway implements WC_Ecpay_Gateway_Interface
{
    public function __construct()
    {
        parent::__construct();

        $this->add_action('lohasit_ecpay_webatm');
    }

    public function get_args(WC_Order $order)
    {
        $order_id = $order->get_id();

        $buyer_name = $order->billing_last_name . $order->billing_first_name;

        $total_fee = $order->order_total;

        $trade_no = ($this->get_option('merchant_id') == '2000132')
        ? uniqid() . $order_id
        : $order_id;

        $args = [
            'ChoosePayment'     => 'WebATM',
            'ClientBackURL'     => $this->get_return_url($order),
            'ItemName'          => $buyer_name . ' 訂單[ ' . $order_id . ' ]',
            'MerchantID'        => $this->get_option('merchant_id'), // 商店編號
            'MerchantTradeDate' => date_i18n('Y/m/d H:i:s'),
            'MerchantTradeNo'   => $trade_no, // 商店交易編號
            'OrderResultURL'    => $this->get_return_url($order), // Client 端回傳付款結果網址
            'PaymentType'       => 'aio',
            'ReturnURL'         => WC()->api_request_url($this->id),
            'TotalAmount'       => round($total_fee),
            'TradeDesc'         => $buyer_name,
        ];

        $args = apply_filters('woocommerce_lohasit_ecpay_webatm_args', $args);

        $args['CheckMacValue'] = $this->create_check_code($args);

        return $args;
    }
}
