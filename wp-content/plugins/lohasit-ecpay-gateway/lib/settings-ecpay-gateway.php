<?php

return [
    'enabled'       => [
        'title'   => '啟用/關閉',
        'type'    => 'checkbox',
        'label'   => '啟用綠界金流',
        'default' => 'yes',
    ],
    'merchant_id'   => [
        'title'       => '特店編號',
        'type'        => 'text',
        'description' => '正式環境請更改為特店申請的資料',
        'default'     => '2000132',
    ],
    'hash_key'      => [
        'title'       => '介接HashKey',
        'type'        => 'text',
        'description' => '正式環境請更改為特店申請的資料',
        'default'     => '5294y06JbISpM5x9',
    ],
    'hash_iv'       => [
        'title'       => '介接HashIV',
        'type'        => 'text',
        'description' => '正式環境請更改為特店申請的資料',
        'default'     => 'v77hoKGq4kWxNNIS',
    ],
    'ecpay_methods' => [
        'title'    => '付款方式',
        'type'     => 'multiselect',
        'class'    => 'wc-enhanced-select',
        // 'description' => '其他付款方式請到<a href="?page=wc-custom-options">此處</a>安裝後方可使用',
        'default'  => ['atm', 'credit', 'webatm'],
        'desc_tip' => false,
        'options'  => [],
    ],
    'debug_mode'    => [
        'title'   => '啟用/關閉',
        'type'    => 'checkbox',
        'label'   => '除錯模式',
        'default' => 'yes',
    ],
];
