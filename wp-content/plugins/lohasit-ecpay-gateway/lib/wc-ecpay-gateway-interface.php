<?php

/**
 * 綠界金流付款方式界面, 其他付款方式必需實現界面所有方法
 */
interface WC_Ecpay_Gateway_Interface
{
    public function get_args(WC_Order $order);
}
