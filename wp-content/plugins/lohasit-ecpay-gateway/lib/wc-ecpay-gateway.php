<?php

/**
 * 綠界金流主要類別, 其它付款方式必須繼承它
 */
class WC_Ecpay_Gateway extends WC_Payment_Gateway
{
    /**
     * 建構子 __construct
     */
    public function __construct()
    {
        $this->id = 'lohasit_ecpay';

        $this->custom_icon = plugins_url('images/ecpay_icon.png', __DIR__);

        $this->method_title = '綠界金流(非官方版)';

        $this->method_description = '讓您的顧客使用綠界第三方金流付款';

        $title = [];

        foreach ($this->get_option('ecpay_methods') as $method) {
            $title[] = $this->trans($method);
        }

        $this->title = implode(',', $title);

        $this->has_fields = false;

        $this->init_form_fields();

        $this->init_settings();

        if ($this->get_option('merchant_id') == '2000132') {
            $this->gateway = 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V2';
        } else {
            $this->gateway = 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V2';
        }

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_lohasit_ecpay_successful_request', [$this, 'ecpay_successful_request']);
    }

    /**
     * 取前後台
     * 付款方式標題
     * @return string
     */
    public function get_default_title()
    {
        $title = $this->trans(str_replace('lohasit_ecpay_', '', $this->id));

        return apply_filters('woocommerce_' . $this->id . '_title', $title, $this->id);
    }

    /**
     * 各付款方式呼叫用請見 gateway 資料夾內的付款方式
     * @param string $id [付款方式代碼]
     */
    protected function add_action($id)
    {
        $this->id = $id;

        $this->icon = apply_filters('woocommerce_' . $this->id . '_icon', $this->custom_icon);

        $this->title = $this->get_default_title();

        $description = sprintf('透過綠界科技使用%s付款', $this->title);

        $this->description = apply_filters('woocommerce_' . $this->id . '_description', $description, $this->id);

        add_action('woocommerce_thankyou_' . $this->id, [$this, 'thankyou_page']); //需與id名稱大小寫相同
        add_action('woocommerce_receipt_' . $this->id, [$this, 'receipt_page']);
        add_action('woocommerce_api_' . $this->id, [$this, 'check_ecpay_response']);
        add_action('woocommerce_api_' . $this->id . '_info', [$this, 'check_ecpay_response_info']);

    }

    /**
     * 訂單完成頁面(感謝頁)
     * @param  string $order_id
     * @return void
     */
    public function thankyou_page($order_id)
    {
        $order = wc_get_order($order_id);
        // 4.1.2 前往感謝頁無需轉變訂單狀態為保留
//        $order->update_status('on-hold');

        if ($description = $this->get_description()) {
            echo wpautop(wptexturize($description));
        }

        $result_msg = '';

        $meta_data = get_post_meta($order_id, '_' . $this->id . '_info', true);

        if ('lohasit_ecpay_atm' === $this->id && $meta_data) {
            WC()->cart->empty_cart();
            $payment_info = '銀行代碼: ' . $meta_data['BankCode'];
            $payment_info .= '<br>虛擬帳號: ' . $meta_data['vAccount'];
            $payment_info .= '<br>繳費期限: ' . $meta_data['ExpireDate'];

            // 20210604 虛擬ATM帳號產生後，狀態轉“保留”，扣庫存量
                $order->update_status('on-hold');


            $result_msg = '<span class="payment-info" id="' . $this->id . '">' . $payment_info . '</span>';
        } elseif ('lohasit_ecpay_cvs' === $this->id && $meta_data) {
            WC()->cart->empty_cart();
            $payment_info = "繳費代碼 : " . $_POST['PaymentNo'];
            $payment_info .= "<br>繳費期限: " . $_POST['ExpireDate'];

            $result_msg = '<span class="payment-info" id="' . $this->id . '">' . $payment_info . '</span>';
        } else {
            if ($this->check_value($_POST)) {
                $result_msg = '交易成功，交易單號：' . $_POST['TradeNo'] . '，處理日期：' . $_REQUEST['TradeDate']; //交易成功
                // if (!$order->has_status('processing')) {
                //     $order->update_status('processing', '收到付款');
                // }
                WC()->cart->empty_cart();
            } elseif ($_POST) {
                $result_msg = '交易失敗。';
            }
        }
        echo '<p>' . $result_msg . '</p>';
    }

    /**
     * 生成綠界表單
     * @param  string $order_id
     * @return string
     */
    public function generate_form($order_id)
    {
        global $woocommerce;

        $order = wc_get_order($order_id);

        $args = $this->get_args($order);

        $gateway = $this->gateway;

        $input_array = array();

        foreach ($args as $key => $value) {
            $args_array[] = '<input type="hidden" name="' . esc_attr($key) . '" value="' . esc_attr($value) . '" />';
        }

        return '<form id="lohasit_payment" name="lohasit_payment" action=" ' . $gateway . ' " method="post" target="_top">' . implode('', $args_array) . '
			<input type="submit" class="button-alt" id="submit_payment_form" value="付款" />
			</form><script>document.forms["lohasit_payment"].submit();</script>';
    }

    /**
     * 送出訂單跳轉頁面(生成綠界表單送出)
     * @param  string $order_id
     * @return void
     */
    public function receipt_page($order_id)
    {
        echo '<p>感謝您的訂購，接下來將導向到付款頁面，請稍後.</p>';
        echo $this->generate_form($order_id);
    }

    /**
     * 送出訂單
     * @param  string $order_id
     * @return array
     */
    public function process_payment($order_id)
    {
        $order = wc_get_order($order_id);



        return [
            'result'   => 'success',
            'redirect' => $order->get_checkout_payment_url(true),
        ];
    }

    /**
     * 接收&檢查回傳參數[交易結果]
     * @return void
     */
    public function check_ecpay_response()
    {
        if ($_POST) {
            $this->write_log(print_r($_POST, true), 'info');

//            if ($this->check_value($_POST)) {

                do_action('woocommerce_lohasit_ecpay_successful_request', [
                    'post'   => $_POST,
                    'action' => 'notify',
                ]);

//            } else {
//                echo '0|ErrorMessage';
//            }
        }
        die;
    }

    /**
     * 接收&檢查回傳參數[付款資訊]
     * @return void
     */
    public function check_ecpay_response_info()
    {
        if ($_POST) {
            $this->write_log(print_r($_POST, true), 'info');

            if ($this->check_value($_POST)) {
                do_action('woocommerce_lohasit_ecpay_successful_request', [
                    'post'   => $_POST,
                    'action' => 'payment_info',
                ]);
            } else {
                echo '0|ErrorMessage';
            }
        }
        die;
    }

    /**
     * hook: woocommerce_lohasit_ecpay_successful_request
     * @param  array $response
     * @return void
     */
    public function ecpay_successful_request($response)
    {
        $post   = $response['post'];
        $action = $response['action'];

        $order_id = ($this->get_option('merchant_id') != '2000132')
        ? $post['MerchantTradeNo']
        : substr($post['MerchantTradeNo'], 13);

        $order = wc_get_order($order_id);

        switch ($action) {
            case 'notify':
                if ($post['RtnCode'] == '1') {
                    if (!$order->has_status('processing')) {
                        $order->update_status('processing', '已收到付款');
                        $result_msg         = '交易成功，交易單號：' . $post['TradeNo'] . '，處理日期：' . $post['TradeDate']; //交易成功
                        list($payment_type) = explode('_', $post['PaymentType']);
                        $meta_key           = '_lohasit_ecpay_' . strtolower($payment_type) . '_response';
                        update_post_meta($order_id, $meta_key, $post);
                        $order->add_order_note($result_msg);
                    }
                } else {
                    $order->update_status('failed', '交易失敗');
                }
                break;
            case 'payment_info':
                if (strstr($post['PaymentType'], 'ATM')) {
                    if ($post['RtnCode'] == '2') {
                        $payment_info = "\n銀行代碼: " . $post['BankCode'];
                        $payment_info .= "\n虛擬帳號: " . $post['vAccount'];
                        $payment_info .= "\n繳費期限: " . $post['ExpireDate'];
                        $meta_value = [
                            'BankCode'   => $post['BankCode'],
                            'vAccount'   => $post['vAccount'],
                            'ExpireDate' => $post['ExpireDate'],
                        ];
                    } else {
                        $payment_info = '取號失敗:' . $post['RtnCode'] . '(' . $post['RtnMsg'] . ')';
                        $order->update_status('failed', $payment_info);
                    }
                }

                if (strstr($post['PaymentType'], 'CVS')) {
                    if ($post['RtnCode'] == '10100073') {
                        $payment_info = "\n繳費代碼 : " . $post['PaymentNo'];
                        $payment_info .= "\n繳費期限: " . $post['ExpireDate'];
                        $meta_value = [
                            'PaymentNo'  => $post['PaymentNo'],
                            'ExpireDate' => $post['ExpireDate'],
                        ];
                    } else {
                        $payment_info = '取號失敗:' . $post['RtnCode'] . '(' . $post['RtnMsg'] . ')';
                        $order->update_status('failed', $payment_info);
                    }
                }

                if (strstr($post['PaymentType'], 'BARCODE')) {
                    if ($post['RtnCode'] == '10100073') {
                        $data = [
                            'Barcode1'        => $post['Barcode1'],
                            'Barcode2'        => $post['Barcode2'],
                            'Barcode3'        => $post['Barcode3'],
                            'TradeAmt'        => $post['TradeAmt'],
                            'ExpireDate'      => $post['ExpireDate'],
                            'MerchantTradeNo' => $post['MerchantTradeNo'],
                        ];
                        $p            = base64_encode(serialize($data));
                        $payment_info = '<a href="' . plugins_url('print.php', __FILE__) . '?p=' . $p . '" target="_blank">列印繳費單</a>';
                    } else {
                        $payment_info = '取號失敗:' . $post['RtnCode'] . '(' . $post['RtnMsg'] . ')';
                    }
                }

                $order_id = ($this->get_option('merchant_id') != '2000132')
                ? $post['MerchantTradeNo']
                : substr($post['MerchantTradeNo'], 13);

                $order = wc_get_order($order_id);
                $order->add_order_note($payment_info, true);

                if ($meta_value) {
                    list($payment_type) = explode('_', $post['PaymentType']);
                    $meta_name          = '_lohasit_ecpay_' . strtolower($payment_type) . '_info';
                    update_post_meta($order_id, $meta_name, $meta_value);
                }
                break;
        }
        ob_clean();
        ob_start();
        echo '1|OK';
        echo ob_get_clean();
        die;
    }

    public function create_check_code($args)
    {
        ksort($args);
        if (isset($args['CheckMacValue'])) {
            unset($args['CheckMacValue']);
        }
        $hash_raw_data = 'HashKey=' . $this->settings['hash_key'] . '&' . urldecode(http_build_query($args)) . '&HashIV=' . $this->settings['hash_iv'];

        $urlencode_data = strtolower(urlencode($hash_raw_data));

        $urlencode_data = str_replace('%2d', '-', $urlencode_data);
        $urlencode_data = str_replace('%5f', '_', $urlencode_data);
        $urlencode_data = str_replace('%2e', '.', $urlencode_data);
        $urlencode_data = str_replace('%21', '!', $urlencode_data);
        $urlencode_data = str_replace('%2a', '*', $urlencode_data);
        $urlencode_data = str_replace('%28', '(', $urlencode_data);
        $urlencode_data = str_replace('%29', ')', $urlencode_data);

        return strtoupper(md5($urlencode_data));
    }

    public function check_value($post)
    {
        $MyCheckMacValue = $this->create_check_code($post);

        if ($MyCheckMacValue == $post['CheckMacValue']) {
            return true;
        }

        return false;
    }

    public function process_admin_options()
    {
        if (parent::process_admin_options()) {
            $post_data = $this->get_post_data();
            if (isset($post_data['woocommerce_ecpay_enable_methods'])
                && !empty($post_data['woocommerce_ecpay_enable_methods'])
            ) {
                foreach ($post_data['woocommerce_ecpay_enable_methods'] as $method) {
                    delete_option('woocommerce_ecpay_' . $method . '_settings');
                    $options = array(
                        'enabled' => 'yes',
                    );
                    update_option('woocommerce_ecpay_' . $method . '_settings', $options);
                }
            }
        }
    }

    // **************************** admin page *********************************

    protected function trans($text)
    {
        return lohasit_ecpay_gateway()->trans($text);
    }

    public function init_form_fields()
    {
        $this->form_fields = include __DIR__ . '/settings-ecpay-gateway.php';

//        $ecpay_methods = get_option('woocommerce_lohasit_ecpay_methods');
        $ecpay_methods = ['credit', 'webatm', 'atm' ];

        $options = [];

        foreach ($ecpay_methods as $method) {
            $code = str_replace('lohasit_ecpay_', '', $method);
            if (lohasit_ecpay_gateway()->is_installed($code)) {
                $options[$code] = $this->trans($code);
            }
        }

        $this->form_fields['ecpay_methods']['options'] = $options;

        if (isset($_GET['section']) && $_GET['section'] != 'lohasit_ecpay') {
            unset($this->form_fields['merchant_id']);
            unset($this->form_fields['hash_key']);
            unset($this->form_fields['hash_iv']);
            unset($this->form_fields['ecpay_methods']);
        }
    }

    public function admin_options()
    {
        ?>
		<h3>綠界金流(非官方版)</h3>
		<p>此外掛可以讓您使用綠界金流</p>
		<table class='form-table'>
			<?php $this->generate_settings_html();?>
		</table>
		<?php

    }

    protected function write_log($message, $type = 'error')
    {

        $source = 'lohasit-ecpay-gateway';

        if ('yes' === $this->get_option('debug_mode') && 'info' === $type) {
            lohasit_logger($type . ': ' . $message, $source, $type);
        }
    }


}