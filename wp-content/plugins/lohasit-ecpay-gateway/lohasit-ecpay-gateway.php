<?php

/*
Plugin Name: 樂活綠界金流(非官方版)
Description: 樂活壓板系統 - WooCommerce - 綠界金流(非官方版)
Version: 3.0.0
Author: Lohas IT (alanchang15)
 */

defined('ABSPATH') or die;

class Lohasit_Ecpay_Gateway
{
    private static $_instance = null;

    // [20220122] 綠界金流：預設付款選項，無需從資料庫撈取
    public $methods =  ['credit', 'atm', 'webatm'];

    protected $hook;

    public static function forge()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __construct()
    {
        $this->hook = 'check_atm_payment_expiry';

        register_activation_hook(__FILE__, [$this, 'plugin_activated']);

        register_deactivation_hook(__FILE__, [$this, 'plugin_deactivated']);

        add_action('plugins_loaded', [$this, 'init_payment_gateway']);

        add_action('init', [$this, 'schedule_job']);
    }

    public function plugin_activated()
    {
        if (!get_option('woocommerce_lohasit_ecpay_methods')) {
            update_option('woocommerce_lohasit_ecpay_methods', $this->methods);
        }

        if (!get_option('woocommerce_lohasit_ecpay_settings')) {
            $ecpay = [
                'enabled'        => 'yes',
                'merchant_id'    => '2000132',
                'hash_key'       => '5294y06JbISpM5x9',
                'hash_iv'        => 'v77hoKGq4kWxNNIS',
                'enable_methods' => ['credit', 'atm', 'webatm'],
            ];
            update_option('woocommerce_lohasit_ecpay_settings', $ecpay);
        }

        if (!get_option('woocommerce_lohasit_ecpay_credit_settings')) {
            $ecpay_credit = [
                'enabled' => 'yes',
                'title'   => $this->trans('credit'),
            ];
            update_option('woocommerce_lohasit_ecpay_credit_settings', $ecpay_credit);
        }

        if (!get_option('woocommerce_lohasit_ecpay_atm_settings')) {
            $ecpay_atm = [
                'enabled' => 'yes',
                'title'   => $this->trans('atm'),
            ];
            update_option('woocommerce_lohasit_ecpay_atm_settings', $ecpay_atm);
        }

        if (!get_option('woocommerce_lohasit_ecpay_webatm_settings')) {
            $ecpay_webatm = [
                'enabled' => 'yes',
                'title'   => $this->trans('webatm'),
            ];
            update_option('woocommerce_lohasit_ecpay_webatm_settings', $ecpay_webatm);
        }
    }

    public function plugin_deactivated()
    {
        delete_option('woocommerce_lohasit_ecpay_methods');
        delete_option('woocommerce_lohasit_ecpay_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_3_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_6_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_12_settings');
        delete_option('woocommerce_lohasit_ecpay_atm_settings');
        delete_option('woocommerce_lohasit_ecpay_webatm_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_settings');
        delete_option('woocommerce_lohasit_ecpay_cvs_settings');
        delete_option('woocommerce_lohasit_ecpay_barcode_settings');
        delete_option('woocommerce_lohasit_ecpay_credit_fixed_settings');
    }

    /**
     * hook: plugins_loaded
     * @param  string $methods
     * @return void
     */
    public function init_payment_gateway($methods)
    {
        if (!class_exists('WC_Payment_Gateway')) {
            return;
        }

        if (!class_exists('WC_Ecpay_Gateway')) {
            require_once __DIR__ . '/lib/wc-ecpay-gateway-interface.php';
            require_once __DIR__ . '/lib/wc-ecpay-gateway.php';
        }

        $this->methods = array_keys($this->get_payments());

        // add_filter('woocommerce_gateway_title', [$this, 'payment_gateway_title'], 999, 2);

        add_filter('woocommerce_payment_gateways', [$this, 'add_ecpay_gateway']);

        add_filter('woocommerce_available_payment_gateways', [$this, 'ecpay_available_payment_gateways'], 1);

        add_filter('woocommerce_ecpay_icon', [$this, 'payment_icon']);

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), [$this, 'admin_edit_link']);
    }

    /**
     * hook: woocommerce_gateway_title
     * @param  string $title
     * @param  string $id
     * @return string
     */
    // public function payment_gateway_title($title, $id)
    // {
    //     if (strstr($id, 'lohasit_ecpay_')) {
    //         $key = str_replace('lohasit_ecpay_', '', $id);
    //         return $this->trans($key);
    //     }

    //     return $title;
    // }

    /**
     * hook: woocommerce_ecpay_icon
     * @param  string $url
     * @return string
     */
    public function payment_icon($url)
    {
        $path = str_replace([plugins_url(), basename(__DIR__)], '', $url);

        if (file_exists(get_template_directory() . $path)) {
            return get_template_directory_uri() . $path;
        }

        return $url;
    }

    /**
     * hook: woocommerce_payment_gateways
     * @param string $methods
     */
    public function add_ecpay_gateway($methods)
    {
        global $post;

        $methods[] = 'WC_Ecpay_Gateway';

        if (!is_admin() || (is_admin() && $post->post_type == 'shop_order')) {

            $options = $this->get_options();

            $enable_methods = $options['ecpay_methods'];

            foreach ($enable_methods as $method) {
                $filename = 'wc-ecpay-' . $method . '-gateway.php';
                if (file_exists(__DIR__ . '/gateway/' . $filename)) {
                    require_once __DIR__ . '/gateway/' . $filename;
                    if (strstr($method, '-')) {
                        $tmp = array_map(
                            'ucfirst',
                            explode(
                                '-', $method
                            )
                        );
                        $method = implode('_', $tmp);
                    }
                    $method = 'WC_Ecpay_' . ucfirst($method) . '_Gateway';

                    $methods[] = $method;
                }
            }
        }

        return $methods;
    }

    /**
     * 取全部付款方式
     * @return array
     */
    public function get_payments()
    {
        $payment_methods = [
            'credit'       => '信用卡',
            'webatm'       => '網路ATM',
            'atm'          => 'ATM虛擬帳號',
            // 缺 lohasit_ecpay_cvs gateway/wc-ecpay-cvs-gateway.php
            'cvs'          => '超商代碼繳費',
            // 缺 lohasit_ecpay_barcode gateway/wc-ecpay-barcode-gateway.php
            'barcode'      => '超商條碼繳費',
            // 缺 lohasit_ecpay_credit_3 gateway/wc-ecpay-credit-3-gateway.php
            'credit_3'     => '信用卡分3期',
            // 缺 lohasit_ecpay_credit_6 gateway/wc-ecpay-credut-6-gateway.php
            'credit_6'     => '信用卡分6期',
            // 缺 lohasit_ecpay_credit_12 gateway/wc-ecpay-credit-12-gateway.php
            'credit_12'    => '信用卡分12期',
            // (特殊情況暫時不處理)缺 lohasit_ecpay_credit_fixed gateway/wc-ecpay-credit-fixed-gateway.php
            'credit_fixed' => '信用卡定期定額',
        ];

        foreach ($payment_methods as $method => $name) {
            if (!$this->is_installed($method)) {
                unset($payment_methods[$method]);
            }
        }

        return $payment_methods;
    }

    /**
     * 付款方式轉中文
     * @param  string $text
     * @return string
     */
    public function trans($text)
    {
        $translation = $this->get_payments();

        $translation = apply_filters('lohasit_ecpay_translation', $translation);

        return $translation[$text];
    }

    /**
     * 取可用付款方式
     * @return array
     */
    public function get_available_methods()
    {
        foreach ($this->methods as $method) {
            $row               = new stdClass;
            $row->method       = $method;
            $row->name         = $this->trans($method);
            $row->code         = basename(__DIR__) . '/gateway/wc-ecpay-' . $method . '-gateway.php';
            $row->is_installed = $this->is_installed($method);
            $methods[]         = $row;
        }

        return $methods;
    }

    /**
     * 付款方式是否安裝
     * @param  string  $method
     * @return boolean
     */
    public function is_installed($method)
    {
        return (file_exists(__DIR__ . '/gateway/wc-ecpay-' . $method . '-gateway.php'));
    }

    /**
     * 取綠界金流設定
     * @return array
     */
    public function get_options()
    {
        return get_option('woocommerce_lohasit_ecpay_settings', true);
    }

    /**
     * hook: woocommerce_available_payment_gateways
     * @param  array $gateways
     * @return array
     */
    public function ecpay_available_payment_gateways($gateways)
    {
        unset($gateways['lohasit_ecpay']);

        return $gateways;
    }

    /**
     * hook: plugin_action_links_lohasit-ecpay-gateway
     * @param  array  $links
     * @return array
     */
    public function admin_edit_link($links = [])
    {
        return array_merge(
            [
                'settings' => '<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout&section=lohasit_ecpay') . '">' . __('Settings') . '</a>',
            ],
            $links);
    }

/*
 * 20210604 排程：虛擬ATM帳號逾期未付款，訂單將轉換為“取消”，補庫存 by JJ
 */
    public function schedule_job()
    {
        $schedule = 'daily';

        $uniqid = md5($this->hook . $schedule);

        $date = date("Y-m-d 12:00:00", strtotime("+1 day"));

        $timestamp = strtotime($date);

        if (!wp_next_scheduled($this->hook)) {
            wp_schedule_event($timestamp, $schedule, $this->hook, [
                'uniqid' => $uniqid,
            ]);
        }
            add_action( $this->hook, [$this, 'check_atm_payment_expiry'] );
    }

    /*
     * 20210604 虛擬ATM帳號逾期未付款，訂單將轉換為“取消”，補庫存 by JJ
     */
    public function check_atm_payment_expiry()
    {
        // 撈出所有實用ATM虛擬帳號付款的訂單
        // 逐個檢查繳費逾期日
        // 逾期繳費的訂單，轉狀態為：'取消'，補充庫存

        $args = array(

            'post_status' => 'wc-on-hold',
            'post_type' => 'shop_order',
            'posts_per_page'      => -1,
            'meta_query' => array(

                array(

                    'key' => '_payment_method',
                    'value' => 'lohasit_ecpay_atm',

                )

            )
        );

        $queries = get_posts($args);
        foreach($queries as $query) {

            $order = wc_get_order($query->ID);

            $payment_expiry =  (get_post_meta($query->ID, '_lohasit_ecpay_atm_info', true))['ExpireDate'];

            $msg = '此訂單已超過繳費期限（'.  $payment_expiry .'), 訂單已取消。';

            if ( date('Y/m/d') > $payment_expiry){

                $order->update_status('cancelled');

                $order->add_order_note($msg);

            }
        }
    }
}

$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));

if (in_array('woocommerce/woocommerce.php', get_option('active_plugins'))) {

    function lohasit_ecpay_gateway()
    {
        return Lohasit_Ecpay_Gateway::forge();
    }

    lohasit_ecpay_gateway();

    // alan: 以下只是範例, 視該專案需求寫到子佈景內
    // 前台付款方式圖示不顯示
    // add_filter('woocommerce_lohasit_ecpay_credit_icon', '__return_false');
    // add_filter('woocommerce_lohasit_ecpay_webatm_icon', '__return_false');
    // add_filter('woocommerce_lohasit_ecpay_atm_icon', '__return_false');
    // 更改前台付款方式名稱
    // add_filter('woocommerce_lohasit_ecpay_credit_title', function () {return '線上刷卡';}, 999);
    // 更改前台付款方式描述
    // add_filter('woocommerce_lohasit_ecpay_credit_description', function () {return '透過綠界使用信用卡線上刷卡付費';}, 999);

    // add_action('plugins_loaded', function () {
}
