<?php

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    require_once ( '../../../wp-load.php' );

    /** @var Wc2_Shipping_ecpay_Home $ecpay_home */
    global $ecpay_home;

    $post = $_POST;

    ksort( $post );

    $message = [
        'tcat' => [
            '300' => '訂單處理中(已收到訂單資料)',
            '310' => '上傳電子訂單檔處理中',
            '311' => '上傳退貨電子訂單處理中',
            '325' => '退貨訂單處理中(已收到訂單資料)',
            '3001' => '轉運中(即集貨)',
            '3002' => '不在家',
            '3003' => '配完',
            '3004' => '送錯BASE (送錯營業所)',
            '3005' => '送錯CENTER(送錯轉運中心)',
            '3006' => '配送中',
            '3007' => '公司行號休息',
            '3008' => '地址錯誤，聯繫收件人',
            '3009' => '搬家',
            '3010' => '轉寄(如原本寄到A，改寄B)',
            '3011' => '暫置營業所(收件人要求至營業所取貨)',
            '3012' => '到所(收件人要求到站所取件)',
            '3013' => '當配下車(當日配送A至B營業所，已抵達B營業所)',
            '3014' => '當配上車(當日配送從A至B營業所，已抵達A營業所)',
            '3015' => '空運配送中',
            '3016' => '配完狀態刪除',
            '3017' => '退回狀態刪除(代收退貨刪除)',
            '5001' => '損壞，站所將協助退貨',
            '5002' => '遺失',
            '5003' => 'BASE列管(寄件人和收件人聯絡不到)',
            '5004' => '一般單退回',
            '5005' => '代收退貨',
            '5006' => '代收毀損',
            '5007' => '代收遺失',
            '5008' => '退貨配完',
            '7001' => '超大(通常發生於司機取件，不取件)',
            '7002' => '超重(通常發生於司機取件，不取件)',
            '7003' => '地址錯誤，聯繫收件人',
            '7004' => '航班延誤',
            '7005' => '託運單刪除',
            '7024' => '另約時間',
            '7025' => '電聯不上',
            '7026' => '資料有誤',
            '7027' => '無件可退',
            '7028' => '超大超重',
            '7029' => '已回收',
            '7030' => '別家收走',
            '7031' => '商品未到',
        ],
        'ecan' => [
            '325' => '退貨訂單處理中(已收到訂單資料)',
            '3003' => '配完',
            '3006' => '配送中',
            '3110' => '轉配郵局',
            '3111' => '配送外包',
            '3112' => '再配',
            '3113' => '異常',
            '3114' => '再取',
        ]
    ];

    if($ecpay_home->check_value($post)) {
        if($post['LogisticsType'] == 'Home') {
            $code = $post['RtnCode'];
            if('300' == $code) {
                $order_id = $post['MerchantTradeNo'];
                $order = new WC_Order($order_id);
                if ( get_post_meta( $order_id, 'allpay_logistics', true ) == '' ) {
                    $note = sprintf( '物流編號:%s ',$post['AllPayLogisticsID'] );
                    $order->add_order_note( $note, false );
                    add_post_meta( $order_id, 'allpay_logistics', json_encode($post) );
                }
            } else {
                $order_id = $post['MerchantTradeNo'];
                $order = new WC_Order($order_id);
                $subtype = strtolower($post['LogisticsSubType']);
                if('tcat' == $subtype) {
                    if(array_key_exists($code, $message['tcat']))
                        $order->add_order_note($message['tcat'][$code]);
                } elseif('ecan' == $subtype) {
                    if(array_key_exists($code, $message['ecan']))
                        $order->add_order_note($message['ecan'][$code]);
                }
            }
            echo "1|OK";
        }
    }
    exit;
}