<?php

/*
Plugin Name: 樂活綠界物流B2C宅配(非官方版)
Description: 樂活壓板系統 - Woocommerce - 綠界物流宅配
Version: 2.1
Author: Lohas IT (alanchang15)
Text Domain: wc-ecpay-b2c-logistic-home
 */

$active_plugins2 = apply_filters('active_plugins', get_option('active_plugins'));

if (in_array('woocommerce/woocommerce.php', $active_plugins2)) {
    require_once __DIR__ . '/wc2-shipping-ecpay-home.php';

    if (!function_exists('get_woo_version')) {
        function get_woo_version()
        {
            return WC()->version;
        }
    }

    if (!function_exists('get_shipping_option')) {
        function get_shipping_option($chosen_shipping)
        {
            global $wpdb;
            $settings = array();
            $table    = $wpdb->prefix . 'options';
            $name     = 'woocommerce_' . $chosen_shipping;
            $sql      = "SELECT option_value FROM $table WHERE option_name LIKE '%$name%'";
            $row      = $wpdb->get_row($sql);
            if ($row) {
                $settings = unserialize($row->option_value);
            }

            return $settings;
        }
    }
}
