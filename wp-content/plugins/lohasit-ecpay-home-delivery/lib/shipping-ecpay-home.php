<?php

class Shipping_Ecpay_Home extends WC_Shipping_Method
{
    public function __construct($instance_id = 0)
    {
        $this->init_form_fields();
        $this->init_settings();

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    protected function name($id)
    {
        $default_name = array(
            'home_coldtcat_shipping' => '黑貓宅急便冷藏',
             'home_tcat_shipping' => '黑貓宅急便',
            'home_ecan_shipping' => '宅配通',
        );
        return $default_name[$id];
    }

    public function init_form_fields()
    {
        $country = new WC_Countries();
        $states  = $country->get_states('TW');

        $form_fields = array(
            'enabled'                => array(
                'title'   => __('Enable', 'woocommerce'),
                'type'    => 'checkbox',
                'label'   => __('Enable', 'woocommerce') . $this->name($this->id),
                'default' => 'no',
            ),
            'title'                  => array(
                'title'    => __('Title', 'woocommerce'),
                'type'     => 'text',
                'default'  => __($this->name($this->id), 'woocommerce'),
                'desc_tip' => true,
            ),
            'availability'           => array(
                'title'   => __('Method availability', 'woocommerce'),
                'type'    => 'select',
                'default' => 'all',
                'class'   => 'availability wc-enhanced-select',
                'options' => array(
                    'all'      => __('All allowed countries', 'woocommerce'),
                    'specific' => __('Specific Countries', 'woocommerce'),
                ),
            ),
            'countries'              => array(
                'title'             => __('Specific Countries', 'woocommerce'),
                'type'              => 'multiselect',
                'class'             => 'wc-enhanced-select',
                'css'               => 'width: 450px;',
                'default'           => '',
                'options'           => WC()->countries->get_shipping_countries(),
                'custom_attributes' => array(
                    'data-placeholder' => __('Select some countries', 'woocommerce'),
                ),
            ),
            'cost'                   => array(
                'title'   => __('費用(本島)', 'woocommerce'),
                'type'    => 'text',
                'default' => '120',
            ),
            'free_cost'              => array(
                'title'   => __('訂單金額多少以上免運費(本島)'),
                'type'    => 'text',
                'default' => '1000',
            ),
			'outstate_cost'                   => array(
                'title'   => __('費用(離島)', 'woocommerce'),
                'type'    => 'text',
                'default' => '120',
            ),
            'outstate_free_cost'              => array(
                'title'   => __('訂單金額多少以上免運費(離島)'),
                'type'    => 'text',
                'default' => '1000',
            ),
            'ecpay_merchant_id'      => array(
                'title'       => __('商店代號', 'woocommerce'),
                'type'        => 'text',
                'default'     => '2000132',
                'description' => __('特店編號(MerchantID)', 'woocommerce'),
            ),
            'ecpay_hash_key'         => array(
                'title'       => __('HashKey', 'woocommerce'),
                'type'        => 'text',
                'default'     => '5294y06JbISpM5x9',
                'description' => __('All in one 介接的 HashKey', 'woocommerce'),
            ),
            'ecpay_hash_iv'          => array(
                'title'       => __('HashIV', 'woocommerce'),
                'type'        => 'text',
                'default'     => 'v77hoKGq4kWxNNIS',
                'description' => __('All in one 介接的 HashIV', 'woocommerce'),
            ),
            'ecpay_sender_state'     => array(
                'title'   => __('商店所在縣市', 'woocommerce'),
                'type'    => 'select',
                'class'   => 'wc-enhanced-select',
                'css'     => 'width: 450px;',
                'default' => '',
                'options' => $states,
            ),
            'ecpay_sender_name'      => array(
                'title'       => __('寄件人', 'woocommerce'),
                'type'        => 'text',
                'default'     => '購物網',
                'description' => '必填',
            ),
            'ecpay_sender_cellphone' => array(
                'title'       => __('寄件人手機', 'woocommerce'),
                'type'        => 'text',
                'default'     => '0988888888',
                'description' => '必填',
            ),
            'ecpay_sender_zip_code'  => array(
                'title'       => __('寄件人郵遞區號', 'woocommerce'),
                'type'        => 'text',
                'default'     => '320',
                'description' => '必填',
            ),
            'ecpay_sender_address'   => array(
                'title'       => __('寄件人地址', 'woocommerce'),
                'type'        => 'text',
                'default'     => '請填寫寄件人地址',
                'description' => '必填',
            ),
        );
            $this->form_fields = $form_fields;

    }
}
