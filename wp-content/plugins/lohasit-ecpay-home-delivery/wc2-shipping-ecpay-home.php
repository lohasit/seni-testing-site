<?php

class Wc2_Shipping_Ecpay_Home
{

    private static $_instance = null;
    private $methods          = array();

    public static function forge()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __construct()
    {
        register_activation_hook(__FILE__, array($this, 'plugin_activated'));
        add_action('woocommerce_shipping_init', array($this, 'init_shipping_ecpay_home'));

        if (is_admin()) {
            // add_action('add_meta_boxes', array($this, 'add_print_button'));
            //由後台更新訂單狀待完成時處理
            // add_action( 'woocommerce_order_status_completed', array( $this, 'run_package_request' ), 1);
            add_filter('manage_edit-shop_order_columns', array($this, 'allpay_e_express_add_column'));
            add_action('manage_shop_order_posts_custom_column', array($this, 'allpay_e_express_add_column_action'));

        }
    }

    public function allpay_e_express_add_column($columns)
    {
        $columns['allpay_e_express_column'] = "宅配物流單號";
        return $columns;
    }

    public function allpay_e_express_add_column_action($column)
    {
        if ($column == 'allpay_e_express_column') {
            global $post;
            $order = new WC_Order($post->ID);

            $shipping           = $order->get_shipping_methods();
            $shipping           = array_pop($shipping);
            // [20220127] 改用$post->ID指定order_id
            $shipping_method_id = empty($shipping)?'':$shipping['method_id'];
            if ($shipping_method_id == 'home_tcat_shipping' || $shipping_method_id == 'home_ecan_shipping' || $shipping_method_id == 'home_coldtcat_shipping') {
                $print_url   = plugins_url('home-print.php?id=', __FILE__);
                $process_url = plugins_url('home-process.php?id=', __FILE__);
                ?>
                <?php
if ($data = $this->get_ecpay_data()):
                    $platform    = ('TCAT' === $data->LogisticsSubType) ? '宅急便' : '宅配通';
                    $print_label = sprintf('列印%s寄件單', $platform);
                    ?>

	                    <button class="button-primary ecpay_express_print" type="button"><?php echo $print_label; ?></button>
	                    <script>
	                        jQuery('.ecpay_express_print').click(function () {
	                            var id = jQuery(this).parents('tr').attr('id');
	                            id = id.replace('post-', '');
	                            window.open('<?php echo $print_url ?>' + id, '_blank');
	                        });
	                    </script>
	                <?php else: ?>

                    <form id="form_name_asd">
                        <!-- //Nelly Changed -->
                        <input type="hidden" name="size_danny" class="test" value="">
                        <select name="size_nelly" id="size_danny_asd" class="size_danny_asd">
                            <option value="0001">60cm</option>
                            <option value="0002">90cm</option>
                            <option value="0003">120cm</option>
                            <option value="0004">150cm</option>
                        </select>
                        <button class="button-primary ecpay_express_process" type="button" id="ecpay_express_process">產生物流訂單</button>
                    </form>
                    <script>//Nelly Changed ID - CLASS=ecpay_express_process

                        //jQuery('.ecpay_express_process').click(function() {
                        //Nelly Changed
                        jQuery(document).ready(function(){
                            //jQuery(".test").val(jQuery(this).val());
                            jQuery(".test").val(jQuery(".test").next().val());
                        });

                        jQuery(document).on('change', ".size_danny_asd", function(){
                            //jQuery(".test").val(jQuery(this).val());
                            //jQuery(".test").val(jQuery(".test").next().val());
                            jQuery(this).prev('.test').val(jQuery(this).val());
                        });

                        //Nelly Changed
                        jQuery(document).on('click', ".ecpay_express_process", function(){
                            var id = jQuery(this).parents('tr').attr('id');

                            var order_item_size = jQuery(".test").val();
                            id = id.replace('post-', '');
                            //alert(id);
                            //window.open('<?php //echo $process_url?>'+id+'&size='+jQuery('#size_danny_asd').val(), 'ecpay', 'width=640,height=480');
                            window.open('<?php echo $process_url ?>'+id+'&size='+order_item_size, 'ecpay', 'width=640,height=480');
                        });
                    </script>

                <?php endif;
            }
        }
    }

    public function plugin_activated()
    {
        if (!function_exists('curl_init')) {
            deactivate_plugins(basename(__FILE__));
            wp_die('抱歉！目前無法啟用這個外掛，請確認PHP環境是否載入curl，<a href="javascript:history.go(-1);">返回</a>。');
        }
    }

    public function init_shipping_ecpay_home()
    {
        if (!class_exists('Shipping_Ecpay_Home')) {
            require_once dirname(__FILE__) . '/lib/shipping-ecpay-home.php';
        }

        $gateway_path = plugin_dir_path(__FILE__) . 'shipping/';

        $classes = glob($gateway_path . 'wc-shipping-home-*.php');

        if (!$classes) {
            return;
        }

        foreach ($classes as $class) {
            $class_name = basename(str_replace('-', '_', $class), '.php');
            if (!class_exists($class_name)) {
                require_once $class;
                if (version_compare(get_woo_version(), '2.6.0') == 1) {
                    $name                 = str_replace('wc_shipping_', '', $class_name) . '_shipping';
                    $this->methods[$name] = $class_name;
                } else {
                    $this->methods[] = $class_name;
                }
            }
        }
        add_action('woocommerce_checkout_update_order_meta', array($this, 'my_custom_checkout_field_update_order_meta'));
        add_action('woocommerce_review_order_after_shipping', array($this, 'output_total_excluding_tax'));
        add_action('woocommerce_after_checkout_validation', array($this, 'validate_phone_number'));
        add_filter('woocommerce_ship_to_different_address_checked', '__return_false');
        add_filter('woocommerce_shipping_methods', array($this, 'add_ecpay_home_shipping'));
        // add_action( 'woocommerce_checkout_order_processed', array( $this, 'run_package_request' ) );
    }

    public function my_custom_checkout_field_update_order_meta($order_id)
    {
        if (!empty($_POST['tcat'])) {
            update_post_meta($order_id, 'tcat Field', sanitize_text_field($_POST['tcat']));
        }
        if (!empty($_POST['ecan'])) {
            update_post_meta($order_id, 'ecan Field', sanitize_text_field($_POST['ecan']));
        }
        if (!empty($_POST['coldtcat'])) {
            update_post_meta($order_id, 'coldtcat Field', sanitize_text_field($_POST['coldtcat']));
        }
    }

    public function output_total_excluding_tax()
    {

        $chosen_methods  = WC()->session->get('chosen_shipping_methods');
        $chosen_shipping = $chosen_methods[0];

        if ($chosen_shipping == 'home_tcat_shipping') {

            ?>
            <script type="text/javascript">


                var ele = document.createElement("tr");
                ele.setAttribute("id", "package-receive-time-for-tcat");
                ele.innerHTML = '<th>配送時段</th><td><select name="tcat"><option value="1">9:00-12:00</option><option value="2">12:00-17:00</option><option value="3">17:00-20:00</option><option value="4">不限時</option></select></td>';

                jQuery(".shipping").after(ele);


            </script>
            <?php
} elseif ($chosen_shipping == 'home_coldtcat_shipping') {

            ?>
            <script type="text/javascript">


                var ele = document.createElement("tr");
                ele.setAttribute("id", "package-receive-time-for-coldtcat");
                ele.innerHTML = '<th>配送時段</th><td><select name="coldtcat"><option value="1">9:00-12:00</option><option value="2">12:00-17:00</option><option value="3">17:00-20:00</option><option value="4">不限時</option></select></td>';

                jQuery(".shipping").after(ele);


            </script>
            <?php
} elseif ($chosen_shipping == 'home_ecan_shipping') {

            ?>
            <script type="text/javascript">


                var ele = document.createElement("tr");
                ele.setAttribute("id", "package-receive-time-for-ecan");
                ele.innerHTML = '<th>配送時段</th><td><select name="ecan"><option value="1">9:00-12:00</option><option value="2">12:00-17:00</option><option value="3">17:00-20:00</option><option value="4">不限時</option></select></td>';

                jQuery(".shipping").after(ele);


            </script>
            <?php
}
    }

    public function add_ecpay_home_shipping($methods)
    {
        $methods = array_merge($methods, $this->methods);
        return $methods;
    }

    public function get_ecpay_data($id = '')
    {
        if (empty($id)) {
            global $post;
            $id = $post->ID;
        }
        $data = get_post_meta($id, 'ecpay_logistics', true);
        $data = json_decode($data);

        return $data;
    }

    // 檢查手機號碼
    public function validate_phone_number()
    {
        $chosen_methods  = WC()->session->get('chosen_shipping_methods');
        $chosen_shipping = $chosen_methods[0];

        if ($chosen_shipping != 'home_coldtcat_shipping' || $chosen_shipping != 'home_ecan_shipping' || $chosen_shipping != 'home_tcat_shipping') {
            return;
        }

        $phone = (isset($_POST['billing_phone']) ? trim($_POST['billing_phone']) : '');
        if (!preg_match("/^[0][1-9]{1,3}[0-9]{6,8}$/", $phone)) {
            throw new Exception('聯絡電話請輸入正確手機號碼,格式為0988888888');
        }
    }

    public function add_print_button()
    {
        // if (!$this->get_ecpay_data()) return;

        global $post;

        $order = new WC_Order($post->ID);

        $shipping           = $order->get_shipping_methods();
        $shipping           = array_pop($shipping);
        $shipping_method_id = $shipping['method_id'];

        if ($shipping_method_id == 'home_coldtcat_shipping' ||
            $shipping_method_id == 'home_ecan_shipping' || $shipping_method_id == 'home_tcat_shipping'
        ) {
            add_meta_box(
                'woocommerce-order-ecpay-button',
                __('綠界宅配Home', 'woocommerce'),
                array($this, 'order_meta_box_print_button'),
                'shop_order',
                'side',
                'default'
            );
        }
    }

    public function order_meta_box_print_button()
    {

    }

    public function create_check_code($post, $settings = '')
    {
        if (empty($settings)) {

            if ($post['LogisticsSubType'] == 'TCAT' && $post['Temperature'] == '0002') {
                $shipping = 'home_coldtcat_shipping';
            } elseif ($post['LogisticsSubType'] == 'TCAT') {
                $shipping = 'home_tcat_shipping';
            } else {
                $shipping = 'home_ecan_shipping';
            }

            if (version_compare(get_woo_version(), '2.6.0') == 1) {
                $settings = get_shipping_option($shipping);
            } else {
                $shipping_setting = 'woocommerce_' . $shipping . '_settings';
                $settings         = get_option($shipping_setting);
            }

            unset($post['LogisticsSubType']);
        }

        if (isset($post['CheckMacValue'])) {
            unset($post['CheckMacValue']);
        }

        uksort($post, function ($a, $b) {
            return strcasecmp($a, $b);
        });

        $hash_raw_data  = 'HashKey=' . $settings['ecpay_hash_key'] . '&' . urldecode(http_build_query($post)) . '&HashIV=' . $settings['ecpay_hash_iv'];
        $urlencode_data = strtolower(urlencode($hash_raw_data));

        $urlencode_data = str_replace('%2d', '-', $urlencode_data);
        $urlencode_data = str_replace('%5f', '_', $urlencode_data);
        $urlencode_data = str_replace('%2e', '.', $urlencode_data);
        $urlencode_data = str_replace('%21', '!', $urlencode_data);
        $urlencode_data = str_replace('%2a', '*', $urlencode_data);
        $urlencode_data = str_replace('%28', '(', $urlencode_data);
        $urlencode_data = str_replace('%29', ')', $urlencode_data);

        return strtoupper(md5($urlencode_data));
    }

    public function check_value($post)
    {
        if (isset($post['RtnCode'])) {

            if ($post['LogisticsSubType'] == 'TCAT') {
                $shipping = 'home_tcat_shipping';
            } else {
                $shipping = 'home_ecan_shipping';
            }

        } elseif (!isset($post['RtnCode'])) {

            if ($post['LogisticsSubType'] == 'TCAT' && $post['Temperature'] == '0002') {
                $shipping = 'home_coldtcat_shipping';
            } elseif ($post['LogisticsSubType'] == 'TCAT' && $post['Temperature'] == '0001') {
                $shipping = 'home_tcat_shipping';
            } else {
                $shipping = 'home_ecan_shipping';
            }
        }
        if (version_compare(get_woo_version(), '2.6.0') == 1) {
            $settings = get_shipping_option($shipping);
        } else {
            $shipping_setting = 'woocommerce_' . $shipping . '_settings';
            $settings         = get_option($shipping_setting);
        }

        $MyCheckMacValue = $this->create_check_code($post, $settings);

        if ($MyCheckMacValue == $post['CheckMacValue']) {
            return true;
        }
        return false;
    }

    // 處理歐付寶物流資料傳送
    public function run_package_request($order_id, $size)
    {
        $is_collection = 'N';

        $order         = new WC_Order($order_id);
        $total         = $order->get_total();
        $shipping_cost = $order->get_total_shipping();
        $amount        = $total - $shipping_cost;

        $shipping_items = $order->get_shipping_methods();
        $shipping_items = array_pop($shipping_items);

        $chosen_shipping = $shipping_items['method_id'];

        if ($chosen_shipping == 'home_tcat_shipping') {
            $schedule = get_post_meta($order_id, 'tcat Field', true);
        } elseif ($chosen_shipping == 'home_coldtcat_shipping') {
            $schedule = get_post_meta($order_id, 'coldtcat Field', true);
        } elseif ($chosen_shipping == 'home_ecan_shipping') {
            $schedule = get_post_meta($order_id, 'ecan Field', true);
        }

        $sub_type = ($chosen_shipping == 'home_coldtcat_shipping' || $chosen_shipping == 'home_tcat_shipping')
        ? 'TCAT'
        : 'ECAN';

        $TEMP = ($chosen_shipping == 'home_coldtcat_shipping')
        ? '0002'
        : '0001';
        $shipping_setting = 'woocommerce_' . $chosen_shipping . '_settings';

        if (version_compare(get_woo_version(), '2.6.0') == 1) {
            $settings = get_shipping_option($chosen_shipping);
        } else {
            $settings = get_option($shipping_setting);
        }

        $ecpay_url = ($settings['ecpay_merchant_id'] == '2000132')
        ? 'https://logistics-stage.ecpay.com.tw/Express/Create'
        : 'https://logistics.ecpay.com.tw/Express/Create';
        date_default_timezone_set("Asia/Taipei");
        $trade_no = ('2000132' === $settings['ecpay_merchant_id'])
        ? rand(1, 999) . $order_id
        : $order_id;
        $post_data = array(
            'MerchantID'            => $settings['ecpay_merchant_id'],
            'MerchantTradeNo'       => $trade_no,
            'MerchantTradeDate'     => date('Y/m/d H:i:s'),
            'LogisticsType'         => 'HOME',
            'LogisticsSubType'      => $sub_type,
            'GoodsAmount'           => $total,
            'CollectionAmount'      => '',
            'IsCollection'          => $is_collection,
            'GoodsName'             => '網路商品',
            'SenderName'            => $settings['ecpay_sender_name'],
            'SenderPhone'           => '',
            'SenderCellPhone'       => $settings['ecpay_sender_cellphone'],
            'ReceiverName'          => $order->billing_last_name . $order->billing_first_name,
            'ReceiverPhone'         => '',
            'ReceiverCellPhone'     => $order->billing_phone,
            'ReceiverEmail'         => $order->billing_email,
            'TradeDesc'             => '',
            'ServerReplyURL'        => plugins_url('home-notify.php', __FILE__),
            'ClientReplyURL'        => plugins_url('home-process.php', __FILE__),
            // 'LogisticsC2CReplyURL' => plugins_url( 'home-notify.php', __FILE__ ),
            'Remark'                => '',
            'PlatformID'            => '',
            // 'ReceiverStoreID' => '1234',
            // 'ReturnStoreID' => '', // (逆物流)退貨用的商店編號
            'SenderZipCode'         => $settings['ecpay_sender_zip_code'],
            'SenderAddress'         => $settings['ecpay_sender_address'],
            'ReceiverZipCode'       => $order->shipping_postcode,
            'ReceiverAddress'       => $order->shipping_address_1,
            'Temperature'           => $TEMP, // 0001:常溫 (預設值) 0002:冷藏 0003:冷凍
            'Distance'              => '00', // 00:同縣市 (預設值) 01:外縣市 02:離島
            'Specification'         => $size, // 0001: 60cm (預設值) 0002: 90cm 0003: 120cm 0004: 150cm
            'ScheduledPickupTime'   => $schedule, // 1: 9~12 2: 12~17 3: 17~20 4: 不限時(固定 4 不限時)
            'ScheduledDeliveryTime' => $schedule, // 1: 9~12 2: 12~17 3: 17~20 4:不限時 5:20~21(需限定區域)
        );

        $out_state = array('澎湖縣', '金門縣', '連江縣');

        $state = $order->billing_state;

        if ($state != $settings['ecpay_sender_state']
            and !in_array($state, $out_state)
        ) {
            $post_data['Distance'] = '01'; // 外縣市
        } else {
            $post_data['Distance'] = '02'; // 離島
        }

        ksort($post_data); // key 由小到大排序

        $post_data = apply_filters('ecpay_home_logistics_create_order_data', $post_data, $order);

        $post_data['CheckMacValue'] = $this->create_check_code($post_data, $settings);

        $post_data['ecpay_url'] = $ecpay_url;

        return $post_data;
    }
}

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    function lohasit_ecpay_shipping_home()
    {
        return Wc2_Shipping_ecpay_Home::forge();
    }

    $GLOBALS['ecpay_home'] = lohasit_ecpay_shipping_home();

    add_filter('woocommerce_states', 'woocommerce_tw_states2');

    function woocommerce_tw_states2($states)
    {
        if (!isset($states['TW'])) {
            $states['TW'] = array(
                '基隆市' => '基隆市',
                '台北市' => '台北市',
                '新北市' => '新北市',
                '宜蘭縣' => '宜蘭縣',
                '桃園市' => '桃園市',
                '新竹市' => '新竹市',
                '新竹縣' => '新竹縣',
                '苗栗縣' => '苗栗縣',
                '台中市' => '台中市',
                '彰化縣' => '彰化縣',
                '南投縣' => '南投縣',
                '雲林縣' => '雲林縣',
                '嘉義市' => '嘉義市',
                '嘉義縣' => '嘉義縣',
                '台南市' => '台南市',
                '高雄市' => '高雄市',
                '屏東縣' => '屏東縣',
                '花蓮縣' => '花蓮縣',
                '台東縣' => '台東縣',
                '澎湖縣' => '澎湖縣',
                '金門縣' => '金門縣',
                '連江縣' => '連江縣',
            );
        }
        return $states;
    }

    add_filter('woocommerce_package_rates', 'wp_overwrite_tcat_cost2', 100, 2);

    function wp_overwrite_tcat_cost2($rates, $package)
    {
        global $woocommerce;
        $state = $woocommerce->customer->get_shipping_state();
        $out   = array('澎湖縣', '金門縣', '連江縣');

        foreach ($rates as $rate) {
            if ($rate->id === 'home_coldtcat_shipping'
                or $rate->id === 'home_ecan_shipping'
                or $rate->id === 'home_tcat_shipping'
            ) {
                if (in_array($state, $out)) {
                    global $woocommerce;
                    $settings = get_shipping_option($rate->id);
                    //$rate->cost = $settings['outstate_cost'];
                    $rate->cost = ((int) $woocommerce->cart->cart_contents_total >= (int) $settings['outstate_free_cost'])
                    ? 0 : (int) $settings['outstate_cost'];
                }
                //Set the TAX
                //$rate->taxes[1] = 1000 * 0.2;
            }
        }

        return $rates;
    }
}
?>