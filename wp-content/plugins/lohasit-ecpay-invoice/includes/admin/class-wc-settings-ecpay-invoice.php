<?php

defined('ABSPATH') or die;

class WC_Settings_Ecpay_Invoice extends WC_Settings_Page
{
    public function __construct()
    {
        $this->id    = 'lohasit_ecpay_invoice';
        $this->label = '樂活 - 綠界電子發票設定';

        add_filter('woocommerce_settings_tabs_array', [$this, 'add_settings_page'], 20);
        add_action('woocommerce_settings_' . $this->id, [$this, 'output']);
        add_action('woocommerce_settings_save_' . $this->id, [$this, 'save']);
    }

    public function get_settings()
    {

        return apply_filters('woocommerce_' . $this->id . '_settings', [

            [
                'title' => '樂活 - 綠界電子發票設定',
                'type'  => 'title',
                'desc'  => '',
                'id'    => 'lohasit_ecpay_invoice_options',
            ],

            [
                'title'    => '特店編號',
                'desc'     => '特店編號(MerchantID)',
                'id'       => 'lohasit_ecpay_invoice_merchant_id',
                'type'     => 'text',
                'css'      => 'min-width:300px;',
                'default'  => '2000132',
                'desc_tip' => false,
            ],

            [
                'title'    => '介接 Hash Key',
                'desc'     => '介接 Hash Key',
                'id'       => 'lohasit_ecpay_invoice_hash_key',
                'type'     => 'text',
                'css'      => 'min-width:300px;',
                'default'  => 'ejCk326UnaZWKisg',
                'desc_tip' => false,
            ],

            [
                'title'    => '介接 Hash IV',
                'desc'     => '介接 Hash IV',
                'id'       => 'lohasit_ecpay_invoice_hash_iv',
                'type'     => 'text',
                'css'      => 'min-width:300px;',
                'default'  => 'q9jcZX8Ib9LM8wYk',
                'desc_tip' => false,
            ],

            // [
            //     'title'    => '延遲天數',
            //     'desc'     => '若為延遲開立時，延遲天數須介於 1 至 15 天內',
            //     'id'       => 'lohasit_ecpay_invoice_delay_day',
            //     'type'     => 'text',
            //     'css'      => 'min-width:300px;',
            //     'default'  => '0',
            //     'desc_tip' => false,
            // ],

            [
                'type' => 'sectionend',
                'id'   => 'lohasit_ecpay_invoice_style_options',
            ],

        ]); // End pages settings
    }
}

return new WC_Settings_Ecpay_Invoice;
