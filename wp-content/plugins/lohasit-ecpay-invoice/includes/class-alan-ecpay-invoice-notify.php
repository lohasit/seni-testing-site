<?php

class EcpayInvoiceNotify extends WC_Payment_Gateway
{
    public function __construct()
    {
        $this->id = 'ecpay_invoice';

        add_action('woocommerce_api_' . $this->id, array($this, 'ecpay_invoice_notify'));
    }

    public function ecpay_invoice_notify()
    {
        global $woo_ecpay_invoice;

        if ($_REQUEST['od_sob']) {
            if ($woo_ecpay_invoice->get_option('merchant_id') == '2000132') {
                list($order_id) = explode('_', $_REQUEST['od_sob']);
            } else {
                $order_id = $_REQUEST['od_sob'];
            }

            if ($_REQUEST['invoicenumber']) {
                $order = new WC_Order($order_id);
                update_post_meta($order_id, '_ecpay_invoice_no', $_REQUEST['invoicenumber']);
                $order->add_order_note('發票號碼:' . $_REQUEST['invoicenumber'], true);
                $woo_ecpay_invoice->invoice_notify($order_id, $_REQUEST['invoicenumber']);
            }
        }

        exit("1|OK");
    }
}

new EcpayInvoiceNotify;
