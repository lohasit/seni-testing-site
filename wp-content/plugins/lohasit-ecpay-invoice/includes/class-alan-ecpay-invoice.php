<?php

class AlanEcpayInvoice
{
    public static $instance = null;

    protected $base_url = null;

    public static function forge()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->base_url = ($this->merchant_id == '2000132')
        ? 'https://einvoice-stage.ecpay.com.tw/'
        : 'https://einvoice.ecpay.com.tw/';

        do_action('lohasit_ecpay_invoice_init');
    }

    public function __get($key)
    {
        $key = 'lohasit_ecpay_invoice_' . $key;

        return get_option($key);
    }

    protected function create_check_code($data = array())
    {
        if (isset($data['CheckMacValue'])) {
            unset($data['CheckMacValue']);
        }

        // (作廢原因)計算檢查碼時，需將此參數排除
        if (isset($data['Reason'])) {
            unset($data['Reason']);
        }

        ksort($data);

        $hash_raw_data = 'HashKey=' . $this->hash_key .
        '&' . urldecode(http_build_query($data)) .
        '&HashIV=' . $this->hash_iv;

        $urlencode_data = strtolower(urlencode($hash_raw_data));

        $urlencode_data = str_replace('%2d', '-', $urlencode_data);
        $urlencode_data = str_replace('%5f', '_', $urlencode_data);
        $urlencode_data = str_replace('%2e', '.', $urlencode_data);
        $urlencode_data = str_replace('%21', '!', $urlencode_data);
        $urlencode_data = str_replace('%2a', '*', $urlencode_data);
        $urlencode_data = str_replace('%28', '(', $urlencode_data);
        $urlencode_data = str_replace('%29', ')', $urlencode_data);

        //發現「(」和「)」需再從%2528、%2529轉換回來
        $urlencode_data = str_replace('%252d', '-', $urlencode_data);
        $urlencode_data = str_replace('%255f', '_', $urlencode_data);
        $urlencode_data = str_replace('%252e', '.', $urlencode_data);
        $urlencode_data = str_replace('%2521', '!', $urlencode_data);
        $urlencode_data = str_replace('%252a', '*', $urlencode_data);
        $urlencode_data = str_replace('%2528', '(', $urlencode_data);
        $urlencode_data = str_replace('%2529', ')', $urlencode_data);

        $check_code = strtoupper(md5($urlencode_data));

        do_action_ref_array('lohasit_ecpay_invoice_debug_check_code', array($hash_raw_data, $urlencode_data, $check_code));

        return $check_code;
    }

    public function check_value($data = array())
    {
        $my_check_mac_value = $this->create_check_code($data);

        return ($my_check_mac_value === $data['CheckMacValue']);
    }

    // 發票開立
    public function issue($data = array())
    {
        if ((int) $this->delay_day > 0) {
            unset($data['vat']);
        }

        $args = $data;

        unset($args['InvoiceRemark']);
        unset($args['ItemName']);
        unset($args['ItemWord']);

        //如果載具編號有+號，則轉換為空白來產生驗證碼
        $args['CarruerNum'] = str_replace('+', ' ', $args['CarruerNum']);

        $data['CheckMacValue'] = $this->create_check_code($args);

        // 延遲開立發票(預約開立發票)
        if (isset($data['DelayFlag']) and $data['DelayFlag'] == 1) {
            $response = $this->server_post('Invoice/DelayIssue', $data);
        } else {
            $response = $this->server_post('Invoice/Issue', $data);
        }

        return $response;
    }

    // 發票開立通知
    public function notify($data = array())
    {
        $data['CheckMacValue'] = $this->create_check_code($data);

        $data = apply_filters('lohasit_ecpay_invoice_notify_args', $data);

        return $this->server_post('Notify/InvoiceNotify', $data);
    }

    // 觸發開立發票
    public function trigger($data = array())
    {
        $data['CheckMacValue'] = $this->create_check_code($data);

        $response = $this->server_post('Invoice/TriggerIssue', $data);

        return $response;
    }

    // 發票查詢
    public function query($data = array())
    {

    }

    // 作廢發票
    public function invalid($data = array())
    {
        $data['CheckMacValue'] = $this->create_check_code($data);

        $response = $this->server_post('Invoice/IssueInvalid', $data);

        return $response;
    }

    // 開立折讓

    // 作廢折讓

    // wp_remote_post
    public function server_post($uri, $data = array())
    {
        $result = array();

        $url = $this->base_url . $uri;

        foreach ($data as &$value) {
            $value = urldecode($value);
        }

        $args = array(
            'method'      => 'POST',
            'timeout'     => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => array(),
            'body'        => $data,
            'cookies'     => array(),
        );

        $args = apply_filters('lohasit_ecpay_invoice_server_post_args', $args);

        $response = wp_remote_post($url, $args);

        // print_r($response);

        if (is_wp_error($response)) {
            do_action('lohasit_ecpay_invoice_server_post_error', $response->get_error_message());
            throw new Exception('網路問題: ' . $response->get_error_message());
        } else {
            if (isset($response['response']['code'])
                and !empty($response['response']['code'])
                and $response['response']['code'] == 200
                and isset($response['body'])
                and !empty($response['body'])
            ) {
                do_action('lohasit_ecpay_invoice_server_post_response_success', $response);
                if ($response['body'] == '無執行權限!') {
                    throw new Exception('傳送資料到綠界發生錯誤:' . $response['body']);
                } else {
                    parse_str($response['body'], $result);
                }
            } else {
                do_action('lohasit_ecpay_invoice_server_post_response_error', $response);
                // 500 Internal Server Error
                throw new Exception('綠界主機發生錯誤: ' . $response['response']['code'] . ' ' . $response['response']['message']);
            }
        }

        return $result;
    }
}
