<?php

class Alan_Invoice_Checkout
{
    protected $checkout_data = [];

    public function validation()
    {
        $my_invoice_type     = $_POST['my_invoice_type'];
        $love_code           = $_POST['love_code'];
        $carruer_type        = $_POST['carruer_type'];
        $carruer_num_card    = $_POST['carruer_num_card'];
        $carruer_num_mobile  = $_POST['carruer_num_mobile'];
        $customer_identifier = $_POST['customer_identifier'];
        $customer_name       = $_POST['customer_name'];

        //將手機載具號碼的空白移除
        $carruer_num_mobile = str_replace(' ', '', $carruer_num_mobile);
        $carruer_num_mobile = str_replace('　', '', $carruer_num_mobile);

        if (!$my_invoice_type) {
            wc_add_notice('請選擇發票類型', 'error');
        }

        switch ($my_invoice_type) {
            case '1':
                if (!$love_code) {
                    wc_add_notice('捐贈發票,請選擇捐贈機構!', 'error');
                }
                break;
            case '2':
                if (!$carruer_type and $carruer_type != 0) {
                    wc_add_notice('請選擇發票載具', 'error');
                }
                if ($carruer_type == '2' and (!$carruer_num_card or strlen($carruer_num_card) < 16)) {
                    wc_add_notice('請填寫正確自然人憑證條碼共16碼', 'error');
                }
                // if ($carruer_type == '3' and (!$carruer_num_mobile or strlen($carruer_num_mobile) != 8)) {
                if ($carruer_type == '3' and !preg_match('/^\/{1}[0-9a-zA-Z+-.]{7}$/', $carruer_num_mobile)) {
                    wc_add_notice('請填寫正確手機條碼共8碼', 'error');
                }
                break;
            case '3':
                if (!$customer_identifier) {
                    wc_add_notice('請填寫公司統一編號', 'error');
                }
                if (!$customer_name) {
                    wc_add_notice('請填寫公司抬頭', 'error');
                }
                break;
        }
    }

    public function my_checkout_order_processed($order_id)
    {
        $my_invoice_type     = $_POST['my_invoice_type'];
        $love_code           = $_POST['love_code'];
        $carruer_type        = $_POST['carruer_type'];
        $carruer_num_card    = $_POST['carruer_num_card'];
        $carruer_num_mobile  = $_POST['carruer_num_mobile'];
        $customer_identifier = $_POST['customer_identifier'];
        $customer_name       = $_POST['customer_name'];

        //將手機載具號碼的空白移除
        $carruer_num_mobile = str_replace(' ', '', $carruer_num_mobile);
        $carruer_num_mobile = str_replace('　', '', $carruer_num_mobile);

        switch ($my_invoice_type) {
            case '1':
                add_post_meta($order_id, '_ecpay_love_code', $love_code);
                break;
            case '2':
                add_post_meta($order_id, '_ecpay_carruer_type', $carruer_type);
                if ($carruer_type == '2') {
                    add_post_meta($order_id, '_ecpay_carruer_num_card', $carruer_num_card);
                }
                if ($carruer_type == '3') {
                    add_post_meta($order_id, '_ecpay_carruer_num_mobile', $carruer_num_mobile);
                }
                break;
            case '3':
                add_post_meta($order_id, '_ecpay_customer_identifier', $customer_identifier);
                add_post_meta($order_id, '_ecpay_customer_name', $customer_name);
                break;
        }

        add_post_meta($order_id, '_ecpay_my_invoice_type', $my_invoice_type);
    }

    public function einvoice_form($checkout)
    {
        $this->checkout_data = $checkout_data = WC()->session->get('checkout_data');

        woocommerce_form_field('my_invoice_type', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '發票',
            'options'  => [
                '2' => '電子發票',
                '1' => '捐贈發票',
                '3' => '三聯式發票',
            ],
        ], $checkout->get_value('my_invoice_type') ?? $checkout_data['my_invoice_type']);

        echo $this->donation($checkout);
        echo $this->carruer_type($checkout);
        echo $this->company($checkout);
    }

    public function donation($checkout)
    {
        ob_start();
        $love_code = AlanLoveCode::get();
        woocommerce_form_field('love_code', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '愛心單位',
            'options'  => $love_code,
        ], $checkout->get_value('love_code') ?? $checkout_data['love_code']);

        return ob_get_clean();
    }

    public function carruer_type($checkout)
    {
        ob_start();
        woocommerce_form_field('carruer_type', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '發票載具',
            'options'  => [
                '1' => '綠界科技電子發票載具',
                '2' => '自然人憑證載具',
                '3' => '手機條碼載具',
            ],
        ], $checkout->get_value('carruer_type') ?? $checkout_data['carruer_type']);

        woocommerce_form_field('carruer_num_card', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '自然人憑證條碼',
        ], $checkout->get_value('carruer_num_card') ?? $checkout_data['carruer_num_card']);

        woocommerce_form_field('carruer_num_mobile', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '手機條碼',
        ], $checkout->get_value('carruer_num_mobile') ?? $checkout_data['carruer_num_mobile']);

        return ob_get_clean();
    }

    public function company($checkout)
    {
        ob_start();

        woocommerce_form_field('customer_identifier', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '統一編號',
        ], $checkout->get_value('customer_identifier') ?? $checkout_data['customer_identifier']);

        woocommerce_form_field('customer_name', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '公司抬頭',
        ], $checkout->get_value('customer_name') ?? $checkout_data['customer_name']);

        return ob_get_clean();
    }

    public function footer_script()
    {

        ?>
        <script>
        jQuery(document).ready(function($) {
            $('#carruer_num_mobile_field').hide();
            $('#carruer_num_card_field').hide();
            function toggle(type) {
                // console.log(type);
                switch (type) {
                    case '1':
                        $('#love_code_field').show();
                        $('#carruer_type_field').hide();
                        $('#customer_identifier_field').hide();
                        $('#customer_name_field').hide();
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').hide();
                        break;
                    case '2':
                        $('#carruer_type_field').show();
                        $('#love_code_field').hide();
                        $('#customer_identifier_field').hide();
                        $('#customer_name_field').hide();
                        break;
                    case '3':
                        $('#customer_identifier_field').show();
                        $('#customer_name_field').show();
                        $('#love_code_field').hide();
                        $('#carruer_type_field').hide();
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').hide();
                        break;
                }
            }

            function toggle_carruer(ctype) {
                switch (ctype) {
                    case '0':
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').hide();
                        break;
                    case '1':
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').hide();
                        break;
                    case '2':
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').show();
                        break;
                    case '3':
                        $('#carruer_num_card_field').hide();
                        $('#carruer_num_mobile_field').show();
                        break;
                    case '4':
                        $('#carruer_num_mobile_field').hide();
                        $('#carruer_num_card_field').hide();
                        break;
                }
            }

            var my_invoice_type = '<?php echo $this->checkout_data['my_invoice_type']; ?>';
            var ctype = $('#carruer_type option:selected').val();

            if (my_invoice_type) {
                toggle(my_invoice_type);
                toggle_carruer(ctype);
            } else {
                $('#carruer_type_field').show();
                $('#love_code_field').hide();
                $('#customer_identifier_field').hide();
                $('#customer_name_field').hide();
            }

            $('.invoice-select').change(function(e) {
                e.preventDefault();
                var type = $('.invoice-select option:selected').val();
                toggle(type);
            });

            $('#carruer_type').change(function(e) {
                e.preventDefault();
                var ctype = $('#carruer_type option:selected').val();
                toggle_carruer(ctype)
            });
        });
        </script>
        <?php

    }
}

return new Alan_Invoice_Checkout;