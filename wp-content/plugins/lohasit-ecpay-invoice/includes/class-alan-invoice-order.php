<?php

class Alan_Invoice_Order
{
    use Admin_Invoice;
    /**
     * hook: add_meta_boxes
     */
    public function add_invoice_metabox()
    {
        add_meta_box(
            'ecpay_invoice',
            '綠界電子發票資訊',
            [$this, 'display_invoice_metabox'],
            'shop_order',
            'side',
            'high'
        );
    }

    /**
     * hook: wp_ajax_trigger_immediately
     * @return void
     */
    public function trigger_immediately()
    {
        $order_id = $_POST['order_id'];
        echo $this->invoice_issue($order_id);
        die();
    }

    public function display_invoice_metabox($post)
    {
        $order_id = $post->ID;
        $order    = wc_get_order($order_id);
        $items    = $order->get_items();

        if (count($items) == 1) {
            foreach ($items as $item) {
                if ((int) $item['line_subtotal'] == 0) {
                    return;
                }
            }
        }

        $delay_days = lohasit_ecpay_invoice()->get_option('delay_day');

        $invoice_created     = get_post_meta($order->id, '_ecpay_invoice_created', true);
        $invoice_no          = get_post_meta($order->id, '_ecpay_invoice_no', true);
        $order_no            = get_post_meta($order->id, '_ecpay_invoice_order_no', true);
        $my_invoice_type     = get_post_meta($order->id, '_ecpay_my_invoice_type', true);
        $love_code           = get_post_meta($order->id, '_ecpay_love_code', true);
        $carruer_type        = get_post_meta($order->id, '_ecpay_carruer_type', true);
        $customer_identifier = get_post_meta($order->id, '_ecpay_customer_identifier', true);
        $customer_name       = get_post_meta($order->id, '_ecpay_customer_name', true);
        $invoice_invalid     = get_post_meta($order->id, '_ecpay_invoice_invalid', true);
        $type                = [
            '1' => '捐贈發票',
            '2' => '電子發票',
            '3' => '三聯式發票',
        ];

        switch ($my_invoice_type) {
            case '0':

            case '1':
                $help_text = AlanLoveCode::get($love_code);
                break;
            case '2':
                if ($carruer_type == '1') {
                    $help_text = '綠界科技電子發票載具';
                } elseif ($carruer_type == '2') {
                    $help_text = '自然人憑證條碼';
                } elseif ($carruer_type == '3') {
                    $help_text = '手機條碼';
                } elseif ($carruer_type == '4') {
                    $help_text = '索取實體紙本';
                } elseif ($carruer_type == '0' || $carruer_type == '') {
                    $help_text = '無載具';
                }
                break;
            case '3':
                $help_text = $customer_identifier . '/' . $customer_name;
                break;
        }
        ?>
        <div class="help">
            <?php // alan: 不使用延遲開立發票 ?>
            <?php echo ($delay_days > 0) ? '延遲' . $delay_days . '天開立' : ''; ?>
            <?php if ($invoice_no && !$invoice_invalid): ?>
            <span style="color:red;">說明: 訂單狀態改取消會觸發作廢發票</span>
            <?php endif;?>
        </div>
        <?php if ($my_invoice_type): ?>
            <table class="ecpay-invoice-info">
                <tr>
                    <th>
                </tr>
                <tr>
                    <th>發票號碼:</th>
                    <td>
                    <?php if ($invoice_no && $invoice_invalid): ?>
                        <s><?php echo $invoice_no ?></s> (已作廢)
                    <?php elseif ($invoice_no && !$invoice_invalid): ?>
                        <?php echo $invoice_no; ?>
                    <?php else: ?>
                        發票尚未開立
                    <?php endif?>
                    </td>
                </tr>
                <tr>
                    <th>發票類型:</th>
                    <td>
                    <?php echo implode('/', array($type[$my_invoice_type], $help_text)); ?>
                    </td>
                </tr>
            </table>
        <?php else: ?>
            <?php $this->handle_non_invoice_data();?>
        <?php endif;

    }

    protected function handle_non_invoice_data()
    {
        global $pagenow;

        if ('post-new.php' === $pagenow && $_GET['post_type'] == 'shop_order') {
            $this->invoice_form();
        } else {
            echo '<h3>此訂單沒有綠界電子發票資料。</h3>';
        }
    }

    // 開立發票
    public function invoice_issue($order_id, $notify = true)
    {
        if (get_post_meta($order_id, '_ecpay_invoice_created', true) == '1') {
            return;
        }

        try {
            $order = wc_get_order($order_id);

            $items = $order->get_items();

            $amount = $order->get_total();
            //訂單金額0元將不提供發票
            if ($amount == 0) {
                $order->add_order_note('訂單金額0元不產生發票。');
                return;
            }
            $shipping_cost = $order->get_total_shipping();

            $order_items = array();
            //針對加價購商品再寫一個…
            foreach ($items as $id => $item) {
                $totalItemsPrice = $item['tm_epo_product_original_price'];
                if ($totalItemsPrice) {
                    //這時候是單價
                    $totalItemsPrice = unserialize($totalItemsPrice);
                    $totalItemsPrice = $totalItemsPrice[0]; //主商品單價
                } else {
                    //如果此品項未使用TM系統，則價格改用item預設
                    $totalItemsPrice = $item['line_subtotal'] / $item['qty']; //主商品單價
                }
                $order_items['ItemName'][]    = $item['name'];
                $order_items['ItemCount'][]   = $item['qty'];
                $order_items['ItemWord'][]    = '件';
                $order_items['ItemPrice'][]   = $totalItemsPrice;
                $order_items['ItemTaxType'][] = 1;
                $order_items['ItemAmount'][]  = $totalItemsPrice * $item['qty'];

                /**
                 * 若有加價購商品(Product Extra Option)的話，
                 * ※加價購外掛TM可能會一個商品有多個加價購項目，
                 * 如果不是「修改價格」，而是「額外費用」的話，
                 * 會變成「fee」$order->get_item('fee');
                 */
                if ($item['tmcartepo_data']) {
                    $extraItems = unserialize($item['tmcartepo_data']);
                    foreach ($extraItems as $extraItem) {
                        $order_items['ItemName'][]    = $extraItem['value'];
                        $order_items['ItemCount'][]   = $item['qty'];
                        $order_items['ItemWord'][]    = '件';
                        $order_items['ItemPrice'][]   = $extraItem['price'] / $extraItem['quantity'];
                        $order_items['ItemTaxType'][] = 1;
                        $order_items['ItemAmount'][]  = $extraItem['price'] * $item['qty'];
                    }
                }

            }

            //取得折扣
            //先問系統是不是用紅利點數扣(Yith Points and rewards)
            $discount_point_amount = get_post_meta($order_id, '_ywpar_redemped_points', true);
            $discount_point_name   = '購物金折抵';
            //取得並獨立折扣品項
            $discount_coupon_amount = round(get_post_meta($order_id, '_cart_discount', true));
            if ($discount_coupon_amount != $discount_point_amount && $discount_coupon_amount > $discount_point_amount) {
                $discount_coupon_amount = $discount_coupon_amount - $discount_point_amount;
                $discount_coupon_name   = '優惠折扣';
                //如果有優惠折扣的話
                $order_items['ItemName'][]    = $discount_coupon_name;
                $order_items['ItemCount'][]   = 1;
                $order_items['ItemWord'][]    = '筆';
                $order_items['ItemPrice'][]   = '-' . $discount_coupon_amount;
                $order_items['ItemTaxType'][] = 1;
                $order_items['ItemAmount'][]  = '-' . $discount_coupon_amount;
            }

            //如果有點數折扣的話
            if ($discount_point_amount) {
                $order_items['ItemName'][]    = $discount_point_name;
                $order_items['ItemCount'][]   = 1;
                $order_items['ItemWord'][]    = '筆';
                $order_items['ItemPrice'][]   = '-' . $discount_point_amount;
                $order_items['ItemTaxType'][] = 1;
                $order_items['ItemAmount'][]  = '-' . $discount_point_amount;
            }

            //如果有運費的話
            if ($shipping_cost > 1) {
                $order_items['ItemName'][]    = '運費';
                $order_items['ItemCount'][]   = 1;
                $order_items['ItemWord'][]    = '筆';
                $order_items['ItemPrice'][]   = $shipping_cost;
                $order_items['ItemTaxType'][] = 1;
                $order_items['ItemAmount'][]  = $shipping_cost;
            }

            $invoice_created     = get_post_meta($order->id, '_ecpay_invoice_created', true);
            $invoice_no          = get_post_meta($order->id, '_ecpay_invoice_no', true);
            $my_invoice_type     = get_post_meta($order->id, '_ecpay_my_invoice_type', true);
            $love_code           = get_post_meta($order->id, '_ecpay_love_code', true);
            $carruer_type        = get_post_meta($order->id, '_ecpay_carruer_type', true);
            $carruer_num_card    = get_post_meta($order->id, '_ecpay_carruer_num_card', true);
            $carruer_num_mobile  = get_post_meta($order->id, '_ecpay_carruer_num_mobile', true);
            $customer_identifier = get_post_meta($order->id, '_ecpay_customer_identifier', true);
            $customer_name       = get_post_meta($order->id, '_ecpay_customer_name', true);

            $Print              = '0';
            $Donation           = '2';
            $LoveCode           = '';
            $CarruerType        = '';
            $CarruerNum         = '';
            $CustomerID         = '';
            $CustomerIdentifier = '';
            $CustomerName       = '';

            switch ($my_invoice_type) {
                case '1':
                    $Donation = '1';
                    $LoveCode = $love_code;
                    break;
                case '2':
                    if ($carruer_type == '1') {
                        // 合作特店載具'
                        list($customer_id) = explode('@', $order->billing_email);
                        $CustomerID        = $customer_id;
                        $CarruerType       = '1';
                    } elseif ($carruer_type == '2') {
                        // 自然人憑證條碼
                        $CarruerType = '2';
                        $CarruerNum  = $carruer_num_card;

                    } elseif ($carruer_type == '3') {
                        // 手機條碼
                        $CarruerType = '3';
                        $CarruerNum  = $carruer_num_mobile;
                    } elseif ($carruer_type == '4') {
                        // 索取實體紙本
                        $Print        = '1';
                        $CustomerName = $order->billing_first_name . $order->billing_last_name;
                    }
                    break;
                case '3':
                    $CustomerIdentifier = $customer_identifier;
                    $CustomerName       = $customer_name;
                    $Print              = '1';
                    break;
            }

            $ecpay_invoice = AlanEcpayInvoice::forge();
            //先取得信用卡末四碼
            $card4no = get_post_meta($order->id, 'ecpay_card4no', true);

            $order_no = (lohasit_ecpay_invoice()->get_option('merchant_id') == '2000132')
            ? $order_id . uniqid()
            : $order->get_order_number();

            $args = [
                'TimeStamp'          => time(),
                'MerchantID'         => lohasit_ecpay_invoice()->get_option('merchant_id'),
                'RelateNumber'       => $order_no,
                'CustomerID'         => $CustomerID,
                'CustomerIdentifier' => $CustomerIdentifier,
                'CustomerName'       => $CustomerName,
                'CustomerAddr'       => $order->billing_address_1,
                'CustomerPhone'      => $order->billing_phone,
                'CustomerEmail'      => $order->billing_email,
                'ClearanceMark'      => '', // 當課稅類別[TaxType]為 2(零稅率)時，則該參數請帶 1(經海關出口)或 2(非經海關出口)。
                'Print'              => $Print, // 列印註記 0.不列印 1.列印
                'Donation'           => $Donation, // 捐贈註記 1.捐贈 2.不捐贈
                'LoveCode'           => $LoveCode, // 愛心碼
                'CarruerType'        => $CarruerType, // 載具類別
                'CarruerNum'         => $CarruerNum, // 載具編號
                'TaxType'            => 1, // 課稅類別 1.應稅 2.零稅率 3.免稅
                'SalesAmount'        => $amount, // 發票金額
                'InvoiceRemark'      => ($card4no) ? '信用卡末四碼' . $card4no : '', // 發票備註GC_minos:2017-08-21加入
                'ItemName'           => implode('|', $order_items['ItemName']),
                'ItemCount'          => implode('|', $order_items['ItemCount']),
                'ItemWord'           => implode('|', $order_items['ItemWord']),
                'ItemPrice'          => implode('|', $order_items['ItemPrice']),
                'ItemTaxType'        => implode('|', $order_items['ItemTaxType']),
                'ItemAmount'         => implode('|', $order_items['ItemAmount']),
                'InvType'            => '07', // 字軌類別 07.一般稅額 08.特種稅額
                'vat'                => '', // 1:含稅 0:未稅
            ];

            $args['CustomerName']  = lohasit_ecpay_invoice()->convertSign(urlencode($args['CustomerName']));
            $args['CustomerAddr']  = lohasit_ecpay_invoice()->convertSign(urlencode($args['CustomerAddr']));
            $args['CustomerEmail'] = urlencode($args['CustomerEmail']);
            $args['InvoiceRemark'] = urlencode($args['InvoiceRemark']);
            $args['ItemName']      = urlencode($args['ItemName']);
            $args['ItemWord']      = urlencode($args['ItemWord']);

            if ((int) lohasit_ecpay_invoice()->get_option('delay_day') > 0 and true === $notify) {
                $args['DelayFlag'] = '1';
                $args['DelayDay']  = (int) lohasit_ecpay_invoice()->get_option('delay_day');
                $args['Tsr']       = $order_id; // 原本寫$args['RelateNumber'];
                $args['PayType']   = '2';
                $args['PayAct']    = 'ECPAY';
                $args['NotifyURL'] = WC()->api_request_url('ecpay_invoice');
            }

            $response = $ecpay_invoice->issue($args);

            $status = false;

            if ($response['RtnCode'] == '1') {
                $status = true;
                add_post_meta($order_id, '_ecpay_invoice_created', 1);
                add_post_meta($order_id, '_ecpay_invoice_order_no', $response['OrderNumber']);
                if (isset($response['InvoiceNumber']) and true === $notify) {
                    add_post_meta($order_id, '_ecpay_invoice_no', $response['InvoiceNumber']);
                    $order->add_order_note('發票號碼:' . $response['InvoiceNumber'], true);
                    $this->invoice_notify($order_id, $response['InvoiceNumber']);
                } else {
                    $this->invoice_trigger($order_id);
                }
            }
            $msg = $response['RtnMsg'];
        } catch (Exception $e) {
            // 例外錯誤處理。
            $msg = $e->getMessage();
        }

        $order->add_order_note($msg);

        return json_encode([
            'status' => $status,
            'msg'    => $msg,
        ]);
    }

    // Alan: 作廢發票
    public function invoice_invalid($order_id)
    {
        $invoiceNumber = null;

        $order = wc_get_order($order_id);

        // 作廢發票
        try {
            $invoiceNumber = get_post_meta($order_id, '_ecpay_invoice_no', true);

            if ($invoiceNumber) {
                $ecpay_invoice = AlanEcpayInvoice::forge();

                $args = [
                    'TimeStamp'     => time(),
                    'MerchantID'    => lohasit_ecpay_invoice()->get_option('merchant_id'),
                    'InvoiceNumber' => $invoiceNumber,
                    'Reason'        => '%E8%A8%82%E5%96%AE%E5%8F%96%E6%B6%88', // urlencode('訂單取消')
                ];

                $response = $ecpay_invoice->invalid($args);
                /*
                [
                'RtnCode' => 1,
                'RtnMsg' => '發票作廢成功',
                'InvoiceNumber' => 'XN123456',
                'CheckMacValue' => 'xxxxxxxxxxxxxx',
                ]
                 */

                if ($response['RtnCode'] == '1') {
                    $msg           = $response['RtnMsg'];
                    $invoiceNumber = $response['InvoiceNumber'];
                    add_post_meta($order_id, '_ecpay_invoice_invalid', $invoiceNumber);
                    $order->add_order_note($msg . ' ' . $invoiceNumber, true);
                } else {
                    $order->add_order_note($msg);
                }
            }

        } catch (Exception $e) {
            $msg = $e->getMessage();
            $order->add_order_note($msg);
        }
    }

    /** 依據先前所設定的延遲開立天數，待延遲開立時間到，系統會自動開立上傳財政部，並
     *  通知消費者(買家)電子發票已開立。（若未設定延遲開立天數，觸發後立即開立發票）
     */
    public function invoice_trigger($order_id)
    {
        $msg    = '開立失敗';
        $status = false;

        try {
            $order = wc_get_order($order_id);

            $args = [
                'TimeStamp'  => time(),
                'MerchantID' => lohasit_ecpay_invoice()->get_option('merchant_id'),
                'Tsr'        => $order_id,
                'PayType'    => '2',
            ];

            $ecpay_invoice = AlanEcpayInvoice::forge();

            $response = $ecpay_invoice->trigger($args);

            if ($response['RtnCode'] == '4000003') {
                $msg = '延遲開立發票成功';
            } elseif ($response['RtnCode'] == '4000004') {
                $msg = '開立發票成功';
            } else {
                $msg = $response['RtnMsg'];
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }

        $order->add_order_note('綠界電子發票:' . $msg . PHP_EOL . '回覆代碼:' . $response['RtnCode']);
    }

    public function invoice_notify($order_id, $invoice_no)
    {
        try {
            $order = wc_get_order($order_id);

            $args = [
                'TimeStamp'  => time(),
                'MerchantID' => lohasit_ecpay_invoice()->get_option('merchant_id'),
                'InvoiceNo'  => $invoice_no,
                'NotifyMail' => urlencode($order->billing_email),
                'Notify'     => 'E',
                'InvoiceTag' => 'I',
                'Notified'   => 'C',
            ];

            $ecpay_invoice = AlanEcpayInvoice::forge();

            $response = $ecpay_invoice->notify($args);

            if ($response['RtnCode'] == '1') {
                $msg = '成功發送-電子發票開立作業通知';
            } else {
                $msg = $response['RtnMsg'];
            }
        } catch (Exception $e) {
            // 例外錯誤處理。
            $msg = $e->getMessage();
        }

        $order->add_order_note($msg);
    }

    public function ecpay_invoice_add_column($columns = [])
    {
        $columns['ecpay_invoice_column'] = '電子發票';

        return $columns;
    }

    public function ecpay_invoice_add_column_action($column)
    {
        if ('ecpay_invoice_column' === $column) {
            global $post;
            $order = wc_get_order($post->ID);
            $items = $order->get_items();

            if (count($items) == 1) {
                foreach ($items as $item) {
                    if ((int) $item['line_subtotal'] == 0) {
                        return;
                    }
                }
            }
//
//            $my_invoice_type = get_post_meta($order->id, '_ecpay_my_invoice_type', true);
//            $invoice_no      = get_post_meta($order->id, '_ecpay_invoice_no', true);
//            $invoice_invalid = get_post_meta($order->id, '_ecpay_invoice_invalid', true);
            // [20220127] 改用$post->ID指定order_id
            $my_invoice_type = get_post_meta($post->ID, '_ecpay_my_invoice_type', true);
            $invoice_no      = get_post_meta($post->ID, '_ecpay_invoice_no', true);
            $invoice_invalid = get_post_meta($post->ID, '_ecpay_invoice_invalid', true);

            if ($my_invoice_type) {
                if ($invoice_no && $invoice_invalid) {
                    printf('發票號碼: <s>%s</s>', $invoice_no);
                } elseif ($invoice_no && !$invoice_invalid) {
                    printf('發票號碼: %s', $invoice_no);
                } else {
                    $url = 'javascript:;';
                    printf('<a href="%s" data-id="%s" class="button button-primary invoice-issue">開立</a>', $url, $order->get_id());
                }
            } else {
                printf('<span style="color:%s;">無開立發票資料</span>', 'red');
            }
        }
    }

    public function admin_footer_script()
    {
        global $pagenow;

        if ('post-new.php' === $pagenow && $_REQUEST['post_type'] == 'shop_order'):
            lohasit_ecpay_invoice()->checkout->footer_script();
        endif;

        if ('edit.php' === $pagenow && $_REQUEST['post_type'] == 'shop_order'):

        ?>
        <script>
        jQuery(function($){
            $('.invoice-issue').on('click', function(e){
                e.preventDefault();
                $(this).text('稍候').attr('disabled', true);
                var ajax_data = {
                    'action':'trigger_immediately',
                    'order_id': $(this).data('id'),
                };
                $.ajax(woocommerce_admin_meta_boxes.ajax_url,{
                    type:'POST',
                    data:ajax_data,
                    dataType:'json',
                    success:function(resp){
                        if (resp.status) {
                            $(this).text('已開立').attr('disabled', false);
                            window.location.reload();
                        }
                    }
                });
            });
        });
        </script>
        <?php endif;

    }

    public function save_invoice_fields($postID)
    {
        lohasit_ecpay_invoice()->checkout->my_checkout_order_processed($postID);
    }
}

return new Alan_Invoice_Order;
