<?php

trait Admin_Invoice
{
    public function invoice_form()
    {
        woocommerce_form_field('my_invoice_type', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '發票',
            'options'  => [
                '2' => '電子發票',
                '1' => '捐贈發票',
                '3' => '三聯式發票',
            ],
        ], '2');
        echo $this->donation();
        echo $this->carruer_type();
        echo $this->company();
    }

    public function donation()
    {
        ob_start();
        $love_code = AlanLoveCode::get();
        woocommerce_form_field('love_code', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '愛心單位',
            'options'  => $love_code,
        ]);

        return ob_get_clean();
    }

    public function carruer_type()
    {
        ob_start();
        woocommerce_form_field('carruer_type', [
            'type'     => 'select',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '發票載具',
            'options'  => [
                '1' => '綠界科技電子發票載具',
                '2' => '自然人憑證載具',
                '3' => '手機條碼載具',
            ],
        ]);

        woocommerce_form_field('carruer_num_card', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '自然人憑證條碼',
        ]);

        woocommerce_form_field('carruer_num_mobile', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '手機條碼',
        ]);

        return ob_get_clean();
    }

    public function company()
    {
        ob_start();

        woocommerce_form_field('customer_identifier', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '統一編號',
        ]);

        woocommerce_form_field('customer_name', [
            'type'     => 'text',
            'required' => true,
            'class'    => ['form-row-wide invoice-select'],
            'label'    => '公司抬頭',
        ]);

        return ob_get_clean();
    }
}
