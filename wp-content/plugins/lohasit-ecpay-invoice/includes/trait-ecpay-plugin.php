<?php

trait Ecpay_Plugin
{
    //為了符合.NET轉回碼
    public function convertSign($string)
    {
        if ($string) {
            $string = str_replace('%252d', '-', $string);
            $string = str_replace('%255f', '_', $string);
            $string = str_replace('%252e', '.', $string);
            $string = str_replace('%2521', '!', $string);
            $string = str_replace('%252a', '*', $string);
            $string = str_replace('%2528', '(', $string);
            $string = str_replace('%2529', ')', $string);
        }
        return $string;
    }

    public function get_option($key = '')
    {
        $key = 'lohasit_ecpay_invoice_' . $key;

        if (get_option($key)) {
            return get_option($key);
        } else {
            $default = [
                'lohasit_ecpay_invoice_merchant_id' => '2000132',
                'lohasit_ecpay_invoice_hash_key'    => 'ejCk326UnaZWKisg',
                'lohasit_ecpay_invoice_hash_iv'     => 'q9jcZX8Ib9LM8wYk',
            ];

            return $default[$key];
        }
    }
}
