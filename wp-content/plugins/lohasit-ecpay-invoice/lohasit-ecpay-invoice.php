<?php

/*
Plugin Name: 樂活綠界電子發票(非官方版)
Description: 樂活壓板系統 - Woocommerce - 綠界電子發票
Version: 3.0.0
Author: Lohas IT (alanchang15)
 */

defined('ABSPATH') or die;

defined('ECPAY_EINVOICE') or define('ECPAY_EINVOICE', true);

require_once __DIR__ . '/includes/trait-ecpay-plugin.php';
require_once __DIR__ . '/includes/trait-admin-invoice.php';

class Lohasit_Ecpay_Invoice
{
    use Ecpay_Plugin;

    private static $_instance = null;

    protected $loader = [];

    public static function forge()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __construct()
    {
        register_activation_hook(__FILE__, [$this, 'plugin_activated']);

        add_action('plugins_loaded', [$this, 'init_ecpay_einvoice']);
    }

    public function __get($key)
    {
        return (array_key_exists($key, $this->loader))
        ? $this->loader[$key]
        : null;
    }

    public function plugin_activated()
    {
        $default = [
            'lohasit_ecpay_invoice_merchant_id' => '2000132',
            'lohasit_ecpay_invoice_hash_key'    => 'ejCk326UnaZWKisg',
            'lohasit_ecpay_invoice_hash_iv'     => 'q9jcZX8Ib9LM8wYk',
            'lohasit_ecpay_invoice_delay_day'   => '0',
        ];

        foreach ($default as $key => $value) {
            if (!get_option($key)) {
                update_option($key, $value);
            }
        }
    }

    protected function require_files($files = [])
    {
        foreach ($files as $file) {
            if (file_exists($require_file = plugin_dir_path(__FILE__) . $file)) {
                $basename = basename($file, '.php');
                $basename = str_replace(['class-alan-invoice-', 'class-alan-', '-'], ['', '_'], $basename);
                $object   = require_once $require_file;
                if (is_object($object)) {
                    $this->loader[$basename] = $object;
                }
            }
        }
    }

    /**
     * hook: plugins_loaded
     * @return void
     */
    public function init_ecpay_einvoice()
    {
        $this->require_files([
            'includes/class-alan-invoice-checkout.php',
            'includes/class-alan-invoice-order.php',
            'includes/class-alan-ecpay-invoice.php',
            'includes/class-alan-love-code.php',
            'includes/class-alan-ecpay-invoice-notify.php',
        ]);

        // dd($this);

        if (is_admin()) {
            // Admin Settings via settings API
            add_filter('woocommerce_get_settings_pages', [$this, 'add_settings_page']);
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), [$this, 'admin_edit_link']);
            add_action('add_meta_boxes', [$this->order, 'add_invoice_metabox']);
            add_action('woocommerce_order_status_cancelled', [$this->order, 'invoice_invalid'], 10, 1);
            add_action('wp_ajax_trigger_immediately', [$this->order, 'trigger_immediately']);
            // 訂單列表新增欄位 電子發票
            add_filter('manage_edit-shop_order_columns', [$this->order, 'ecpay_invoice_add_column'], 2);
            add_action('manage_shop_order_posts_custom_column', [$this->order, 'ecpay_invoice_add_column_action'], 2);
            add_action('admin_footer', [$this->order, 'admin_footer_script']);
            add_action('save_post', [$this->order, 'save_invoice_fields']);
        } else {
            add_action('wp_footer', [$this->checkout, 'footer_script']);
            add_action('woocommerce_after_checkout_billing_form', [$this->checkout, 'einvoice_form'], 10, 1);
            add_action('woocommerce_after_checkout_validation', [$this->checkout, 'validation'], 12, 1);
            add_action('woocommerce_checkout_order_processed', [$this->checkout, 'my_checkout_order_processed']);
        }
    }

    /**
     * hook: woocommerce_get_settings_pages
     * @return array
     */
    public function add_settings_page()
    {
        $settings[] = include plugin_dir_path(__FILE__) . 'includes/admin/class-wc-settings-ecpay-invoice.php';

        return $settings;
    }

    /**
     * hook: plugin_action_links_lohasit-ecpay-invoice
     * @param  array $links
     * @return array
     */
    public function admin_edit_link($links)
    {
        return array_merge(
            [
                'settings' => '<a href="' . admin_url('admin.php?page=wc-settings&tab=lohasit_ecpay_invoice') . '">' . __('Settings') . '</a>',
            ],
            $links
        );
    }

    public function check_ecpay_invoice_response()
    {
        echo '1';
        die;
    }
}

// ini_set('display_errors', 1);
// error_reporting(-1);

if (in_array('woocommerce/woocommerce.php', get_option('active_plugins'))) {
    function lohasit_ecpay_invoice()
    {
        return Lohasit_Ecpay_Invoice::forge();
    }

    lohasit_ecpay_invoice();
}
