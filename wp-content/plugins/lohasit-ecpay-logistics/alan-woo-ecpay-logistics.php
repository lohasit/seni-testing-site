<?php

/*
Plugin Name: 樂活綠界超商取貨(B2C/C2C)(非官方版)
Description: 樂活壓板系統 - Woocommerce - 綠界超商取貨(B2C/C2C)(非官方版)
Version: 3.0.0
Author: Lohas IT (alanchang15)
Domain Path: /languages
 */

// ini_set('display_errors', 1);
// error_reporting(-1);

defined('ABSPATH') || exit;

defined('ECPAY_LOGISTICS_PLUGIN_NAME') || define('ECPAY_LOGISTICS_PLUGIN_NAME', plugin_basename(__FILE__));

function alan_woo_ecpay_logistics_deactivate()
{
    if (!function_exists('deactivate_plugins')) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    deactivate_plugins(ECPAY_LOGISTICS_PLUGIN_NAME);
}

function alan_woo_ecpay_logistics_admin_notice()
{
    printf('<div class="updated"><p>%1$s</p></div>', '請安裝啟用 WooCommerce 外掛!');

    if (isset($_GET['activate'])) {
        unset($_GET['activate']);
    }
}

function alan_woo_ecpay_logistics_dependencies()
{
    if (class_exists('WC_Shipping_Method')) {
        require_once plugin_dir_path(__FILE__) . 'includes/class-alan-woo-ecpay-logistics.php';

        function alan_woo_ecpay_logistics_plugin()
        {
            $plugin = Alan_Woo_ECPay_Logistics::get_instance();
            return $plugin;
        }

        if (!function_exists('write_log')) {
            function write_log($message, $source, $type = 'error')
            {
                $plugin = alan_woo_ecpay_logistics_plugin();
                $option = $plugin->get_shipping_option();

                if ('yes' === $option['debug_mode']) {
                    lohasit_logger($message, $source, $type);
                }
            }
        }
        alan_woo_ecpay_logistics_plugin()->run();
    } else {
        add_action('init', 'alan_woo_ecpay_logistics_deactivate');
        add_action('admin_notices', 'alan_woo_ecpay_logistics_admin_notice');
    }
}

add_action('plugins_loaded', 'alan_woo_ecpay_logistics_dependencies', 1);

add_action('wp_ajax_nopriv_save_checkout_data', 'save_checkout_data');
add_action('wp_ajax_save_checkout_data', 'save_checkout_data');

function save_checkout_data()
{
    alan_woo_ecpay_logistics_plugin()
        ->get_checkout()
        ->save_checkout_data();
}
