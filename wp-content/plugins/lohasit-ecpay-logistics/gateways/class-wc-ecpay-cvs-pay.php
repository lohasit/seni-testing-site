<?php

class WC_ECPay_CVS_Pay extends WC_Payment_Gateway
{
    public function __construct()
    {
        $this->setup_properties();

        $this->init_form_fields();
        $this->init_settings();

        $this->title       = $this->get_option('title');
        $this->description = $this->get_option('description');

        $this->icon = plugins_url('images/ecpay_icon.png', __DIR__);

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_thankyou_' . $this->id, [$this, 'thankyou_page']);
    }

    public function setup_properties()
    {
        $this->id                 = 'lohasit_ecpay_cvs_pay';
        $this->method_title       = '綠界超商取貨付款(非官方版)';
        $this->method_description = '讓您的顧客使用綠界超商取貨付款';
    }

    public function get_title()
    {
        if (is_admin()) {
            global $post;
            $store = get_post_meta($post->ID, '_ecpay_store', true);
        } else {
            $store = $_SESSION['_ecpay_store'];
        }

        $suffix = ($store) ? alan_woo_ecpay_logistics_plugin()->get_store_name($store) : '';

        return $this->title . $suffix;
    }

    public function init_form_fields()
    {
        $this->form_fields = [
            'enabled'     => [
                'title'   => __('Enable/Disable', 'woocommerce'),
                'label'   => '啟用超商取貨付款',
                'type'    => 'checkbox',
                'default' => 'no',
            ],

            'title'       => [
                'title'       => __('Title', 'woocommerce'),
                'type'        => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                'default'     => '超商取貨付款',
                'desc_tip'    => false,
            ],

            'description' => [
                'title'       => __('Description', 'woocommerce'),
                'type'        => 'textarea',
                'description' => __('This controls the description which the user sees during checkout.', 'woocommerce'),
                'default'     => '透過綠界科技使用超商取貨付款',
                'desc_tip'    => false,
            ],
        ];
    }

    public function process_payment($order_id)
    {
        $order = wc_get_order($order_id);
//
//        $option = alan_woo_ecpay_logistics_plugin()->get_shipping_option();

        // if ('admin_bulk_action' === $option['package_operation']) {
//        $order->update_status('pending');
        // }

        WC()->cart->empty_cart();

        return [
            'result'   => 'success',
            'redirect' => $this->get_return_url($order),
        ];
    }

    /*
     *  Hook: woocommerce_thankyou_
     *  20210604 產生超商付款賬單後，訂單狀態轉“保留”
     */
    public function thankyou_page($order_id)
    {
        $order = wc_get_order($order_id);
        $order->update_status('on-hold');
    }

}
