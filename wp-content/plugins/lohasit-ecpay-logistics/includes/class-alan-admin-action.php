<?php

class Alan_Admin_Action
{
    use Alan_ECPay_Shipping;

    public function add_thickbox()
    {
        global $post;

        if ($post->post_type != 'shop_order') {
            return;
        }

        add_thickbox();
        ?>
        <script>
        jQuery(window).bind('tb_unload', function () {
            location.reload();
        });

        </script>
        <?php

    }

    public function handle()
    {
        $action = $_REQUEST['action'];

        $order_id = $_REQUEST['order_id'];

        $type = $_REQUEST['type'];

        if ($action && $order_id) {
            return call_user_func_array([$this, $action], [$order_id, $type]);
        }
    }

    public function send_ecpay_cvs_order($order_id, $type = '')
    {
        $this->get_checkout()->order_status_processing($order_id);
    }

    public function print_ecpay_cvs_trade($order_id, $type = '')
    {
        $cvs_data = get_post_meta($order_id, '_ecpay_cvs_data', true);

        if (strstr($cvs_data['LogisticsSubType'], 'C2C')) {
            $type = $cvs_data['LogisticsSubType'];
            $this->batch_print_ecpay_cvs_trade($order_id, $type);
        } else {
            $data = $this->get_ecpay()->print_trade_document($cvs_data);
            $this->render('admin/print_b2c.php', $data);
        }
    }

    protected function render($template, $data = [])
    {
        $template_path = apply_filters('woocommerce_ecpay_logistics_print_template_path', plugin_dir_path(__DIR__) . 'templates/');

        return wc_get_template($template, $data, '', $template_path);
    }

    protected function c2c_single_trade($order_id)
    {
        $print_trade = get_post_meta($order_id, '_ecpay_print_trade', true);

        if (!$print_trade) {
            $cvs_data = get_post_meta($order_id, '_ecpay_cvs_data', true);
            $html     = $this->get_ecpay()->print_trade_document($cvs_data);
            $parser   = new Alan_HTML_Parser($this->get_ecpay(), $this->get_shipping_option());
            $dom      = $parser->print_label($cvs_data['LogisticsSubType'], $html);
            update_post_meta($order_id, '_ecpay_print_trade', base64_encode($dom->saveHtml()));
            update_post_meta($order_id, '_ecpay_print', 'yes');
            $this->c2c_single_trade($order_id);
        }

        $order = wc_get_order($order_id);
        $order->update_status('ecpay-picking');

        echo base64_decode($print_trade);
    }

    public function batch_print_ecpay_cvs_trade($order_id, $type = '')
    {
        $ids = explode(',', $order_id);

        if ('C2C' === $this->get_shipping_option('cvs_type')) {
            ob_start();
            foreach ($ids as $k => $order_id) {
                $order    = wc_get_order($order_id);
                $cvs_send = get_post_meta($order_id, '_ecpay_cvs_send', true);
                if ('yes' !== $cvs_send) {
                    continue;
                }
                // if ('processing' !== $order->get_status()) {
                //     continue;
                // }
                switch ($type) {
                    case 'UNIMARTC2C':
                        echo '<div style="display:inline-block;">';
                        $this->c2c_single_trade($order_id);
                        if ('yes' === $this->get_shipping_option('test_mode')) {
                            echo '<br><br><br>';
                        }
                        echo '</div><script>document.title="統一超商交貨便";</script>';
                        break;
                    case 'FAMIC2C':
                        echo '<div style="display:inline-block;">';
                        $this->c2c_single_trade($order_id);
                        echo '</div><style>img{padding-right:30px;padding-bottom:30px;}</style><script>document.title="全家店到店";</script>';
                        break;
                    case 'HILIFEC2C':
                        echo '<div style="display:inline-block;">';
                        $this->c2c_single_trade($order_id);
                        echo '</div><script>document.title="萊爾富店到店";</script>';
                        break;
                }

            }
            /*
            woocommerce_ecpay_logistics_single_trade_unimartc2c
            woocommerce_ecpay_logistics_single_trade_famic2c
            woocommerce_ecpay_logistics_single_trade_hilifec2c
             */
            $content = apply_filters('woocommerce_ecpay_logistics_single_trade_' . strtolower($type), ob_get_clean(), $order_id, $this);
            $this->render('admin/print_c2c.php', [
                'test_mode' => $this->get_shipping_option('test_mode'),
                'content'   => $content,
            ]);
        } else {
            add_filter('woocommerce_ecpay_logistics_b2c_args', function ($args) use ($order_id) {
                $ids           = explode(',', $order_id);
                $logistics_ids = [];
                foreach ($ids as $id) {
                    $cvs_data        = get_post_meta($id, '_ecpay_cvs_data', true);
                    $logistics_ids[] = $cvs_data['AllPayLogisticsID'];
                }
                $args['AllpayLogisticsID'] = implode(',', $logistics_ids);

                return $args;
            });

            $data = $this->get_ecpay()->print_trade_document();

            $this->render('admin/print_b2c.php', $data);
        }
    }

    public function query_ecpay_cvs_order($order_id, $type = '')
    {
        $cvs_data = get_post_meta($order_id, '_ecpay_cvs_data', true);

        $response = $this->get_ecpay()->query_cvs_order($order_id, $cvs_data);

        echo '<label>廠商編號:</label>' . $response['MerchantID'] . '<br>';
        echo '<label>廠商交易編號:</label>' . $response['MerchantTradeNo'] . '<br>';
        echo '<label>綠界物流交易編號:</label>' . $response['AllPayLogisticsID'] . '<br>';
        echo '<label>商品金額:</label>' . $response['GoodsAmount'] . '<br>';
        echo '<label>物流方式:</label>' . $response['LogisticsType'] . '<br>';
        echo '<label>物流費用:</label>' . $response['HandlingCharge'] . '<br>';
        echo '<label>訂單成立時間:</label>' . $response['TradeDate'] . '<br>';
        echo '<label>物流狀態:</label>' . $response['LogisticsStatus'] . '<br>';
        echo '<label>商品名稱:</label>' . $response['GoodsName'] . '<br>';
        echo '<label>配送編號:</label>' . $response['ShipmentNo'] . '<br>';
    }
}
