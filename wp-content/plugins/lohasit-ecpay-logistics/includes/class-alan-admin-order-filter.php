<?php

class Alan_Admin_Order_Filter
{
    use Alan_ECPay_Shipping;

    public function display_ecpay_cvs_dropdown()
    {
        /*
         * 20210605 判斷訂單列表才顯示超商取貨方式的篩選
         */
        if ($_GET['post_type'] == 'shop_order') {

            global $wpdb;

            $options = ['' => '依超商業者篩選'];
            $options = array_merge($options, $this->get_active_ecpay_cvs_methods());

            if (!empty($_REQUEST['_ecpay_cvs_sub_type']) && $_REQUEST['_ecpay_cvs_sub_type'] != '') {
                $selected_option_value = $_REQUEST['_ecpay_cvs_sub_type'];
            } else {
                $selected_option_value = '';
            }

            $select_field_html = '<select name="_ecpay_cvs_sub_type" id="ecpay_cvs_sub_type">';
            foreach ($options as $option_value => $option_name) {

                if ($selected_option_value == $option_value) {
                    $selected = ' selected="selected"';
                } else {
                    $selected = '';
                }

                $select_field_html .= '<option value="' . $option_value . '"' . $selected . '>' . $option_name . '</option>';
            }

            $select_field_html .= '</select>';

            echo $select_field_html;
        }

    }

    public function get_active_ecpay_cvs_methods()
    {
        $option    = $this->get_shipping_option();
        $active_store = $option['shipping_store'];
        $logistics = $option['logistics'];
        $active_logistics = [];

        foreach ($active_store as $k => $code) {
            if (array_key_exists($code, $logistics)) {
                $active_logistics[$code] = $logistics[$code];
            }
        }

        if ($option['cvs_type'] == 'C2C') {
            $c2c = [];
            foreach ($active_logistics as $code => $name) {
                $c2c[$code . 'C2C'] = $name . 'C2C';
            }
            $logistics = $c2c;
        }

        return $logistics;
    }

    public function ecpay_cvs_filter($query)
    {
        $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : '';

        if ('shop_order' === $post_type && isset($_REQUEST['_ecpay_cvs_sub_type'])) {
            $query->query_vars['meta_key']   = '_ecpay_cvs_sub_type';
            $query->query_vars['meta_value'] = $_REQUEST['_ecpay_cvs_sub_type'];
        }
    }
}
