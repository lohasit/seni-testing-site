<?php

class Alan_Admin_Order
{
    use Alan_ECPay_Shipping;

    public function after_shipping_address($order)
    {
        if ($this->shipping_id != $this->get_shipping_method_id($order)) {
            return;
        }

        $store    = get_post_meta($order->get_id(), '_ecpay_store', true);
        $cvs_send = get_post_meta($order->get_id(), '_ecpay_cvs_send', true);
        ?>
        <br class="clear" />
        <h4>綠界超商取貨</h4>
        <div class="address">
            <?php $this->display_store_info($store);?>
        </div>
        <?php

    }

    public function order_item_shipping($value, $item)
    {
        if ($this->shipping_id != $this->get_shipping_method_id($item->get_order())) {
            return $value;
        }

        $order_id = $item->get_order_id();
        $store    = get_post_meta($order_id, '_ecpay_store', true);
        $suffix   = ($store) ? $this->get_store_name($store) : '';

        if (strstr($value, $suffix)) {
            return $value;
        }

        return $value . $suffix;
    }

    public function register_order_status()
    {
        register_post_status('wc-ecpay-picking', array(
            'label'                     => '揀貨中',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop('揀貨中 <span class="count">(%s)</span>', '揀貨中 <span class="count">(%s)</span>'),
        ));

        register_post_status('wc-ecpay-delivered', array(
            'label'                     => '已出貨',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop('已出貨 <span class="count">(%s)</span>', '送達門市 <span class="count">(%s)</span>'),
        ));
    }

    public function order_statuses($order_statuses = [])
    {
        if (!$order_statuses && function_exists('wc_order_status_manager_get_order_status_posts')) {
            $order_status_posts = wc_order_status_manager_get_order_status_posts();

            $filtered_statuses = array();

            foreach ($order_status_posts as $status) {
                $filtered_statuses['wc-' . $status->post_name] = $status->post_title;
            }

            $order_statuses = $filtered_statuses;
        }

        $new_order_statuses = [];

        // add new order status after processing
        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
            if ('wc-processing' === $key) {
                $new_order_statuses['wc-ecpay-delivered'] = '已出貨';
                $new_order_statuses['wc-ecpay-picking']   = '揀貨中';
            }
        }

        return (array) $order_statuses + (array) $new_order_statuses;
    }

    public function ecpay_cvs_add_column($columns = [])
    {
        $columns['ecpay_cvs_column'] = '超商物流單號';

        return $columns;
    }

    public function ecpay_cvs_add_column_action($column)
    {
        if ('ecpay_cvs_column' === $column) {
            global $post;
            $order    = wc_get_order($post->ID);
            $shipping = $order->get_shipping_methods();
            $shipping = array_pop($shipping);
            // [20220127] 判斷$shipping是否是空值
            if ( !empty($shipping) && 'lohasit_ecpay_cvs' === $shipping->get_method_id()) {
                $this->render_button($order);
            }
        }
    }

    protected function render_button($order)
    {
        $store    = get_post_meta($order->get_id(), '_ecpay_store', true);
        $cvs_send = get_post_meta($order->get_id(), '_ecpay_cvs_send', true);
        ?>
        <?php if ('yes' === $cvs_send): ?>
            <a class="button button-primary print-trade" href="<?php echo $this->print_trade_url($order->get_id()); ?>&TB_iframe=true&width=900&height=800">列印寄件單</a>
        <?php else: ?>
            <a class="button button-primary send-order" href="<?php echo $this->send_order_url($order->get_id()); ?>&TB_iframe=true&width=900&height=800">傳送物流訂單</a>
        <?php endif;

    }

    public function footer()
    {
        if (isset($_REQUEST['_ecpay_cvs_sub_type']) && !empty($_REQUEST['_ecpay_cvs_sub_type'])):
            $bulk_actions['batch_print_ecpay_cvs_trade'] = '列印超商取貨寄件單';
            $bulk_actions                                = apply_filters('woocommerce_ecpay_cvs_bulk_actions', $bulk_actions);

            $url = admin_url('admin.php?action=batch_print_ecpay_cvs_trade&type=' . $_REQUEST['_ecpay_cvs_sub_type'] . '&order_id=');
            ?>

	<script>
	jQuery(function($){
	    // 清空select選項
	    $("select[name='action'], select[name='action2']").empty();
	    <?php foreach ($bulk_actions as $action => $title): ?>
	    $('<option>').val('<?php echo $action; ?>').html('<?php echo esc_attr($title); ?>').appendTo("select[name='action'], select[name='action2']");
	    <?php endforeach;?>

            $('.action').addClass('ecpay-action');

            $('.bulkactions').delegate('.ecpay-action', 'click', function (event) {
                event.preventDefault();
                var actionselected = $(this).attr("id").substr(2);
                var action = $('select[name="' + actionselected + '"]').val();
                if (action == 'batch_print_ecpay_cvs_trade') {
                    var template = action;
                    var checked = [];
                    $('tbody th.check-column input[type="checkbox"]:checked').each(
                        function() {
                            checked.push($(this).val());
                        }
                    );

                    if (!checked.length) {
                        alert('請先勾選訂單!');
                        return;
                    }

                    var order_ids = checked.join(',');
                    var url = '<?php echo $url; ?>' + order_ids + '&TB_iframe=true&width=900&height=800';
                    tb_show('批次列印寄件單', url);

                    jQuery(window).bind('tb_unload', function () {
                        location.reload();
                    });
                    // window.open('<?php echo $url; ?>' + order_ids,'_blank');
                }
            });
        });
        </script>
        <?php endif;?>
        <script>
        jQuery(function($) {
            $('.print-trade, .send-order').on('click', function(e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var title = $(this).hasClass('print-trade')
                ? '列印寄件單' : '傳送物流訂單';
                tb_show(title, url);

                jQuery(window).bind('tb_unload', function () {
                    location.reload();
                });
            });
        });
        </script>
        <?php

    }
}
