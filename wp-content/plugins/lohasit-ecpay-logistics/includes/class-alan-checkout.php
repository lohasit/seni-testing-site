<?php

class Alan_Checkout
{
    use Alan_ECPay_Shipping;

    public function add_ecpay_map()
    {
        $option         = $this->get_shipping_option();
        $chosen_methods = WC()->session->get('chosen_shipping_methods');
        $suffix         = ('C2C' === $option['cvs_type']) ? 'C2C' : '';
        $store          = $_SESSION['_ecpay_store'];
        $logistics      = apply_filters('woocommerce_ecpay_logistics', $option['logistics']);
        ?>
        <?php
if (in_array($this->shipping_id, $chosen_methods) && $option['shipping_store']): ?>
        <tr class="shipping_option">
            <th colspan="2">超商業者</th>
            <td>
                <select name="shipping_option" class="input-select" id="shipping_option">
                    <option value="">--請選擇--</option>
                    <?php foreach ($logistics as $code => $name): ?>
                    <?php if (in_array($code, $option['shipping_store'])): ?>
                    <?php $selected = ($store && $store['LogisticsSubType'] == $code . $suffix) ? 'selected' : '';?>
                    <option value="<?php echo $code . $suffix; ?>" <?php echo $selected; ?>><?php echo $name; ?></option>
                    <?php endif;?>
                    <?php endforeach;?>
                </select>
            </td>
        </tr>
        <?php do_action('woocommerce_ecpay_logistics_before_map_form');?>
        <tr class="shipping_option">
            <td colspan="3">
                <?php $this->generate_map_form();?>
            </td>
        </tr>
        <?php do_action('woocommerce_ecpay_logistics_after_map_form');?>
        <script>
        jQuery(function($){
            $('#shipping_option').each(function(){
                $('#LogisticsSubType').val($('#shipping_option option:selected').val());
            });
            $('#shipping_option').on('change', function(){
                $('#LogisticsSubType').val($(this).val());
            });
            $('#ecpay_map').on('click', function(){
                var sub_type = $('#shipping_option option:selected').val();
                var checkout_data = $('form[name="checkout"]').serialize();

                if (sub_type == '') {
                    alert('請選擇超商業者');
                    return false;
                }

                $.ajax({
                    url: '<?php echo admin_url("admin-ajax.php"); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'save_checkout_data',
                        data: checkout_data,
                    },
                    success: function(resp){
                        console.log(resp);
                    },
                });
            });
        });
        </script>
        <?php endif;

    }

    public function save_checkout_data()
    {
        parse_str($_POST['data'], $checkout_data);


        WC()->session->set('checkout_data', $checkout_data);

        wp_send_json(['status' => true]);
    }

    public function get_checkout_value($input, $key)
    {
        $checkout_data = WC()->session->get('checkout_data');

        if (isset($checkout_data[$key])) {
            return $checkout_data[$key];
        }
    }

    public function map_button_text($text)
    {
        if (isset($_SESSION['_ecpay_store'])) {
            $text = '重選門市';
        }

        return $text;
    }

    protected function generate_map_form()
    {
        $ecpay = $this->get_ecpay();

        $ecpay->cvs_map('選取門市');

        $this->display_store_info($_SESSION['_ecpay_store']);
    }

    public function save_store_info()
    {
        /*
        Array
        (
        [MerchantID] => 2000933
        [MerchantTradeNo] => ECPay
        [LogisticsSubType] => FAMIC2C
        [CVSStoreID] => 001779
        [CVSStoreName] => 工業店
        [CVSAddress] => 台北市南港區三重路１９之４號
        [CVSTelephone] => 02-24326001
        [CVSOutSide] =>
        [ExtraData] =>
        )
         */

        if ($_POST) {
            $_SESSION['_ecpay_store'] = $_POST;
            wp_redirect(wc_get_checkout_url());
        }

        die;
    }

    public function validate_map_result()
    {
        $chosen_methods = WC()->session->get('chosen_shipping_methods');

        if (!in_array($this->shipping_id, $chosen_methods)) {
            return;
        }

        $store = $_SESSION['_ecpay_store'];

        if (!$store) {
            wc_add_notice('請選擇取貨超商門市', 'error');
        } else {
            WC()->session->set('_ecpay_store', $store);
        }

        $name = trim($_POST['billing_last_name']) . trim($_POST['billing_first_name']);
        // 含有中文
        if (preg_match("/[^x00-x80]/", $name)
            && (mb_strlen($name, 'UTF-8') < 2 || mb_strlen($name, 'UTF-8') > 5)
        ) {
            wc_add_notice('姓名請輸入最少兩個中文字, 最多五個中文字', 'error');
        } elseif (strlen($name) < 4 || strlen($name) > 10) {
            wc_add_notice('姓名請輸入最少4個字元, 最多10個字元', 'error');
        }
    }

    // hook: woocommerce_checkout_order_processed
    public function checkout_order_processed($order_id)
    {
        $order = wc_get_order($order_id);

        if ($this->shipping_id == $this->get_shipping_method_id($order)) {
            $store = $_SESSION['_ecpay_store'];
            update_post_meta($order_id, '_ecpay_store', $store);
            update_post_meta($order_id, '_shipping_city', '');
            update_post_meta($order_id, '_shipping_state', '');
            update_post_meta($order_id, '_shipping_postcode', '');
            update_post_meta($order_id, '_shipping_company', $this->get_store_name($store, false));
            update_post_meta($order_id, '_shipping_address_1', $store['CVSAddress']);
            update_post_meta($order_id, '_shipping_address_2', $store['CVSStoreName']);
            unset($_SESSION['_ecpay_store']);

            // 超商取貨付款
            if ($this->payment_id === $order->get_payment_method()) {
                if ('checkout_processed' === $this->get_shipping_option('package_operation')) {
                    wp_schedule_single_event(time() + 30, 'send_ecpay_order', [$order_id]);
                }
            }
        }
    }

    // hook: woocommerce_order_status_processing
    public function order_status_processing($order_id)
    {
        ignore_user_abort(true);
        set_time_limit(0);

        try {
            $cvs_send = get_post_meta($order_id, '_ecpay_cvs_send', true);

            $order = wc_get_order($order_id);

            if ('yes' !== $cvs_send) {
                $response = $this->run_package_request($order_id);
                if ($response->status == '1') {
                    update_post_meta($order_id, '_ecpay_cvs_data', $response->result);
                    update_post_meta($order_id, '_ecpay_cvs_send', 'yes');
                    update_post_meta($order_id, '_ecpay_cvs_sub_type', $response->result['LogisticsSubType']);

                    $cvs_data = $response->result;
                    if (substr($cvs_data['LogisticsSubType'], 'C2C')) {
                        $html   = $this->get_ecpay()->print_trade_document($cvs_data);
                        $parser = new Alan_HTML_Parser($this->get_ecpay(), $this->get_shipping_option());
                        $dom    = $parser->print_label($cvs_data['LogisticsSubType'], $html);
                        update_post_meta($order_id, '_ecpay_print_trade', base64_encode($dom->saveHtml()));
                    }

                    if ('processing' !== $order->get_status()) {
                        $order->update_status('processing');
                    }

                    $order->add_order_note('綠界物流編號: ' . $response->result['AllPayLogisticsID']);

                    if ($cvs_data['LogisticsSubType'] == 'UNIMARTC2C') {
                        // $note = '可至統一超商ibon列印服務單，或透過API列印服務單。';
                        $note = '超商取貨單號: ' . $cvs_data['CVSPaymentNo'] . $cvs_data['CVSValidationNo'];
                        $order->add_order_note($note, true);
                    } else {
                        $note = '超商取貨單號: ' . $cvs_data['CVSPaymentNo'];
                        $order->add_order_note($note, true);
                    }

                    // echo 'OK';
                    echo '<script>window.parent.tb_remove();</script>';
                } else {
                    throw new Exception($response->message);
                }
            } else {
                ob_clean();
                ob_start();
                echo '<center>已經送過訂單</center>';
                echo '<pre>';
                print_r(get_post_meta($order_id, '_ecpay_cvs_data', true));
                echo '</pre>';
                echo ob_get_clean();
            }

        } catch (Exception $e) {
            write_log(
                $e->getMessage,
                'lohasit-ecpay-logistics'
            );
            $order->add_order_note($e->getMessage());
            echo $e->getMessage();
        }
    }

    protected function run_package_request($order_id)
    {
        $order    = wc_get_order($order_id);
        $store    = get_post_meta($order_id, '_ecpay_store', true);
        $settings = $this->get_shipping_option();
        $ecpay    = $this->get_ecpay();

        $is_collection      = ($this->payment_id === $order->get_payment_method()) ? 'Y' : 'N';
        $colletction_amount = ('Y' === $is_collection) ? intval($order->get_total()) : '';
        $goods_amount       = intval($order->get_total());
        $trade_date         = date('Y/m/d H:i:s', current_time('timestamp'));

        $args = [
            'MerchantID'           => $settings['merchant_id'],
            'MerchantTradeNo'      => $this->get_order_number($order),
            'MerchantTradeDate'    => $trade_date,
            'LogisticsType'        => 'CVS',
            'LogisticsSubType'     => $store['LogisticsSubType'],
            'GoodsAmount'          => $goods_amount,
            'CollectionAmount'     => $colletction_amount,
            'IsCollection'         => $is_collection,
            'GoodsName'            => '網路商品',
            'SenderName'           => $settings['sender_name'],
            'SenderPhone'          => '',
            'SenderCellPhone'      => $settings['sender_cellphone'],
            'ReceiverName'         => trim($order->billing_last_name) . trim($order->billing_first_name),
            'ReceiverPhone'        => '',
            'ReceiverCellPhone'    => $order->billing_phone,
            'ReceiverEmail'        => $order->billing_email,
            'TradeDesc'            => '',
            'ServerReplyURL'       => WC()->api_request_url('ecpay_response'),
            'ClientReplyURL'       => '',
            'LogisticsC2CReplyURL' => WC()->api_request_url('ecpay_response'),
            'Remark'               => '',
            'PlatformID'           => '',
            'ReceiverStoreID'      => $store['CVSStoreID'],
            'ReturnStoreID'        => '', // (逆物流)退貨用的商店編號
        ];

        $args['CheckMacValue'] = $ecpay->create_check_code($args);

        $str = $ecpay->send_cvs_order($args);

        $response            = new stdClass;
        list($status, $data) = explode('|', $str);
        $response->status    = $status;

        if ($status == '1') {
            parse_str($data, $result);
            $response->result  = $result;
            $response->message = 'OK';
            // $note              = '物流狀態: ' . $result['RtnCode'] . ' ' . $result['RtnMsg'];
            // $order->add_order_note($note);
        } else {
            $response->message = $data;
        }

        return $response;
    }

    /**
     * hook: woocommerce_after_checkout
     * 20210602 結帳頁從超商選項返回時，判斷未登入用戶是否曾勾選[建立帳號] by JJ
     */
    public function checkout_checkbox_createaccount()
    {
        $checkout_data = WC()->session->get('checkout_data');

        if  (isset($checkout_data['createaccount'] ) && $checkout_data['createaccount'] == 1) {
            ?>
            <script>
                jQuery(function($){
                    $("#createaccount").prop("checked", true);
                });
            </script>
            <?php
        }
    }

}
