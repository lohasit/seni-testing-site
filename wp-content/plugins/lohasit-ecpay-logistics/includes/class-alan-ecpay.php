<?php

class Alan_ECPay
{
    protected $map_url;

    protected $create_url;

    protected $unimart_print_url;

    protected $fami_print_url;

    protected $hilife_print_url;

    protected $b2c_print_url;

    protected $query_url;

    use Alan_ECPay_Shipping;

    public function setup_properties()
    {
        if ('yes' === $this->get_shipping_option('test_mode')) {
            $this->map_url           = 'https://logistics-stage.ecpay.com.tw/Express/map';
            $this->create_url        = 'https://logistics-stage.ecpay.com.tw/Express/Create';
            $this->unimart_print_url = 'https://logistics-stage.ecpay.com.tw/Express/PrintUniMartC2COrderInfo';
            $this->fami_print_url    = 'https://logistics-stage.ecpay.com.tw/Express/PrintFAMIC2COrderInfo';
            $this->hilife_print_url  = 'https://logistics-stage.ecpay.com.tw/Express/PrintHILIFEC2COrderInfo';
            $this->b2c_print_url     = 'https://logistics-stage.ecpay.com.tw/helper/printTradeDocument';
            $this->query_url         = 'https://logistics-stage.ecpay.com.tw/Helper/QueryLogisticsTradeInfo/V2';
        } else {
            $this->map_url           = 'https://logistics.ecpay.com.tw/Express/map';
            $this->create_url        = 'https://logistics.ecpay.com.tw/Express/Create';
            $this->unimart_print_url = 'https://logistics.ecpay.com.tw/Express/PrintUniMartC2COrderInfo';
            $this->fami_print_url    = 'https://logistics.ecpay.com.tw/Express/PrintFAMIC2COrderInfo';
            $this->hilife_print_url  = 'https://logistics.ecpay.com.tw/Express/PrintHILIFEC2COrderInfo';
            $this->b2c_print_url     = 'https://logistics.ecpay.com.tw/helper/printTradeDocument';
            $this->query_url         = 'https://logistics.ecpay.com.tw/Helper/QueryLogisticsTradeInfo/V2';
        }

        return $this;
    }

    public function server_post($url, $data = [])
    {
        set_time_limit(0);

        $args = [
            'method'      => 'POST',
            'timeout'     => 90,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => [],
            'body'        => $data,
            'cookies'     => [],
        ];

        $response = wp_remote_post($url, $args);

        if (is_wp_error($response)) {
            error_log('alan-woo-ecpay-logistics error: ' . $response->get_error_message());
            throw new Exception($response->get_error_message());
        }

        return $response['body'];
    }

    public function cvs_map($button_text = '選取門市')
    {
        $button_text = apply_filters('woocommerce_ecpay_logistics_cvs_button_text', $button_text);

        $device = (wp_is_mobile()) ? 1 : 0;

        $args = apply_filters('woocommerce_ecpay_logistics_map_args', [
            'MerchantID'     => $this->get_shipping_option('merchant_id'),
            'LogisticsType'  => 'CVS',
            // 'ServerReplyURL' => wc_get_checkout_url(),
            'ServerReplyURL' => WC()->api_request_url('receive_map_data'),
            'Device'         => $device,
        ]);

        ?>
        <form id="map" action="<?php echo $this->map_url; ?>" method="POST">
            <input type="hidden" name="LogisticsSubType" id="LogisticsSubType">
            <?php foreach ($args as $name => $value): ?>
            <input type="hidden" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo $value; ?>">
            <?php endforeach;?>
            <input id="ecpay_map" type="submit" class="btn alt" style="width:100%;" value="<?php echo $button_text; ?>">
        </form>
<!--        [20220127] 修改警示訊息顏色，附加class可彈性調整-->
<!--        --><?php //echo apply_filters('woocommerce_ecpay_logistics_help_html', '<p class="woocommerce_ecpay_logistics_help_html" style="color: #c9302c;">*聯絡電話請填寫手機號碼</p>'); ?>
        <?php echo apply_filters('woocommerce_ecpay_logistics_help_html', '<p class="woocommerce_ecpay_logistics_help_html">*聯絡電話請填寫手機號碼</p>'); ?>
        <?php

    }

    public function create_check_code($post, $settings = '')
    {
        if (empty($settings)) {
            $settings = $this->get_shipping_option();
        }

        if (isset($post['CheckMacValue'])) {
            unset($post['CheckMacValue']);
        }

        // ksort($post);
        uksort($post, function ($a, $b) {
            return strcasecmp($a, $b);
        });

        $hash_raw_data  = 'HashKey=' . $settings['hash_key'] . '&' . urldecode(http_build_query($post)) . '&HashIV=' . $settings['hash_iv'];
        $urlencode_data = strtolower(urlencode($hash_raw_data));

        $urlencode_data = str_replace('%2d', '-', $urlencode_data);
        $urlencode_data = str_replace('%5f', '_', $urlencode_data);
        $urlencode_data = str_replace('%2e', '.', $urlencode_data);
        $urlencode_data = str_replace('%21', '!', $urlencode_data);
        $urlencode_data = str_replace('%2a', '*', $urlencode_data);
        $urlencode_data = str_replace('%28', '(', $urlencode_data);
        $urlencode_data = str_replace('%29', ')', $urlencode_data);

        return strtoupper(md5($urlencode_data));
    }

    public function check_value($post)
    {
        $settings = $this->get_shipping_option();

        $MyCheckMacValue = $this->create_check_code($post, $settings);

        if ($MyCheckMacValue == $post['CheckMacValue']) {
            return true;
        }

        return false;
    }

    public function send_cvs_order($paramters = [])
    {
        $paramters = apply_filters('woocommerce_ecpay_logistics_create_order_args', $paramters);
        $response  = $this->server_post($this->create_url, $paramters);

        return $response;
    }

    public function print_trade_document($cvs_data = [])
    {
        $is_b2c = false;

        $paramters = [
            'AllpayLogisticsID' => $cvs_data['AllPayLogisticsID'],
            'CVSPaymentNo'      => $cvs_data['CVSPaymentNo'],
            'MerchantID'        => $this->get_shipping_option('merchant_id'),
            'PlatformID'        => '',
        ];

        if ($cvs_data['LogisticsSubType'] == 'UNIMARTC2C') {
            $paramters['CVSValidationNo'] = $cvs_data['CVSValidationNo'];
        }

        if (strstr($cvs_data['LogisticsSubType'], 'C2C')) {
            $sub_type  = str_replace('C2C', '', $cvs_data['LogisticsSubType']);
            $sub_type  = strtolower($sub_type);
            $url       = $this->{$sub_type . '_print_url'};
            $paramters = apply_filters('woocommerce_ecpay_logistics_c2c_args', $paramters);
        } else {
            $is_b2c = true;
            unset($paramters['CVSPaymentNo']);
            $url       = $this->b2c_print_url;
            $paramters = apply_filters('woocommerce_ecpay_logistics_b2c_args', $paramters);
        }

        $paramters['CheckMacValue'] = $this->create_check_code($paramters);

        if ($is_b2c) {
            unset($paramters['LogisticsSubType']);
        }

        try {
            if (true === $is_b2c) {
                $data['url']       = $url;
                $data['paramters'] = $paramters;
                return $data;
            } else {
                $html = $this->server_post($url, $paramters);
                return $html;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function query_cvs_order($order_id, $cvs_data = [])
    {
        try {
            $paramters = [
                'MerchantID'        => $this->get_shipping_option('merchant_id'),
                'AllPayLogisticsID' => $cvs_data['AllPayLogisticsID'],
                'TimeStamp'         => current_time('timestamp'),
                'PlatformID'        => '',
            ];

            $paramters['CheckMacValue'] = $this->create_check_code($paramters);

            $query = $this->server_post($this->query_url, $paramters);

            parse_str($query, $response);

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
