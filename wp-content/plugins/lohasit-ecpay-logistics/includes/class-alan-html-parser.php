<?php

class Alan_HTML_Parser
{
    protected $client;

    protected $option;

    public function __construct($client, $option)
    {
        $this->client = $client;

        $this->option = $option;
    }

    public function print_label($sub_type, $html)
    {
        if (strstr($sub_type, 'C2C')) {
            $method = str_replace('c2c', '', strtolower($sub_type)) . '_c2c';
        }

        return call_user_func_array([$this, $method], [$html]);
    }

    public function unimart_c2c($html)
    {
        $base_url = ('yes' === $this->option['test_mode'])
        ? 'http://61.57.231.104/C2C/C2CWeb/'
        : 'https://epayment.7-11.com.tw/C2C/C2CWeb/';

        $dom  = $this->dom_document($html);
        $form = $dom->getElementById('PostForm');
        $url  = $form->getAttribute('action');

        $args = $this->get_form_args($dom);
        $next = $this->client->server_post($url, $args);

        $dom  = $this->dom_document($next);
        $form = $dom->getElementById('transF');
        $url  = $form->getAttribute('action');

        $args = $this->get_form_args($dom);
        $next = $this->client->server_post($url, $args);

        $url = $base_url . 'C2C.aspx';

        $dom = $this->dom_document($next);

        $args = $this->get_form_args($dom);
        $next = $this->client->server_post($url, $args);

        $dom         = $this->dom_document($next);
        $xp          = new DOMXpath($dom);
        $nodes       = $xp->query('//input[@name="AjaxString"]');
        $ajax_string = $nodes->item(0)->getAttribute('value');

        parse_str($ajax_string, $args);

        $pin_code_number = $args['paymentno'] . $args['validationno'];

        $url = $base_url . 'PrintC2CPinCode.aspx';

        $label = $this->client->server_post($url, ['PinCodeNumber' => $pin_code_number]);

        $body = str_replace('</head>', '<base href="' . $base_url . '"></head>', $label);

        if ('yes' === $this->option['test_mode']) {
            $body = str_replace('https', 'http', $body);
        }

        return $this->dom_document($body);
    }

    public function fami_c2c($html)
    {
        $dom   = $this->dom_document($html);
        $xp    = new DOMXpath($dom);
        $nodes = $xp->query('//img');

        return $this->dom_document('<img src="' . $nodes->item(0)->getAttribute('src') . '" style="width:100%;">');
    }

    public function hilife_c2c($html)
    {
        $base_url = ('yes' === $this->option['test_mode'])
        ? 'http://ecServiceDev.hilife.com.tw/'
        : 'http://ecService.hilife.com.tw/';

        $dom  = $this->dom_document($html);
        $form = $dom->getElementById('PostForm');
        $url  = $form->getAttribute('action');

        $body = $this->client->server_post($url, []);

        $body = str_replace(
            ['</head>', 'big5'],
            ['<base href="' . $base_url . '"></head>', 'utf-8'],
            $body
        );

        return $this->dom_document(mb_convert_encoding($body, 'UTF-8'));
    }

    protected function dom_document($html)
    {
        $dom = new DomDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $html);

        return $dom;
    }

    protected function get_form_args($dom)
    {
        $args  = [];
        $xp    = new DOMXpath($dom);
        $nodes = $xp->query('//input');

        foreach ($nodes as $node) {
            $args[$node->getAttribute('name')] = $node->getAttribute('value');
        }

        return $args;
    }
}
