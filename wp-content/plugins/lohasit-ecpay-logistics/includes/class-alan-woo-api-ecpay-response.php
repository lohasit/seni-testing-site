<?php

class Alan_Woo_API_ECPay_Response
{
    use Alan_ECPay_Shipping;

    public function check_response()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            die('0|ERROR');
        }

        try {

            $response = $_POST;
            $order_id   = $this->get_order_id($response);
            $infomation = print_r($response, true);
            write_log(
                '#' . $order_id . ' ecpay cvs response: ' . $infomation,
                'lohasit-ecpay-logistics',
                'info'
            );
            $order = wc_get_order($order_id);

            do_action('woocommerce_ecpay_logistics_receive_response', $response, $order);

            // 買家已到店取貨
            if (in_array($response['RtnCode'], ['2067', '3022'])) {
                if ($this->payment_id === $order->get_payment_method()) {
                    // // 綠界電子發票自動開立(官方外掛)
                    // if (get_option('wc_ecpayinvoice_active_model') && has_action('ecpay_auto_invoice')) {
                    //     do_action('ecpay_auto_invoice', $order_id);
                    // }
                    // // 歐付寶電子發票自動開立(官方外掛)
                    // if (get_option('wc_allpayinvoice_active_model') && has_action('allpay_auto_invoice')) {
                    //     do_action('allpay_auto_invoice', $order_id);
                    // }
                    // 超商取貨付款已取貨鉤子
                    do_action('woocommerce_ecpay_logistics_receive_response_cvs_pay', $order_id);
                } else {
                    // 超商取貨已取貨鉤子
                    do_action('woocommerce_ecpay_logistics_receive_response_cvs', $order_id);
                }
                $order->update_status('completed', '物流狀態: ' . $response['RtnCode'] . ' 買家已到店取貨');
            } else {
                $order->add_order_note('物流狀態: ' . $response['RtnCode'] . ' ' . $response['RtnMsg']);
                // 門市收到寄件商品
                if (in_array($response['RtnCode'], ['2068', '2063', '2073', '3018'])) {
                    $order->update_status('ecpay-delivered');
                    $order->add_order_note($response['RtnMsg'], true);
                }

            }

            /*
             * 20210604 超商未取貨的訂單，自動更新訂單狀態為“取消”，補充庫存；
             */
            $cancel_code = ['3020', '3021', '2046', '2074', '2076', '2078',  '2079', '2080', '2081', '2082', '2083', '2084', '2085', '2086', '2087', '2088', '2089', '2092', '2093'];

            if ( in_array($response['RtnCode'], $cancel_code ) && $order->get_status() !== 'cancelled' ) {
                    $order->update_status('cancelled');
            }

            die('1|OK');
        } catch (Exception $e) {
            die('0|' . $e->getMessage());
        }
    }
}
