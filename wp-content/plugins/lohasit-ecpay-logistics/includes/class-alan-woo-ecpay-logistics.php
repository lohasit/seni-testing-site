<?php

defined('ABSPATH') || exit;

class Alan_Woo_ECPay_Logistics
{
    private static $_instance = null;

    protected $loader;

    protected $plugin_name;

    protected $version;

    protected $shipping_id;

    protected $payment_id;

    protected $ecpay;

    protected $checkout;

    public static function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __construct()
    {
        $this->setup_properties();

        $this->load_dependencies();

        if (is_admin()) {
            $this->define_admin_hooks();
        } else {
            $this->define_public_hooks();
        }

        $this->define_shipping_hooks();
        $this->define_payment_hooks();
    }

    public function setup_properties()
    {
        $data = get_file_data(
            plugin_dir_path(dirname(__DIR__)) . ECPAY_LOGISTICS_PLUGIN_NAME,
            ['Version', 'Text Domain'],
            ECPAY_LOGISTICS_PLUGIN_NAME
        );

        $this->version = $data[0];

        $this->textdomain = $data[1];

        $this->plugin_name = ECPAY_LOGISTICS_PLUGIN_NAME;

        $this->shipping_id = 'lohasit_ecpay_cvs';

        $this->payment_id = 'lohasit_ecpay_cvs_pay';
    }

    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    public function get_loader()
    {
        return $this->loader;
    }

    public function get_version()
    {
        return $this->version;
    }

    public function get_ecpay()
    {
        return $this->ecpay->setup_properties();
    }

    public function get_checkout()
    {
        return $this->checkout;
    }

    public function run()
    {
        $this->loader->run();
    }

    private function load_dependencies()
    {
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-wp-loader.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-html-parser.php';
        require_once plugin_dir_path(__DIR__) . 'includes/trait-alan-ecpay-shipping.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-checkout.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-admin-order.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-admin-action.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-admin-order-filter.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-ecpay.php';
        require_once plugin_dir_path(__DIR__) . 'includes/class-alan-woo-api-ecpay-response.php';
        require_once plugin_dir_path(__DIR__) . 'gateways/class-wc-ecpay-cvs-pay.php';

        $this->loader = new Alan_WP_Loader;

        $this->checkout = new Alan_Checkout($this->shipping_id);

        $this->ecpay = new Alan_ECPay($this->shipping_id);
    }

    private function define_admin_hooks()
    {
        $this->loader->add_filter('plugin_action_links_' . $this->plugin_name, $this, 'admin_edit_link');

        $admin_order = new Alan_Admin_Order($this->shipping_id);
        $this->loader->add_action('woocommerce_admin_order_data_after_shipping_address', $admin_order, 'after_shipping_address');
        $this->loader->add_filter('woocommerce_order_item_get_method_title', $admin_order, 'order_item_shipping', 10, 2);
        $this->loader->add_action('admin_footer', $admin_order, 'footer');
        // 訂單列表新增欄位 超商物流單號
        $this->loader->add_filter('manage_edit-shop_order_columns', $admin_order, 'ecpay_cvs_add_column', 1);
        $this->loader->add_action('manage_shop_order_posts_custom_column', $admin_order, 'ecpay_cvs_add_column_action', 1);

        $admin_action = new Alan_Admin_Action($this->shipping_id);
        // $this->loader->add_action('admin_footer', $admin_action, 'add_thicbox');
        $this->loader->add_action('admin_action_send_ecpay_cvs_order', $admin_action, 'handle');
        $this->loader->add_action('admin_action_query_ecpay_cvs_order', $admin_action, 'handle');
        $this->loader->add_action('admin_action_print_ecpay_cvs_trade', $admin_action, 'handle');
        $this->loader->add_action('admin_action_batch_print_ecpay_cvs_trade', $admin_action, 'handle');

        $admin_filter = new Alan_Admin_Order_Filter($this->shipping_id);

        $this->loader->add_action('restrict_manage_posts', $admin_filter, 'display_ecpay_cvs_dropdown');
        $this->loader->add_filter('parse_query', $admin_filter, 'ecpay_cvs_filter');
    }

    private function define_public_hooks()
    {
        $this->loader->add_action('woocommerce_review_order_after_shipping', $this->checkout, 'add_ecpay_map');
        $this->loader->add_action('woocommerce_api_receive_map_data', $this->checkout, 'save_store_info');
        $this->loader->add_action('woocommerce_after_checkout_validation', $this->checkout, 'validate_map_result');
        $this->loader->add_action('woocommerce_checkout_order_processed', $this->checkout, 'checkout_order_processed');
        $this->loader->add_filter('woocommerce_ecpay_logistics_cvs_button_text', $this->checkout, 'map_button_text');
        $this->loader->add_filter('woocommerce_checkout_get_value', $this->checkout, 'get_checkout_value', 10, 2);
        $this->loader->add_filter('woocommerce_available_payment_gateways', $this, 'remove_payment_cod');
        $this->loader->add_action('woocommerce_after_checkout_form', $this->checkout, 'checkout_checkbox_createaccount', 10);
    }

    private function define_shipping_hooks()
    {
        $this->loader->add_action('woocommerce_shipping_init', $this, 'init_shipping');
        $this->loader->add_filter('woocommerce_shipping_methods', $this, 'add_shipping_method');
        $this->loader->add_action('woocommerce_order_status_processing', $this->checkout, 'order_status_processing');

        $this->loader->add_action('send_ecpay_order', $this, 'event_send_ecpay_order');

        $this->loader->add_filter('woocommerce_localisation_address_formats', $this, 'addressFormats');

        $admin_order = new Alan_Admin_Order($this->shipping_id);
        $this->loader->add_action('init', $admin_order, 'register_order_status');
        $this->loader->add_filter('wc_order_statuses', $admin_order, 'order_statuses');
    }

    private function define_payment_hooks()
    {
        $this->loader->add_filter('woocommerce_payment_gateways', $this, 'add_payment_gateways', 20, 1);

        $api = new Alan_Woo_API_ECPay_Response($this->shipping_id);
        $this->loader->add_action('woocommerce_api_ecpay_response', $api, 'check_response');

        $this->loader->add_action('woocommerce_thankyou', $this, 'cvs_store_info');
        $this->loader->add_action('woocommerce_view_order', $this, 'cvs_store_info');

        // email hooks
        $this->loader->add_action('woocommerce_email_order_details', $this, 'email_cvs_store_info', 10, 4);
        $this->loader->add_action('init', $this, 'remove_email_header_hook');
        $this->loader->add_action('woocommerce_email_header', $this, 'email_header', 10, 2);
        $this->loader->add_filter('woocommerce_email_subject_customer_processing_order', $this, 'processing_email_subject', 10, 2);
        $this->loader->add_filter('woocommerce_email_subject_customer_note', $this, 'note_email_subject', 10, 2);

        // Danny - order > setting > order status > export hooks : import_custom_order_statuses
	$this->loader->add_action( 'wp_ajax_wc_order_status_manager_import_custom_order_statuses', $this, 'import_custom_order_statuses' );
    }

    public function admin_edit_link($links = [])
    {
        return array_merge(
            [
                'settings' => '<a href="' . admin_url('admin.php?page=wc-settings&tab=shipping') . '">' . __('Settings') . '</a>',
            ],
            $links
        );
    }

    public function event_send_ecpay_order($order_id)
    {
        $this->get_checkout()->order_status_processing($order_id);
    }

    // ----------------- shipping hooks ---------------------

    public function init_shipping()
    {
        require_once plugin_dir_path(__DIR__) . 'shippings/class-wc-shipping-ecpay-cvs.php';
    }

    public function add_shipping_method($methods)
    {
        $methods[$this->shipping_id] = 'WC_Shipping_ECPay_CVS';

        return $methods;
    }

    public function addressFormats($formats)
    {
        $formats['TW']      = "{last_name}{first_name}\n{postcode} {state_code}{city}{address_1}\n{company}\n{address_2}\n";
        $formats['default'] = $formats['TW'];

        return $formats;
    }

    // ----------------- payment hooks -----------------------

    public function remove_email_header_hook()
    {
        $this->loader->remove_action('woocommerce_email_header', [WC()->mailer(), 'email_header']);
    }

    public function email_header($email_heading, $email)
    {
        $template = 'emails/email-header.php';

        if ('customer_processing_order' === $email->id &&
            $this->shipping_id === $this->get_shipping_method_id($email->object)
        ) {
            $email_heading = '感謝您的訂購';
        }

        if ('customer_note' === $email->id &&
            $this->shipping_id === $this->get_shipping_method_id($email->object) &&
            'ecpay-delivered' === $email->object->get_status()
        ) {
            $email_heading = '商品已送達指定門市';
        }

        wc_get_template($template, ['email_heading' => $email_heading]);
    }

    public function processing_email_subject($subject, $order)
    {
        if ($this->payment_id === $order->get_payment_method()) {
            return '已收到您的訂單-' . $order->get_payment_method_title();
        }

        if ($this->shipping_id === $this->get_shipping_method_id($order)) {
            $store = get_post_meta($order->get_id(), '_ecpay_store', true);
            return $subject . '-' . $order->get_shipping_method();
        }

        return $subject;
    }

    public function note_email_subject($subject, $order)
    {
        if ('ecpay-delivered' === $order->get_status()) {
            if ($this->shipping_id === $this->get_shipping_method_id($order)) {
                $suffix = ($this->payment_id === $order->get_payment_method())
                ? $order->get_payment_method_title()
                : $order->get_shipping_method();

                return '商品已送達指定門市-' . $suffix;
            }
        }

        return $subject;
    }

    public function remove_payment_cod($available_gateways)
    {
        if (is_admin()) {
            return;
        }

        $chosen_methods = WC()->session->get('chosen_shipping_methods');

        if (in_array('home_ecan_shipping', $chosen_methods) // 宅配通
             || in_array('home_tcat_shipping', $chosen_methods) // 黑貓宅急便
             || in_array('home_coldtcat_shipping', $chosen_methods) // 黑貓宅急便(冷藏)
        ) {
            unset($available_gateways['lohasit_ecpay_cvs_pay']);
        }

        if (in_array('lohasit_ecpay_cvs', $chosen_methods)) {
            // 超商取貨移除貨到付款
            unset($available_gateways['cod']);
        }

        if (true === $this->cart_has_virtual_product()) {
            unset($available_gateways['lohasit_ecpay_cvs_pay']);
        }

        return $available_gateways;
    }

    public function cart_has_virtual_product()
    {
        $has_virtual_products = false;

        $virtual_products = 0;

        $products = WC()->cart->get_cart();

        foreach ($products as $product) {
            $product_id = $product['product_id'];
            $is_virtual = get_post_meta($product_id, '_virtual', true);
            if ($is_virtual == 'yes') {
                $virtual_products += 1;
            }
        }

        if (count($products) == $virtual_products) {
            $has_virtual_products = true;
        }

        return $has_virtual_products;
    }

    public function add_payment_gateways($gateways = [])
    {
        $gateways[] = 'WC_ECPay_CVS_Pay';

        return $gateways;
    }

    public function cvs_store_info($order_id)
    {
        $order = wc_get_order($order_id);

        if ($this->shipping_id === $this->get_shipping_method_id($order)) {
            $store  = get_post_meta($order_id, '_ecpay_store', true);
            $suffix = ($this->payment_id === $order->get_payment_method()) ? '付款' : '';
            echo '<h2>取貨' . $suffix . '門市資訊</h2>';
            $this->display_store_info($store);
            $cvs_send = get_post_meta($order_id, '_ecpay_cvs_send', true);
            if ('yes' === $cvs_send) {
                $cvs_data            = get_post_meta($order_id, '_ecpay_cvs_data', true);
                $tracking['UNIMART'] = 'https://eservice.7-11.com.tw/e-tracking/search.aspx';
                $tracking['FAMI']    = 'https://www.famiport.com.tw/distribution_search.asp?page=4';
                $tracking['HILIFE']  = 'http://www.hilife.com.tw/serviceInfo_search.aspx';
                $store_type          = str_replace('C2C', '', $cvs_data['LogisticsSubType']);
                $button              = '&nbsp;&nbsp;<a href="' . $tracking[$store_type] . '" target="_blank">貨件狀態查詢</a>';
                echo '<p><label>配送編號:</label>&nbsp;&nbsp;' . $cvs_data['CVSPaymentNo'] . $cvs_data['CVSValidationNo'] . $button . '</p>';
            }
        }
    }

    public function email_cvs_store_info($order, $sent_to_admin, $plain_text, $email)
    {
        $this->cvs_store_info($order->get_id());
    }

    // -------------------------------------------------------

    public function get_shipping_option($key = '')
    {
        global $wpdb;
        $option = [];
        $table  = $wpdb->prefix . 'options';
        $name   = 'woocommerce_' . $this->shipping_id;
        $sql    = "SELECT option_value FROM $table WHERE option_name LIKE '%$name%' AND option_name <> 'woocommerce_" . $this->payment_id . "_settings'";
        $row    = $wpdb->get_row($sql);
        if ($row) {
            $option = unserialize($row->option_value);
        }

        return ($key && array_key_exists($key, $option)) ? $option[$key] : $option;
    }

    public function get_shipping_option_key()
    {
        global $wpdb;
        $option = [];
        $table  = $wpdb->prefix . 'options';
        $name   = 'woocommerce_' . $this->shipping_id;
        $sql    = "SELECT option_name FROM $table WHERE option_name LIKE '%$name%' AND option_name <> 'woocommerce_" . $this->payment_id . "_settings'";
        $row    = $wpdb->get_row($sql);

        return $row->option_name;
    }

    public function get_shipping_method_id($order)
    {
        $shipping_method    = @array_shift($order->get_shipping_methods());
        $shipping_method_id = $shipping_method['method_id'];

        return $shipping_method_id;
    }

    public function get_store_name($store = [], $parentheses = true)
    {
        $logistics = $this->get_shipping_option('logistics');

        $sub_type = (strstr($store['LogisticsSubType'], 'C2C'))
        ? str_replace('C2C', '', $store['LogisticsSubType'])
        : $store['LogisticsSubType'];

        $name = (array_key_exists($sub_type, $logistics))
        ? $logistics[$sub_type]
        : '';

        return ($store && true === $parentheses) ? ' (' . $name . ')' : $name;
    }

    public function display_store_info($store = [])
    {
        if ($store && empty($store['CVSTelephone'])) {
            $store['CVSTelephone'] = '系統未提供';
        }

        ?>
        <p><label>門市名稱:</label>&nbsp;&nbsp;<?php echo ($store) ? $store['CVSStoreName'] : ''; ?> <?php echo $this->get_store_name($store); ?></p>
        <p><label>門市地址:</label>&nbsp;&nbsp;<?php echo ($store) ? $store['CVSAddress'] : ''; ?></p>
        <p><label>門市電話:</label>&nbsp;&nbsp;<?php echo ($store) ? $store['CVSTelephone'] : ''; ?></p>
        <?php

    }

    public function get_order_number($order)
    {
        if (is_numeric($order)) {
            $order = wc_get_order($order);
        }

        $order_number = ('yes' === $this->get_shipping_option('test_mode'))
        ? strtoupper(uniqid()) . $order->get_order_number()
        : $order->get_order_number();

        return $order_number;
    }

    public function get_order_id($response = [])
    {
        if (!isset($response['MerchantTradeNo'])) {
            throw new InvalidArgumentException('缺少必要參數');
        }

        if ('yes' === $this->get_shipping_option('test_mode')) {
            return substr($response['MerchantTradeNo'], 13);
        }

        return $response['MerchantTradeNo'];
    }

        /**
         * Import custom order statuses
         *
         * @since 1.3.0
         */
        public function import_custom_order_statuses() {
            check_ajax_referer( 'import-custom-order-statuses', 'security' );
            wc_order_status_manager()->get_order_statuses_instance()->ensure_statuses_have_posts();
            //wp_send_json_success( wc_get_order_statuses() );
            $order_statuses = array(
                'wc-ecpay-picking'     => _x( '揀貨中', 'Order status', 'woocommerce' ),
                'wc-ecpay-delivered'   => _x( '已出貨', 'Order status', 'woocommerce' ),
            );
            return apply_filters( 'wc_order_statuses', $order_statuses );
        }
}
