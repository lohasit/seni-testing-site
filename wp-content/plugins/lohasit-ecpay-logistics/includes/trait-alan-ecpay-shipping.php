<?php

trait Alan_ECPay_Shipping
{
    protected $shpping_id;

    protected $payment_id;

    public function __construct($shipping_id)
    {
        $this->shipping_id = $shipping_id;

        $this->payment_id = $shipping_id . '_pay';
    }

    public function __call($method, $args)
    {
        return call_user_func_array([alan_woo_ecpay_logistics_plugin(), $method], $args);
    }

    public function send_order_url($order_id)
    {
        return admin_url('admin.php?action=send_ecpay_cvs_order&order_id=' . $order_id);
    }

    public function print_trade_url($order_id)
    {
        return admin_url('admin.php?action=print_ecpay_cvs_trade&order_id=' . $order_id);
    }
}

