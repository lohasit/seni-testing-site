<?php

defined('ABSPATH') || exit;

class WC_Shipping_ECPay_CVS extends WC_Shipping_Method
{
    public function __construct($instance_id = 0)
    {
        $this->id                 = 'lohasit_ecpay_cvs';
        $this->instance_id        = absint($instance_id);
        $this->method_title       = '綠界超商取貨';
        $this->method_description = '讓您的顧客使用綠界超商取貨';
        $this->supports           = [
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal',
        ];

        $this->init_form_fields();
        $this->init_settings();

        $this->title        = $this->get_option('title');
        $this->cost         = $this->get_option('cost');
        $this->availability = $this->get_option('availability');
        $this->countries    = $this->get_option('countries');
        $this->requires     = $this->get_option('requires');
        $this->free_cost    = $this->get_option('free_cost');

        $this->logistics = [
            'UNIMART' => '統一超商',
            'FAMI'    => '全家',
            'HILIFE'  => '萊爾富',
        ];

        $this->shipping_store = $this->get_option('shipping_store');


        add_action('woocommerce_update_options_shipping_' . $this->id, [$this, 'process_admin_options']);
    }

    public function init_form_fields()
    {
        $settings = include plugin_dir_path(__DIR__) . 'shippings/settings-ecpay-cvs.php';

        // woocommece_shipping_settings_ecpay_cvs
        $this->instance_form_fields = apply_filters('woocommerce_shipping_settings_' . $this->id, $settings);
    }

    public function process_admin_options()
    {
        parent::process_admin_options();

        $options = get_option($this->get_instance_option_key());

        foreach ($this->logistics as $key => $value) {
            if (array_key_exists($key, $_POST['data'])) {
                $options['shipping_store'][] = $key;
            }
        }

        $options['logistics'] = $this->logistics;

        return update_option($this->get_instance_option_key(), $options, 'yes');

    }

    public function calculate_shipping($package = [])
    {
        $shipping_cost = $this->get_option('cost');

        $store = $_SESSION['_ecpay_store'];

        if (isset($store['CVSOutSide']) && $store['CVSOutSide'] == '1') {
            $shipping_cost = $this->get_option('outside_cost');
        }

        $cost = ((int) WC()->cart->cart_contents_total >= (int) $this->get_option('free_cost'))
        ? 0
        : $shipping_cost;

        $rate = [
            'id'       => $this->id,
            'label'    => $this->title,
            'cost'     => $cost,
            'calc_tax' => 'per_item',
        ];

        // Register the rate
        $this->add_rate($rate);

        do_action('woocommerce_' . $this->id . '_shipping_add_rate', $this, $rate);
    }

    public function validate_shipping_options_table_field($key)
    {
        return false;
    }

    public function generate_shipping_options_table_html()
    {
        ob_start();
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">啟用超商</th>
            <td class="forminp" id="<?php echo $this->id; ?>_options">
            <table class="shippingrows" cellspacing="0">
                <tbody>
                <?php foreach ($this->logistics as $key => $value): ?>
                    <tr class="option-tr">
                        <td>
                            <input type="checkbox" name="<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo (in_array($key, $this->shipping_store)) ? 'checked' : ''; ?>>
                            <?php echo $value; ?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </td>
        </tr>
        <?php

        return ob_get_clean();
    }
}
