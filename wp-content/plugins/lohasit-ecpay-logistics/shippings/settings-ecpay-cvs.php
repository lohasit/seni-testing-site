<?php

return [
    'title'             => [
        'title'   => __('Title', 'woocommerce'),
        'type'    => 'text',
        'default' => '超商取貨',
    ],
    'cost'              => [
        'title'   => __('費用', 'woocommerce'),
        'type'    => 'text',
        'default' => '60',
    ],
    'outside_cost'      => [
        'title'       => __('離島費用(澎湖)', 'woocommerce'),
        'type'        => 'text',
        'description' => '僅統一超商適用',
        'default'     => '100',
    ],
    'free_cost'         => [
        'title'   => __('訂單金額多少以上免運費'),
        'type'    => 'text',
        'default' => '1000',
    ],
    'test_mode'         => [
        'title'   => '測試模式',
        'type'    => 'checkbox',
        'label'   => '啟用測試模式',
        'default' => 'yes',
    ],
    'cvs_type'          => [
        'title'   => '物流類型',
        'type'    => 'select',
        'options' => [
            'C2C' => 'C2C',
            'B2C' => 'B2C',
        ],
    ],
    'merchant_id'       => [
        'title'   => __('商店代號', 'woocommerce'),
        'type'    => 'text',
        'default' => '2000933',
    ],
    'hash_key'          => [
        'title'   => __('HashKey', 'woocommerce'),
        'type'    => 'text',
        'default' => 'XBERn1YOvpM9nfZc',
    ],
    'hash_iv'           => [
        'title'   => __('HashIV', 'woocommerce'),
        'type'    => 'text',
        'default' => 'h1ONHk4P4yqbl5LK',
    ],
    'sender_name'       => [
        'title'   => __('寄件人', 'woocommerce'),
        'type'    => 'text',
        'default' => '購物網',
    ],
    'sender_cellphone'  => [
        'title'   => __('寄件人手機', 'woocommerce'),
        'type'    => 'text',
        'default' => '0988888888',
    ],
    'shipping_store'    => [
        'type' => 'shipping_options_table',
    ],
    'package_operation' => [
        'title'       => __('傳送物流訂單時機', 'woocommerce'),
        'type'        => 'select',
        'class'       => 'b2c-ecpay-package-operation wc-enhanced-select',
        'default'     => 'checkout_processed',
        'description' => '設定"顧客結帳完成時",只有超商取貨付款會生效,其他付款方式為收到付款後,訂單狀態變更為處理中時<br>批次列印出貨單: 訂單 => 處理中 => 篩選超商業者 => 勾選訂單 => 列印超商取貨寄件單',
        'options'     => [
            'admin_bulk_action'  => '(手動傳送) 後台訂單列表按鈕',
            'checkout_processed' => '(自動傳送) 顧客結帳完成時',
        ],
    ],

    'debug_mode'         => [
        'title'   => '除錯模式',
        'type'    => 'checkbox',
        'label'   => '啟用除錯模式',
        'default' => 'yes',
    ],
];
