<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>列印寄件單</title>
    <style type="text/css">
    body {
        overflow-x: hidden;
    }
    h2 {
        margin: 200px auto 20px;
        text-align: center;
    }
    div.notice {
        border: 5px solid #ccc;
        background-color: #efefef;
        padding: 20px;
        margin: 0 auto;
        width: 630px;
        text-align: center;
    }
    </style>
    <script>
    function submitForm() {
        document.forms['ecpay_form'].submit();
    }
    </script>
</head>
<body onload="submitForm();">
<div class="notice">
    請稍候!正在處理中...
    <form name="ecpay_form" action="<?php echo $url ?>" method="POST">
        <?php foreach ($paramters as $name => $val): ?>
        <input type="hidden" name="<?php echo $name ?>" value="<?php echo $val ?>">
        <?php endforeach?>
        如果頁面沒有重新導向到列印頁面,請<input type="submit" value="按此列印">
    </form>
</div>
</body>
</html>