<html>
    <head>
    <style>
    body {
        overflow-x: hidden;
    }
    .PrintBody
    {
        margin: 0;
        width: 21cm;
    }
    .PrintToolsBlock
    {
        background-color: #ddffdd;
        border-left: 6px solid #4CAF50;
        padding: 10px;
        margin-bottom: 10px;
        width: 820px;
        position: fixed;
        z-index: 999;
    }
    #PrintBlock
    {
        padding-top: 60px;
        margin-left: 10px;
    }
    .PrintButton
    {
        width: 100px;
        height: 35px;
    }
    .PrintWarningWord
    {
        color: #ff0000;
    }
    </style>
    <script>
    function printBlock() {
        var oldPage = document.body.innerHTML;
        var divElements = document.getElementById('PrintBlock').innerHTML;
        document.body.innerHTML = "<html><head><title></title></head><body>" + divElements + "</body></html>";
        window.print();
        document.body.innerHTML = oldPage;
    }
    </script>
    </head>
    <body class="PrintBody">
        <div class="PrintToolsBlock" style="">
            <button id="btnPrint" class="PrintButton" onclick="printBlock();">列印</button>
            <?php if ('yes' === $test_mode): ?>
            <span class="PrintWarningWord">此為測試標籤，無法寄送商品</span>
            <?php endif;?>
        </div>
        <div id="PrintBlock">
            <?php echo $content; ?>
        </div>
    </body>
</html>