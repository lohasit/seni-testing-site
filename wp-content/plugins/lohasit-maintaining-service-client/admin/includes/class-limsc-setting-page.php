<?php
class LimscSetting
{
    public function __construct()
    {
        $this->register_hooks();
    }

    public static function boot()
    {
        return new self;
    }
    private function register_hooks()
    {
        add_action('admin_menu', array($this, 'add_setting_page'));
        add_action('admin_init', array($this, 'register_settings'));
    }

    public function add_setting_page()
    {
        if(current_user_can('administrator')) {
            add_options_page(
                __('Lohasit Maintaining Service Setting', 'lohasit-msc'),
                __('Lohasit Maintaining Service Setting', 'lohasit-msc'),
                'manage_options',
                'limsc-settings',
                array($this, 'limsc_setting_page')
            );
        }
    }

    public function limsc_setting_page()
    {
        wp_enqueue_script('jquery-ui-datepicker');
        ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <form action="options.php" method="post">
            <?php settings_fields('limsc-settings'); ?>
            <?php do_settings_sections('limsc-settings'); ?>
            <hr>
            <?php submit_button(); ?>
        </form>
        <script>
            jQuery(function($){
                $('h2').append('<hr>');
                $('.datepicker').datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
        <?php
    }

    public function register_settings()
    {
        register_setting('limsc-settings', 'limsc_due_date');
        register_setting('limsc-settings', 'limsc_client_name');
        register_setting('limsc-settings', 'limsc_turn_on');
        register_setting('limsc-settings', 'limsc_alert_date');
        register_setting('limsc-settings', 'limsc_alert_message');
        register_setting('limsc-settings', 'limsc_alert_button_url');
        register_setting('limsc-settings', 'limsc_lockedup_page_logo_turn_on');
        register_setting('limsc-settings', 'limsc_lockedup_page_iframe_url');
        register_setting('limsc-settings', 'limsc_lockedup_page_custom_css');
        add_settings_section(
            'limsc-setting-section-general',
            __('General', 'lohasit-msc'),
            array($this, 'limsc_setting_section_general'),
            'limsc-settings'
        );
        add_settings_field(
            'limsc_client_name',
            __('Client Name', 'lohasit-msc'),
            array($this, 'limsc_setting_field_client_name'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        add_settings_field(
            'limsc_turn_on',
            __('Turn on/off', 'lohasit-msc'),
            array($this, 'limsc_setting_field_turn_on'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        add_settings_field(
            'limsc_due_date',
            __('Due Date', 'lohasit-msc'),
            array($this, 'limsc_setting_field_due_date'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        add_settings_field(
            'limsc_alert_date',
            __('Alert Date', 'lohasit-msc'),
            array($this, 'limsc_setting_field_alert_date'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        add_settings_field(
            'limsc_alert_message',
            __('Alert Message', 'lohasit-msc'),
            array($this, 'limsc_setting_field_alert_message'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        add_settings_field(
            'limsc_lockedup_alert_button_url',
            __('Alert Button Url', 'lohasit-msc'),
            array($this, 'limsc_setting_field_alert_button_url'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        // Alan: 用不到先拿掉
        // add_settings_field(
        //     'limsc_lockedup_page_logo_turn_on',
        //     __('Locked Page Logo', 'lohasit-msc'),
        //     array($this, 'limsc_setting_field_lockedup_page_logo_turn_on'),
        //     'limsc-settings',
        //     'limsc-setting-section-general'
        // );
        add_settings_field(
            'limsc_lockedup_page_iframe_url',
            __('Locked Page Iframe Url', 'lohasit-msc'),
            array($this, 'limsc_setting_field_lockedup_page_iframe_url'),
            'limsc-settings',
            'limsc-setting-section-general'
        );
        // Alan: 用不到先拿掉
        // add_settings_field(
        //     'limsc_lockedup_page_custom_css',
        //     __('Locked Page Custom Css', 'lohasit-msc'),
        //     array($this, 'limsc_setting_field_lockedup_page_custom_css'),
        //     'limsc-settings',
        //     'limsc-setting-section-general'
        // );
    }

    public function limsc_setting_section_general()
    {
        return;
    }

    public function limsc_setting_field_due_date()
    {
        $this->generate_option_html([
            'type' => 'text',
            'id'   => 'limsc_due_date',
            'name' => 'limsc_due_date',
            'class' => ['datepicker']
        ]);
    }

    public function limsc_setting_field_turn_on()
    {
        $this->generate_option_html([
            'type' => 'checkbox',
            'id' => 'limsc_turn_on',
            'name' => 'limsc_turn_on',
            'label' => __('Turn on', 'lohasit-msc'),
        ]);
    }

    public function limsc_setting_field_alert_date()
    {
        $this->generate_option_html([
            'type' => 'text',
            'id'   => 'limsc_alert_date',
            'name' => 'limsc_alert_date',
            'class' => ['datepicker']
        ]);
    }

    public function limsc_setting_field_alert_message()
    {
        $this->generate_option_html([
            'type' => 'textarea',
            'id'   => 'limsc_alert_message',
            'name' => 'limsc_alert_message',
            'rows' => 5,
            'cols' => 200
        ]);
    }

    public function limsc_setting_field_lockedup_page_logo_turn_on()
    {
        $this->generate_option_html([
            'type' => 'checkbox',
            'id'   => 'limsc_lockedup_page_logo_turn_on',
            'name' => 'limsc_lockedup_page_logo_turn_on'
        ]);
    }

    public function limsc_setting_field_lockedup_page_iframe_url()
    {
        $this->generate_option_html([
            'type' => 'textarea',
            'id'   => 'limsc_lockedup_page_iframe_url',
            'name' => 'limsc_lockedup_page_iframe_url',
            'rows' => 1,
            'cols' => 200
        ]);
    }

    public function limsc_setting_field_client_name()
    {
        $this->generate_option_html([
            'type' => 'text',
            'id'   => 'limsc_client_name',
            'name' => 'limsc_client_name',
        ]);
    }

    public function limsc_setting_field_lockedup_page_custom_css()
    {
        $this->generate_option_html([
            'type' => 'textarea',
            'id'   => 'limsc_lockedup_page_custom_css',
            'name' => 'limsc_lockedup_page_custom_css',
            'rows' => 10,
            'cols' => 100
        ]);
    }

    public function limsc_setting_field_alert_button_url()
    {
        $this->generate_option_html([
            'type' => 'textarea',
            'id'   => 'limsc_alert_button_url',
            'name' => 'limsc_alert_button_url',
            'rows' => 1,
            'cols' => 200
        ]);
    }

    function generate_option_html($field)
    {
        if(!array_key_exists('class', $field))
            $field['class'] = [];
        switch ($field['type']) {
            case 'checkbox':
                echo "<input type='checkbox' id='" . $field['id'] . "'class='".implode(' ', $field['class'])."' name='" . $field['name'] . "' value='1'";
                if (get_option($field['name']) == '1') {
                    echo "checked='checked'";
                }
                echo ">";
                echo "<label for='" . $field['id'] . "'>" . $field['label'] . "</label>";
                break;
            case 'textarea':
                echo "<textarea rows='" . $field['rows'] . "' cols='" . $field['cols'] . "' id='" . $field['id'] . "'class='" . implode(' ', $field['class']) . "' name='" . $field['name'] . "'>";
                echo get_option($field['name']);
                echo "</textarea>";
                break;
            case 'text':
                echo "<input type='text' id='" . $field['id'] . "'class='". implode(' ', $field['class']) ."' name='" . $field['name'] . "' value='" . get_option($field['name']) . "'>";
                break;
            case 'radio':
                $option = get_option($field['name']);
                foreach ($field['option'] as $value => $label) {
                    echo "<input type='radio' id='" . $field['name'] . '_' . $value . "' name='" . $field['name'] . "' value='" . $value . "'";
                    if ($option == $value) {
                        echo "checked='checked'";
                    }
                    echo ">";
                    echo "<label for='" . $field['name'] . '_' . $value . "'>" . $label . "</label>";
                    echo "<br>";
                }
                break;
        }
    }
}