<style>
    #lohasit-maintaining-service-expiration-message {
        cursor: pointer;
    }
    #lohasit-maintaining-service-expiration-message .mask {
        position: fixed;
        height: 100%;
        width: 100%;
        background-color: rgba(10,10,10,.9);
        top: 0;
        left: 0;
        z-index: 99998;
    }
    #lohasit-maintaining-service-expiration-message .message {
        z-index: 99999;
        text-align: center;
        position: fixed;
        background-color: white;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        width: 80%;
        padding: 10px;
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
    }
</style>
<div id="lohasit-maintaining-service-expiration-message" style="display: none;">
    <div class="mask"></div>
    <div class="message">
        <?=$message?>
    </div>
</div>
<script>
    jQuery(function($){
        $('#lohasit-maintaining-service-expiration-message').on('click', function(){
            $(this).hide();
        });
    });
</script>