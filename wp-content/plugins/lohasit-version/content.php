<?php

$authors = [
    1  => 'HM',
    2  => 'ymlin',
    3  => 'Danny',
    4  => '小汪',
    5  => '阿傑',
    6  => 'Manson',
    7  => 'Alan',
    8  => 'Han',
    9  => 'JJ',
    10 => 'Seni',
    11 => 'Stephen',
    12 => 'Harley',
    13 => 'Anya',
    14 => '世萍',
];

$types = [
    'dev'     => [
        1 => 'DEV-新增',
        2 => 'DEV-修正',
    ],
    'feature' => [
        1 => '優化調整',
        2 => '問題修正',
        3 => '更新功能',
        4 => '新增外掛',
        5 => '刪除外掛',
    ],
];

$data = [

    [
        'version' => '4.1.4',
        'date'    => '2022/04/11',
        'list'    => [
            ['feature', 2, 'YITH 願望清單列表的桌機與手機版結構共用', 9],
            ['feature', 2, 'YITH 修復願望清單可變商品選規格按鈕的鏈接', 9],
            ['feature', 2, '調整YITH 願望清單列表樣式', 14],

        ],
    ],
    [
        'version' => '4.1.3',
        'date'    => '2022/04/07',
        'list'    => [
            ['feature', 3, '更新WP核心 5.9.2 => 5.9.3', 9],
            ['feature', 3, '更新ACF版本 5.11.4 => 5.12.1', 9],

        ],
    ],
    [
        'version' => '4.1.2',
        'date'    => '2022/04/06',
        'list'    => [
            ['feature', 2, '前往感謝頁無需轉變訂單狀態為保留', 7],
            ['feature', 4, '新增YITH快速查看商品外掛', 13],
            ['feature', 1, '修改YITH快速查看的彈窗排版', 9],
            ['feature', 1, '複寫會員中心願望清單的手機版結構', 9],
            ['feature', 1, '會員中心修改並優化RWD樣式', 14],
            ['feature', 1, 'YITH快速查看彈窗區新增樣式和JS效果', 9],
            ['feature', 1, 'YITH快速查看彈窗區新增動態定價價格表、優惠提示與點數提示', 9],
            ['feature', 1, 'YITH快速查看彈窗區新增客制分享按鈕', 9],
            ['feature', 1, 'YITH快速查看彈窗區新增商品分頁內容：描述、額外資訊、評價留言區', 9],
            ['feature', 1, 'YITH快速查看彈窗區新增"你可能喜歡"商品', 9],
            ['feature', 1, 'YITH快速查看彈窗區新增"相關"商品', 9],
            ['feature', 1, '快速預覽內容：動態定價可隨著數量動態更新', 9],
        ],
    ],

    [
        'version' => '4.1.1',
        'date'    => '2022/03/23',
        'list'    => [
            ['feature', 1, '新增 $_GET["delete_fs"] 的HTTP參數以便手動刪除 fs_active_plugins', 9],
            ['feature', 3, 'Wordpress版本安全性更新：5.9 => 5.9.2', 9],
            ['feature', 2, '購物車頁物流空值仍為陣列類型除錯', 7],
            ['feature', 1, '帳戶資訊頁 - 新增多語系提示', 9],
        ],
    ],

    [
        'version' => '4.1.0',
        'date'    => '2022/03/15',
        'list'    => [
            ['feature', 4, 'YITH WooCommerce Quick View', 13],
            ['feature', 1, '更新翻譯 Variation Swatches for WooCommerce', 13],
            ['feature', 1, '更新翻譯 YITH WooCommerce Color and Label Variations', 13],
            ['feature', 2, '修正YITH動態定價運算的資料類型為int', 9],
        ],
    ],

    [
        'version' => '4.0.9',
        'date'    => '2022/03/11',
        'list'    => [
            ['feature', 5, 'Variation Swatches for WooCommerce', 13],
            ['feature', 4, 'YITH WooCommerce Color and Label Variations', 13],
        ],
    ],
    [
        'version' => '4.0.8',
        'date'    => '2022/03/05',
        'list'    => [
            ['feature', 2, '啟用WP Activity Log 發生 fs_active_plugins取值為False 的替代值', 7],
            ['feature', 5, '移除外掛『樂活金物流設定功能』', 9],
            ['feature', 1, '開放原生金物流設定：購物車系統 > 設定 > 運送方式 | 付款', 9],
        ],
    ],
    [
        'version' => '4.0.7',
        'date'    => '2022/03/03',
        'list'    => [
            ['feature', 3, '更新RankMath版本 1.0.80 => 1.0.83', 9],
            ['feature', 2, '『金物流設定』移除子選項：自定義物流', 9],
            ['feature', 2, '『金物流設定』修正金流欄位的更新錯誤', 9],
            ['feature', 1, '更新YITH前台的文字標籤為中文(Loco Translate無法翻譯的內容)', 9],
            ['feature', 1, '更新中文語系的旗幟', 9],
        ],
    ],

    [
        'version' => '4.0.6',
        'date'    => '2022/02/11',
        'list'    => [
            ['feature', 1, '移除OylmpusWP以外的主題', 9],
            ['feature', 1, '將形象和電商共用的客制功能加入子主題lohasit-custom.php', 9],
            ['feature', 1, '將Danny設計的資安相關程式碼搬動到子主題下lohasit-defend.php', 9],
            ['feature', 2, '修復SVG無法上傳到媒體庫問題', 9],
            ['feature', 2, '開放所有最高權限角色可以操作Woocommerce的設定分頁', 9],
            ['feature', 1, '調整『編輯』、『作者』和『客戶』角色權限', 9],
            ['feature', 5, '因兼容問題，暫時關閉Login Customizer外掛', 9],
            ['feature', 4, '安裝Classic Editor傳統編輯器 ', 9],
        ],
    ],

    [
        'version' => '4.0.5',
        'date'    => '2022/02/05',
        'list'    => [
            ['feature', 3, '補充優惠券列表的多語系功能', 9],
            ['feature', 3, '修正優惠券的顯示在會員中心的邏輯', 9],
            ['feature', 3, '判斷區分折價來源：詢價優惠 / 點數優惠 / 折價券優惠。兼容多語系', 9],
        ],
    ],

    [
        'version' => '4.0.4',
        'date'    => '2022/02/05',
        'list'    => [
            ['feature', 1, '重分配外掛『樂活購物車客製化』的檔案結構', 9],
            ['feature', 3, '新增前後台出生日期欄位', 9],
            ['feature', 3, '針對非最高權限隱藏後台提示文字', 9],
            ['feature', 3, '歸納後台行銷功能相關的選單位置', 9],
            ['feature', 3, '限制非最高權限對行銷功能(動態定價、詢價、點數、願望清單)設定的可視範圍', 9],
            ['feature', 3, '更新會員中心和結帳頁的欄位', 9],
            ['feature', 2, '修正外掛『超商取貨』啟用時的空值報錯', 9],
            ['feature', 2, '修正外掛『革命滑塊』的多語系衝突問題', 9],
            ['feature', 2, '修正外掛『綠界金流』的啟用時無預選項目的報錯', 9],
            ['feature', 2, '修正外掛『WP活動日誌』最高權限的查看限制', 9],
            ['feature', 2, '修正革命滑塊的多語系衝突問題', 9],
            ['feature', 3, '更新外掛『會員升級』的版型', 9],
            ['feature', 2, '修正外掛『會員升級』的獲取角色的函式', 9],

        ],
    ],

    [
        'version' => '4.0.3',
        'date'    => '2022/02/05',
        'list'    => [
            ['feature', 1, '封存SliderRevolution', 9],
            ['feature', 1, '封存WP Extended Search', 9],
            ['feature', 1, '封存Wordfence Security', 9],
            ['feature', 1, '封存ManageWP-Worker', 9],
            ['feature', 1, '封存Classic Editor', 9],
            ['feature', 1, '封存ACF QuickEdit Fields', 9],
            ['feature', 1, '封存Custom Post Type UI', 9],
            ['feature', 1, '封存YITH WooCommerce Product Bundles Premium', 9],

        ],
    ],

    [
        'version' => '4.0.1',
        'date'    => '2022/02/05',
        'list'    => [
            ['feature', 4, 'ACF QuickEdit Fields', 9],
            ['feature', 4, 'RankMath', 9],
            ['feature', 4, 'Advanced Custom Fields', 9],
            ['feature', 4, 'Custom Post Type UI', 9],
            ['feature', 5, 'Lohasit Google OAuth', 9],
            ['feature', 5, 'Hide Admin Notice', 9],
            ['feature', 5, 'Admin Menu Editor', 9],
            ['feature', 5, 'Woocommerce Checkout Form', 9],
            ['feature', 5, 'Lohasit Admin CSS and JS', 9],
            ['feature', 5, 'Ocean Extra', 9],
            ['feature', 5, 'Lohasit WC Member Woocommerce', 9],
            ['feature', 5, 'Smush', 9],
        ],
    ],

    [
        'version' => '4.0.0',
        'date'    => '2022/02/05',
        'list'    => [
            ['feature', 1, '更新WordPress版本至5.9', 9],
            ['feature', 1, '更新Woocommerce版本至5.3.2', 9],
            ['feature', 1, '更替主題為：OlympusWP', 9],
        ],
    ],
    [
        'version' => '3.0.10',
        'date'    => '2021/12/25',
        'list'    => [
            ['feature', 1, '訂單狀態匯入客製化安裝', 3],
            ['feature', 1, '訂單狀態新增7-11,全家跟萊爾富的物流訂單狀態串接', 3],
            ['feature', 2, '增加資安弱掃問題修正Class並修正SQL Injection問題', 3],
        ],
    ],
    [
        'version' => '3.0.9',
        'date'    => '2021/10/16',
        'list'    => [
            ['feature', 1, '會員級別外掛調整＆多語系', 7],
        ],
    ],
    [
        'version' => '3.0.8',
        'date'    => '2021/09/15',
        'list'    => [
            ['feature', 3, '會員級別外掛今年及前年總消費額分開計算', 12],
            ['feature', 3, '會員級別外掛今年比去年總消費額高立即升級', 12],
        ],
    ],
    [
        'version' => '3.0.7',
        'date'    => '2021/09/10',
        'list'    => [
            ['feature', 3, '級別外掛增加活動日期區間', 12],
            ['feature', 1, '級別外掛鉤子置於檔案開頭', 12],
            ['feature', 2, '級別外掛避免刪除有在使用的角色', 12],
            ['feature', 1, '級別外掛引入bootstrap及美化', 12],
        ],
    ],
    [
        'version' => '3.0.6',
        'date'    => '2021/09/7',
        'list'    => [
            ['feature', 2, '匯出顧客外掛按鈕文案修正', 12],
            ['feature', 2, '匯出顧客外掛去重複按紐', 12],
            ['feature', 3, '匯出顧客外掛增加起始日期結束日期', 12],
        ],
    ],
    [
        'version' => '3.0.5',
        'date'    => '2021/09/3',
        'list'    => [
            ['feature', 1, '前台訂單再次訂購調整', 7],
            ['feature', 1, '後台手動新增訂單新增綠界電子發票所需欄位', 7],
        ],
    ],
    [
        'version' => '3.0.4',
        'date'    => '2021/09/3',
        'list'    => [
            ['feature', 1, '會員級別根據權限判斷該帳號是否可升級', 12],
            ['feature', 2, '會員級別停用bootstrap以免跑版', 12],
            ['feature', 3, '會員級別取代user role editor新增角色功能', 12],
        ],
    ],
    [
        'version' => '3.0.3',
        'date'    => '2021/08/24',
        'list'    => [
            ['feature', 4, 'lohasit-woocommerce-level-management', 12],
            ['feature', 5, 'woocommerce-customer-level', 12],
            ['feature', 5, 'Lohasit WC Member Woocommerce', 12],
        ],
    ],
    [
        'version' => '3.0.2',
        'date'    => '2021/07/22',
        'list'    => [
            ['feature', 2, '動態定價 - 所以移除共用規則的設定，所有規則默認為“可共用”', 9],
            ['feature', 1, '會員權限 - 新建角色：商店管理員lohasit_wc_manager', 9],
            ['feature', 1, '針對商店管理員角色，隱藏部分YITH願望清單和動態定價的設定分頁', 9],
            ['feature', 2, '會員等級 - 修復會員等級新增與升級的功能', 9],
            ['feature', 1, '會員列表 - 使用者列表增加可調整排序的會員註冊日期', 9],
            ['feature', 1, '會員列表 - 使用者列表增加可調整排序的使用者ID欄位', 9],
            ['feature', 1, '後台選單 - 非最高權限無法使用後台選單的工具功能', 9],
        ],
    ],
    [
        'version' => '3.0.1',
        'date'    => '2021/06/09',
        'list'    => [
            ['feature', 1, '結帳頁 - 結帳頁從超商選項返回時，判斷未登入用戶是否曾勾選[建立帳號]', 9],
            ['feature', 2, '結帳頁 - 補充運送至其他地址的聯絡號碼欄位', 9],
            ['feature', 1, '會員中心 - 補充寄送地址的電話號碼欄位', 9],
            ['feature', 1, '會員中心 - 調整地址欄位順序：市/縣 > 區 > 地址', 9],
            ['feature', 1, '商品內頁 - 顯示優惠商品（簡單商品）的折扣百分比以及剩下的金額', 9],
            ['feature', 2, '後台編輯商品 - 恢復外部商品選項', 9],
            ['feature', 1, '訂單 - 產生超商付款賬單/虛擬ATM帳號後，訂單轉“保留”狀態，扣庫存', 9],
            ['feature', 1, '訂單 - 買家未到超商取貨/超過虛擬ATM繳費期限，訂單轉“取消”狀態，補庫存', 9],
            ['feature', 2, '訂單 - 修復訂單發票號碼的匯出資料', 9],
            ['feature', 1, '使用者 - 移除姓氏，運送地址2，賬單地址2的欄位', 9],
            ['feature', 1, '優惠券 - 創建一個短代碼以顯示優惠券列表並將其放入我的帳戶頁面中以查看', 10],
            ['feature', 2, '會員升級 - 添加需要的表並修復 sql 查詢問題。', 10],
            ['feature', 2, '會員購物歷史 - sql 查詢中的固定表名稱。', 10],
            ['feature', 1, '購物車 - 虛擬商品和實體商品需分開結帳。', 9],

        ],
    ],
    [
        'version' => '3.0.0',
        'date'    => '2021/06/01',
        'list'    => [
            ['feature', 3, 'WordPress 5.5.1 => 5.7.2', 7],
            ['feature', 3, 'WooCommerce 4.5.2 => 5.3.0', 7],
            ['feature', 3, 'OceanWP 1.8.9 => 2.0.8', 7],
            ['feature', 3, '所有外掛升級到最新版本', 7],
            ['feature', 5, '刪除綠界官方外掛(金流/物流/電子發票)', 7],
            ['feature', 4, '新增Alan版綠界金流物流外掛(lohasit-ecpay-gateway/lohasit-ecpay-logistics)', 7],
            ['feature', 3, '新增WooCommerce客製化外掛, 郵遞區號自動帶入, 其他WooCommerce擴充功能請寫在此外掛(lohasit-woocommerce-customized)', 7],
            ['feature', 2, '樂活SSO登入(lohasit-google-oauth)修改調整', 7],
            ['feature', 1, '樂活金物流設定外掛(lohasit-woocommerce-pay-and-shipping-setting)調整', 7],
            ['feature', 1, 'OceanWP子佈景後台選單翻譯調整', 7],
            ['feature', 3, 'OceanWP子佈景加入d(),dd()函式', 7],
            ['feature', 4, '新增Alan版綠界電子發票外掛(lohasit-ecpay-invoice)', 7],
        ],
    ],
    [
        'version' => '2.0.2',
        'date'    => '2021/01/11',
        'list'    => [
            ['feature', 1, '樂活維護管理客戶端中文翻譯。', 7],
            ['feature', 3, '樂活維護管理客戶端加入遠端更改密碼功能。', 7],
        ],
    ],
    [
        'version' => '2.0.1',
        'date'    => '2020/10/13',
        'list'    => [
            ['feature', 3, '壓版的結帳流程，當客人填錯手機格式時，需要在當下的頁面，就能跳出錯誤提醒的訊息。', 5],
            ['feature', 3, '購物車頁面只需要focusout就可自動更新購物車。', 5],
            ['feature', 3, '處理優惠券移除後重整頁面的問題。', 5],
            ['feature', 3, '補上紀錄超商資訊到訂單備註。', 5],
            ['feature', 3, '自訂woocommerce帳號的密碼強度判斷。', 5],
            ['feature', 3, '在購物車頁面增加一個「返回商店」的按鈕。。', 5],
        ],
    ],
    [
        'version' => '2.0.0',
        'date'    => '2020/09/25',
        'list'    => [
            ['feature', 3, 'WordPress核心從 5.3.2 => 5.5.1', 5],
            ['feature', 3, '更新主題Ocean 從 1.8.3 => 1.8.9。', 5],
            ['feature', 3, '停止自動更新WP核心、WP主題、WP外掛。', 5],
            ['feature', 4, 'Media Cleaner  6.0.4 掃描沒有使用到的媒體。', 5],
            ['feature', 4, 'Media Deduper 1.5.3 在上傳時以及後續檢查媒體庫時，列出重複的媒體。', 5],
            ['feature', 4, 'WP Security Audit Log/WP Activity Log 4.1.2。', 5],
            ['feature', 4, 'Yoast SEO Multilingual 1.2.4。', 5],
            ['feature', 4, 'Code Snippets 2.14.0', 5],
            ['feature', 5, 'Share Locker 1.9.0', 5],
            ['feature', 5, 'WPML Page Builders 1.1.3。', 5],
            ['feature', 5, 'Transient Cleaner 1.5.7，因為WordPress核心4.9開始就包含此功能了。', 5],
            ['feature', 5, 'Upload Media by Zip 0.9.1。', 5],
            ['feature', 5, 'WP REST API 2.0-beta15。', 5],
            ['feature', 5, 'Showpayment(訂單列表顯示付款方式)。', 5],
            ['feature', 3, '刪除ARForms - Premium WordPress Form Builder 3.7.1', 5],
            ['feature', 1, 'Adminimize 1.11.6 => 1.11.7。', 5],
            ['feature', 1, '商品的類型去掉「外部商品」。', 5],
            ['feature', 1, '調整商店管理員可控制的權限，也可新增「vip」。', 5],
            ['feature', 3, 'WooCommerce 從3.9.2 => 4.5.2。', 5],
            ['feature', 1, 'WooCommerce「報表」的「顧客清單」增加匯出功能。', 5],
            ['feature', 1, 'WooCommerce的優惠卷限制要輸入標題/折扣碼後才能新增。', 5],
            ['feature', 3, '綠界金流 改v1.0 1.0.10222 => ECPay Payment for WooCommerce 1.2.2007070', 5],
            ['feature', 1, '在使用目前綠界金流時，商品資訊改成分別帶入實際商品的資訊，而不是只有一項總結的商品。', 5],
            ['feature', 1, '在使用目前綠界金流時，若是測試加入自定義前綴字串。', 5],
            ['feature', 1, '在使用目前綠界金流時，商品名稱可以設定特殊字元(單雙引號)。', 5],
            ['feature', 2, 'Lohasit Version外掛取資料方法調整。', 5],
            ['feature', 2, '移除Lohasit Google OAuth外掛中的後台切換角色選單。', 5],
            ['feature', 3, 'Allpay Ecpay Invoice 1.4.1 => ECPay Invoice for WooCommerce 1.1.2007070', 5],
            ['feature', 3, '綠界C2C(超商取貨-門市寄/取) 1.0 => ECPay Logistics for WooCommerce 1.3.2007070', 5],
            ['feature', 3, 'Checkout Field Editor for WooCommerce 從1.4.0 => 1.4.3', 5],
            ['feature', 3, 'YITH WooCommerce Dynamic Pricing and Discounts Premium 1.5.8 => 1.7.0。', 5],
            ['feature', 3, 'YITH WooCommerce Points and Rewards Premium 1.7.2 => 1.8.2。', 5],
            ['feature', 3, 'YITH WooCommerce Wishlist Premium 3.0.5 => 3.0.14。', 5],
            ['feature', 3, '刪除WooCommerce Product Bundles 5.1.4 => YITH WooCommerce Product Bundles Premium 1.4.1。', 5],
            ['feature', 3, 'AddToAny Share Buttons 從1.7.39 => 1.7.42。', 5],
            ['feature', 3, 'Advanced Floating Content變更為Lite版本，精簡化，1.2.0。', 5],
            ['feature', 3, 'Classic Editor 從1.5 => 1.6。', 5],
            ['feature', 3, 'Duplicate Post變更為Yoast版本，從3.2.4 變更為3.2.6。', 5],
            ['feature', 3, 'Force Regenerate Thumbnails2.0.6，變更為Regenerate Thumbnails3.1.3。', 5],
            ['feature', 3, 'Loco Translate 從2.3.1 => 2.4.3。', 5],
            ['feature', 3, 'MailPoet 3 (New) 從3.43.0 => 3.50.1，可調整寄信、收信、回信者資訊。', 5],
            ['feature', 3, 'Post SMTP 從2.0.10 => 2.0.15。', 5],
            ['feature', 3, 'Slider Revolution 從6.1.7 => 6.2.23。', 5],
            ['feature', 3, 'User Role Editor 從4.53 => 4.56.1。', 5],
            ['feature', 3, 'W3 Total Cache 從0.12.0 => 0.14.4。', 5],
            ['feature', 3, '更新Messenger功能，從MobileMonkey版本的WP Chatbot 4.3 => The Official Facebook Chat Plugin 1.7。', 5],
            ['feature', 3, 'Real Media Library 從4.0.6 => 4.9.6。', 5],
            ['feature', 3, 'Yoast SEO 從12.8.1 => 14.9。', 5],
            ['feature', 3, 'WPML CMS Nav 從1.5.2 => 1.5.5。', 5],
            ['feature', 3, 'WPML Media 從2.5.5 => 2.6.0。', 5],
            ['feature', 3, 'WPML Multilingual CMS 從4.3.18 => 4.4.2。', 5],
            ['feature', 3, 'WPML Sticky Links 從1.5.0 => 1.5.4。', 5],
            ['feature', 3, 'WPML String Translation 從3.0.13 => 3.1.3。', 5],
            ['feature', 3, 'WPML Translation Management 從2.9.10 => 2.10.1。', 5],
            ['feature', 3, 'Smush 從3.4.2 => 3.7.0。', 5],
            ['feature', 3, 'Captcha by BestWebSoft 4.2.8 => reCaptcha by BestWebSoft 1.58。', 5],
            ['feature', 3, 'Social Login WordPress Plugin - AccessPress Social Login Lite 3.4.4 => 3.4.6。', 5],
            ['feature', 3, 'WordPress Importer 從0.6.4 => 0.7。', 5],
            ['feature', 3, 'WP-Optimize - Clean, Compress, Cache 從3.0.16 => 3.1.4。', 5],
            ['feature', 3, 'WooCommerce Conversion Tracking 從2.0.6 => 2.0.8。', 5],
            ['feature', 3, 'WooCommerce Multilingual 從4.10.1 => 4.10.3', 5],
            ['feature', 3, '更新Elementor Pro 2.10.0 => Elementor Pro 3.0.5。', 5],
            ['feature', 3, '更新Elementor 2.9.11 => Elementor 3.0.10。', 5],
            ['feature', 3, '更新Elementor - Header, Footer & Blocks 1.5.1 => 1.5.3。', 5],
        ],
    ], [
        'version' => '1.8.1',
        'date'    => '2020/08/19',
        'list'    => [
            ['feature', 2, '修正超商取貨SESSION無法儲存問題', 7],
            ['feature', 1, '運送結帳表單增加收件人電話', 8],
            ['feature', 1, '訂單備註金物流方式不屬於會員備註(不寄信)', 8],
            ['feature', 2, '修正綠界金流因付款方式不寄信造成無法付款(綠界採撈取備註)', 8],
            ['feature', 1, '超取訂單的店址存為運送地址', 8],
            ['feature', 1, '超取託運單號顯示與寄信', 8],
        ],
    ],
    [
        'version' => '1.8.0',
        'date'    => '2020/06/24',
        'list'    => [
            ['feature', 5, '移除佈景betheme', 7],
            ['feature', 4, '新增佈景oceanwp', 7],
            ['feature', 4, '新增elementor外掛', 7],
        ],
    ],
    [
        'version' => '1.7.9',
        'date'    => '2020/06/24',
        'list'    => [
            ['feature', 5, '移除舊自定義金物流外掛', 8],
            ['feature', 4, '建立新自定義金物流外掛', 8],
            ['feature', 1, '修改物流編號不屬於會員備註(不寄信)', 8],
        ],
    ],
    [
        'version' => '1.7.8',
        'date'    => '2020/02/03',
        'list'    => [
            ['feature', 2, '聯絡電話只接受手機格式', 7],
            ['feature', 2, '修正超商取貨付款自動扣庫存', 7],
            ['feature', 1, '超商取貨新增狀態已出貨,買家取貨訂單自動改完成', 7],
            ['feature', 4, 'Classic Editor 1.5', 7],
            ['feature', 1, '更新 Wordpress 5.3.2(更新BeTheme佈景及所有外掛)', 7],
        ],
    ],
    [
        'version' => '1.7.7',
        'date'    => '2019/10/26',
        'list'    => [
            ['dev', 1, '加入lodash js，並在前台載入', 1],
            ['dev', 2, '重構自定義物流功能', 1],
            ['feature', 1, '自動更新購物車的動作加入debounce', 1],
            ['feature', 1, '訂單取消恢復庫存', 1],
            ['feature', 1, '革命滑塊加入常用動態圖片(svg)', 5],
            ['feature', 2, '修正綠界超商物流在後台的訂單動作按鈕事件異常的問題', 1],
        ],
    ],
    [
        'version' => '1.7.6',
        'date'    => '2019/9/12',
        'list'    => [
            ['feature', 1, '更新betheme翻譯檔', 1],
            ['feature', 1, '新增自動取消未付款訂單的機制', 1],
            ['feature', 1, '移除後台post列表頁不必要的欄位，調整後列表手機板樣式', 5],
            ['feature', 2, '修正使得top bar的搜尋欄位支援商品搜尋及wpml', 1],
            ['feature', 2, '修正Yithc紅利點數派發時間為立即發送', 1],
            ['feature', 4, '新增外掛 - WP Chatbot(FB Messenger功能)', 1],
            ['feature', 5, '移除外掛 - FB Messenger Live Chat(FB Messenger功能)', 1],
            ['dev', 2, '修正betheme載入後台css與js的時機', 1],
        ],
    ],
    [
        'version' => '1.7.5',
        'date'    => '2019/5/20',
        'list'    => [
            ['feature', 5, '移除買A送B外掛(Lohasit WC Buy A Get B  for wcommerce)', 1],
            ['feature', 3, '更新FB live messenger外掛 1.4.1 => 1.4.8', 1],
        ],
    ],
    [
        'version' => '1.7.4',
        'date'    => '2019/4/10',
        'list'    => [
            ['feature', 2, '訂單匯出功能加入訂單備註欄位，修正css沒載入的問題，調整匯出欄位順序', 1],
            ['dev', 2, '整理壓板系統自行開發的外掛資訊及檔案名稱', 1],
        ],
    ],
    [
        'version' => '1.7.3',
        'date'    => '2019/3/14',
        'list'    => [
            ['feature', 2, '修正WooCommerce選單的翻譯方式', 1],
            ['feature', 3, '更新Adminimize外掛 1.11.2 => 1.11.4', 1],
            ['feature', 3, '更新Post SMTP外掛 1.9.4 => 1.9.8', 1],
            ['feature', 3, '更新 W3 Total Cache外掛 0.9.5.4 => 0.9.7.2', 1],
            ['dev', 2, '調整翻譯選單的程式碼', 1],
        ],
    ],
    [
        'version' => '1.7.2',
        'date'    => '2019/2/25',
        'list'    => [
            ['feature', 2, '修正購物紀錄無法查詢的問題', 1],
            ['feature', 2, '修正hide plugin updates notifications外掛中，不正常enqueue scripts的問題', 1],
            ['dev', 1, '加入自動載入css&js檔案的功能', 1],
        ],
    ],
    [
        'version' => '1.7.1',
        'date'    => '2019/1/31',
        'list'    => [
            ['feature', 2, '解決Postman SMTP無法正常取得Google OAuth認證的問題', 5],
            ['feature', 2, '年度統計報表改為只統計已完成訂單', 1],
        ],
    ],
    [
        'version' => '1.7.0',
        'date'    => '2019/1/18',
        'list'    => [
            ['feature', 1, '更新ARForms翻譯', 1],
            ['feature', 1, '修正ARForm匯出的中文CSV無法直接用Excel開啟的問題', 1],
            ['feature', 1, '未銷售商品列表加入匯出功能,修改為多國語系', 1],
            ['feature', 1, '針對非管理員隱藏後台Admin bar上的語系切換及Simply Show Hooks選單', 1],
            ['feature', 1, '加強訂單/顧客資料匯出外掛的可客制性, 全面改為多語架構', 1],
            ['feature', 3, '修改綠界物流自訂filter順序的位置,解決送出物流訂單驗證碼錯誤的問題', 5],
            ['feature', 4, '新增社群分享按鈕外掛 Add to Any (1.7.33)', 1],
            ['feature', 5, '移除外掛Wp Product Refund', 1],
            ['feature', 5, '移除外掛Wp Product Bundle', 1],
            ['dev', 2, '修正將網址從HTML移除掉的方法', 1],
        ],
    ],
    [
        'version' => '1.6.1',
        'date'    => '2019/1/10',
        'list'    => [
            ['feature', 1, '修改ARforms選單翻譯', 1],
            ['feature', 1, '修改CSV Import suite選單翻譯', 1],
        ],
    ],
    [
        'version' => '1.6.0',
        'date'    => '2019/1/9',
        'list'    => [
            ['feature', 4, '加入/會員購物紀錄活躍度列表功能外掛 WC Customer Shopping List & Activity Ratios 1.0.0', 1],
            ['feature', 4, '加入未銷售商品列表外掛 WC Unsold Product List 1.0.0', 1],
        ],
    ],
    [
        'version' => '1.5.0',
        'date'    => '2019/1/9',
        'list'    => [
            ['feature', 1, '新增會員資料匯出功能', 1],
            ['feature', 5, '刪除舊的匯出訂單外掛 WooCommerce Customer/Order CSV Export', 1],
            ['feature', 5, '刪除多餘的手機號碼欄位外掛 Wc Shipping Mobilephone', 1],
            ['feature', 5, '刪除撿貨清單匯出外掛 Wc Picking List Exportation', 1],
        ],
    ],
    [
        'version' => '1.4.0',
        'date'    => '2019/1/8',
        'list'    => [
            ['feature', 3, '更新ARForms外掛 2.7.8 => 3.6', 1],
            ['feature', 4, '新增訂單匯出的外掛 - WC Export Orders 2.0', 1],
            ['feature', 5, '刪除撿貨單外掛', 1],
            ['dev', 1, '從HTML的資源連結中移除網站網址資訊', 1],
            ['dev', 2, 'Betheme字型選項讀取新增的自訂字型檔', 1],
            ['dev', 2, 'plugins資料夾加入空白的index.php', 1],
        ],
    ],
    [
        'version' => '1.3.2',
        'date'    => '2018/11/26',
        'list'    => [
            ['feature', 1, '會員升級外掛，去掉“會員降級“此區塊', 6],
            ['feature', 1, '新增Google SSO登入外掛 - Lohasit Google OAuth 0.0.1，壓板加入SSO登入機制', 6],
            ['feature', 1, 'Betheme新增至五組自訂字型檔', 1],
            ['feature', 2, '修正YITH動態定價無法正常使用', 5],
            ['dev', 1, '壓板git新增mo檔', 1],
            ['dev', 1, '壓板git安裝composer套件 - google/apiclient', 1],
            ['dev', 1, '壓板git新增缺少的核心翻譯檔', 1],
        ],
    ],
    [
        'version' => '1.3.1',
        'date'    => '2018/10/11',
        'list'    => [
            ['feature', 1, '調整三個YITH外掛的選單位置，紅利點數及動態定價移至(優惠設定)內，願望清單移至(Woocommerce)內', 5],
            ['feature', 1, '翻譯:繁中-woocommerce-product-csv-import-suite', 5],
            ['feature', 3, '社群登入外掛Social Login WordPress Plugin - AccessPress Social Login Lite 3.2.7 => 3.3.8', 5],
        ],
    ],
    [
        'version' => '1.3.0',
        'date'    => '2018/09/25',
        'list'    => [
            ['dev', 1, '判斷字串內是否有中文的輔助函式', 1],
            ['dev', 1, '角色的body class(從原本custom-admin-css外掛中移除此功能)', 1],
            ['feature', 1, '綠界超商物流送出的訂單資料，收貨人姓名調整中英文順序', 1],
            ['feature', 1, '綠界宅配物流送出的訂單資料，收貨人姓名調整中英文順序', 1],
            ['feature', 1, 'WordPress從文章內容產生的摘要內的點點點樣式', 1],
            ['feature', 1, '綠界物流判斷使用設備類型，顯示相對應之電子地圖畫面', 4],
            ['feature', 2, 'MailPoet 3.6.2 - wordpress4.7之前版本的相容問題(缺少get_user_locale函式)', 5],
            ['feature', 3, 'advanced-floating-content，3.2.4 => 3.4.4', 1],
            ['feature', 3, 'FB Messenger Live Chat 外掛，1.3.3 =>1.4.1', 1],
            ['feature', 3, 'WP Real Media Library外掛，3.4.5 => 4.0.6', 1],
            ['feature', 3, 'MailPoet(3.6.2)、 YITH動態定價(1.4.5)、YITH紅利點數(1.5.3)', 5],
            ['feature', 4, 'WP REST API 2.0外掛，(real media library 4.0.6所需)', 1],
            ['feature', 5, 'updraftplus', 1],
        ],
    ],
    [
        'version' => '1.2.3',
        'date'    => '2018/08/06',
        'list'    => [
            ['feature', 2, '修正收取物流通知的方法', 1],
            ['feature', 2, '修正WordPress Editor的翻譯錯誤，列->行', 1],
            ['feature', 2, '修正綠界超商及宅配物流免運門檻計算方法', 1],
            ['feature', 2, '超商取貨付款金流，訂單狀態改為正在處理中', 1],
            ['feature', 3, '更新願望清單至2.2.1版', 2],
        ],
    ],
    [
        'version' => '1.2.2',
        'date'    => '2018/07/06',
        'list'    => [
            ['dev', 1, '新增後台CSS檔案並載入，位置betheme-child/assets/css/admin-css.css', 1],
            ['dev', 1, '前台copyright文字的方法，新增自訂hook', 1],
            ['feature', 2, '將Woocommerce預設的報表匯出CSV功能加入BOM檔頭，使其支援中文', 1],
            ['feature', 2, '修復WooCommerce Customer/Order CSV Export中，匯出結束時間的計算BUG', 1],
            ['feature', 2, '修正綠界金流的導回網址', 1],
            ['feature', 2, '修正紅利點數在可變商品無法設定百分比的問題', 1],
        ],
    ],
    [
        'version' => '1.2.1',
        'date'    => '2018/06/12',
        'list'    => [
            ['dev', 2, '修正更改姓名順序的程式碼', 1],
            ['dev', 2, '修正Betheme的mfn_get_authors方法，提高效能', 1],
            ['feature', 1, '紀錄物流狀態的所有通知到訂單備註', 1],
            ['feature', 1, '修改預設footer及logo為公司資訊', 1],
            ['feature', 1, '翻譯後台選單(MailPoet，Slider Revolution，WooCommerce)', 1],
            ['feature', 3, '更新 Manage WP worker至4.5.0', 1],
            ['feature', 4, '續約管理外掛 Lohasit Maintaining Service Client', 1],
        ],
    ],
    [
        'version' => '1.2.0',
        'date'    => '2018/05/29',
        'list'    => [
            ['dev', 2, '修改電子發票外掛名稱', 1],
            ['feature', 1, '使後臺訂單搜尋支援中文全姓名', 1],
            ['feature', 1, '更改woocommerce密碼強度限制至弱', 1],
            ['feature', 1, '當紅利點數可用折扣金額為0時，隱藏購物車及結帳頁折抵訊息', 1],
            ['feature', 1, '新增庫存匯出功能，庫存報表支援顯示中文', 1],
            ['feature', 3, '更新 Slider Revolution 至 5.4.7.3', 4],
            ['feature', 4, '新增WC Statistics Report外掛-新增自訂報表', 1],
        ],
    ],
    [
        'version' => '1.1.7',
        'date'    => '2018/05/02',
        'list'    => [
            ['feature', 1, '結帳頁重新選取超商運送方式時，重設超商資訊(選擇從統一->全家 或 全家->統一時)', 1],
            ['feature', 1, '結帳完紀錄綠界超商資訊到訂單備註', 1],
            ['feature', 2, '修正ecpay物流外掛，回傳網址錯誤的問題', 1],
            ['feature', 2, '修正動態定價優惠券與其他外掛優惠卷使用時造成的衝突問題(filter沒回傳值)', 1],
            ['feature', 5, '移除 mail from外掛', 1],
        ],
    ],
    [
        'version' => '1.1.6',
        'date'    => '2018/03/27',
        'list'    => [
            ['dev', 2, '將壓板自訂程式碼檔案 lohas-custom.php移到子主題', 1],
            ['feature', 1, '讓WooCommerce Customer/Order CSV Export匯出的csv檔加上BOM檔頭，可以直接用Excel開啟而不會出現亂碼', 1],
            ['feature', 1, '新增Mailpoet選項，讓MailPoet使用WP Mail', 1],
            ['feature', 1, 'woocommerce-points-and-rewards 可折抵紅利做無條件捨去小數點', 1],
            ['feature', 1, '移除Admin bar上非管理員看得到的項目，包含wp-logo與w3 total cache的項目', 1],
            ['feature', 2, '調整Woocommerce Zoom Magnifier Premium產生的圖片藝廊Wrapper高度', 1],
            ['feature', 2, '修正Mailpoet 使用WP Mail發送HTML信件的header沒設定的問題', 1],
            ['feature', 2, '修正wc-ecpay-c2c-logistic接收通知的問題，產生物流訂單及取貨後，會新增備註到訂單', 1],
            ['feature', 3, '更新Real Media Library至 3.4.5', 1],
        ],
    ],
    [
        'version' => '1.1.5',
        'date'    => '2017/11/27',
        'list'    => [
            ['dev', 2, '將壓板功能在functions.php的程式碼移至lohasit-custom.php並優化部分程式碼', 1],
            ['dev', 2, '加入style-static.css到betheme-child', 1],
            ['feature', 3, '更新壓板版本號外掛', 1],
        ],
    ],
    [
        'version' => '1.1.4',
        'date'    => '2017/10/20',
        'list'    => [
            ['feature', 2, '修正會員單筆消費無法升級 以及會員升級頁面顯示問題', 2],
            ['feature', 2, '1.批次管理刪除 2.修正無法產生物流單號問題', 2],
            ['feature', 2, '修正google+無法登入', 2],
            ['feature', 2, '修正warning :implode() invalid arguement', 2],
            ['feature', 3, '電子發票外掛allpay-ecpay-e-invoice 更新至版本1.4.1', 1],
            ['feature', 4, '加入WPML外掛', 1],
        ],
    ],
    [
        'version' => '1.1.3',
        'date'    => '2017/10/20',
        'list'    => [
            ['feature', 2, '移除將price hash打亂的程式碼段', 1],
            ['feature', 5, '移除WP Admin UI & Visitor Map Who\'s Online外掛', 1],
        ],
    ],
    [
        'version' => '1.1.2',
        'date'    => '2017/10/06',
        'list'    => [
            ['feature', 1, '修正購物車頁面 Apply coupon 沒被翻譯到的問題', 2],
            ['feature', 2, '解決登入時因顯示admin bar而使網頁下方出現一條空白區域的問題', 1],
            ['feature', 4, '新增yoast seo 外掛', 2],
        ],
    ],
    [
        'version' => '1.1.1',
        'date'    => '2017/09/28',
        'list'    => [
            ['feature', 1, '修改自定義物流，可以下拉選擇國家', 2],
            ['feature', 1, '修改自定義綠界物流，可以跟著WC-設定-運送方式中的物流同步', 2],
            ['feature', 1, '綠界超商宅配改成固定顯示在上面運送方式列做設定', 2],
            ['feature', 1, '加入結帳手機號碼欄位外掛', 2],
            ['feature', 1, '讓後台訂單列表顯示金流付款方式 & 修改綠界金流結帳欄位顯示方式', 2],
            ['feature', 1, '移除在會員中心裡的會員階級內容', 1],
            ['feature', 1, '移除壓版不必要且有可能造成衝突的script', 1],
            ['feature', 2, '修正購物車頁面選單不正常掉落的問題', 1],
            ['feature', 2, '修正購物車頁的自定義物流出不來', 2],
            ['feature', 2, '修正送到綠界時間晚8hr / Nelly修改綠界物流外掛 產生物流單號 原本只能第一個id能產生 改成每筆訂單都ok', 2],
        ],
    ],
    [
        'version' => '1.1.0',
        'date'    => '2017/09/11',
        'list'    => [
            ['dev', 2, '版本號外掛刪除多餘空行', 1],
            ['feature', 1, '自定義物流修正成標題可以自定義', 2],
            ['feature', 1, '新增全館商品優惠 商品頁都有折扣顯示(wp_product_bundle) 新增退貨機制', 2],
            ['feature', 1, '常溫-當成預設運送選項，拿掉無運送選項', 3],
            ['feature', 1, '買A送B-搜尋商品ID加上商品名稱', 3],
            ['feature', 1, '修改綠界金流設定-點擊文字即可看到設定內容(在title上加入', 3],
            ['feature', 1, '修改綠界金流設定-paypal選項不讓客戶勾選', 3],
            ['feature', 1, '買A送B禮物加上醒目標籤', 1],
            ['feature', 1, '超取建立出貨單的姓名改成抓取運送地址欄位的姓名，原本是帳單的姓名', 2],
            ['feature', 1, '修改全館商品可以在商品頁出現折扣', 3],
            ['feature', 1, '修改全館商品優惠，假如出現負值，就改成0', 2],
            ['feature', 1, '拿掉會員中心的圓圈', 1],
            ['feature', 1, '修正並移除優化內容內造成錯誤的無用js', 1],
            ['feature', 1, '修正並移除優化內容內造成錯誤的無用js(多餘的jquery.min.js', 1],
            ['feature', 2, '修正綠界宅配 異常出現在訂單問題', 2],
            ['feature', 2, '修正超取和電子發票 訂單欄位重複問題', 2],
            ['feature', 2, '修正"我的帳號"中螢幕小於1326px時，右邊的排版會亂掉的問題', 3],
            ['feature', 2, '解決WooCommerce Customer/Order CSV Export下載檔案時檔頭被加上BOM而無法正常開啟的問題', 1],
            ['feature', 4, '新增login-customizer外掛', 1],
            ['feature', 4, '新增撿貨單外掛Wc Picking List Exportation', 2],
        ],
    ],
]
?>
<!--
▐████████████████████████████▌
▐────────────────────────▄───▌
▐───────────────────────██───▌
▐─────────────────────▄████──▌
▐────────────────────▐█████──▌
▐───────────────────▐███████─▌
▐─────────────────▄█████████─▌
▐───────────────▄████████████▌
▐▄───────────▄███████████████▌
▐██████▄──▄██████████████████▌
▐████████████████████████████▌
▐─███████████████████████████▌
▐──██████████████████████████▌
▐───█████████████████████████▌
▐────████████████████████████▌
▐─────████████████████▀─█──▄█▌
▐──────███████████████▌─█─▐██▌
▐──────████████████████▄▄▄███▌
▐──────████▀▀─█─█████████████▌
▐──────████▌──█─█████████████▌
▐───────█████▄▄▄█████████████▌
▐────────████████████████████▌
▐─────────████████▄▀▀▌███████▌
▐──────────▀█████████▄███████▌
▐────────────▀███████████████▌
▐───────────────▀████████████▌
▐───────────────────────▀▀▀▀█▌
▐───────────────────────────█▌
▐────────────────────────────▌
▐────────────────────────────▌
▐████████████████████████████▌
-->
<div class="lohasit-versions">
    <?php foreach ($data as $d): ?>
        <div class="block">
            <div class="header">
                <div class="version"><?=$d['version']?></div>
                <div class="date"><?=$d['date']?></div>
            </div>
            <div class="data">
                <?php foreach ($d['list'] as $l): ?>
                <div class="item">
                    <div class="content"><strong><?=$types[$l[0]][$l[1]]?>:</strong> <?=$l[2]?></div>
                    <div class="author"><?=$authors[$l[3]]?></div>
                </div>

                <?php endforeach;?>
            </div>
        </div>
    <?php endforeach;?>
</div>
<script>

    console.log(
        '%c'+
    '▐████████████████████████████▌\n'+
    '▐────────────────────────▄───▌\n'+
    '▐───────────────────────██───▌\n'+
    '▐─────────────────────▄████──▌\n'+
    '▐────────────────────▐█████──▌\n'+
    '▐───────────────────▐███████─▌\n'+
    '▐─────────────────▄█████████─▌\n'+
    '▐───────────────▄████████████▌\n'+
    '▐▄───────────▄███████████████▌\n'+
    '▐██████▄──▄██████████████████▌\n'+
    '▐████████████████████████████▌\n'+
    '▐─███████████████████████████▌\n'+
    '▐──██████████████████████████▌\n'+
    '▐───█████████████████████████▌\n'+
    '▐────████████████████████████▌\n'+
    '▐─────████████████████▀─█──▄█▌\n'+
    '▐──────███████████████▌─█─▐██▌\n'+
    '▐──────████████████████▄▄▄███▌\n'+
    '▐──────████▀▀─█─█████████████▌\n'+
    '▐──────████▌──█─█████████████▌\n'+
    '▐───────█████▄▄▄█████████████▌\n'+
    '▐────────████████████████████▌\n'+
    '▐─────────████████▄▀▀▌███████▌\n'+
    '▐──────────▀█████████▄███████▌\n'+
    '▐────────────▀███████████████▌\n'+
    '▐───────────────▀████████████▌\n'+
    '▐───────────────────────▀▀▀▀█▌\n'+
    '▐───────────────────────────█▌\n'+
    '▐────────────────────────────▌\n'+
    '▐──────────kroutony──────────▌\n'+
    '▐████████████████████████████▌\n',
        'background: #bfbfbf; color: black');



</script>
