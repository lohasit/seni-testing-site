<?php
/*
 * Plugin Name: Lohasit Version
 * Description: 樂活壓板系統 - 壓板版本號
 * Version: 4.1.4
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 */
?>
<?php
if (!function_exists('get_plugin_data')) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}
$data = get_plugin_data(__FILE__);
define('LOHASIT_VERSION', $data['Version']);
class LohasItVersion
{
    private static $instance;
    public function __construct()
    {
        $this->hooks();
    }
    private function hooks()
    {
        add_action('admin_menu', array($this, 'menuBuild'));
        add_action('admin_head', array($this, 'adminHeadStyle'));
    }
    public function menuBuild()
    {
        if (current_user_can('administrator')) {
            $title = '壓版版本' . LOHASIT_VERSION;
            add_menu_page($title, $title, 'manage_options', 'liv_menu', array($this, 'LivMenuPage'), '', 0);
        }
    }
    public function adminHeadStyle()
    {
        ?>
        <link rel="stylesheet" href="<?=plugin_dir_url(__FILE__) . 'asset/css/lohasit-version.css'?>">
        <?php

    }
    public function LivMenuPage()
    {
        include_once 'content.php';
    }
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
add_action('plugins_loaded', array('LohasItVersion', 'getInstance'));
?>
