<?php
defined('ABSPATH') or die('oops');

class Lohasit_Wc_Custom_Shipping_Method_1 extends Lohasit_Wc_Custom_Shipping_Method_Base
{
    public function __construct($instance_id = 0)
    {
        $this->id           = 'lohasit_wc_custom_shipping_method_1';
        $title              = $this->get_option('title');
        $this->method_title = $title ? $title : '自定義物流1';
        $this->title        = $title;
        $this->enabled      = $this->get_option('enabled');
        $this->fee          = $this->get_option('fee');
        $this->availability = $this->get_option('availability');
        $this->countries    = $this->get_option('countries');
        $this->free_shipping_amount = $this->get_option('free_shipping_amount');

        $this->init();
    }
}