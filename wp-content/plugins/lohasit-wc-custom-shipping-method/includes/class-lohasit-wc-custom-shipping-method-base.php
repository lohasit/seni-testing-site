<?php
defined('ABSPATH') or die('oops');

abstract class Lohasit_Wc_Custom_Shipping_Method_Base extends WC_Shipping_Method
{
    protected $free_shipping_amount;

    public function init_form_fields()
    {
        $this->form_fields = [
            'enabled' => [
                'title'   => __('Enable', 'lcsm'),
                'type'    => 'checkbox',
                'label'   => __('Enable', 'lcsm'),
                'default' => 'no',
            ],
            'title' => [
                'title'       => __('Method title', 'lcsm'),
                'type'        => 'text',
                'description' => __('Title to be displayed', 'lcsm'),
                'default'     => '自定義物流',
            ],
            'availability' => [
                'title'   =>  __('Country availability', 'lcsm'),
                'type'    => 'select',
                'options' => [
                    'all'       => __('All shipping countries are available', 'lcsm'),
                    'including' => __('Including from selected countries', 'lcsm'),
                    'excluding' => __('Excluding from selected countries', 'lcsm'),
                ]
            ],
            'countries' => [
                'title' => __('Countries', 'lcsm'),
                'class' => 'wc-enhanced-select',
                'type' => 'multiselect',
                'options' => WC()->countries->get_allowed_countries()
            ],
            'fee' => [
                'title'   => __('Fee', 'lcsm'),
                'type'    => 'text',
            ],
            'free_shipping_amount' => [
                'title'   => __('Order amount of free shipping', 'lcsm'),
                'type'    => 'text',
            ]
        ];
    }

    protected function init()
    {
        $this->init_form_fields();
        $this->init_settings();
        add_action("woocommerce_update_options_shipping_{$this->id}", [$this, 'process_admin_options']);
    }

    public function calculate_shipping($package = [])
    {
        $fee = ($this->free_shipping_amount && (int)WC()->cart->cart_contents_total >= (int)$this->free_shipping_amount)
            ? 0
            : (int)$this->fee;
        $rate = array(
            'id' => $this->id,
            'label' => $this->title,
            'cost' => $fee,
            'calc_tax' => 'per_item'
        );

        $this->add_rate($rate);
    }
}