<?php
/*
 * Plugin Name: Lohasit WC Custom Shipping Method
 * Description: 樂活壓板系統 - Woocommerce - 自定義物流
 * Version: 1.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
*/
defined('ABSPATH') or die('oops');

class Lohasit_Custom_Shipping_Methods
{
    private static $instance;

    public static function get_instance()
    {
        if(is_null(self::$instance))
            self::$instance = new self;
        return self::$instance;
    }

    public function __construct()
    {
        load_plugin_textdomain('lcsm', '', plugin_basename( dirname( __FILE__ ) ) . '/languages/');
        $this->require_files();
        $this->register_hooks();
    }

    private function register_hooks()
    {
        add_filter('woocommerce_shipping_methods', [$this, 'add_shipping_method']);
        add_action('woocommerce_shipping_init', [$this, 'require_shipping_method']);
    }

    private function require_files()
    {
    }

    public function require_shipping_method()
    {
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-base.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-1.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-2.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-3.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-4.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-5.php';
        require_once plugin_dir_path(__FILE__) . '/includes/class-lohasit-wc-custom-shipping-method-6.php';
    }

    public function add_shipping_method($methods)
    {
        $methods['Lohasit_Wc_Custom_Shipping_Method_1'] = 'Lohasit_Wc_Custom_Shipping_Method_1';
        $methods['Lohasit_Wc_Custom_Shipping_Method_2'] = 'Lohasit_Wc_Custom_Shipping_Method_2';
        $methods['Lohasit_Wc_Custom_Shipping_Method_3'] = 'Lohasit_Wc_Custom_Shipping_Method_3';
        $methods['Lohasit_Wc_Custom_Shipping_Method_4'] = 'Lohasit_Wc_Custom_Shipping_Method_4';
        $methods['Lohasit_Wc_Custom_Shipping_Method_5'] = 'Lohasit_Wc_Custom_Shipping_Method_5';
        $methods['Lohasit_Wc_Custom_Shipping_Method_6'] = 'Lohasit_Wc_Custom_Shipping_Method_6';
        return $methods;
    }
}
Lohasit_Custom_Shipping_Methods::get_instance();