jQuery.fn.extend({
    addCols:function(xs, sm, md, lg) {
        if(xs > 0 && xs <= 12) {
            jQuery(this).addClass('col-xs-' + xs);
        }
        if(sm > 0 && sm <= 12) {
            jQuery(this).addClass('col-sm-' + sm);
        }
        if(md > 0 && md <= 12) {
            jQuery(this).addClass('col-md-' + md);
        }
        if(lg > 0 && lg <= 12) {
            jQuery(this).addClass('col-lg-' + lg);
        }
    },
    removeCols:function(xs, sm, md, lg) {
        if(xs > 0 && xs <= 12) {
            jQuery(this).removeClass('col-xs-' + xs);
        }
        if(sm > 0 && sm <= 12) {
            jQuery(this).removeClass('col-sm-' + sm);
        }
        if(md > 0 && md <= 12) {
            jQuery(this).removeClass('col-md-' + md);
        }
        if(lg > 0 && lg <= 12) {
            jQuery(this).removeClass('col-lg-' + lg);
        }
    },
    addRow:function(){
        jQuery(this).addClass('row');
    },
    removeRow:function(){
        jQuery(this).removeClass('row');
    }
});