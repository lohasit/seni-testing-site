<?php

/**
 * Created by PhpStorm.
 * User: ymlin
 * Date: 2018/2/6
 * Time: 上午11:43
 */
class Chir_Statistics
{
    public static function customer_order_month_activity($user_id,$date) {
        // Date calculations to limit the query
        $today_year = $date["year"];
        $today_month = $date["month"];
        $next_year = 0;
        $next_month = 0;
        $day = date( 'd' );
        if ($today_month == '12') {
            $next_month = '01';
            $next_year = $today_year+1;
        } else{
            $next_month = $today_month+1;
            $next_year = $today_year;
        }

        // The query arguments
        $args = array(
            // WC orders post type
            'post_type'   => 'shop_order',
            // Only orders with status "completed" (others common status: 'wc-on-hold' or 'wc-processing')
            'post_status' => array( 'wc-completed' ),
            // all posts
            'numberposts' => -1,
            // for current user id
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'date_query' => array(
                //orders published on last 30 days
                array(
                    'after'     => array(
                        'year'  => $today_year,
                        'month' => $today_month,
                    ),
                    'before'    => array(
                        'year'  => $next_year,
                        'month' => $next_month,
                    ),
                    'inclusive' => true,
                ),
            ),
        );

        // Get all customer orders
        $customer_orders =  (new WP_Query( $args ))->get_posts();
        return sizeof($customer_orders);

    }
    public static function customer_order_quarter_activity($user_id,$date) {

        // Date calculations to limit the query
        $today_year = $date["year"];
        $today_month = $date["month"];
        $next_year = 0;
        $next_month = 0;
        $day = date( 'd' );
        $Q1 = array("01", "02", "03");
        $Q2 = array("04", "05", "06");
        $Q3 = array("07", "08", "09");
        $Q4 = array("10", "11", "12");
        if (in_array($today_month, $Q1)){
            $today_month = "01";
            $next_month = "03";
        }
        else if (in_array($today_month, $Q2)){
            $today_month = "04";
            $next_month = "06";
        }
        else if (in_array($today_month, $Q3)){
            $today_month = "07";
            $next_month = "09";
        }
        else if (in_array($today_month, $Q4)){
            $today_month = "10";
            $next_month = "12";
        }


        // The query arguments
        $args = array(
            // WC orders post type
            'post_type'   => 'shop_order',
            // Only orders with status "completed" (others common status: 'wc-on-hold' or 'wc-processing')
            'post_status' => array( 'wc-completed' ),
            // all posts
            'numberposts' => -1,
            // for current user id
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'date_query' => array(
                //orders published on last 30 days
                array(
                    'after'     => array(
                        'year'  => $today_year,
                        'month' => $today_month,
                    ),
                    'before'    => array(
                        'year'  => $today_year,
                        'month' => $next_month,
                    ),
                    'inclusive' => true,
                ),
            ),
        );

        // Get all customer orders
        $customer_orders =  (new WP_Query( $args ))->get_posts();
        return sizeof($customer_orders) / 3;

    }
    public static function customer_order_year_activity($user_id,$date) {
        // Date calculations to limit the query
        $today_year = $date["year"];
        $next_year = $today_year+1;

        // The query arguments
        $args = array(
            // WC orders post type
            'post_type'   => 'shop_order',
            // Only orders with status "completed" (others common status: 'wc-on-hold' or 'wc-processing')
            'post_status' => array( 'wc-completed' ),
            // all posts
            'numberposts' => -1,
            // for current user id
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'date_query' => array(
                //orders published on last 30 days
                array(
                    'after'     => array(
                        'year'  => $today_year,

                    ),
                    'before'    => array(
                        'year'  => $next_year,

                    ),
                    'inclusive' => true,
                ),
            ),
        );

        // Get all customer orders
        $customer_orders =  (new WP_Query( $args ))->get_posts();
        return sizeof($customer_orders) / 12;

    }
    /*單一會員購買紀錄*/
    public static function every_mem_buy_record($search_type, $search_info, $order_by, $order, $page, $limit, $date) {


        global $wpdb;
        $table_user = $wpdb->prefix . 'users';
        $table_usermeta = $wpdb->prefix . 'usermeta';
        $capabilities = $wpdb->prefix . 'capabilities';

        $sql_1 = "SELECT
			b.*
			FROM
			(SELECT
				A.ID as u_id,
				A.user_login as account,
				B.meta_value as role,
				C.meta_value as name,
				D.meta_value as email
			FROM
				$table_user A ,
             	(SELECT * FROM $table_usermeta WHERE meta_key='$capabilities') B,
				(SELECT * FROM $table_usermeta WHERE meta_key='billing_first_name') C ,
				(SELECT * FROM $table_usermeta WHERE meta_key='billing_email') D
 where
				A.ID = B.user_id AND 
                A.ID = C.user_id AND
				A.ID = D.user_id 				
			) b";



        $sql = "";
        $sql = $sql . $sql_1;

        if ($search_type == 1) {
            $sql_search = " where b.u_id LIKE '%$search_info%' ";
            $sql = $sql . $sql_search;
        } else if ($search_type == 2) {
            $sql_search = " where b.name LIKE '%$search_info%'";
            $sql = $sql . $sql_search;
        }
        else if ($search_type == 3) {
            $sql_search = " where b.email LIKE '%$search_info%' ";
            $sql = $sql . $sql_search;
        }

        if ($order_by != '' && $order != '') {
            $sql_order = " order by b.$order_by $order ";
            $sql = $sql . $sql_order;
        }
        $sql_count = $sql;
        $result_count = $wpdb->get_results($sql_count);
        if ($page != '' && $limit != '') {
            $pages = 0 + ($page - 1) * $limit;
            $sql_limit = "  limit $pages,$limit";
            $sql = $sql . $sql_limit;
        }


        $results = $wpdb->get_results($sql);

        foreach($results as $key => $result){
            $u_id = $result->u_id;
            $results[$key]->month_buy = self::customer_order_month_activity($u_id,$date);
            $results[$key]->quarter_buy = self::customer_order_quarter_activity($u_id,$date);
            $results[$key]->year_buy = self::customer_order_year_activity($u_id,$date);
        }

        return array(
            'results' => $results,
            'count' => count($result_count)
        );

    }
    /*單一會員購買商品排序*/
    public static function one_mem_product_order($customer_id, $search_type, $search_info, $order_by, $order, $page, $limit) {

        global $wpdb;
        $table_order_detail = $wpdb->prefix . 'mem_order_detail';
        $post = $wpdb->prefix . 'posts';

        $sql_1 = "SELECT
			b.*
			from
			(SELECT @rownum := @rownum+1 AS 'rank', a.* FROM (SELECT `user_id`, product_id,product_name, SUM(product_quantity) as total,SUM(product_line_total) as line_total FROM $table_order_detail where user_id='$customer_id' and  order_id in(SELECT ID FROM $post WHERE post_status='wc-completed')
			";
        $sql_2 = " GROUP BY product_id,`user_id` order by total DESC) a, (SELECT @rownum := 0) r ) b ";
        $sql = "";

        $term_relationship = $wpdb->prefix . 'term_relationships';
        $mem_order = $wpdb->prefix . 'mem_order';

        if ($search_type == 1) {
            $sql = $sql_1 . $sql_2;
            $sql_search = " where b.product_id='$search_info'";
            $sql = $sql . $sql_search;
        } else if ($search_type == 2) {
            $sql = $sql_1 . $sql_2;
            $sql_search = " WHERE product_id in (SELECT object_id FROM $term_relationship WHERE term_taxonomy_id='$search_info' ) ";
            $sql = $sql . $sql_search;
        } else if ($search_type == 3) {
            $search_date = explode("+", $search_info);
            $from = $search_date[0];
            $to = $search_date[1];
            $date_to = new DateTime($to);
            date_modify($date_to, '+1 day');
            $to = $date_to->format('Y-m-d H:i:s');
            if ($from != '' && $to != '') {
                $sql_search = " and `order_id` in (SELECT order_id FROM $mem_order WHERE order_status='completed' and `order_date`<'$to' and `order_date` >= '$from') ";
                $sql = $sql_1 . $sql_search . $sql_2;
            } else if ($from != '' && $to == '') {
                $sql_search = " and `order_id` in (SELECT order_id FROM $mem_order WHERE order_status='completed' and `order_date` >='$from') ";
                $sql = $sql_1 . $sql_search . $sql_2;
            } else if ($from == '' && $to != '') {
                $sql_search = " and `order_id` in (SELECT order_id FROM $mem_order WHERE order_status='completed' and `order_date` <'$to') ";
                $sql = $sql_1 . $sql_search . $sql_2;
            }

        } else {
            $sql = $sql_1 . $sql_2;
        }
        if ($order_by != '' && $order != '') {
            $sql_order = " order by b.$order_by $order ";
            $sql = $sql . $sql_order;
        }
        $sql_count = $sql;
        $result_count = $wpdb->get_results($sql_count);
        if ($page != '' && $limit != '') {
            $pages = 0 + ($page - 1) * $limit;
            $sql_limit = "  limit $pages,$limit";
            $sql = $sql . $sql_limit;
        }
//
//        var_dump($sql);
//        die();


        $result = $wpdb->get_results($sql);
        foreach ($result as $test) {
            $product_id = $test->product_id;
            $cat_name = array();
            $arr = wp_get_post_terms($product_id, 'product_cat', array('fields' => 'ids'));
            foreach ($arr as $arrs) {
                $category = get_term($arrs, 'product_cat');
                $cat_name[] = $category->name;
            }
            $test->cat_name = $cat_name;

        }
        return array(
            'results' => $result,
            'count' => count($result_count),

        );

    }

}