<?php
/**
 * Plugin Name: Lohasit WC Customer Shopping List & Activity Ratios
 * Description: 樂活壓板系統 - Woocommerce - 會員購物紀錄/活躍度列表
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 * Version: 1.0.0
 */

define('CHIR_STATISTICS_PLUGIN_DIR', plugin_dir_url(__FILE__));

class Chir_Statistics_Plugin
{
    private static $instance;

    public function __construct() {
        $this->includes();
        $this->hooks();
    }

    public static function get_instance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function includes() {
        require_once(plugin_dir_path( __FILE__ ) .'includes/class-chir-memstatistics.php');
    }

    private function hooks() {
		add_action('admin_menu', array($this, 'add_menus'));
    }

	public function add_menus() {
		add_submenu_page('woocommerce', '會員購物紀錄/活躍度', '會員購物紀錄/活躍度', 'manage_woocommerce', 'buying-history', array($this, 'add_buying_history_page'));
    }

	public function get_orderby_url( $name ) {
		$page_url     = get_permalink();
		$order_params = '';
		$page_url .= '?';
		foreach ( $_GET as $key => $value ) {
			if ( ! in_array( $key,array( 'orderby','order','page_num' ) ) ) {
				$page_url .= $key . '=' . $value . '&';
			}
		}
		$order_params .= 'orderby=' . $name . '&';
		if ( isset( $_GET['orderby'] ) && $_GET['orderby'] == $name ) {
			if ( $_GET['order'] == 'asc' ) {
				$order_params .= 'order=desc&';
			} else {
				$order_params .= 'order=asc&';
			}
		} else {
			$order_params .= 'order=asc&';
		}
		$order_params.='page_num=1';
		return $page_url . $order_params;
	}

	public function get_orderby_icon_css( $name ) {
		if ( isset( $_GET['orderby'] ) && $_GET['orderby'] == $name ) {
			if ( $_GET['order'] == 'asc' ) {
				$css = 'order-asc';
			} else {
				$css = 'order-desc';
			}
		} else {
			$css = 'order-asc';
		}

		return $css;
	}

	public function get_next_page_url() {
		$page_url = get_permalink();
		$params   = '?';
		foreach ( $_GET as $key => $value ) {
			if ( $key != 'page_num' ) {
				$params .= $key . '=' . $value . '&';
			}
		}
		if ( isset( $_GET['page_num'] ) ) {
			$page_num = $_GET['page_num']+1;
			$params .= 'page_num=' . $page_num;
		}

		return $page_url . $params;
	}

	public function get_prev_page_url() {
		$page_url = get_permalink();
		$params   = '?';
		foreach ( $_GET as $key => $value ) {
			if ( $key != 'page_num' ) {
				$params .= $key . '=' . $value . '&';
			}
		}
		if ( isset( $_GET['page_num'] ) && $_GET['page_num'] == 1 ) {
			$params .= 'page_num=1';
		} elseif ( isset( $_GET['page_num'] ) && $_GET['page_num'] > 1 ) {
			$page_num = $_GET['page_num']-1;
			$params .= 'page_num=' . $page_num;
		}

		return $page_url . $params;
	}

	public function get_page_num_url( $page_num ) {
		$page_url = get_permalink();
		$params   = '?';
		foreach ( $_GET as $key => $value ) {
			if ( $key != 'page_num' ) {
				$params .= $key . '=' . $value . '&';
			}
		}
		$params .= 'page_num=' . $page_num;

		return $page_url . $params;
	}

	public function get_next_10_page_url( $page_num,$max_page_num ) {
		$tail         = $page_num%10;
		$current_page = ceil( $page_num/10 );
		$page_url     = get_permalink();
		$params       = '?';
		foreach ( $_GET as $key => $value ) {
			if ( $key != 'page_num' ) {
				$params .= $key . '=' . $value . '&';
			}
		}
		if ( $tail == 0 ) {
			$current_page += 1;
		}
		$new_page_num = $current_page*10+$tail;
		if ( $new_page_num > $max_page_num ) {
			$new_page_num = $max_page_num;
		}

		$params .= 'page_num=' . $new_page_num;

		return $page_url . $params;
	}

	public function get_last_10_page_url( $page_num ) {
		$tail         = $page_num%10;
		$current_page = ceil( $page_num/10 );
		$current_page -= 2;
		$page_url = get_permalink();
		$params   = '?';
		foreach ( $_GET as $key => $value ) {
			if ( $key != 'page_num' ) {
				$params .= $key . '=' . $value . '&';
			}
		}
		if ( $tail == 0 ) {
			$current_page += 1;
		}
		$new_page_num = $current_page*10+$tail;
		if ( $page_num >= 1 && $page_num <= 10 ) {
			$params .= 'page_num=1';
		} else {
			$params .= 'page_num=' . $new_page_num;
		}

		return $page_url . $params;
	}

    public function add_buying_history_page() {
		include_once('templates/buying-history.php');
    }
}

add_action('plugins_loaded', array('Chir_Statistics_Plugin', 'get_instance'));