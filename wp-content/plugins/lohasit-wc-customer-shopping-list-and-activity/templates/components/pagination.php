
<nav aria-label="Page navigation">
<ul class="pagination" role="navigation" aria-label="Pagination">
    <?php if($page_num/10>1):?>
        <li class="pagination-previous">
            <a href="<?php echo $this->get_last_10_page_url($page_num);?>">上十頁</a>
        </li>
    <?php endif;?>
    <li class="pagination-previous">
        <a href="<?php echo $this->get_prev_page_url();?>">上一頁</a>
    </li>
    <?php $page_offset = ceil($page_num/10)-1;?>
    <?php for($i = ($page_offset*10+1);$i <= ($page_offset*10+10);$i++):?>
        <?php if($i>$max_page_num) break;?>
        <li class="<?php echo ($page_num==$i)?'active':'';?>">
            <a href="<?php echo $this->get_page_num_url($i);?>"><?php echo $i;?></a>
        </li>

    <?php endfor;?>
    <?php if($page_num < $max_page_num):?>
        <li class="pagination-next">
            <a href="<?php echo $this->get_next_page_url();?>">下一頁</a>
        </li>
    <?php endif;?>
    <?php if( ( ($page_offset + 1) * 10 + 1 ) <= $max_page_num ):?>
        <li class="pagination-next">
            <a href="<?php echo $this->get_next_10_page_url($page_num, $max_page_num);?>">下十頁</a>
        </li>
    <?php endif;?>
</ul>
</nav>