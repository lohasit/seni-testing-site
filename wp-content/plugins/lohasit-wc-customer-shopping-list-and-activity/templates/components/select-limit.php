<div class="select-limit">
    <label for="limit"> 共 <?php echo $result_count;?> 筆，每頁筆數</label>
    <select name="limit" id="limit">
        <option value="5" <?php echo ($limit==5)?'selected="selected"':'';?>>5</option>
        <option value="15" <?php echo ($limit==15)?'selected="selected"':'';?>>15</option>
        <option value="30" <?php echo ($limit==30)?'selected="selected"':'';?>>30</option>
        <option value="50" <?php echo ($limit==50)?'selected="selected"':'';?>>50</option>
        <option value="100" <?php echo ($limit==100)?'selected="selected"':'';?>>100</option>
    </select>
</div>
<script>
    jQuery('select[name="limit"]').change(function(){
        var limit = jQuery(this).val();
        var page_url = "<?php echo basename($_SERVER['REQUEST_URI']);?>";
        <?php foreach($_GET as $key=>$value):?>
        <?php if($key != 'limit' && $key != 'page' && $key != 'page_num'):?>
        page_url+="&<?php echo $key;?>=<?php echo $value;?>";
        <?php endif;?>
        <?php endforeach;?>
        page_url+="&page_num=1";
        page_url+="&limit="+limit;
        window.location = page_url;
    });
</script>
