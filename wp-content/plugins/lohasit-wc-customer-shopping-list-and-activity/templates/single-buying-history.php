<?php
$parent_page_url=menu_page_url('buying-history',false);
if(!isset($_GET['u_id'])){
    echo "<h1>沒有正確的會員ID</h1>";
    return;
}
$page_num  = 1;
$limit     = 50;
$order_by     = (isset($_GET['orderby']))     ? $_GET['orderby']:'';
$order        = (isset($_GET['order']))       ? $_GET['order']:'';
$page_num     = (isset($_GET['page_num']))    ? $_GET['page_num']:$page_num;
$limit        = (isset($_GET['limit']))       ? $_GET['limit']:$limit;
$s            = (isset($_GET['s']))           ? $_GET['s']:'';
$search_type  = (isset($_GET['search_type'])) ? $_GET['search_type']:'';
$u_id         = (isset($_GET['u_id']))        ? $_GET['u_id']:'';
//$results      = Chir_Statistics::one_mem_product_order($u_id, $search_type, $s, $order_by, $order, $page_num, $limit);
//$data         = $results['results'];
//$result_count = $results['count'];

$name    = get_user_meta($u_id, 'billing_last_name', true).get_user_meta($u_id, 'billing_first_name', true);

$all_categories = get_categories(
    array(
        'taxonomy'     => 'product_cat',
        'orderby'      => 'term_name',
        'show_count'   => '-1'
    )
);


$query_args = [
    'numberposts' => - 1,
    'meta_key'    => '_customer_user',
    'meta_value'  => $u_id,
    'post_type'   => array( 'shop_order' ),
    'post_status' => array( 'wc-completed' ),
];

if($search_type == 3 && $s) {
    $from = explode(' ', $s)[0];
    $to = explode(' ', $s)[1];
    if($from || $to) {
        $query_args['date_query'] = [];
    }
    if($from) {
        $query_args['date_query'][0]['after'] = $from;
    }
    if($to) {
        $query_args['date_query'][0]['before'] = $to;
    }
}
if(isset($query_args['date_query'])) $query_args['date_query'][0]['inclusive'] = true;
$query = new WP_Query($query_args);
$user_orders = $query->get_posts();
$total = 0;
$total_items = [];
foreach ($user_orders as $user_order) {
    $wc_order = wc_get_order($user_order->ID);
    foreach ($wc_order->get_items() as $item) {
        $product_id = ($item['variation_id']) ? $item['variation_id'] : $item['product_id'];
        if(isset($total_items[$product_id])) {
            $total_items[$product_id]['qty'] += $item['qty'];
            $total_items[$product_id]['line_total'] += $item['line_total'];
        } else {
            $total_items[$product_id] = [
                'title' => $item['name'],
                'qty' => $item['qty'],
                'sku' => '',
                'cats' => get_the_terms(WP_Post::get_instance($item['product_id']), 'product_cat'),
                'line_total' => $item['line_total']
            ];
        }
    }
}
uasort($total_items, function($a, $b){
    return $a['line_total'] < $b['line_total'];
});
$rank = 0;
$old_total = '';
foreach ($total_items as $key => $total_item) {
    if($total_item['line_total'] != $old_total) $rank++;
    $old_total = $total_item['line_total'];
    $total_items[$key]['rank'] = $rank;
}

if($order_by == 'rank') {
    if($order == 'desc') {
        uasort($total_items, function($a, $b){
            return $a['rank'] < $b['rank'];
        });
    } elseif($order == 'asc') {
        uasort($total_items, function($a, $b){
            return $a['rank'] > $b['rank'];
        });
    }
} elseif($order_by == 'product_id') {
    if($order == 'desc') {
        uasort($total_items, function($a, $b){
            return strcasecmp($a['sku'], $b['sku']);
        });
    } elseif($order == 'asc') {
        uasort($total_items, function($a, $b){
            return strcasecmp($a['sku'], $b['sku']) * -1;
        });
    }
} elseif($order_by == 'product_name') {
    if($order == 'desc') {
        uasort($total_items, function($a, $b){
            return strcasecmp($a['title'], $b['title']);
        });
    } elseif($order == 'asc') {
        uasort($total_items, function($a, $b){
            return strcasecmp($a['title'], $b['title']) * -1;
        });
    }
} elseif($order_by == 'total') {
    if($order == 'desc') {
        uasort($total_items, function($a, $b){
            return $a['qty'] < $b['qty'];
        });
    } elseif($order == 'asc') {
        uasort($total_items, function($a, $b){
            return $a['qty'] > $b['qty'];
        });
    }
} elseif($order_by == 'line_total') {
    if($order == 'desc') {
        uasort($total_items, function($a, $b){
            return $a['line_total'] < $b['line_total'];
        });
    } elseif($order == 'asc') {
        uasort($total_items, function($a, $b){
            return $a['line_total'] > $b['line_total'];
        });
    }
}
if($search_type == 2 && $s) {
    $total_items = array_filter($total_items, function($order_item) use($s){
        foreach($order_item['cats'] as $term) {
            if($s == $term->term_id) return true;
        }
        return false;
    });
}
$result_count = sizeof($total_items);
$max_page_num = ceil(sizeof($total_items) / $limit);
?>
<style>
    dl.variation {
        display: inline;
    }
    dl.variation > dt,
    dl.variation > dd{
        display: inline-block;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?=CHIR_STATISTICS_PLUGIN_DIR;?>includes/admin/asset/css/table-style.css">
<div class="content-body container-fluid" style="visibility: visible;">
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
            <div>
                <a href="<?php echo $parent_page_url;?>">返回會員購物歷史紀錄</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
            <h3 class="page-title"><?php echo sprintf('會員 %s 所購買的商品列表', $name);?></h3>
        </div>
    </div>
    <div class="row search-row">
        <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
            <div class="type-picking inline">
                <select id="search-type-picking">
                    <option value="0">請選擇篩選項目</option>
                    <!--					<option value="1">商品貨號</option>-->
                    <option value="2">商品分類</option>
                    <option value="3">時段篩選</option>
                </select>
            </div>
            <div class="search-field search-input col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <?php include_once('components/input-s.php');?>
            </div>
            <div class="search-field product-cat col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <select name="cat_id" id="cat_id">
                    <option value="">請選擇商品分類</option>
                    <?php foreach($all_categories as $c):?>
                        <option value="<?php echo $c->term_id;?>" <?php echo (isset($_GET['cat_id']) && $_GET['cat_id'] == $c->term_id)?'selected="selected"':''; ?>>
                            <?php echo $c->name;?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="search-field date-picker col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <?php include_once( 'components/date-picker.php' );?>
            </div>
            <div class="inline search-button-field">
                <button type="button" id="search-button" class="btn btn-success">搜尋</button>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
            <?php include( 'components/select-limit.php' );?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
            <table class="list-table table table-striped table-hover">
                <thead>
                <tr>
                    <th class="cell-center cell-width-10">
                        <a class="table-header <?php echo $this->get_orderby_icon_css('rank');?>" href="<?php echo $this->get_orderby_url('rank');?>">
                            名次
                        </a>
                    </th>
                    <th class="cell-center cell-width-10">
                        <a class="table-header <?php echo $this->get_orderby_icon_css('product_id');?>" href="<?php echo $this->get_orderby_url('product_id');?>">
                            商品貨號
                        </a>
                    </th>
                    <th>
                        <a class='table-header <?php echo $this->get_orderby_icon_css('product_name');?>' href="<?php echo $this->get_orderby_url('product_name');?>">
                            商品名稱
                        </a>
                    </th>
                    <th>
                        規格
                    </th>
                    <th>
                        <div class='table-header'>
                            商品分類
                        </div>
                    </th>
                    <th class="cell-center cell-width-10">
                        <a class='table-header <?php echo $this->get_orderby_icon_css('total');?>' href='<?php echo $this->get_orderby_url('total');?>'>
                            購買數量
                        </a>
                    </th>
                    <th class="cell-center cell-width-10">
                        <a class='table-header <?php echo $this->get_orderby_icon_css('line_total');?>' href='<?php echo $this->get_orderby_url('line_total');?>'>
                            金額
                        </a>
                    </th>
                </tr>
                </thead>
                <tbody>

                <?php

                ?>
                <?php if($total_items == 0):?>
                    <tr>
                        <td colspan="7">查無結果</td>
                    </tr>
                <?php endif;?>
                <?php
                foreach ($total_items as $product_id => $total_item):
                    $product = wc_get_product($product_id);
                    if($product) {
                        $sku = $product->get_sku();
                        $product_title = $product->get_title();
                        $parent_id = $product_id;
                        if($product->get_type() == 'variation') {
                            /** @var WC_Product_Variation $product */
                            $parent_id = wp_get_post_parent_id($product_id);
                            $variations = $product->get_formatted_variation_attributes();
                        }
                        $edit_link = get_edit_post_link($parent_id);
                        $product_categories = $product->get_categories();
                    } else {
                        $sku = '';
                        $product_title = $total_item['title'];
                        $edit_link = '';
                        $variations = '';
                        $product_categories = '';
                    }
                ?>
                <tr>
                    <td class="cell-center"><?=$total_item['rank']?></td>
                    <td class="cell-center"><?=$sku?></td>
                    <td>
                        <?php if($edit_link):?>
                        <a href="<?=$edit_link?>"><?=$product_title?></a>
                        <?php else:?>
                        <span><?=$product_title?></span>
                        <?php endif;?>
                    </td>
                    <td><?=$variations?></td>
                    <td><?=$product_categories?></td>
                    <td class="cell-center"><?=$total_item['qty']?></td>
                    <td class="cell-center"><?=$total_item['line_total']?></td>
                </tr>

                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <?php include( 'components/pagination.php' );?>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=CHIR_STATISTICS_PLUGIN_DIR;?>includes/admin/asset/js/date-picker.js"></script>
<script src="<?=CHIR_STATISTICS_PLUGIN_DIR;?>includes/admin/asset/js/jq-bs-extend.js"></script>
<script src="<?=CHIR_STATISTICS_PLUGIN_DIR;?>includes/admin/asset/js/cy-admin-custom.js"></script>
<script>
    (function(){
        var $=jQuery.noConflict();
        $('.search-field').hide();

        <?php if($search_type != ''):?>
        $('#search-type-picking').val(<?php echo $search_type;?>);
        <?php endif; ?>

        var searchType = $('#search-type-picking').val();
        var target = 0;
        switch(searchType) {
            case '1':
                $('input[name="s"]').val('<?php echo $s;?>');
                target = 'search-input';
                break;
            case '2':
                $('#cat_id').val('<?php echo $s;?>');
                target = 'product-cat';
                break;
        <?php if($search_type == '3'):?>
            case '3':
                $('input[name="from"]').val('<?php echo explode(' ',$s)[0];?>');
                $('input[name="to"]').val('<?php echo explode(' ',$s)[1];?>');
                target = 'date-picker';
                break;
        <?php endif;?>
        }
        if(target) {
            $('.search-field').each(function(){
                if($(this).hasClass(target))
                    $(this).show();
                else
                    $(this).hide();
            });
        }


        if(searchType!=0) {
            $('.search-button-field').addCols(12,12,4,3);
            $('.type-picking').addCols(12,12,4,3);
            $('#search-type-picking').width('100%');
        }else{
            $('.search-button-field').removeCols(12,12,4,3);
            $('.type-picking').removeCols(12,12,4,3);
            $('#search-type-picking').width('auto');
        }

        $('#search-type-picking').change(function(){
            var target = 0;
            var val = $(this).val();
            if(val != 0) {
                $('.search-button-field').addCols(12,12,4,3);
                $('.type-picking').addCols(12,12,4,3);
                $(this).width('100%');
            }else{
                $('.search-button-field').removeCols(12,12,4,3);
                $('.type-picking').removeCols(12,12,4,3);
                $(this).width('auto');
            }
            switch(val) {
                case '1':
                    target = 'search-input';
                    break;
                case '2':
                    target = 'product-cat';
                    break;
                case '3':
                    target = 'date-picker';
                    break;
            }
            $('.search-field').each(function(){
                if($(this).hasClass(target)){
                    $(this).show();
                }else{
                    $(this).hide();
                }

            });
        });
        $('#search-button').click(function(){
            var searchType = $('#search-type-picking').val();
            var limit  = $('#limit').val();
            var url = "<?php echo $parent_page_url;?>&n=2&page_num=1&orderby=rank&order=asc&u_id=<?php echo $u_id;?>";
            var s = '';
            if(searchType == '3') {
                s = $('input[name="from"]').val() + '+' +$('input[name="to"]').val();
            }else if(searchType == '1') {
                s = $('input[name="s"]').val();
            } else if(searchType == '2') {
                s = $('#cat_id').val();
            }
            if(searchType != '0') {
                url+='&search_type='+searchType+'&s='+s;
            }

            url+='&limit='+limit;
            window.location=url;
        });
        $('.content-body').css('visibility','visible');
    })();
</script>
