<?php

class Woce_Downloader_Factory
{
    /**
     * @param $type
     * @return bool|Woce_Csv_Downloader|Woce_Excel_Downloader
     * @throws Exception
     */
    public static function get_downloader($type)
    {
        $downloader = false;
        switch(strtolower($type)) {
            case 'csv':
                $downloader = new Woce_Csv_Downloader();
                break;
            case 'excel':
                $downloader = new Woce_Excel_Downloader();
                break;
        }
        if($downloader == false)
            throw new Exception('Unknown Downloader Type', 'woce_unknown_downloader_type');

        return $downloader;
    }
}