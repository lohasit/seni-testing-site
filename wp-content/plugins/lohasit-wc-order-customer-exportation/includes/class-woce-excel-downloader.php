<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
class Woce_Excel_Downloader
{
    private function create_xls_spreadsheet($sheet_data)
    {
        $spreadsheet = new Spreadsheet();
        foreach ($sheet_data as $sheet_num => $page) {
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($sheet_num);
            $sheet->setTitle($page['title']);
            foreach ($page['data'] as $row => $row_data) {
                foreach ($row_data as $column => $col_data) {
                    $sheet->setCellValueByColumnAndRow($column + 1, $row + 1, $col_data);
                }
            }
        }
        return $spreadsheet;
    }

    public function download($file_name, $sheet_data)
    {
        $spreadsheet = $this->create_xls_spreadsheet($sheet_data);
        ob_clean();
        $writer = new Xls($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        exit;
    }
}