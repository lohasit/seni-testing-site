��    U      �      l      l  ;   m  8   �     �     �          #     +     2     B     O     _     m     �     �     �     �  	   �     �     �     �     �     �     �       	     
     
   "     -     0     H     W  
   d     o  	   }  
   �     �  %   �     �  	   �     �     �       	             )     0     ?  
   M     X     e     {     �     �     �     �     �     �     �     �  
   		     	     	  
    	     +	     <	     J	     [	     h	     |	     �	     �	     �	  '   �	     �	  9   �	     5
     G
     N
     ]
     l
     t
     }
     �
  >   �
  �  �
  =   �  =   �     4     S     b     q  	   x     �     �     �     �     �     �     �     �               "     )     0     C     J     ]     p     w     �  	   �     �  "   �     �     �     �     �  	   �     �       &        4  	   M     W     d     w     �     �     �     �     �     �     �     �     �     �                          0     =     P  	   c     m     t     |     �     �     �     �     �     �     �     �     �  &        +  6   F     }     �  	   �     �     �     �     �     �  >   �    Totally <span class="font-weight-bold">%d</span> customers  Totally <span class="font-weight-bold">%d</span> orders (filter from _MAX_ records) : Ascending order : Descending order Account Amount Billing Address Billing City Billing Country Billing Email Billing First Name Billing Phone Billing Postcode Billing State Customer Note Customers Date Email End Date Member Export Export Customers Export Orders Fields File Type Final page First page ID IDs, separated by comma Invoice Number Invoice Type Item Price Item Subtotal Last page Loading... Lohas IT Lohasit WC Order/Customer Exportation Membership Level (Role) Next page No records found Order Date From Order Date To Order IDs Order Status Orders Payment Method Processing... Product ID Product Name Product Regular Price Product Sale Price Product Variations Quantity Query Query Result Refunded Amount Registered Date Registration Date From Registration Date To Remove All SKU Search: Select All Shipping Address Shipping City Shipping Country Shipping Fee Shipping First Name Shipping Method Shipping Phone Shipping Postcode Shipping State Showing 0 to 0 record, totally 0 record Showing _MENU_ records Showing _START_ to _END_ records, totally _TOTAL_ records Start Date Member Status Total Discount User Birthdate User ID User IDs Username https://www.lohaslife.cc/ 樂活壓板系統 - Woocommerce - 匯出訂單及顧客資料 Project-Id-Version: WC Export Orders
POT-Creation-Date: 2019-04-10 12:02+0800
PO-Revision-Date: 2022-02-15 11:10+0000
Last-Translator: 
Language-Team: 繁體中文
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.5.5; wp-5.9  共 <span class="font-weight-bold">%d</span> 筆顧客資料  共 <span class="font-weight-bold">%d</span> 筆訂單資料 (從 _MAX_ 項紀錄中搜尋) : 正序排列 : 反序排列 帳號 總金額 訂購者地址 訂購者鄉鎮市 訂購者國家 訂購者E-mail 訂購者名字 訂購者市話 訂購者郵遞區號 訂購者縣 / 市 訂單備註 會員 日期 E-mail 結束日期會員 匯出 匯出顧客資料 匯出訂單資料 欄位 檔案格式 最後一頁 第一頁 編號 可輸入多組,使用逗號分隔 發票號碼 發票類型 單價 小計 上一頁 載入中... Lohas IT 樂活 Woocommerce 訂單/顧客導出 會員級別（角色） 下一頁 查無紀錄 訂單開始時間 訂單結束時間 訂單ID 訂單狀態 訂單 付款方式 處理中... 商品編號 商品名稱 商品原價 商品折扣價 商品規格 數量 查詢 查詢結果 已退費金額 註冊時間 註冊開始時間 註冊結束時間 全不選 貨號 搜尋: 全選 收件地址 收件鄉鎮市 收件國家 運費 收件者名字 運送方式 收件者電話 收件郵遞區號 收件縣 / 市 顯示第 0 至 0 項紀錄, 共 0 項 顯示第 _MENU_ 項紀錄 顯示第 _START_ 至 _END_ 項紀錄, 共 _TOTAL_ 項 開始日期成員 狀態 總折扣 出生日期 會員ID 會員ID 帳號 https://www.lohaslife.cc/ 樂活壓板系統 - Woocommerce - 匯出訂單及顧客資料 