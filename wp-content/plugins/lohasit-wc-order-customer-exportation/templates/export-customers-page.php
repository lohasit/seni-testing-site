<?php
$data_helper = new Woce_Customer_Data_Helper();
$headers = $data_helper->get_default_data_headers();
if($_POST) {
    if($_POST['action'] == 'query') {
        $start_time = sanitize_text_field($_POST['start_time']);
        $end_time = sanitize_text_field($_POST['end_time']);
        $customer_id = sanitize_text_field($_POST['customer_id']);
        if(!empty($_POST['filter_column'])) {
            $data_helper->set_filter_column($_POST['filter_column']);
            update_option('woce_export_option_customer_filter_column', $_POST['filter_column']);
        }
        if($start_time)
            $data_helper->start($start_time);
        if($end_time)
            $data_helper->end($end_time);
        $data_helper->inclusive();

        if($customer_id) {
            $data_helper->include(explode(',', $customer_id));
        }
        do_action('woce_customer_query_data_helper', $data_helper, $_POST);
        do_action('woce_customer_query_data', $_POST);
        $output_data = apply_filters('woce_customer_query_output_data', $data_helper->get_output_data(), $_POST);
    } elseif($_POST['action'] == 'download') {
        $query_args = unserialize(str_replace('\"', '"', $_POST['query_args']));
        $filter_column = unserialize(str_replace('\"', '"', $_POST['filter_column']));
        $data_helper->set_query_args($query_args)
            ->include($_POST['include'])
            ->set_filter_column($filter_column);
        do_action('woce_customer_download_data_helper', $data_helper);
        do_action('woce_customer_download_data');
        if($_POST['export_type'] == 'excel')
            $data_helper->download_excel();
        else
            $data_helper->download_csv();
    }
}
$export_option_filter_column = get_option('woce_export_option_customer_filter_column');
?>
<div class="container-fluid" id="woce-container">
    <form action="" method="post">
        <div class="row">
            <div class="col">
                <h1><?=__('Export Customers', 'woce')?></h1>
            </div>
        </div>
        <?php do_action('before_woce_customer_query_form_fields') ?>
        <div class="row">
            <div class="form-group col-12 col-md-6 row">
                <label for="start-time" class="font-weight-bold col col-md-3 col-form-label"><?=__('Registration Date From', 'woce')?></label>
                <div class="col">
                    <input type="text" name="start_time" id="start-time" class="form-control form-control-sm time-picker" value="<?=(isset($_POST['start_time']))?$_POST['start_time']:''?>" autocomplete="off">
                </div>
            </div>
            <div class="form-group col-12 col-md-6 row">
                <label for="end-time" class="font-weight-bold col col-md-3 col-form-label"><?=__('Registration Date To', 'woce')?></label>
                <div class="col">
                    <input type="text" name="end_time" id="end-time" class="form-control form-control-sm time-picker" value="<?=(isset($_POST['end_time']))?$_POST['end_time']:''?>" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12 col-md-6 row">
                <label for="search-customer-id" class="font-weight-bold col col-md-3 col-form-label" data-toggle="tooltip" data-placement="top" title="<?=__('IDs, separated by comma', 'woce')?>"><?=__('User IDs', 'woce')?></label>
                <div class="col">
                    <input type="text" name="customer_id" id="search-customer-id" class="form-control form-control-sm" value="<?=(isset($_POST['customer_id']))?$_POST['customer_id']:''?>">
                </div>
            </div>
        </div>
        <?php do_action('woce_customer_query_form_fields') ?>
        <div class="row">
            <div class="form-group col row">
                <label for="filter-columns" class="font-weight-bold col-12 col-form-label">
                    <span><?=__('Fields', 'woce')?></span>
                    <a href="#" class="select-all-column"><?=__('Select All', 'woce')?></a>
                    <a href="#" class="unselect-all-column"><?=__('Remove All', 'woce')?></a>
                </label>
                <div class="col">
                    <select name="filter_column[]" id="filter-columns" class="init-select2" multiple="multiple" style="width:100%;">
                        <?php foreach($headers as $column => $label):?>
                            <?php
                            $column_selected = (is_array($export_option_filter_column) && in_array($column, $export_option_filter_column))?'selected="selected"':'';
                            ?>
                            <option value="<?=$column?>" <?=$column_selected?>><?=$label?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <?php do_action('after_woce_customer_query_form_fields') ?>
        <input type="hidden" name="action" value="query">
        <?=apply_filters('woce_customer_query_button_html',
            sprintf('<button class="btn btn-success btn-sm">%s</button>',
                apply_filters('woce_customer_query_button_text', __('Query', 'woce'))
            )
        )?>
        <?//php if(isset($_POST['action']) && $_POST['action'] == 'query'):?>
            <?//=apply_filters('woce_customer_query_result_button_html',
                //sprintf('<button class="btn btn-info show-data-result btn-sm">%s</button>',
                  //  apply_filters('woce_customer_query_result_button_text', __('Query Result', 'woce'))
               // )
           // )?>
        <?//php endif;?>
    </form>
</div>
<?php if(isset($_POST['action']) && $_POST['action'] == 'query'):
    $table_headers = $output_data[0];
    unset($output_data[0]);
    $table_data = $output_data;
?>
    <div id="woce-modal-container" style="display:none;">
        <div class="mask"></div>
        <div class="content">
            <button type="button" class="close-modal btn btn-danger">X</button>
            <div class="output-form-wrapper container-fluid">
                <form action="" method="post">
                    <?php do_action('before_woce_customer_download_form_fields') ?>
                    <div class="row form-group form-controls">
                        <div class="col">
                            <?=sprintf(__(' Totally <span class="font-weight-bold">%d</span> customers', 'woce'), count($table_data))?>
                        </div>
                    </div>
                    <div class="row form-group form-controls">
                        <div class="col">
                            <span><?=__('File Type', 'woce')?>:</span>
                            <div class="form-check form-check-inline">
                                <input type="radio" name="export_type" class="form-control form-check-input" id="export-type-csv" value="csv" checked="checked">
                                <label for="export-type-csv" class="form-check-label">CSV</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" name="export_type" class="form-control form-check-input" id="export-type-excel" value="excel">
                                <label for="export-type-excel" class="form-check-label">Excel</label>
                            </div>
                            <input type="hidden" name="query_args" value='<?=serialize($data_helper->get_query_args())?>'>
                            <input type="hidden" name="filter_column" value='<?=serialize($data_helper->get_filter_column())?>'>
                            <input type="hidden" name="action" value="download">
                            <input type="submit" class="btn btn-primary btn-sm" value="<?=__('Export', 'woce')?>">
                        </div>
                    </div>
                    <?php do_action('woce_customer_download_form_fields') ?>
                    <div class="output-data-wrapper row">
                        <div class="col">
                            <table class="output-data-table table table-sm table-bordered nowrap">
                                <thead>
                                <tr>
                                    <?php foreach($table_headers as $col_name => $header):?>
                                        <th>
                                            <?php if($col_name == 'user_id'):?>
                                                <input type="checkbox" id="check-all-output-data" checked="checked">
                                                <label for="check-all-output-data"><?=$header?></label>
                                            <?php else:?>
                                                <?=$header?>
                                            <?php endif;?>
                                        </th>
                                    <?php endforeach;?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($table_data as $user_id => $data):
                                    ?>
                                    <tr data-group="<?=$user_id?>">
                                        <?php foreach($data as $col_name => $cell_value):?>
                                            <td>
                                                <?php if($col_name == 'user_id'):
                                                    $hash = substr(sha1(rand()),0, 6);
                                                    $cell_id = 'cell-' . $hash;
                                                    ?>
                                                    <input type="checkbox" name="include[]" id="<?=$cell_id?>" value="<?=$user_id?>" class="cell-id cell-id-<?=$user_id?>" checked="checked">
                                                    <label for="<?=$cell_id?>" class="post-id-check"><?=trim($user_id)?></label>
                                                <?php else:?>
                                                    <?=trim($cell_value)?>
                                                <?php endif;?>
                                            </td>
                                        <?php endforeach;?>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php do_action('after_woce_customer_download_form_fields') ?>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>
