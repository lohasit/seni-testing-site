<?php
/**
 * Class WC_Report_Category_Sales
 * 年度分類銷售報表
 */

class WC_Report_Category_Sales extends WC_Admin_Report {

	use WsttsDownloadHelper;

	public function get_chart_legend() {
		$legend = [];

		return $legend;
	}
	public function get_main_chart() {

	}
	public function output_report() {

		Wstts_Api::form_html();

		if(isset($_POST['action']) && $_POST['action'] === 'export') {
			$year  = $_POST['year'];
			$orders  = (new WsttsOrder())
				->start($year.'-01-01')
				->end($year.'-12-31')
				->inclusive()
				->orderby('post_date', 'ASC')
				->all()
				->get();

			$month_orders = [
				1 => [],
				2 => [],
				3 => [],
				4 => [],
				5 => [],
				6 => [],
				7 => [],
				8 => [],
				9 => [],
				10 => [],
				11 => [],
				12 => [],
			];

			$all_cats = get_terms(['taxonomy' => 'product_cat']);

			$cats_statistics = [];
			foreach ($all_cats as $cat ) {
				for ( $i = 1; $i <= 12; $i ++ ) {
					$cats_statistics[$i][$cat->term_id]['count'] = 0;
					$cats_statistics[$i][$cat->term_id]['amount'] = 0;
				}
			}
			foreach ($orders as $order ) {
				/** @var WC_Order $order */
				$order_month = date('m', strtotime($order->order_date));
				array_push($month_orders[(int)$order_month], $order);
			}
			foreach ($month_orders as $month => $orders_in_month ) {
				foreach ($orders_in_month as $order ) {
					/** @var WC_Order $order**/
					$order_items = $order->get_items();
					foreach ($order_items as $order_item ) {
						/** @var WC_Product $item_product */
						$product_cats = get_the_terms($order_item['product_id'], 'product_cat');
						foreach ($product_cats as $product_cat ) {
							$cats_statistics[$month][$product_cat->term_id]['count']++;
							$cats_statistics[$month][$product_cat->term_id]['amount'] += $order_item['line_total'];
						}
					}
				}
			}

			$data_header = array_values(
				array_map(function($cat){
					return $cat->name;
				}, $all_cats)
			);

			$data_header = array_merge((array)"", $data_header, (array)__('Subtotal of month', 'wc-statistics'));

			$data_set = [];

			$year_total = 0;

			$cat_total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

			array_push($data_set, $data_header);

			foreach($cats_statistics as $month => $line) {
				$data_line = array_map(function($line){
					return round($line['amount']);
				},$line);
				$subtotal = 0;
				$index = 0;
				foreach ($data_line as $key => $amount) {
					$cat_total[$index] += $amount;
					$subtotal += $amount;
					$index++;
				}
				$year_total += $subtotal;
				$data_line = array_merge((array)Wstts_Api::get_month_name($month), $data_line, (array)$subtotal);
				array_push($data_set, $data_line);
			}

			$cat_total_line = array_merge((array)__('Subtotal of category', 'wc-statistics'), $cat_total);
			array_push($data_set, $cat_total_line);

			$year_total_line = [];
			$data_width = sizeof($data_header);
			for($i = 1; $i <= $data_width - 2; $i++) {
				array_push($year_total_line, '');
			}
			$year_total_line = array_merge($year_total_line, (array)__('Total', 'wc-statistics'), (array)$year_total);
			array_push($data_set, $year_total_line);

			$file_name = __('Category Sales Report of Year', 'wc-statistics') . $year;
			$this->download_csv($file_name, $data_set);
		}
	}
}