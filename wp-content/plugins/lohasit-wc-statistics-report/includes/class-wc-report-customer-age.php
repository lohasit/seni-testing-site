<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/1/30
 * Time: 下午 06:35
 */

class WC_Report_Customer_Age extends WC_Admin_Report{

	use WsttsReportHelper;

	private $raw_data;

	public function __construct()
	{
		$this->data_helper = $this->get_data_helper();

		$this->calculate_range();

		$this->raw_data = $this->data_helper
			->set_range(date('Y-m-d', $this->start_date), date('Y-m-d', $this->end_date))
			->get_age_data();
	}

	public function get_chart_legend()
	{
		$legend[] = array(
			'title' => sprintf(__('15 - 25 years old: %s', 'wc-statistics'), '<strong>' . $this->raw_data['15_25'] . '</strong>' ),
			'color' => $this->chart_colors[0],
			'highlight_series' => 0
		);
		$legend[] = array(
			'title' => sprintf(__('25 - 35 years old: %s', 'wc-statistics'), '<strong>' . $this->raw_data['25_35'] . '</strong>' ),
			'color' => $this->chart_colors[1],
			'highlight_series' => 1
		);
		$legend[] = array(
			'title' => sprintf(__('35 - 45 years old: %s', 'wc-statistics'), '<strong>' . $this->raw_data['35_45'] . '</strong>' ),
			'color' => $this->chart_colors[2],
			'highlight_series' => 2
		);
		$legend[] = array(
			'title' => sprintf(__('45 - 55 years old: %s', 'wc-statistics'), '<strong>' . $this->raw_data['45_55'] . '</strong>' ),
			'color' => $this->chart_colors[3],
			'highlight_series' => 3
		);
		$legend[] = array(
			'title' => sprintf(__('above 55 years old: %s', 'wc-statistics'), '<strong>' . $this->raw_data['55_'] . '</strong>' ),
			'color' => $this->chart_colors[4],
			'highlight_series' => 4
		);
		return $legend;
	}

	public function get_main_chart()
	{
		$data = [
			['15-25', $this->raw_data['15_25']],
			['25-35', $this->raw_data['25_35']],
			['35-45', $this->raw_data['35_45']],
			['45-55', $this->raw_data['45_55']],
			['55-', $this->raw_data['55_']],
		];
		$this->main_chart_html($data);
	}

	public function output_report()
	{
		$this->report_html();
	}
}