<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/1/30
 * Time: 下午 06:35
 */

class WC_Report_Customer_Gender extends WC_Admin_Report{

	use WsttsReportHelper;

	private $raw_data;

	public function __construct()
	{
		$this->data_helper = $this->get_data_helper();

		$this->calculate_range();

		$this->raw_data = $this->data_helper
			->set_range(date('Y-m-d', $this->start_date), date('Y-m-d', $this->end_date))
			->get_gender_data();
	}

	public function get_chart_legend()
	{
		$legend[] = array(
			'title' => sprintf(__('Female: %s', 'wc-statistics'), '<strong>' . $this->raw_data['female'] . '</strong>' ),
			'color' => $this->chart_colors[0],
			'highlight_series' => 0
		);

		$legend[] = array(
			'title' => sprintf(__('Male: %s', 'wc-statistics'), '<strong>' . $this->raw_data['male'] . '</strong>' ),
			'color' => $this->chart_colors[1],
			'highlight_series' => 1
		);
		return $legend;
	}

	public function get_main_chart()
	{
		$data = [
			[__('Female', 'wc-statistics'), $this->raw_data['female']],
            [__('Male', 'wc-statistics'), $this->raw_data['male']],
        ];
		$this->main_chart_html($data);
	}

	public function output_report()
	{
        $this->report_html();
	}
}