<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/1/30
 * Time: 下午 06:35
 */

class WC_Report_Customer_State extends WC_Admin_Report{

	use WsttsReportHelper;

	private $raw_data;

	public function __construct()
	{
		$this->data_helper = $this->get_data_helper();

		$this->calculate_range();

		$this->raw_data = $this->data_helper
			->set_range(date('Y-m-d', $this->start_date), date('Y-m-d', $this->end_date))
			->get_state_data();
	}

	public function get_chart_legend()
	{
		$legend[] = array(
			'title' => sprintf(__('Taiwan Northern Area: %s', 'wc-statistics'), '<strong>' . $this->raw_data['north'] . '</strong>' ),
			'color' => $this->chart_colors[0],
			'highlight_series' => 0
		);
		$legend[] = array(
			'title' => sprintf(__('Taiwan Central Area: %s', 'wc-statistics'), '<strong>' . $this->raw_data['central'] . '</strong>' ),
			'color' => $this->chart_colors[1],
			'highlight_series' => 1
		);
		$legend[] = array(
			'title' => sprintf(__('Taiwan Southern Area: %s', 'wc-statistics'), '<strong>' . $this->raw_data['south'] . '</strong>' ),
			'color' => $this->chart_colors[2],
			'highlight_series' => 2
		);
		$legend[] = array(
			'title' => sprintf(__('Taiwan Eastern Area: %s', 'wc-statistics'), '<strong>' . $this->raw_data['east'] . '</strong>' ),
			'color' => $this->chart_colors[3],
			'highlight_series' => 3
		);
		$legend[] = array(
			'title' => sprintf(__('Taiwan Islands Area: %s', 'wc-statistics'), '<strong>' . $this->raw_data['outer'] . '</strong>' ),
			'color' => $this->chart_colors[4],
			'highlight_series' => 4
		);
		return $legend;
	}

	public function get_main_chart()
	{
		$data = [
			[__('Taiwan Eastern Area', 'wc-statistics'), $this->raw_data['north']],
			[__('Taiwan Central Area', 'wc-statistics'), $this->raw_data['central']],
			[__('Taiwan Southern Area', 'wc-statistics'), $this->raw_data['south']],
			[__('Taiwan Eastern Area', 'wc-statistics'), $this->raw_data['east']],
			[__('Taiwan Islands Area', 'wc-statistics'), $this->raw_data['outer']],
		];
		$this->main_chart_html($data);
	}

	public function output_report()
	{
		$this->report_html();
	}
}