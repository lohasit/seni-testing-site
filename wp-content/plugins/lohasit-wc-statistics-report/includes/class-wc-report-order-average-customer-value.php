<?php
/**
 * 平均客單價
 */

class WC_Report_Order_Average_Customer_Value extends WC_Admin_Report
{
	use WsttsReportHelper;

	private $raw_data;

	public function __construct() {

		$this->data_helper = $this->get_data_helper();

		$this->calculate_range();

		$this->raw_data = $this->data_helper
			->set_range(date('Y-m-d', $this->start_date), date('Y-m-d', $this->end_date))
			->get_average_customer_value();
	}

	public function get_chart_legend()
	{
		$legend[] = array(
			'title' => sprintf(__('Average Customer Value', 'wc-statistics').': %s', '<strong>' . wc_price($this->raw_data['average_customer_value']) . '</strong>' ),
			'color' => $this->chart_colors[0],
			'highlight_series' => 0
		);

		return $legend;
	}

	public function get_main_chart()
	{
		$data = [
			[__('Average Customer Value', 'wc-statistics'), $this->raw_data['average_customer_value']],
		];
		$this->main_chart_html($data);
	}

	public function output_report()
	{
		$this->report_html();
	}
}