<?php
/**
 * 已退費訂單
 */

class WC_Report_Order_Refund extends WC_Admin_Report
{
	public $chart_colours = array();

	public function get_chart_legend()
	{
		$legend = array();

		$total_refunded_orders_count_query = [
			'data' => array(
				'post_status' => array(
					'type'            => 'post_data',
					'function'        => 'COUNT',
					'name'            => 'total_refund_count'
				)
			),
			'where' => array(
				array(
					'key'      => 'post_status',
					'value'    => 'wc-refunded',
					'operator' => '='
				)
			),
			'query_type'   => 'get_var',
			'filter_range' => true,
			'order_types'  => wc_get_order_types( 'order-count' ),
			'order_status' => array('completed', 'processing', 'on-hold', 'cancelled', 'refunded', 'pending', 'failed')
		];

        $total_orders_count_query = [
	        'data' => array(
		        'ID' => array(
			        'type'            => 'post_data',
			        'function'        => 'COUNT',
			        'name'            => 'orders'
		        )
	        ),
	        'query_type'   => 'get_var',
	        'filter_range' => true,
	        'order_types'  => wc_get_order_types('order-count'),
	        'order_status' => array('completed', 'processing', 'on-hold', 'cancelled', 'refunded', 'pending', 'failed')
        ];

		$total_refunded_orders_count = $this->get_order_report_data( $total_refunded_orders_count_query );
		$total_orders_count = $this->get_order_report_data( $total_orders_count_query );

		$legend[] = array(
			'title' => sprintf(__('%s refunded orders in total', 'wc-statistics'), '<strong>' . $total_refunded_orders_count . '</strong>' ),
			'color' => $this->chart_colours['total_refund_count'],
			'highlight_series' => 1
		);

		$refunded_orders_rate = $total_refunded_orders_count / $total_orders_count * 100;
		$refunded_orders_rate = round($refunded_orders_rate, 1);
		if($total_orders_count == 0)
            $refunded_orders_rate = 0;

		$legend[] = array(
			'title' => sprintf(__('Refunded Rate: %s %%', 'wc-statistics'), '<strong>' . $refunded_orders_rate . '</strong>' ),
			'color' => 'red'
		);
		return $legend;
	}

	public function output_report()
	{
		$ranges = array(
			'year'         => __( 'Year', 'wc-statistics' ),
			'last_month'   => __( 'Last Month', 'wc-statistics' ),
			'month'        => __( 'This Month', 'wc-statistics' ),
			'7day'         => __( 'Last 7 Days', 'wc-statistics' )
		);

		$this->chart_colours = [
			'total_refund_count' => '#3498db',
		];

		$current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : '7day';

		if ( ! in_array( $current_range, array( 'custom', 'year', 'last_month', 'month', '7day' ) ) ) {
			$current_range = '7day';
		}

		$this->calculate_current_range($current_range);

		include( WC()->plugin_path() . '/includes/admin/views/html-report-by-date.php' );
	}

	public function get_main_chart()
	{
		global $wp_locale;
		$refunded_order_counts_query = [
			'data' => array(
				'post_status' => array(
					'type'            => 'post_data',
					'function'        => 'COUNT',
					'name'            => 'refunded_order_count'
				),
				'post_date' => array(
					'type'     => 'post_data',
					'function' => '',
					'name'     => 'post_date'
				),
			),
			'where' => array(
				array(
					'key'      => 'post_status',
					'value'    => 'wc-refunded',
					'operator' => '='
				)
			),
			'group_by'     => $this->group_by_query,
			'order_by'     => 'post_date ASC',
			'query_type'   => 'get_results',
			'filter_range' => true,
			'order_types'  => wc_get_order_types( 'order-count' )
		];
		$refunded_order_counts    = $this->get_order_report_data( $refunded_order_counts_query );

		$refunded_order_counts    = $this->prepare_chart_data( $refunded_order_counts, 'post_date', 'refunded_order_count' , $this->chart_interval, $this->start_date, $this->chart_groupby );

		$chart_data = json_encode( array(
			'refunded_order_counts'    => array_values($refunded_order_counts)
		) );
		?>
		<div class="chart-container">
			<div class="chart-placeholder main"></div>
		</div>
		<script type="text/javascript">
			var main_chart;

			jQuery(function(){
				var order_data = jQuery.parseJSON( '<?php echo $chart_data; ?>' );
				var drawGraph = function( highlight ) {
					var series = [
						{
							label: "<?php echo esc_js(__('Number of refunded orders', 'wc-statistics')) ?>",
							data: order_data.refunded_order_counts,
							color: '<?php echo $this->chart_colours['total_refund_count']; ?>',
                            bars: { fillColor: '<?php echo $this->chart_colours['total_refund_count' ]; ?>', fill: true, show: true, lineWidth: 0, barWidth: <?php echo $this->barwidth; ?> * 0.5, align: 'center' },
							shadowSize: 0,
							hoverable: false
						}
					];
					if ( highlight !== 'undefined' && series[ highlight ] ) {
						highlight_series = series[ highlight ];

						highlight_series.color = '#9c5d90';

						if ( highlight_series.bars )
							highlight_series.bars.fillColor = '#9c5d90';

						if ( highlight_series.lines ) {
							highlight_series.lines.lineWidth = 5;
						}
					}

					main_chart = jQuery.plot(
						jQuery('.chart-placeholder.main'),
						series,
						{
							legend: {
								show: false
							},
							grid: {
								color: '#aaa',
								borderColor: 'transparent',
								borderWidth: 0,
								hoverable: true
							},
							xaxes: [ {
								color: '#aaa',
								position: "bottom",
								tickColor: 'transparent',
								mode: "time",
								timeformat: "<?php if ( $this->chart_groupby == 'day' ) echo '%d %b'; else echo '%b'; ?>",
								monthNames: <?php echo json_encode( array_values( $wp_locale->month_abbrev ) ) ?>,
								tickLength: 1,
								minTickSize: [1, "<?php echo $this->chart_groupby; ?>"],
								font: {
									color: "#aaa"
								}
							} ],
							yaxes: [
								{
									min: 0,
									minTickSize: 1,
									tickDecimals: 0,
									color: '#ecf0f1',
									font: { color: "#aaa" }
								}
							]
						}
					);

					jQuery('.chart-placeholder').resize();
				}

				drawGraph();

				jQuery('.highlight_series').hover(
					function() {
						drawGraph( jQuery(this).data('series') );
					},
					function() {
						drawGraph();
					}
				);
			});
		</script>
		<?php
	}
}