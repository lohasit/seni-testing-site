<?php
/**
 * Class WC_Report_Total_Sales
 * 年度銷售報表
 */

class WC_Report_Total_Sales extends WC_Admin_Report {

	use WsttsDownloadHelper;

	public function get_chart_legend() {
		$legend = [];

		return $legend;
	}
	public function get_main_chart() {

	}
	public function output_report() {

		Wstts_Api::form_html();

		echo "<pre>";
		if(isset($_POST['action']) && $_POST['action'] === 'export') {
			$year  = $_POST['year'];
			$orders  = (new WsttsOrder())
				->start($year.'-01-01')
				->end($year.'-12-31')
				->inclusive()
				->orderby('post_date', 'ASC')
				->all()
				->get();

			$month_orders = [
				1 => [],
				2 => [],
				3 => [],
				4 => [],
				5 => [],
				6 => [],
				7 => [],
				8 => [],
				9 => [],
				10 => [],
				11 => [],
				12 => [],
			];

			$months_data = [
			];

			for ($month=1; $month<=12; $month++) {
				$months_data[$month] = [
					'month' => $month,
					'item_count' => 0,
					'order_count' => 0,
					'total_shipping_fee' => 0,
					'total_sales_amount' => 0,
					'total_refund_amount' => 0,
					'total_discount_amount' => 0,
				];
			}
			foreach ($orders as $order ) {
				/** @var WC_Order $order */
				$order_month = date('m', strtotime($order->order_date));
				array_push($month_orders[(int)$order_month], $order);
			}
			$data_header = [
				'month'                 => __('Month', 'wc-statistics'),
				'item_count'            => __('Item Count', 'wc-statistics'),
				'order_count'           => __('Order Count', 'wc-statistics'),
				'total_shipping_fee'    => __('Total Shipping Fee', 'wc-statistics'),
				'total_sales_amount'    => __('Total Sales Amount', 'wc-statistics'),
				'total_refund_amount'   => __('Total Refund Amount', 'wc-statistics'),
				'total_discount_amount' => __('Total Discount Amount', 'wc-statistics'),
			];

			foreach ($month_orders as $month => $orders_in_month ) {
				foreach ( $orders_in_month as $order ) {
					$months_data[$month]['order_count']++;
					$months_data[$month]['total_shipping_fee']+= $order->get_total_shipping();
					$months_data[$month]['total_sales_amount']+= $order->get_total();
					$months_data[$month]['total_refund_amount']+= (int)$order->get_total_refunded();
					$months_data[$month]['total_discount_amount']+=round($order->get_total_discount());

					$order_items = $order->get_items();
					foreach ( $order_items as $order_item ) {
						$months_data[$month]['item_count'] += $order_item['qty'];

					}

				}
			}

			$data_set = [];
			$data_set[] = $data_header;
			foreach($months_data as $month => $month_data) {
				$data_line = [];
				foreach ( $data_header as $key => $item ) {
					$data_line[] = $month_data[$key];
				}
				$data_set[] = $data_line;
			}

			print_r($data_set);
			echo "</pre>";

//
			$file_name = __('Category Total Report of Year', 'wc-statistics') . $year;
			$this->download_csv($file_name, $data_set);
		}
	}
}