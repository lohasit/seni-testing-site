<?php
/**
 * Class Wstts_Api
 */

class Wstts_Api {

	public static function form_html()
	{
		include_once(WSTTS_PLUGIN_PATH . 'templates/export-form.php');
	}
	public static function get_month_name($month)
	{
		switch($month) {
			case 1:
				return __('January', 'wc-statistics');
				break;
			case 2:
				return __('February', 'wc-statistics');
				break;
			case 3:
				return __('March', 'wc-statistics');
				break;
			case 4:
				return __('April', 'wc-statistics');
				break;
			case 5:
				return __('May', 'wc-statistics');
				break;
			case 6:
				return __('June', 'wc-statistics');
				break;
			case 7:
				return __('July', 'wc-statistics');
				break;
			case 8:
				return __('August', 'wc-statistics');
				break;
			case 9:
				return __('September', 'wc-statistics');
				break;
			case 10:
				return __('October', 'wc-statistics');
				break;
			case 11:
				return __('November', 'wc-statistics');
				break;
			case 12:
				return __('December', 'wc-statistics');
				break;
			default:
				return '';
				break;
		}
	}
}