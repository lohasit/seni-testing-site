<?php
class WsttsStatisticsData
{
	private $start_date;

	private $end_date;

	private $wpdb;

	private $wp_tables =[];

	public function __construct()
	{
		global $wpdb;
		$this->wpdb = $wpdb;
		$prefix = $wpdb->prefix;
		$this->wp_tables['usermeta']  = $prefix.'usermeta';
		$this->wp_tables['users']  = $prefix.'users';
	}

	private function calculate_age_by_birthday($birthday)
    {
        $age = strtotime($birthday);
        if($age === false){
            return false;
        }
        list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age));
        $now = strtotime("now");
        list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now));
        $age = $y2 - $y1;
        if((int)($m2.$d2) < (int)($m1.$d1))
            $age -= 1;
        return $age;
    }

    public function set_range($start_date, $end_date)
    {
    	$this->start_date = $start_date;
    	$this->end_date = $end_date;
    	return $this;
    }

    public function get_age_data() {
        $sql = $this->wpdb->prepare("
            SELECT meta_value as birthdate 
            FROM {$this->wpdb->usermeta} 
            WHERE 
              meta_key = %s AND 
              user_id IN (
                SELECT ID FROM {$this->wpdb->users} 
                WHERE 
                  user_registered > %s AND 
                  user_registered < %s
              )
            ",
	        ['member_birthdate', $this->start_date, $this->end_date]
        );

        $birthdate_result = $this->wpdb->get_results($sql);

        $ages = [
        	'15_25' => 0,
        	'25_35' => 0,
        	'35_45' => 0,
        	'45_55' => 0,
        	'55_' => 0
        ];

        foreach ($birthdate_result as $result){

            $age = $this->calculate_age_by_birthday($result->birthdate);

            if($age >= 15 && $age <= 25){
                $ages['15_25']++;
            }else if($age > 25 && $age <= 35){
	            $ages['25_35']++;
            }else if($age > 36 && $age <= 45){
	            $ages['35_45']++;
            }else if($age > 45 && $age <= 55){
	            $ages['45_55']++;
            }else if($age > 55 ){
	            $ages['55_']++;
            }
        }
        return $ages;
    }

    public function get_state_data() {

        $area_count = [
            'north' => 0,
            'central' => 0,
            'south' => 0,
            'east' => 0,
            'outer' => 0
        ];

	    $north_areas    = ["宜蘭縣", "新北市", "臺北市", "桃園市", "基隆市", "新竹縣", "新竹市"];
        $central_areas  = ["苗栗縣", "臺中市", "彰化縣","南投縣"];
        $south_areas    = ["嘉義市", "嘉義縣", "雲林縣", "臺南市", "高雄市", "屏東縣"];
        $east_areas     = ["花蓮縣", "臺東縣"];
        $outer_areas    = ["金門縣", "連江縣", "澎湖縣"];

        $customer_ids = get_users([
        	'role' => 'customer',
        	'date_query' => [
		        ['after' => $this->start_date, 'inclusive' => true ],
		        ['before' => $this->end_date, 'inclusive' => false ],
	        ],
	        'fields' => 'ID',
        ]);
	    foreach ($customer_ids as $customer_id ) {
		    $state = get_user_meta($customer_id, 'billing_state', true);
		    if (in_array($state, $north_areas)) {
			    $area_count['north']++;
            } else if (in_array($state, $central_areas)) {
			    $area_count['central']++;
            } else if (in_array($state, $south_areas)) {
			    $area_count['south']++;
            } else if (in_array($state, $east_areas)) {
			    $area_count['east']++;
            } else if (in_array($state, $outer_areas)) {
			    $area_count['outer']++;
            }
        }
        return $area_count;
    }

    public function get_gender_data() {
    	$sql = "
          SELECT count(meta_value) as count 
          FROM {$this->wpdb->usermeta} 
          WHERE 
            meta_value = %s AND 
            meta_key= 'member_gender' AND 
            user_id IN (
              SELECT ID 
              FROM {$this->wpdb->users} 
              WHERE 
              user_registered > %s AND 
              user_registered < %s)";

	    $male_count_sql = $this->wpdb->prepare(
		    $sql,
		    ['1', $this->start_date, $this->end_date]
	    );

	    $female_count_sql = $this->wpdb->prepare(
		    $sql,
		    ['2', $this->start_date, $this->end_date]
	    );

        $male_count= $this->wpdb->get_row($male_count_sql)->count;
        $female_count = $this->wpdb->get_row($female_count_sql)->count;
        return array(
            'female' => $female_count,
            'male'=> $male_count
        );
    }

    public function get_average_customer_value() {
	    $orders  = (new WsttsOrder())
		    ->start($this->start_date)
		    ->end($this->end_date)
		    ->inclusive()
		    ->orderby('post_date', 'ASC')
		    ->all()
		    ->get();

	    $orders_count = sizeof($orders);
	    $orders_total = 0;
	    foreach ($orders as $order ) {
	    	/** @var WC_Order $order */
		    $orders_total += $order->get_total() - $order->get_total_shipping();
	    }

	    if($orders_count == 0) {
		    return [
			    'average_customer_value' => 0
		    ];
	    } else {
		    return [
			    'average_customer_value' => round($orders_total / $orders_count, 1)
		    ];
	    }
    }

    public function get_repurchase_rate()
    {
	    $orders  = (new WsttsOrder())
		    ->start($this->start_date)
		    ->end($this->end_date)
		    ->inclusive()
		    ->orderby('post_date', 'ASC')
		    ->all()
		    ->get();

	    $customer_orders = [];
	    /** @var WC_Order $order */
	    foreach ($orders as $order ) {
		    $customer_id = $order->get_user_id();
		    if(!$customer_orders[$customer_id]) $customer_orders[$customer_id]= [];
		    $customer_orders[$customer_id][] = $order->id;
	    }

	    $purchased_count = sizeof($customer_orders);
	    $repurchased_count = 0;
	    foreach ($customer_orders as $customer_order ) {
		    if(sizeof($customer_order) > 1)
			    $repurchased_count++;
	    }

	    if($purchased_count == 0) {
		    return [
			    'repurchase_rate' => 0
		    ];
	    } else {
		    return [
			    'repurchase_rate' => round($repurchased_count / $purchased_count, 1)
		    ];
	    }
    }

}