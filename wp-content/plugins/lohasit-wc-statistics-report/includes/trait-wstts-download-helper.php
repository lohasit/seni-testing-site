<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/2/1
 * Time: 下午 04:35
 */

trait WsttsDownloadHelper {

	private $csv;

	/**
	 * @param string $file_name
	 */
	public function set_header($file_name = 'file')
	{
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=$file_name.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		return $this;
	}

	public function generate_csv($csv_data)
	{
		ob_start();
		$stream = fopen('php://output' ,'w');
		fputs($stream, ( chr(0xEF) . chr(0xBB) . chr(0xBF) ) );
		foreach ($csv_data as $data) {
			fputcsv($stream, array_values($data));
		}
		$this->csv = ob_get_clean();
		fclose($stream);
		ob_end_clean();
		return $this;
	}

	public function download_csv($file_name = 'file', $csv_data)
	{
		$this->generate_csv($csv_data)->set_header($file_name);
		echo $this->csv;
		exit;
	}
}