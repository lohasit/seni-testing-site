<?php
/*
 * Plugin Name: Lohasit WC Statistics Report
 * Description: 樂活壓板系統 - Woocommerce - 新增自訂報表
 * Version: 1.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 */

defined('ABSPATH') or exit;


class WcStatistics
{
	private static $instance;

	private $custom_admin_report_endpoints = [
		'order-refund',
		'order-cancel',
		'order-average-customer-value',
		'order-repurchase-rate',
		'category-sales',
		'product-sales',
		'total-sales',
		'customer-age',
		'customer-gender',
		'customer-state',
	];

	public function __construct()
	{
		$this->init();
	}

	private function init()
	{
		load_plugin_textdomain('wc-statistics', false, basename(dirname(__FILE__)) . '/languages');
		$this->define_constants();
		$this->includes();
		$this->register_hooks();
	}

	private function includes()
	{
		require_once('includes/class-wstts-order.php');
		require_once('includes/class-wstts-product.php');
		require_once('includes/class-wstts-api.php');
		require_once('includes/class-wstts-statistics-data.php');
		require_once('includes/trait-wstts-download-helper.php');
		require_once('includes/trait-wstts-report-helper.php');

	}

	private function register_hooks()
	{
		add_filter('woocommerce_admin_reports', array($this, 'add_admin_reports'));
		add_filter('woocommerce_reports_charts', array($this, 'add_admin_reports'));
		add_filter('wc_admin_reports_path', array($this, 'include_custom_report_files'), 20, 3);
	}

	private function define_constants()
	{
		$this->define_constant('WSTTS_PLUGIN_PATH', plugin_dir_path(__FILE__));
	}

	private function define_constant($name, $value)
	{
		if(!defined($name))
			define($name, $value);
	}

	public static function get_instance()
	{
		if(is_null(self::$instance))
			self::$instance = new self;
		return self::$instance;
	}

	public static function install()
	{
		require_once('includes/class-wstts-installer.php');
		Wstts_Installer::install();
	}

	public function add_admin_reports($reports)
	{
		$reports['orders']['reports']['order_refund'] = [
			'title'       => __('Refunded Orders', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		$reports['orders']['reports']['order_cancel'] = [
			'title'       => __('Cancelled Orders', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		$reports['orders']['reports']['order_average_customer_value'] = [
			'title'       => __('Average Customer Value', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		$reports['orders']['reports']['order_repurchase_rate'] = [
			'title'       => __('Repurchase Rate', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		/** 使用者註冊資料，不使用 */
		/*
		$reports['customers']['reports']['customer_age'] = [
			'title'       => __('By Age', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		$reports['customers']['reports']['customer_gender'] = [
			'title'       => __('By Gender', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];

		$reports['customers']['reports']['customer_state'] = [
			'title'       => __('By State', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];
        */

		$reports['exports']['title'] = __('Year Report Export', 'wc-statistics');
		$reports['exports']['reports']['category_sales'] = [
			'title'       => __('Category Sales Report of Year', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];
		$reports['exports']['reports']['product_sales'] = [
			'title'       => __('Product Sales Report of Year', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];
		$reports['exports']['reports']['total_sales'] = [
			'title'       => __('Total Sales Report of Year', 'wc-statistics'),
			'description' => '',
			'hide_title'  => false,
			'callback'    => array('WC_Admin_Reports', 'get_report')
		];
		return $reports;
	}

	public function include_custom_report_files($file, $name, $class)
	{
		if(in_array($name, $this->custom_admin_report_endpoints)) {
			$file = WSTTS_PLUGIN_PATH.'includes/class-wc-report-' . $name . '.php';
		}

		return $file;
	}
}

$GLOBALS['WcStatistics'] = WcStatistics::get_instance();