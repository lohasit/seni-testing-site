<?php

/**
 * Plugin Name: Lohasit WC Unsold Product List
 * Description: 樂活壓板系統 - Woocommerce - 未銷售商品列表
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 * Version: 1.0.0
 */
class Wc_Unsold_Product_List
{
    private static $instance;

    public function __construct()
    {
        load_plugin_textdomain('wupl', false, basename(dirname( __FILE__ )) . '/languages');
        $this->includes();
        $this->hooks();
    }

    private function hooks()
    {
        add_action('admin_menu', array($this, 'my_custom_submenu_page'));
        add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
    }


    private function includes()
    {

    }

    public static function get_instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function admin_enqueue_scripts()
    {
        wp_register_style('wupl-jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        wp_register_script('wupl-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
        wp_register_style('wupl-bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    }

    public function my_custom_submenu_page()
    {
        add_submenu_page(
            'woocommerce',
            __('Un-sold product list', 'wupl'),
            __('Un-sold product list', 'wupl'),
            'manage_woocommerce',
            'Unsold_Product',
            array($this, 'unsold_product')
        );

    }

    private function get_export_file_name()
    {
        return apply_filters('wupl_export_file_name', __('Un-sold product list', 'wupl'));
    }

    private function get_table_headers()
    {
        return [
            __('Product Name', 'wupl'),
            __('Price', 'wupl')
        ];
    }

    private function get_list($unsold_pre_date = '', $unsold_nxt_date = '')
    {
        $unsold_pre_date = explode("-", $unsold_pre_date);
        $unsold_nxt_date = explode("-", $unsold_nxt_date);

        $filters = array(
            'post_status'    => array('wc-completed'),
            'post_type'      => 'shop_order',
            'posts_per_page' => -1,
            'paged'          => 1,
            'date_query'     => array(
                array(),
            ),
        );
        if ( ! empty($unsold_pre_date[0])) {
            $filters["date_query"][0]["after"]['year']  = $unsold_pre_date[0];
            $filters["date_query"][0]["after"]['month'] = $unsold_pre_date[1];
            $filters["date_query"][0]["after"]['day']   = $unsold_pre_date[2];
        }
        if ( ! empty($unsold_nxt_date[0])) {
            $filters["date_query"][0]["before"]['year']  = $unsold_nxt_date[0];
            $filters["date_query"][0]["before"]['month'] = $unsold_nxt_date[1];
            $filters["date_query"][0]["before"]['day']   = $unsold_nxt_date[2];

        }
        if ( ! empty($unsold_nxt_date[0]) && ! empty($unsold_pre_date[0])) {
            $filters["date_query"][0]["inclusive"] = true;
        }

        $loop  = new WP_Query($filters);
        $liste = array();
        while ($loop->have_posts()) {
            $loop->the_post();
            $order = new WC_Order($loop->post->ID);
            foreach ($order->get_items() as $key => $lineItem) {
                if ( ! in_array($lineItem['product_id'], $liste)) {
                    array_push($liste, $lineItem['product_id']);
                }
            }
        }


        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'date_query'     => array(
                array(),
            ),
        );
        if ( ! empty($unsold_pre_date[0])) {
            $args["date_query"][0]["after"]['year']  = $unsold_pre_date[0];
            $args["date_query"][0]["after"]['month'] = $unsold_pre_date[1];
            $args["date_query"][0]["after"]['day']   = $unsold_pre_date[2];
        }
        if ( ! empty($unsold_nxt_date[0])) {
            $args["date_query"][0]["before"]['year']  = $unsold_nxt_date[0];
            $args["date_query"][0]["before"]['month'] = $unsold_nxt_date[1];
            $args["date_query"][0]["before"]['day']   = $unsold_nxt_date[2];

        }
        if ( ! empty($unsold_nxt_date[0]) && ! empty($unsold_pre_date[0])) {
            $args["date_query"][0]["inclusive"] = true;
        }

        $loop         = new WP_Query($args);
        $product_list = array();
        while ($loop->have_posts()) {
            $loop->the_post();
            $product_id = $loop->post->ID;
            array_push($product_list, $product_id);
        }

        foreach ($liste as $list) {
            $key = array_search($list, $product_list);
            unset($product_list[$key]);
        }
        return $product_list;
    }

    public function unsold_product()
    {
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('wupl-bootstrap');
        wp_enqueue_style('wupl-jquery-ui');
        wp_enqueue_style('wupl-bootstrap');
        if (isset($_POST['pre_date'])) {
            $pre_date = $_POST['pre_date'];
            update_option('wupl_unsold_pre_date', $pre_date);
        }
        if (isset($_POST['nxt_date'])) {
            $nxt_date = $_POST['nxt_date'];
            update_option('wupl_unsold_nxt_date', $nxt_date);
        }
        $unsold_pre_date = get_option('wupl_unsold_pre_date');
        $unsold_nxt_date = get_option('wupl_unsold_nxt_date');

        $product_list = $this->get_list($unsold_pre_date, $unsold_nxt_date);


        if($_POST['action'] === 'export') {
            $table_data = [$this->get_table_headers()];
            foreach ($product_list as $product_id) {
                $_product = wc_get_product($product_id);
                array_push($table_data, [
                    strip_tags($_product->get_title()),
                    $_product->get_price()
                ]);
            }
            ob_start();
            $stream = fopen('php://output' ,'w');
            fputs($stream, ( chr(0xEF) . chr(0xBB) . chr(0xBF) ) );
            foreach ($table_data as $line ) {
                fputcsv($stream, array_values($line));
            }
            $csv = ob_get_clean();
            fclose($stream);
            ob_end_clean();
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=" . $this->get_export_file_name() . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $csv;
            exit;
        }
        ?>
        <script>
            jQuery(function ($) {
                $('.time-picker').datepicker({
                    dateFormat: 'yy-mm-dd',
                    showButtonPanel: true,
                    changeMonth: true,
                    changeYear: true,
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>
        <style>
            #wupl-container {
                padding: 10px;
            }
            #wupl-container .table {
                margin: 10px 0;
            }
        </style>
        <div class="container-fluid" id="wupl-container">
            <div class="row">
                <div class="col">
                    <h1><?=__('Un-sold product list', 'wupl')?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <form action="" method="post">
                        <div class="row">
                            <div class="form-group col-12 col-md-6 row">
                                <label for="start-time" class="font-weight-bold col col-md-3 col-form-label"><?=__('From date', 'wupl')?></label>
                                <div class="col">
                                    <input type="text" name="pre_date" id="start-time" class="form-control form-control-sm time-picker" value="<?=$unsold_pre_date?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-12 col-md-6 row">
                                <label for="end-time" class="font-weight-bold col col-md-3 col-form-label"><?=__('To date', 'wupl')?></label>
                                <div class="col">
                                    <input type="text" name="nxt_date" id="end-time" class="form-control form-control-sm time-picker" value="<?=$unsold_nxt_date?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <button class="btn btn-success btn-sm"><?=__('Query', 'wupl')?></button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="action" value="query">
                    </form>
                </div>
            </div>

            <?php if(!empty($_POST['action']) && $_POST['action'] == 'query'):?>
            <div class="row export">
                <div class="col">
                    <form action="" method="POST">
                        <div class="form-group">
                            <input type="hidden" name="pre_date" id="start-time" class="form-control form-control-sm time-picker" value="<?=$unsold_pre_date?>" autocomplete="off">
                            <input type="hidden" name="nxt_date" id="end-time" class="form-control form-control-sm time-picker" value="<?=$unsold_nxt_date?>" autocomplete="off">
                            <button class="btn btn-primary btn-sm"><?=__('Export', 'wupl')?></button>
                            <input type="hidden" name="action" value="export">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table class="table table-sm table-striped table-bordered">
                        <thead>
                        <tr>
                            <?php foreach ($this->get_table_headers() as $table_header): ?>
                                <th><?=$table_header?></th>
                            <?php endforeach; ?>
                            
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($product_list)):?>
                        <?php
                        foreach ($product_list as $product_id) {
                            $_product = wc_get_product($product_id);
                            ?>
                            <tr>
                                <td><a href="<?php echo $_product->get_permalink() ?>" target="_blank"><?php echo $_product->get_title(); ?></a></td>
                                <td> <?php echo $_product->get_price_html(); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php else:?>
                            <tr>
                                <td colspan="2"><?=__('No product found', 'wupl')?></td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif;?>
        </div>

        <?php
    }
}

add_action('plugins_loaded', array('Wc_Unsold_Product_List', 'get_instance'));

?>