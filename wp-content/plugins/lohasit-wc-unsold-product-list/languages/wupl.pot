#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: WC Unsold Product List\n"
"POT-Creation-Date: 2019-01-14 15:48+0800\n"
"PO-Revision-Date: 2019-01-14 15:34+0800\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: index.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: index.php:53 index.php:54 index.php:64 index.php:223
msgid "Un-sold product list"
msgstr ""

#: index.php:70
msgid "Product Name"
msgstr ""

#: index.php:71
msgid "Price"
msgstr ""

#: index.php:231
msgid "From date"
msgstr ""

#: index.php:237
msgid "To date"
msgstr ""

#: index.php:245
msgid "Query"
msgstr ""

#: index.php:280
msgid "No product found"
msgstr ""

#: index.php:292
msgid "Export"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WC Unsold Product List"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://wordpress.org/plugins/"
msgstr ""

#. Description of the plugin/theme
msgid "未銷售商品列表"
msgstr ""

#. Author of the plugin/theme
msgid "kroutony"
msgstr ""
