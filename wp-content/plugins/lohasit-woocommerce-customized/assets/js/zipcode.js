(function($){
    /* alan:如果只銷售到台灣隱藏國家欄位 */
    if (zipcode.only_taiwan) {
        $('#billing_country_field, #shipping_country_field').css('display', 'none');
    }

    jQuery(function($){
        $('#billing_state_field, #shipping_state_field')
            .removeClass('form-row-wide')
            .addClass('form-row-first');

        $('#billing_city_field, #shipping_city_field')
            .removeClass('form-row-wide')
            .addClass('form-row-last');

        $('#billing_postcode, #shipping_postcode').on('click', function() {
            if ($(this).attr('name') == 'billing_postcode') {
                var $postcode = $('#billing_postcode');
                var $address = $('#billing_state').val() +
                    $('#billing_city').val() + $('#billing_address_1').val();

            } else if ($(this).attr('name') == 'shipping_postcode') {
                var $postcode = $('#shipping_postcode');
                var $address = $('#shipping_state').val() +
                    $('#shipping_city').val() + $('#shipping_address_1').val();
            }

            $.ajax({
                url: zipcode.api_url,
                type: 'GET',
                dataType: 'json',
                data: {
                    'address': $address,
                },
                beforeSend: function() {
                    $postcode.val(zipcode.waiting)
                        .attr('readonly', true);
                },
                success: function(resp) {
                    if (resp.success) {
                        if (resp.data.zipcode) {
                            $postcode.val(resp.data.zipcode);
                        } else {
                            $postcode.val('');
                            alert(zipcode.confirm + "\r\n" + $address)
                        }

                    } else {
                            $postcode.val('');
                            alert(zipcode.confirm + "\r\n" + $address)
                        // $postcode.unbind('click');
                        // $postcode.val('')
                        //     .attr('readonly', false);
                    }
                },
                error: function(xhr) {
                    $postcode.unbind('click');
                    $postcode.val('')
                        .attr('readonly', false);
                }
            });
        });
    });
})(jQuery);