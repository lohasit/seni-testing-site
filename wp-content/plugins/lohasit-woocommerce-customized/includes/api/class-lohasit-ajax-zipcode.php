<?php

/**
 * 結帳頁3+2郵遞區號ajax
 * @package lohasit
 * @author alanchang15@gmail.com
 */

use Recca0120\Twzipcode\Zipcode;

class Lohasit_Ajax_Zipcode
{
    /**
     * 查詢3+2郵遞區號API網址
     * @var string
     */
    protected $api_url;

    /**
     * 建構子
     * @return void
     */
    public function __construct()
    {
        // $this->api_url = 'https://zip5.5432.tw/json/';
    }

    /**
     * 處理3+2郵遞區號查詢
     * @return void
     */
    public function handle()
    {
        $address = $_GET['address'];

        try {
            $zipcode = Zipcode::parse($address);

            if (strlen($zipcode->zip5()) < 5) {
                wp_send_json_error([
                    'message' => '請確認地址是否填寫正確',
                ]);
            } else {
                wp_send_json_success([
                    'zipcode'     => $zipcode->zip5(),
                    'new_address' => $zipcode->shortAddress(),
                    'message'     => 'success',
                ]);
            }
        } catch (Exception $e) {
            wp_send_json_error([
                'message' => $e->getMessage(),
            ]);
        }

        // $response = wp_remote_get($this->api_url . $address);

        // if (is_array($response)
        //     && !is_wp_error($response)
        //     && $response['response']['code'] == '200'
        // ) {
        //     $body = json_decode($response['body']);
        //     wp_send_json_success([
        //         'zipcode'     => $body->zipcode,
        //         'new_address' => $body->new_adrs2,
        //         'message'     => 'success',
        //     ]);
        // } else {
        //     $error = $response->get_error_message();
        //     lohasit_logger($error, 'zipcode-api');

        //     wp_send_json_error([
        //         'message' => $error,
        //     ]);
        // }
    }
}

return new Lohasit_Ajax_Zipcode;
