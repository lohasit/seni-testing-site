<?php

class Lohasit_Backend
{
    public function set_shop_manager_editable_roles($roles)
    {
        $roles = [
            'author',
            'contributor',
            'subscriber',
            'vip',
            'vvip',
            'editor',
            'translator',
            'seller',
            'customer',
            'shop_manager',
        ];

        return $roles;
    }

    /**
     * YITH的"紅利點數"&"動態定價"位置 加到 "優惠設定"位置、YITH的"願望清單"位置 加到 "購物車系統"位置
     * @param $args
     * @return mixed
     */
    public function adjust_yith_submenu($args)
    {

        $yith_pages = [
            'yith_woocommerce_points_and_rewards',
            'yith_woocommerce_dynamic_pricing_and_discounts',
            'yith_wcwl_panel',

        ];

        if (in_array($args['page'], $yith_pages)) {


            update_user_meta( get_current_user_id(), 'yith-license-banner', '' );

            $args['parent_page'] = 'woocommerce-marketing';

        }

        if($args['page'] == 'yith_woocommerce_request_a_quote'):

            $args['parent_page'] = 'woocommerce';

            endif;


        return $args;

    }

    /**
     * 新訂單產生時，排程檢查是否未付款
     * @hooked woocommerce_new_order
     *
     * @param $order_id
     */
    // public function schedule_cancel_expired_order($order_id)
    // {
    //     // 取得WooCommerce設定的過期分鐘數
    //     $woocommerce_hold_stock_minutes = get_option('woocommerce_hold_stock_minutes');
    //     if ($woocommerce_hold_stock_minutes) {
    //         wp_schedule_single_event(time() + $woocommerce_hold_stock_minutes * 60, 'lohasit_cancel_expired_order', [$order_id, $woocommerce_hold_stock_minutes]);
    //     }
    // }

    /**
     * 取消未付款訂單
     * @hooked lohasit_cancel_expired_order
     *
     * @param $order_id
     * @param $woocommerce_hold_stock_minutes
     */
    // public function cancel_expired_order($order_id, $woocommerce_hold_stock_minutes)
    // {
    //     $order = wc_get_order($order_id);
    //     if (in_array($order->get_status(), ['pending', 'on-hold'])) {
    //         $order->cancel_order(sprintf(__("Pending order over %d minutes", 'lohasit'), $woocommerce_hold_stock_minutes) . '<br>');
    //     }
    // }

    // public function restore_order_stock_after_cancelling($order_id, $from_status, $to_status)
    // {
    //     if (in_array($to_status, apply_filters('lohasit_wc_restock_statuses', ['cancelled', 'failed']))) {
    //         if (get_post_meta($order_id, '_order_stock_reduced', true)) {
    //             $order      = wc_get_order($order_id);
    //             $order_note = '';
    //             foreach ($order->get_items() as $item) {
    //                 $product_id = $item['variation_id'] ? $item['variation_id'] : $item['product_id'];
    //                 $product    = wc_get_product($product_id);
    //                 if ($product->managing_stock()) {
    //                     $item_qty = $item['qty'];
    //                     $product->increase_stock($item_qty);
    //                     $order_note .= $product->get_sku() . $product->get_title() . '，庫存已回復:' . $item_qty . '<br>';
    //                 }
    //             }
    //             $order->add_order_note($order_note);
    //             delete_post_meta($order_id, '_order_stock_reduced');
    //         }
    //     }
    // }

    // === [20220125] 從class-lohasit-jj 轉移過來的函式 開端 ===

    public function modify_pd_type($pd_type = array())
    {
        unset($pd_type['external']);
        return $pd_type;
    }

    public function remove_woocommerce_setting_tabs( $tabs )
    {

        if(!current_user_can('administrator'))  :

            unset($tabs['products']);
            unset($tabs['account']);
            unset($tabs['email']);
            unset($tabs['integration']);
            unset($tabs['advanced']);
            unset($tabs['alg_wc_ev']);
            unset($tabs['lohasit_ecpay_invoice']);
//            unset($tabs['shipping']);
////            unset($tabs['checkout']);

        endif;

        return $tabs;
    }

    public function modify_yith_dynamic_pricing_rule_metaboxes($metaboxes)
    {


//        unset($metaboxes['disable_on_sale']);
        unset($metaboxes['no_apply_with_other_rules']);
//        unset($metaboxes['priority']);
        unset($metaboxes['quantity_based']['options']['single_product']);
        unset($metaboxes['quantity_based']['options']['single_variation_product']);



        return $metaboxes;
    }

    public function modify_yith_dynamic_pricing_rule_options($options)
    {


//        unset($options['discount_mode']['gift_products']);
        unset($options['discount_mode']['discount_whole']);
        unset($options['discount_mode']['category_discount']);
        unset($options['discount_mode']['exclude_items']);

        return $options;
    }

    public function modify_yith_dynamic_pricing_general_options($options)
    {

        if(!current_user_can('administrator')) :

            unset($options['general']['general_enable_shop_manager']);

            unset($options['general']['product_page_section_start']);
            unset($options['general']['product_page_show_message']);
            unset($options['general']['product_page_message_position']);
            unset($options['general']['product_page_show_qty_table']);
            unset($options['general']['product_page_qty_style']);
            unset($options['general']['product_page_qty_position']);
            unset($options['general']['product_page_qty_title']);
            unset($options['general']['product_page_qty_labels']);
            unset($options['general']['product_page_expire_date']);
            unset($options['general']['product_page_show_as_default']);
            unset($options['general']['product_page_default_price_selected']);
            unset($options['general']['product_page_change_price_dynamically']);
            unset($options['general']['product_page_section_end']);

            unset($options['general']['cart_section_start']);
            unset($options['general']['cart_coupon_setting']);
            unset($options['general']['cart_coupon_calculation_setting']);
            unset($options['general']['cart_show_subtotal_mode']);
            unset($options['general']['cart_notices']);
            unset($options['general']['discount_message_on_cart']);
            unset($options['general']['cart_section_end']);

            unset($options['general']['wpml_section_start']);
            unset($options['general']['wpml_extension']);
            unset($options['general']['wpml_section_end']);

        endif;

        return $options;
    }

    public function modify_yith_cart_rule_metaboxes($metaboxes)
    {
        unset($metaboxes['allow_free_shipping']);
        return $metaboxes;
    }

    public function modify_yith_reward_points_tabs($tabs)
    {

        if(!current_user_can('administrator')) :

            unset($tabs['general']);
            unset($tabs['labels']);
            unset($tabs['emails']);
            unset($tabs['import']);

        endif;

        return $tabs;
    }

    public function modify_yith_reward_points_standard_options($tabs)
    {

        if (!current_user_can('administrator')) :

            unset($tabs['points-standard']['assign_points_to_registered_guest']);
            unset($tabs['points-standard']['assign_older_orders_points_to_new_registered_user']);
            unset($tabs['points-standard']['order_status_to_earn_points']);

        endif;

        return $tabs;

    }

    public function modify_yith_wishlist_tabs($tabs)
    {
        if (!current_user_can('administrator')) :

            unset($tabs['ask_an_estimate']);
            unset($tabs['promotion_email']);
            unset($tabs['settings']);
            unset($tabs['add_to_wishlist']);
            unset($tabs['wishlist_page']);
            unset($tabs['premium']);
            unset($tabs['help']);

        endif;

        return $tabs;
    }

    public function modify_yith_request_a_quote_tabs($tabs)
    {
        if (!current_user_can('administrator')) :

            unset($tabs['request']);
            unset($tabs['quote']);
//            unset($tabs['exclusions']);

        endif;

        return $tabs;
    }

    public function modify_yith_request_a_quote_order_metaboxes($metaboxes)
    {
        if (!current_user_can('administrator')) :

//            unset($metaboxes['ywraq_pdf_preview']);
            unset($metaboxes['ywcm_request_response_title2']);
            unset($metaboxes['ywraq_checkout_info']);
            unset($metaboxes['ywraq_override_quote_payment_options']);
            unset($metaboxes['ywraq_pay_quote_now']);
            unset($metaboxes['ywraq_disable_shipping_method']);
            unset($metaboxes['ywraq_lock_editing']);
            unset($metaboxes['ywraq_customer_sep2']);
            unset($metaboxes['ywraq_enable_expiry_date']);
            unset($metaboxes['ywcm_request_expire']);

        endif;

        return $metaboxes;
    }

    public function pdf_font_family( $font ){
        return '"fireflysung", '.$font;
    }

    public function modify_yith_request_a_quote_generals_settings_options($options)
    {


        if( !current_user_can('administrator') ) :
//            dd($options);
            unset($options['show_button_near_add_to_cart']);
            unset($options['show_btn_other_pages']);
            unset($options['show_btn_woocommerce_blocks']);
            unset($options['show_button_on_checkout_page']);
            unset($options['after_click_action']);

            unset($options['show_btn_link']);
            unset($options['raq_checkout_button_style']);
            unset($options['show_btn_link_text']);
            unset($options['checkout_quote_button_label']);
            unset($options['show_product_added']);
            unset($options['show_already_in_quote']);
            unset($options['show_browse_list']);
            unset($options['loader_style']);
            unset($options['loader_image']);
            unset($options['enable_ajax_loading']);

        endif;

        return $options;
    }

    // === [20220125] 從class-lohasit-jj 轉移過來的函式 結尾 ===
}

return new Lohasit_Backend;
