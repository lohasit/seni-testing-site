<?php

class Lohasit_Cart
{
    // [20220125] 從class-lohasit-checkout 轉移過來
    public function add_return_to_shop_btn()
    {
        ?>
        <a href="<?php echo esc_url(wc_get_page_permalink('shop')); ?>" class="shop-button checkout-button button alt wc-forward">
            <?php esc_html_e('Return to shop', 'woocommerce');?>
        </a>
        <?php

    }

    // [20220125] 從class-lohasit-checkout 轉移過來
    public function woocommerce_auto_update_cart()
    {
        if (is_cart()):
            ?>
            <script>
                (function($){
                    $('div.woocommerce').on('change', '.qty', function(){
                        var $updateCartButton = $("[name='update_cart']");
                        $updateCartButton.removeAttr('disabled');
                        $updateCartButton.click();
                    });
                })(jQuery);
            </script>
        <?php endif;
    }

    // [20220125] 從class-lohasit-jj 轉移過來
    public function cart_validation($true, $product_id, $quantity)
    {

        $notice = '';

        // 如果購物車是空的，跳過判斷
        if ( empty(WC()->cart->get_cart()) ) return $true;

        // 如果當下的商品是可變商品，需獲取variation_id進一步判斷
        if ( isset($_POST['variation_id']) && !empty(($_POST['variation_id'])) ) {

            $current_product = wc_get_product($_POST['variation_id']);

        } else {

            $current_product = wc_get_product($product_id);

        }


        foreach (WC()->cart->get_cart() as $cart_item) {

            $cart_product_id = '';

            // 如果購物車含可變商品，需獲取variation_id進一步判斷
            if ( $cart_item['variation_id'] == 0 ) {

                $cart_product_id = $cart_item['product_id'];

            } else {

                $cart_product_id = $cart_item['variation_id'];
            }

            // 如果購物的商品和當下要購買的商品都屬於實體 / 都屬於虛擬，用戶可以將當下要購買的商品加入購物車，否則提出警示
            if ( (wc_get_product($cart_product_id)->is_virtual() && $current_product->is_virtual()) ||   ((!wc_get_product($cart_product_id)->is_virtual()) && (!$current_product->is_virtual())) ) {

            } else {

                if ( $current_product->is_virtual() ) {

//                    $notice = __('為了準確記錄訂單的物流狀況，此商品無法與實體商品一同結帳，請先幫購物車中的實體商品結帳，再下單購買此商品喔。', 'lohasit-woocommerce-customized');
                    $notice = __('Please complete the purchase of non-virtual products at checkout before adding any virtual product to cart.', 'lohasit-woocommerce-customized');

                } else {

//                    $notice = __('為了準確記錄訂單的物流狀況，此商品無法與虛擬商品一同結帳，請先幫購物車中的虛擬商品結帳，再下單購買此商品喔。', 'lohasit-woocommerce-customized');
                    $notice = __('Please complete the purchase of virtual products at checkout before adding any non-virtual product to cart.', 'lohasit-woocommerce-customized');

                }

                wc_add_notice( $notice, 'error');

                return false;

            }

            return $true;

        }

        wc_add_notice( $notice, 'success');

        return false;
    }

}

return new Lohasit_Cart;