<?php

use Recca0120\Twzipcode\Zipcode;

class Lohasit_Checkout
{
    public function __construct()
    {
        if (!class_exists(Zipcode::class)) {
            throw new Exception('recca0120/twzipcode required, please install via composer: comoposer require recca0120/twzipcode');
        }
    }

    public function load_scripts()
    {
        $path = LOHASIT_PLUGIN_URL . 'assets/js/zipcode.js';
        wp_enqueue_script('lohasit-zipcode', $path, ['jquery', 'woocommerce'], time(), true);

        $allowed_countries = WC()->countries->get_allowed_countries();
        $only_taiwan       = (count($allowed_countries) == 1 && array_key_exists('TW', $allowed_countries))
        ? true : false;

        wp_localize_script('lohasit-zipcode', 'zipcode', [
            'api_url'     => WC_AJAX::get_endpoint('zipcode'),
            'waiting'     => '資料處理中...',
            'confirm'     => '請確認您的地址是否正確',
            'only_taiwan' => $only_taiwan,
        ]);

        wp_register_style('custom', LOHASIT_PLUGIN_URL . 'assets/css/custom.css', [], time());
        wp_enqueue_style('custom');
    }

    /**
     * 載入縣市資料
     * @param  array $states
     * @return array
     */
    public function load_states($states = [])
    {
        $states['TW'] = include LOHASIT_PLUGIN_PATH . 'data/states/TW.php';

        return $states;
    }

    /**
     * 載入地區資料
     * @param  array $cities
     * @return array
     */
    public function load_cities($cities = [])
    {
        $cities['TW'] = include LOHASIT_PLUGIN_PATH . 'data/cities/TW.php';

        return $cities;
    }

    /**
     * 台灣地址格式化
     * @param  array $formats
     * @return array
     */
    public function custom_address_format($formats = [])
    {
        $formats['default'] = "{name}\n{postcode}{state_code}{city}{address_1}\n{address_2}\n{company}";
        $formats['TW']      = $formats['default'];

        return $formats;
    }

    public function checkout_validation_tw_mobile($data)
    {
        // 檢查手機號碼
        $phone = (isset($_POST['billing_phone']) ? trim($_POST['billing_phone']) : '');
        if (!preg_match("/^09[0-9]{2}[0-9]{3}[0-9]{3}$/", $phone)) {
            wc_add_notice(__('聯絡電話只接受手機格式，例:手機0988888888'), 'error');
        }

        // 檢查手機號碼
        if ($_POST['ship_to_different_address']) {
            $phone = (isset($_POST['shipping_phone']) ? trim($_POST['shipping_phone']) : '');
            if (!preg_match("/^09[0-9]{2}[0-9]{3}[0-9]{3}$/", $phone)) {
                wc_add_notice(__('收件人電話只接受手機格式，例:手機0988888888'), 'error');
            }
        }
    }

    public function start_ecpay_store_session() {
        if(!session_id()) {
            session_start();
        }
    }

}

return new Lohasit_Checkout;
