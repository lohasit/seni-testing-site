<?php


class Lohasit_Coupon
{

   // [20220128 新增顯示限制：非屬於使用者，非折價券的記錄將隱藏]
    public function show_customer_coupons($atts)
    {

        global $wpdb;
        global $current_user;
        $atts = shortcode_atts([
            'post_type'=>'shop_coupon', // your post type name
            'post_status' => 'publish',
        ],$atts );

        $data= new WP_Query($atts);
        echo '<div class="coupon_table">';

        if($data->have_posts()) :

            while($data->have_posts())  : $data->the_post();

                $is_user_coupon = true;

                if(str_contains(get_the_title(), 'quotediscount_') || str_contains(get_the_title(), 'ywpar_discount_')) :

                    $is_user_coupon = false;

                endif;


                $coupon_receivers = get_post_meta(get_the_ID(), 'customer_email');

                if( $is_user_coupon && (empty($coupon_receivers[0]) || in_array($current_user->user_email, $coupon_receivers[0]))) :

                $limit_coupon = get_post_meta(get_the_ID(), 'usage_limit_per_user', true);
                $expired_date = get_post_meta(get_the_ID(), 'date_expires', true);

                // Get current user id
                $user_id = get_current_user_id();

                // Coupon id
                $coupon_id = get_the_ID();

                // Get an array of used by count based on post_id, meta_key and meta_value (user_id)
                $used_by_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->postmeta WHERE post_id = '$coupon_id' AND meta_key = '_used_by' AND meta_value = '$user_id'");

                $current_time = current_time('timestamp');

                if ($limit_coupon <= $used_by_count  && $limit_coupon != 0) :
//                    $status_coupon = '已使用';
                    $status_coupon = __('Used', 'lohasit-woocommerce-customized');
                    $class_coupon = 'used';
                elseif($expired_date < $current_time && $expired_date != '') :

//                    $status_coupon = '失效';
                    $status_coupon = __('Invalid', 'lohasit-woocommerce-customized');
                    $class_coupon = 'invalid';

                else :

//                    $status_coupon = '有效';
                    $status_coupon = __('Valid', 'lohasit-woocommerce-customized');
                    $class_coupon = 'valid';

                endif;

                if ( $expired_date=='' ):

//                    $expired = '無限';
                    $expired = __('No ', 'lohasit-woocommerce-customized');

                else :

                    $expired = gmdate( 'Y-m-d', $expired_date );

                endif;

                if ($limit_coupon=='0' or $limit_coupon=='') :

//                    $limit_use = '無限';
                    $limit_use = __('No ', 'lohasit-woocommerce-customized');;

                else:

                    $limit_use = $limit_coupon;

                endif;


                echo '<div class="cus-coupon-table">';
                echo '<div class="cus-coupon">';
                echo '<div class="cus-coupon-number">';
                echo '<input type="text" value="'. get_the_title() .'" class="cus-copy-number" style="opacity:0">';
                echo '</div>';
                echo '<div class="cus-copy">';
                echo '<div class="cus-copy-bt"><img src="'. LOHASIT_PLUGIN_URL .'assets/img/coupon-copy-icon.svg"></div>';
                echo '</div>';
                echo '<div class="cus-coupon-info">';
                echo '<div class="cus-title"> '. __("Code:", "lohasit-woocommerce-customized") .  get_the_title()  .'</div>';
                echo '<div class="cus-description">' . get_the_excerpt() .'</div>';
                echo '<div class="cus-use-coupon">'. __("Remaining coupons:", "lohasit-woocommerce-customized") . $limit_use . '</div>';
                echo '<div class="cus-used-coupon">'. __("Used coupons:", "lohasit-woocommerce-customized") . $used_by_count . '</div>';
                echo '<div class="cus-coupon-date">' . __("Expiry date:", "lohasit-woocommerce-customized")  . $expired . '</div>';
                echo '<div class="cus-coupon-tag">';
                echo '<div class="cus-coupon-status-'. $class_coupon .'">' . $status_coupon . '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                endif;

            endwhile;

        endif;

        echo '</table>';
        echo '</div>';
        wp_reset_postdata();



        ?>

        <script>
            jQuery(function($){

                //優惠券複製功能
                $('.cus-copy-bt').click(function(){
                    $(this).siblings().css("display","block");
                    $(this).parent().parent().find('.cus-copy-number').select();
                    navigator.clipboard.writeText( $(this).parent().parent().find('.cus-copy-number').val());
                    $(this).children('img').attr('src','/wp-content/plugins/lohasit-woocommerce-customized/assets/img/fa-regular_copy_active.svg')
                });

            })

        </script>

        <?php

    }

    public function cart_coupon_discount_text($string, $coupon)
    {
//        if ( is_null( $coupon ) || ! is_object( $coupon ) ) {
//            return $string;
//
//        }

        $coupon_label = [

              'quote' =>  __('Quoted discount' , 'lohasit-woocommerce-customized'),
              'points' =>  __('Points redeemed' , 'lohasit-woocommerce-customized'),
              'coupon' =>  sprintf(__('Coupon: %s' , 'lohasit-woocommerce-customized'), $coupon->get_code()),

            ];

        $coupon_type = $coupon->get_code();

        switch($coupon_type) {

            case (str_contains($coupon_type, 'quotediscount_')) :
                $coupon_type = 'quote';
                break;
            case (str_contains($coupon_type, 'ywpar_discount_')):
                $coupon_type = 'points';
                break;
            default:
                $coupon_type = 'coupon';

        }

        return $coupon_label[$coupon_type];

    }

}





return new Lohasit_Coupon;