<?php

/**
 * Alan把JJ寫在子佈景lohasit-custom.php內的拉過來
 */
class Lohasit_JJ
{
    public function adjust_shipping_fields($fields)
    {
        // 新增運送地址的聯絡電話欄位
        $fields['shipping_phone'] = array(
            'label'       => __('聯絡電話', 'lohasit'),
            'required'    => false,
            'class'       => array('form-row-last', 'address-field'),
            'priority'    => 15,
            'placeholder' => __('請輸入聯絡電話號碼', 'lohasit'),

        );

        // 調整運送地址欄位的位置
        $fields['shipping_phone']['priority']     = 15;
        $fields['shipping_company']['priority']   = 17;
        $fields['shipping_state']['priority']     = 18;
        $fields['shipping_city']['priority']      = 19;
        $fields['shipping_address_1']['priority'] = 20;

        // 調整運送地址欄位的寬度
        $fields['shipping_state']['class'] = array('form-row-first', 'address-field');
        $fields['shipping_city']['class']  = array('form-row-last', 'address-field');

        // 移除運送地址中的姓氏欄位和地址2欄位
        unset($fields['shipping_last_name']);
        unset($fields['shipping_address_2']);

        return $fields;
    }

    public function adjust_billing_fields($fields)
    {
        // 調整賬單地址欄位的位置
        $fields['billing_phone']['priority']   = 15;
        $fields['billing_email']['priority']   = 16;
        $fields['billing_company']['priority'] = 17;
        $fields['billing_state']['priority']   = 18;
        $fields['billing_city']['priority']    = 19;
        $fields['billing_city']['address_1']    = 20;

        // 調整賬單地址欄位的寬度
        $fields['billing_phone']['class'] = array('form-row-last', 'address-field');
        $fields['billing_email']['class'] = array('form-row-first', 'address-field');
        $fields['billing_company']['class'] = array('form-row-last', 'address-field');
        $fields['billing_state']['class'] = array('form-row-first', 'address-field');
        $fields['billing_city']['class']  = array('form-row-last', 'address-field');



        // 移除賬單地址中的姓氏欄位和地址2欄位
        unset($fields['billing_last_name']);
        unset($fields['billing_address_2']);

        // 另外調整： wp-content/plugins/woocommerce/includes/class-wc-countries.php

        return $fields;
    }

    public function load_front_page_css()
    {
        $style_uri          = get_stylesheet_directory_uri();
        $cart_css           = '/assets/page/cart.css';
        $checkout_css       = '/assets/page/checkout.css';
        $home_css           = '/assets/page/home.css';
        $single_product_css = '/assets/page/single-product.css';

        if (file_exists(get_stylesheet_directory() . $cart_css) && is_cart()) {
            wp_enqueue_style("lohasit_css", $style_uri . $cart_css);
        }
        if (file_exists(get_stylesheet_directory() . $checkout_css) && is_checkout()) {
            wp_enqueue_style("lohasit_css", $style_uri . $checkout_css);
        }
        if (file_exists(get_stylesheet_directory() . $home_css) && is_home()) {
            wp_enqueue_style("lohasit_css", $style_uri . $home_css);
        }
    }

    public function hide_shipping_details()
    {
        global $wp;

        $order_id = $wp->query_vars['view-order'] ?? $wp->query_vars['order-received'];

        $order = wc_get_order($order_id);

        if ('lohasit_ecpay_cvs' === get_shipping_method_id($order)) {
            ?>
            <script>
                jQuery(function($){
                    $('.woocommerce-column--shipping-address').css('display','none');
                });
            </script>
            <?php

        }
    }

    public function add_discount_details_to_simple_product()
    {
        global $product;

        if ($product->is_type('simple')) {

            $discount_percentage = floor((wc_get_price_to_display($product) / $product->get_regular_price()) * 100);
            $discount_total      = $product->get_regular_price() - wc_get_price_to_display($product);

            if ($product->is_on_sale() || $discount_total != 0) {
                ?>
            <div class="discount-details">
                <span class="discount-percentage"><?=$discount_percentage?>折</span>
                <span class="discount-total">省下 <?=get_woocommerce_currency_symbol()?><?=$discount_total?></span>
            </div>
            <?php

            }
        }
    }

    public function add_discount_details_to_variable_product()
    {
        global $product;

        if ($product->is_type('variable')) {

            $variations_id = wp_list_pluck($product->get_available_variations(), 'variation_id');

            for ($x = 0; $x < count($variations_id); $x++) {
                $variation[$x]       = wc_get_product($variations_id[$x]);
                $discount_percentage = floor((wc_get_price_to_display($variation[$x]) / $variation[$x]->get_regular_price()) * 100);
                $discount_total      = $variation[$x]->get_regular_price() - wc_get_price_to_display($variation[$x]);
                $attribute           = implode(',', array_values($variation[$x]->get_attributes()));

                if ($variation[$x]->is_on_sale() || $discount_total != 0) {
                    ?>
                    <div class="discount-details">
                        <span class="discount-attribute"><?=$attribute?> : </span>
                        <span class="discount-percentage"><?=$discount_percentage?>折</span>
                        <span class="discount-total">省下 <?=get_woocommerce_currency_symbol()?><?=$discount_total?></span>
                    </div>
                    <?php

                }
            }
        }
    }

    public function hide_unwanted_fields()
    {

        ?>
        <script>
            jQuery(function($){
                $('label[for=last_name], input#last_name').hide();
                $('label[for=_billing_last_name], input#_billing_last_name').hide();
                $('label[for=_shipping_last_name], input#_shipping_last_name').hide();
                $('label[for=_billing_address_2], input#_billing_address_2').hide();
                $('label[for=_shipping_address_2], input#_shipping_address_2').hide();
            });
        </script>

        <?php

    }

    public function modify_pd_type($pd_type = array())
    {
        unset($pd_type['external']);
        return $pd_type;
    }

    public function cart_validation($true, $product_id, $quantity)
    {

        $notice = '';

        // 如果購物車是空的，跳過判斷
        if ( empty(WC()->cart->get_cart()) ) return $true;

        // 如果當下的商品是可變商品，需獲取variation_id進一步判斷
        if ( isset($_POST['variation_id']) && !empty(($_POST['variation_id'])) ) {

            $current_product = wc_get_product($_POST['variation_id']);

        } else {

            $current_product = wc_get_product($product_id);

        }


        foreach (WC()->cart->get_cart() as $cart_item) {

            $cart_product_id = '';

            // 如果購物車含可變商品，需獲取variation_id進一步判斷
            if ( $cart_item['variation_id'] == 0 ) {

                $cart_product_id = $cart_item['product_id'];

            } else {

                $cart_product_id = $cart_item['variation_id'];
            }

            // 如果購物的商品和當下要購買的商品都屬於實體 / 都屬於虛擬，用戶可以將當下要購買的商品加入購物車，否則提出警示
            if ( (wc_get_product($cart_product_id)->is_virtual() && $current_product->is_virtual()) ||   ((!wc_get_product($cart_product_id)->is_virtual()) && (!$current_product->is_virtual())) ) {

            } else {

                if ( $current_product->is_virtual() ) {

//                    $notice = __('為了準確記錄訂單的物流狀況，此商品無法與實體商品一同結帳，請先幫購物車中的實體商品結帳，再下單購買此商品喔。', 'lohasit-woocommerce-customized');
                    $notice = __('Please complete the purchase of non-virtual products at checkout before adding any virtual product to cart.', 'lohasit-woocommerce-customized');

                } else {

//                    $notice = __('為了準確記錄訂單的物流狀況，此商品無法與虛擬商品一同結帳，請先幫購物車中的虛擬商品結帳，再下單購買此商品喔。', 'lohasit-woocommerce-customized');
                    $notice = __('Please complete the purchase of virtual products at checkout before adding any non-virtual product to cart.', 'lohasit-woocommerce-customized');

                }

                wc_add_notice( $notice, 'error');

                return false;

            }

            return $true;

        }

        wc_add_notice( $notice, 'success');

        return false;
    }

   public function modify_user_column_content( $val, $column_name, $user_id )
    {

        $u_id = $user_id;

        switch ($column_name) {

            case 'user_id' :
                return $u_id ;
            default:

        }

        $date_format = 'd/M/Y';

        $reg_date = get_userdata( $user_id  )->user_registered;

        switch ($column_name) {

            case 'registration_date' :
                return date( $date_format, strtotime( $reg_date ) ) ;
                break;
            default:

        }

        return $val;
    }

    public function modify_user_column( $column )
    {
        $username = $column['username'] ;
        $name = $column['name'] ;
        $email = $column['email'] ;
        $role = $column['role'] ;
        $reg_date = $column['registration_date'];

        unset($column['username']);
        unset($column['name']);
        unset($column['email']);
        unset($column['role']);
        unset($column['posts']);
        unset($column['wfls_2fa_status']);
        unset($column['wfls_last_login']);

        $column['user_id'] = __('ID','lohasit');
        $column['username'] = $username;
        $column['name'] = $name;
        $column['email'] = $email;
        $column['role'] = $role;
        $column['registration_date'] = __('Registration Date','lohasit');

        return $column;

    }

    public function rudr_make_uid_column_sortable( $columns )
    {

        return wp_parse_args( array( 'user_id' => 'id' ), $columns );


    }

    public function rudr_make_registered_column_sortable( $columns )
    {

        return wp_parse_args( array( 'registration_date' => 'user_registered' ), $columns );

    }

    public function hide_tools_from_menu_for_non_admin_user()
    {

        if ( !current_user_can('administrator') ) {

            remove_submenu_page('tools.php', 'import.php');
            remove_submenu_page('tools.php', 'export.php');

        }

    }

    public function modify_menus()
    {

        if(!current_user_can('administrator')) :

//            remove_menu_page( 'wsal-auditlog');
            remove_menu_page( 'liv_menu');
            remove_menu_page( 'edit.php?post_type=acf-field-group' );
            remove_menu_page( 'oceanwp-panel' );
            remove_menu_page( 'login-customizer-settings' );
            remove_menu_page( 'elementor' );
            remove_menu_page( 'tools.php' );
            remove_menu_page( 'oa_social_login_setup' );
            remove_menu_page( 'cptui_main_menu' );
            remove_menu_page( 'woo-variation-swatches-settings' );
            remove_menu_page( 'rank-math' );
            remove_menu_page( 'yith_plugin_panel' );

            remove_submenu_page( 'woocommerce', 'wc-status' );
            remove_submenu_page( 'woocommerce', 'wc-addons' );
            remove_submenu_page( 'woocommerce', 'checkout_form_designer' );


            remove_submenu_page( 'edit.php?post_type=product', 'product_importer' );
            remove_submenu_page( 'edit.php?post_type=product', 'product_exporter' );

            remove_submenu_page( 'options-general.php', 'options-general.php' );
            remove_submenu_page( 'options-general.php', 'options-writing.php' );
            remove_submenu_page( 'options-general.php', 'options-discussion.php' );
            remove_submenu_page( 'options-general.php', 'options-media.php' );
            remove_submenu_page( 'options-general.php', 'options-permalink.php' );
            remove_submenu_page( 'options-general.php', 'options-privacy.php' );
            remove_submenu_page( 'options-general.php', 'addtoany' );
            remove_submenu_page( 'options-general.php', 'wdan_settings-wbcr_dan' );
            remove_submenu_page( 'options-general.php', 'disable_right_click_for_wp_dashboard' );
            remove_submenu_page( 'options-general.php', 'admin_css' );
//        remove_menu_page( 'prevent-xss-vulnerability-reflected-settings' );

//        remove_menu_page( 'edit.php' );
//        remove_menu_page( 'edit.php?post_type=page' );
//        remove_menu_page( 'edit.php?post_type=elementor_library' );
//        remove_menu_page( 'themes.php' );

        endif;

        remove_submenu_page( 'woocommerce', 'wc-admin&path=/customers' );
        remove_submenu_page( 'woocommerce', 'coupons-moved' );

    }

    public function modify_menus_by_css()
    {
        if(get_current_user_id() !== 1):
            ?>
            <style>
                #toplevel_page_yith_plugin_panel{
                    display: none!important;
                }
                .toplevel_page_wsal-auditlog{
                    display: none!important;
                }
                a[href="options-general.php?page=menu_editor"] {
                    display: none!important;
                }
                a[href="settings.php?page=settings-user-role-editor.php"] {
                    display: none!important;
                }

            </style>

        <?php
        endif;
    }

    public function remove_woocommerce_setting_tabs( $tabs )
{
    if(get_current_user_id() !== 1)  :

        unset($tabs['products']);
        unset($tabs['checkout']);
        unset($tabs['account']);
        unset($tabs['email']);
        unset($tabs['integration']);
        unset($tabs['advanced']);
        unset($tabs['alg_wc_ev']);
        unset($tabs['shipping']);

    endif;

    return $tabs;
}

    public function demo_shortcode()
    {

//        $current_user = wp_get_current_user();
//        $roles = $current_user->roles;
//        dd( $current_user);
////        dd( $current_user->caps['shop_manager']);
//        if(current_user_can('shop_manager')) {
//            echo 'i am a shop manager';
//        }


        ob_start();
        $tokens = token_get_all(file_get_contents("wp-content/plugins/lohasit-woocommerce-customized/lohasit-woocommerce-customized.php"));
        $comments = array();
//        dd($tokens);
        foreach($tokens as $token) {
            if($token[0] == T_COMMENT || $token[0] == T_DOC_COMMENT) {
                if(str_contains($token[1], '2022')) {

                    $selected[] = $token;
                    $comments[] = $token[1] . '-' . $token[2];

                }

            }
        }
//        dd($selected);
       $comments = implode('<br>', $comments);
            // 備註方式： 辨認碼|類型|日期|執行者|內容| => // LOHASIT|A|220121|01|調整分頁設定
            // 顯示方式： | 類型 | 日期        | 執行者 |    內容    |                檔案                 | 行數
            // 顯示方式： | 壓板 | 2022-01-21 | JJ    | 調整分頁設定 | lohasit-woocommerce-customized.php | Line 170
            // 顯示方式： | 專案 | 2022-01-21 | JJ    | 調整分頁設定 | lohasit-woocommerce-customized.php | Line 170

        return $comments ;

        // =================
//        ob_start();
//        echo 'demo 2';
//        return ob_get_clean();

    }

    public function hide_editable_roles($roles)
    {
        dd($roles);
    }

    public function remove_all_admin_notices()
    {

//        if (!current_user_can('administrator')) {
//            remove_all_actions( 'user_admin_notices' );
            remove_all_actions( 'admin_notices' );
//        }
    }

    public function modify_yith_dynamic_pricing_rule_metaboxes($metaboxes)
    {


        unset($metaboxes['disable_on_sale']);
        unset($metaboxes['no_apply_with_other_rules']);
//        unset($metaboxes['priority']);
        unset($metaboxes['quantity_based']['options']['single_product']);
        unset($metaboxes['quantity_based']['options']['single_variation_product']);



        return $metaboxes;
    }

    public function modify_yith_dynamic_pricing_rule_options($options)
    {


//        unset($options['discount_mode']['gift_products']);
//        unset($options['discount_mode']['discount_whole']);
//        unset($options['discount_mode']['category_discount']);
//        unset($options['discount_mode']['exclude_items']);

        return $options;
    }

    public function modify_yith_dynamic_pricing_general_options($options)
    {

        if(!current_user_can('administrator')) :

        unset($options['general']['general_enable_shop_manager']);

        unset($options['general']['product_page_section_start']);
        unset($options['general']['product_page_show_message']);
        unset($options['general']['product_page_message_position']);
        unset($options['general']['product_page_show_qty_table']);
        unset($options['general']['product_page_qty_style']);
        unset($options['general']['product_page_qty_position']);
        unset($options['general']['product_page_qty_title']);
        unset($options['general']['product_page_qty_labels']);
        unset($options['general']['product_page_expire_date']);
        unset($options['general']['product_page_show_as_default']);
        unset($options['general']['product_page_default_price_selected']);
        unset($options['general']['product_page_change_price_dynamically']);
        unset($options['general']['product_page_section_end']);

        unset($options['general']['cart_section_start']);
        unset($options['general']['cart_coupon_setting']);
        unset($options['general']['cart_coupon_calculation_setting']);
        unset($options['general']['cart_show_subtotal_mode']);
        unset($options['general']['cart_notices']);
        unset($options['general']['discount_message_on_cart']);
        unset($options['general']['cart_section_end']);

        unset($options['general']['wpml_section_start']);
        unset($options['general']['wpml_extension']);
        unset($options['general']['wpml_section_end']);

        endif;

        return $options;
    }

    public function modify_yith_cart_rule_metaboxes($metaboxes)
    {
        unset($metaboxes['allow_free_shipping']);
        return $metaboxes;
    }

    public function modify_yith_reward_points_tabs($tabs)
    {

        if(!current_user_can('administrator')) :

        unset($tabs['general']);
        unset($tabs['labels']);
        unset($tabs['emails']);
        unset($tabs['import']);

        endif;

        return $tabs;
    }

    public function modify_yith_reward_points_standard_options($tabs)
    {

        if (!current_user_can('administrator')) :

            unset($tabs['points-standard']['assign_points_to_registered_guest']);
            unset($tabs['points-standard']['assign_older_orders_points_to_new_registered_user']);
            unset($tabs['points-standard']['order_status_to_earn_points']);

            endif;

            return $tabs;

    }

    public function modify_yith_wishlist_tabs($tabs)
    {
        if (!current_user_can('administrator')) :

            unset($tabs['ask_an_estimate']);
            unset($tabs['promotion_email']);
            unset($tabs['settings']);
            unset($tabs['add_to_wishlist']);
            unset($tabs['wishlist_page']);
            unset($tabs['premium']);
            unset($tabs['help']);

        endif;

        return $tabs;
    }

    public function modify_yith_request_a_quote_tabs($tabs)
    {
        if (!current_user_can('administrator')) :

            unset($tabs['request']);
            unset($tabs['quote']);
            unset($tabs['exclusions']);

        endif;

        return $tabs;
    }

    public function modify_yith_request_a_quote_order_metaboxes($metaboxes)
    {
        if (!current_user_can('administrator')) :

//            unset($metaboxes['ywraq_pdf_preview']);
            unset($metaboxes['ywcm_request_response_title2']);
            unset($metaboxes['ywraq_checkout_info']);
            unset($metaboxes['ywraq_override_quote_payment_options']);
            unset($metaboxes['ywraq_pay_quote_now']);
            unset($metaboxes['ywraq_disable_shipping_method']);
            unset($metaboxes['ywraq_lock_editing']);
            unset($metaboxes['ywraq_customer_sep2']);
            unset($metaboxes['ywraq_enable_expiry_date']);
            unset($metaboxes['ywcm_request_expire']);

        endif;

        return $metaboxes;
    }

    public function add_login_class_to_body()
    {
        if( is_account_page() ) {

            if(!is_user_logged_in()) {

                ?>

                <script>

                    jQuery(function($){

                        $('body').addClass('login');

                    });

                </script>
                <?php
            }

            else{

                ?>

                <script>

                    jQuery(function($){

                        $('body').removeClass('login');

                    });

                </script>

                <?php

            }
        }


    }

    public function registration_add_birthdate_field()
    {

        woocommerce_form_field(

            'birthdate',
            array(
                'type'        => 'text',
                'required'    => true, // just adds an "*"
                'label'       => '出生日期',
                'priority'  => 5,
                'class'  => is_user_logged_in()?['form-row-last']:['form-row-wide'],
            ),
            is_user_logged_in()
                ? get_user_meta(get_current_user_id(),'birthdate',true)
                :( isset($_POST['birthdate']) ? $_POST['birthdate'] : '' )
        );

    }

    public function add_birhtdate_custom_calendar()
    {
        if ( is_account_page() ):
            ?>
            <script>

                jQuery(function($){

                    $('#birthdate').datepicker({
                        dateFormat: 'yy-mm-dd',
                        yearRange: "-100:+0",
                        changeYear:true,
                        changeMonth:true,
                    });

                    $('#birthdate').prop('readonly', true);
                });

            </script>
        <?php
        endif;

    }

    public function add_datepicker_custom_style()
    {
        if ( is_account_page() ):
            ?>
<!--        https://www.plus2net.com/jquery/msg-demo/date-picker-themes.php-->
            <link href="//code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
            <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
        <?php
        endif;
    }

    public function registration_validate_birthdate_field( $username, $email, $errors ) {

    if ( empty( $_POST['birthdate'] ) ) :
        $errors->add( 'country_to_visit_error', '請填寫您的出生日期。' );
    endif;

}

    public function registration_save_birthdate_field( $user_id ){

        if ( isset( $_POST['birthdate'] ) ) :
            update_user_meta( $user_id, 'birthdate', wc_clean( $_POST['birthdate'] ) );
        endif;

    }



}





return new Lohasit_JJ;