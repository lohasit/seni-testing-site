<?php

class Lohasit_MyAccount
{

    public function account_details_reminder()
    {
        ?>
        <p class="acc-details-reminder">
            <?php echo __('Please enter your details correctly to receive the notifications of your orders.' ,'lohasit-woocommerce-customized'); ?>
        </p>
        <?php
    }

    public function add_birthday_field()
    {
        $birthday = get_user_meta(get_current_user_id(), 'user_birthday', true);
        ?>
        <p class="form-row form-row-wide">
            <label for="user_birthday">生日</label>
            <input
                type="date"
                class="input-text user-birthday"
                name="user_birthday" id="user_birthday"
                value="<?php echo $birthday; ?>"
                placeholder="YYYY-MM-DD"
                maxlength="10"
                pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"

        </p>
        <?php

    }

    public function save_birthday_field($customer_id)
    {
        if (isset($_POST['user_birthday'])) {
            update_user_meta($customer_id, 'user_birthday', sanitize_text_field($_POST['user_birthday']));
        }
    }

    public function css_remove_last_name()
    {
        if (is_account_page()):
        ?>
            <style>
            .woocommerce-EditAccountForm .woocommerce-form-row--last {
                display: none;
            }
            </style>
            <script>
            // jQuery(function($) {
            //     $('.woocommerce-form-row--first').removeClass('form-row-first');
            // });
            </script>
            <?php endif;

    }

    public function remove_required_fields($required_fields)
    {
        unset($required_fields['account_last_name']);
        return $required_fields;
    }

    // === 從Lohasit_JJ轉移過來的函式 開端===

    public function adjust_shipping_fields($fields)
    {
        // 新增運送地址的聯絡電話欄位
        $fields['shipping_phone'] = array(
            'label'       => __('聯絡電話', 'lohasit'),
            'required'    => false,
            'class'       => array('form-row-last', 'address-field'),
            'priority'    => 15,
            'placeholder' => __('請輸入聯絡電話號碼', 'lohasit'),

        );

        // 調整運送地址欄位的位置
        $fields['shipping_phone']['priority']     = 15;
        $fields['shipping_company']['priority']   = 17;
        $fields['shipping_state']['priority']     = 18;
        $fields['shipping_city']['priority']      = 19;
        $fields['shipping_address_1']['priority'] = 20;

        // 調整運送地址欄位的寬度
        $fields['shipping_state']['class'] = array('form-row-first', 'address-field');
        $fields['shipping_city']['class']  = array('form-row-last', 'address-field');

        // 移除運送地址中的姓氏欄位和地址2欄位
        unset($fields['shipping_last_name']);
        unset($fields['shipping_address_2']);

        return $fields;
    }

    public function adjust_billing_fields($fields)
    {
        // 調整賬單地址欄位的位置
        $fields['billing_phone']['priority']   = 15;
        $fields['billing_email']['priority']   = 16;
        $fields['billing_company']['priority'] = 17;
        $fields['billing_state']['priority']   = 18;
        $fields['billing_city']['priority']    = 19;
        $fields['billing_city']['address_1']    = 20;

        // 調整賬單地址欄位的寬度
        $fields['billing_phone']['class'] = array('form-row-last', 'address-field');
        $fields['billing_email']['class'] = array('form-row-first', 'address-field');
        $fields['billing_company']['class'] = array('form-row-last', 'address-field');
        $fields['billing_state']['class'] = array('form-row-first', 'address-field');
        $fields['billing_city']['class']  = array('form-row-last', 'address-field');



        // 移除賬單地址中的姓氏欄位和地址2欄位
        unset($fields['billing_last_name']);
        unset($fields['billing_address_2']);

        // 另外調整： wp-content/plugins/woocommerce/includes/class-wc-countries.php

        return $fields;
    }

    public function hide_shipping_details()
    {
        global $wp;

        $order_id = $wp->query_vars['view-order']==true? $wp->query_vars['view-order']:$wp->query_vars['order-received'];

        $order = wc_get_order($order_id);

        if ('lohasit_ecpay_cvs' === get_shipping_method_id($order)) {
            ?>
            <script>
                jQuery(function($){
                    $('.woocommerce-column--shipping-address').css('display','none');
                });
            </script>
            <?php

        }
    }

    public function add_login_class_to_body()
    {
        if( is_account_page() ) {

            if(!is_user_logged_in()) {

                ?>

                <script>

                    jQuery(function($){

                        $('body').addClass('login');

                    });

                </script>
                <?php
            }

            else{

                ?>

                <script>

                    jQuery(function($){

                        $('body').removeClass('login');

                    });

                </script>

                <?php

            }
        }


    }

    public function registration_add_birthdate_field()
    {

        woocommerce_form_field(

            'user_birthdate',
            array(
                'type'        => 'text',
                'required'    => true, // just adds an "*"
                'label'       => __('Birthdate' , 'lohasit-woocommerce-customized'),
                'priority'  => 5,
                'class'  => is_user_logged_in()?['form-row-last']:['form-row-wide'],
                'custom_attributes' => array( 'pattern' => "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])")
            ),
            is_user_logged_in()
                ? get_user_meta(get_current_user_id(),'user_birthdate',true)
                :( isset($_POST['user_birthdate']) ? $_POST['user_birthdate'] : '' )
        );

    }

    public function add_birhtdate_custom_calendar()
    {
        if ( is_account_page() ):
            ?>
            <script>

                jQuery(function($){

                    $('#user_birthdate').datepicker({
                        dateFormat: 'yy-mm-dd',
                        yearRange: "-100:+0",
                        changeYear:true,
                        changeMonth:true,
                    });

                    $('#user_birthdate').prop('readonly', true);
                });

            </script>
        <?php
        endif;

    }

    public function add_datepicker_custom_style()
    {
        if ( is_account_page() ):
            ?>
            <!--        https://www.plus2net.com/jquery/msg-demo/date-picker-themes.php-->
<!--            <link href="//code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css" rel="stylesheet">-->
<!--            <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css" rel="stylesheet">-->
        <?php
        endif;
    }

    public function registration_validate_birthdate_field( $username, $email, $errors ) {

        if ( empty( $_POST['user_birthdate'] ) ) :
            $errors->add( 'country_to_visit_error', '請填寫您的出生日期。' );
        endif;

    }

    public function registration_save_birthdate_field( $user_id ){

        if ( isset( $_POST['user_birthdate'] ) ) :
            update_user_meta( $user_id, 'user_birthdate', wc_clean( $_POST['user_birthdate'] ) );
        endif;

    }

    public function make_birthdate_field_required( $required_fields ){

        $required_fields['user_birthdate'] = __('User Birthdate', 'lohasit-woocommerce-customized');
        return $required_fields;

    }

    // === 從Lohasit_JJ轉移過來的函式 結尾===
}

return new Lohasit_MyAccount;
