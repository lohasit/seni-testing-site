<?php

class Lohasit_Order
{
    protected $excepts = [];

    public function __construct()
    {
        // 排除名單(若付款方式支援再次付款可以加入此名單)
        $this->excepts = get_option('lohasit_pay_button_excepts') ?: [];
    }

    /**
     * hook: woocommerce_my_account_my_orders_actions
     * @param  array $actions
     * @param  WC_Order $order
     * @return array
     */
    public function remove_pay_button($actions, WC_Order $order)
    {
        $payment_method = $order->get_payment_method();

        // alan: 不在排除名單內的付款方式,我的帳號=>訂單列表移除付款按鈕
        if (!in_array($payment_method, $this->excepts)) {
            unset($actions['pay']);
        }

        return $actions;
    }

    /**
     * 訂單詳細頁面再次購買按鈕
     * @param  array  $statuese [description]
     * @return array
     */
    public function valid_order_statuses_for_order_again($statuese = [])
    {
        return ['completed', 'failed', 'cancelled', 'processing', 'pending'];
    }

    /**
     * 訂單列表頁再次購買
     * @return array
     */
    public function my_order_actions($actions, $order)
    {
        $order_again_url = wp_nonce_url(add_query_arg('order_again', $order->get_id(), wc_get_cart_url()), 'woocommerce-order_again');

        $has_statuses = apply_filters('woocommerce_valid_order_statuses_for_order_again', ['completed']);

        if ($order->has_status($has_statuses)) {

            $actions['name'] = [
                'url'  => $order_again_url,
                'name' => __('Order again', 'woocommerce'),
            ];
        }

        return $actions;
    }
}

return new Lohasit_Order;
