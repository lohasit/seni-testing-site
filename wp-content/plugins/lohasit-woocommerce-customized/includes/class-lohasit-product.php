<?php

class Lohasit_Product
{

    // 已轉移
    public function add_discount_details_to_simple_product()
    {
        global $product;

        if ($product->is_type('simple')) {

            $discount_percentage = floor((wc_get_price_to_display($product) / $product->get_regular_price()) * 100);
            $discount_total      = $product->get_regular_price() - wc_get_price_to_display($product);

            if ($product->is_on_sale() || $discount_total != 0) {
                ?>
                <div class="discount-details">
                    <span class="discount-percentage"><?=$discount_percentage?>折</span>
                    <span class="discount-total">省下 <?=get_woocommerce_currency_symbol()?><?=$discount_total?></span>
                </div>
                <?php

            }
        }
    }

    // 已轉移
    public function add_discount_details_to_variable_product()
    {
        global $product;

        if ($product->is_type('variable')) {

            $variations_id = wp_list_pluck($product->get_available_variations(), 'variation_id');

            for ($x = 0; $x < count($variations_id); $x++) {
                $variation[$x]       = wc_get_product($variations_id[$x]);
                $discount_percentage = floor((wc_get_price_to_display($variation[$x]) / $variation[$x]->get_regular_price()) * 100);
                $discount_total      = $variation[$x]->get_regular_price() - wc_get_price_to_display($variation[$x]);
                $attribute           = implode(',', array_values($variation[$x]->get_attributes()));

                if ($variation[$x]->is_on_sale() || $discount_total != 0) {
                    ?>
                    <div class="discount-details">
                        <span class="discount-attribute"><?=$attribute?> : </span>
                        <span class="discount-percentage"><?=$discount_percentage?>折</span>
                        <span class="discount-total">省下 <?=get_woocommerce_currency_symbol()?><?=$discount_total?></span>
                    </div>
                    <?php

                }
            }
        }
    }
}

return new Lohasit_Product;