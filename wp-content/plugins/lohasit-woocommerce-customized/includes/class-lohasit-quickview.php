<?php

class Lohasit_Quickview
{
    public function __construct()
    {

        // Modified files: 4.1.2 修改YITH快速查看的彈窗排版
        // wp-content/plugins/yith-woocommerce-quick-view-premium/includes/functions.yith-wcqv.php
        // wp-content/plugins/yith-woocommerce-quick-view-premium/templates/yith-quick-view-content.php

    // 商品詳細頁可用鉤子
    // add_action('woocommerce_single_product_summary', 'add_custom_product_section'); // 不可行
    // add_action('woocommerce_before_variations_form', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_before_single_variation', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_single_variation', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_before_add_to_cart_button', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_before_add_to_cart_quantity', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_after_add_to_cart_quantity', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_after_add_to_cart_button', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_after_single_variation', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_after_variations_form', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_after_add_to_cart_form', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_product_meta_start', 'add_custom_product_section'); // 可行
    // add_action('woocommerce_share', 'add_custom_product_section'); // 不可行
    // add_action('woocommerce_after_single_product_summary', 'add_custom_product_section'); // 不可行
    // add_action('woocommerce_after_single_product', 'add_custom_product_section'); // 不可行

        // 4.1.2 YITH快速查看彈窗區新增樣式和JS效果


        add_action( 'woocommerce_before_main_content', [$this, 'custom_style_for_quick_view'], 10);
        add_action( 'woocommerce_after_main_content', [$this, 'custom_script_for_quick_view'], 10);


//        // 4.1.2 YITH快速查看彈窗區新增動態定價價格表、優惠提示與點數提示
        add_action('yith_wcqv_product_summary', [$this, 'add_dynamic_pricing_and_rewards_point'], 23);
//
//        // 4.1.2 YITH快速查看彈窗區新增客制分享按鈕：預設臉書和Line分享按鈕
        add_action( 'yith_wcqv_product_summary', [$this, 'custom_share_buttons'] , 44 );
//
//        // 4.1.2 YITH快速查看彈窗區新增商品分頁內容：描述、額外資訊、評價留言區
        add_action( 'yith_wcqv_product_summary', 'woocommerce_output_product_data_tabs', 45 );
//
//        // 4.1.2 YITH快速查看彈窗區新增“你可能喜歡”商品
        add_action( 'yith_wcqv_after_product_summary', 'woocommerce_upsell_display', 50 );
//
//        // 4.1.2 YITH快速查看彈窗區新增“相關”商品
        add_action( 'yith_wcqv_after_product_summary', 'woocommerce_output_related_products', 55 );

        // 4.1.2 YITH快速查看彈窗區根據所選的商品屬性及時更新價格表
        add_action('wp_ajax_get_variation_discounted_price', [$this, 'get_variation_discounted_price'], 10);

        // 4.1.2 YITH快速查看彈窗區根據所選的商品屬性及時更新價格表
        add_action('wp_ajax_nopriv_get_variation_discounted_price', [$this, 'get_variation_discounted_price'], 10);

    }

    public function get_variation_discounted_price()
    {

        $variation_id = $_POST['data'][0]['variation_id'];

        if($variation_id == null) :

            $table = 0;

            else :

                $table = do_shortcode('[yith_ywdpd_quantity_table product="'.$variation_id.'"]');

                endif;



        wp_send_json($table) ;

    }

    public function custom_style_for_quick_view()
    {
        if(is_shop()) :
        ?>
        <style>
            .woocommerce-Tabs-panel{
                display:none;
            }
            #tab-description{
                display:block;
            }

            .summary-content{
                width: 100%;
                /*float: right;*/
                /*position: relative;*/
            }

        </style>
        <?php
        endif;
    }

    public function custom_script_for_quick_view()
    {
        if(is_shop()) :
        ?>
        <script>
            jQuery(function($){

                // 4.1.2 快速預覽內容：動態定價可隨著數量與屬性動態更新

                $('body').on('change', 'input[name=variation_id]', function() {

                    let  variation_id = 0;
                    let parameter = [];


                    if($(this).val() !== '') {

                        if ($("#ywdpd-table-discounts").length !== 0) {

                        parameter.push({variation_id: $(this).val()});

                        $.ajax({
                            url: '<?php echo admin_url("admin-ajax.php"); ?>',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                action: 'get_variation_discounted_price',
                                data: parameter,
                            },
                            success: function (resp) {

                                if (resp != 0) {

                                    $('.custom-yith-table').html(resp);
                                    let current_qty = $('input[name=quantity]').val();
                                    $('input[name=quantity]').val(current_qty).change();

                                }
                            },
                        });
                    }else{
                        console.log('no table');
                    }
                    }
                });


                // 4.1.2 快速預覽內容：修復商品分頁內容的切換效果

                $('body').on('click', 'li', function(){

                    $('.woocommerce-Tabs-panel').hide();
                    let selected = $(this).prop('id');
                    selected = selected.replace('tab-title-' ,  '');
                    $('#tab-' + selected).show();
                });

            });
        </script>
            <?php
        endif;

    }

    public function add_dynamic_pricing_and_rewards_point()
    {

        echo '<div id="custom-product-section">';

        if (is_plugin_active('yith-woocommerce-dynamic-pricing-and-discounts-premium/init.php')) :

            if (get_option( 'ywdpd_show_quantity_table') == 'yes') :

        echo '<div class="custom-yith-table">';
        echo do_shortcode('[yith_ywdpd_quantity_table]');
        echo '</div>';

            endif;

        echo do_shortcode('[yith_ywdpd_product_note]');

        endif;

        if (is_plugin_active('yith-woocommerce-points-and-rewards-premium/init.php')) :

        echo do_shortcode('[yith_points_product_message]');

        endif;

        echo '</div>';

    }

    public function custom_share_buttons()
    {

        if (is_plugin_active('add-to-any/add-to-any.php')) :

            $product = wc_get_product(get_the_ID());
            $product_url = home_url() . '/product/' . $product->get_slug();
            $link_name = $product->get_title();
            $link_note = '';
            $share_url = $product_url . '/&linkname=' . $link_name . '&linknote=' . $link_note;

            ?>
            <script src="https://kit.fontawesome.com/01150bd6a6.js" crossorigin="anonymous"></script>
            <div class="custom-sharer">
                Share to:
                <a class="custom_share_buttons" target="_blank" href='https://www.addtoany.com/add_to/facebook?linkurl=<?php echo $share_url;?>'><i class="fa-brands fa-facebook"></i></a>
                <a class="custom_share_buttons" target="_blank" href='https://www.addtoany.com/add_to/line?linkurl=<?php echo $share_url;?>'><i class="fa-brands fa-line"></i></a>

            </div>

        <?php

        endif;


    }

}

if (is_plugin_active('yith-woocommerce-quick-view-premium/init.php')) :

    return new Lohasit_Quickview;

    endif;

