<?php

class Lohasit_Wishlist
{
    public function __construct()
    {
        /**
         * 加入願望清單404修正
         */
        add_filter('init', [$this, 'initialize'], 10, 2);

        /**
         * 修改願望清單網址
         */
        add_filter('yith_wcwl_wishlist_page_url', [$this, 'modify_yith_wishlist_url'], 10, 2);

        /**
         * 會員中心選單重新排列,加入願望清單
         */
        add_filter('query_vars', [$this, 'query_vars']);
        add_action('wp_loaded', [$this, 'rewrite_rules']);
        add_filter('woocommerce_account_menu_items', [$this, 'init_menu_items'], 99, 1);

        add_action('init', function () {
            $new_items = ['my-wishlist'];
            foreach ($new_items as $slug) {
                $method = str_replace('-', '_', $slug) . '_page';
                add_action('woocommerce_account_' . $slug . '_endpoint', $method);
            }
            foreach ($new_items as $slug) {
                add_rewrite_endpoint($slug, EP_ROOT | EP_PAGES);
            }
        });
    }

    public function initialize()
    {
        @list(, $view, $token) = @explode('/', $_SERVER['REQUEST_URI']);
        if ('view' === $view && $token) {
            wp_safe_redirect(home_url('my-account/my-wishlist'));
            die;
        }
    }

    public function modify_yith_wishlist_url($base_url, $action)
    {
        return home_url('/my-account/my-wishlist');
    }

    public function init_menu_items($items)
    {
        // 選單項重新排列
        $items = [
            'dashboard'       => '控制台',
            'orders'          => '訂單',
            'downloads'       => '下載',
            'edit-address'    => '地址',
            'edit-account'    => '帳戶詳細資料',
            'my-points'       => '紅利點數紀錄',
            'my-wishlist'     => '願望清單',
            'customer-logout' => '登出',
        ];
        return $items;
    }

    public function my_wishlist_page()
    {
        echo do_shortcode('[yith_wcwl_wishlist per_page="10" pagination="yes"]');
    }

    public function query_vars($vars)
    {
        $new_items = array('my-wishlist');

        foreach ($new_items as $slug) {
            $vars[] = $slug;
        }

        return $vars;
    }

    public function rewrite_rules()
    {
        flush_rewrite_rules();
    }
}

return new Lohasit_Wishlist;
