<?php

if (!function_exists('get_shipping_method_id')) {
    function get_shipping_method_id($order)
    {
        $shipping_method    = @array_shift($order->get_shipping_methods());
        $shipping_method_id = $shipping_method['method_id'];

        return $shipping_method_id;
    }
}


// 載入前台樣式檔案 [測試用]
function load_front_page_css()
{
    $style_uri          = get_stylesheet_directory_uri();
    $cart_css           = '/assets/page/cart.css';
    $checkout_css       = '/assets/page/checkout.css';
    $home_css           = '/assets/page/home.css';
    $single_product_css = '/assets/page/single-product.css';

    if (file_exists(get_stylesheet_directory() . $cart_css) && is_cart()) {
        wp_enqueue_style("lohasit_css", $style_uri . $cart_css);
    }
    if (file_exists(get_stylesheet_directory() . $checkout_css) && is_checkout()) {
        wp_enqueue_style("lohasit_css", $style_uri . $checkout_css);
    }
    if (file_exists(get_stylesheet_directory() . $home_css) && is_home()) {
        wp_enqueue_style("lohasit_css", $style_uri . $home_css);
    }
}

// 短代碼 [測試用]
function demo_shortcode()
{

//        $current_user = wp_get_current_user();
//        $roles = $current_user->roles;
//        dd( $current_user);
////        dd( $current_user->caps['shop_manager']);
//        if(current_user_can('shop_manager')) {
//            echo 'i am a shop manager';
//        }


    ob_start();
    $tokens = token_get_all(file_get_contents("wp-content/plugins/lohasit-woocommerce-customized/lohasit-woocommerce-customized.php"));
    $comments = array();
//        dd($tokens);
    foreach($tokens as $token) {
        if($token[0] == T_COMMENT || $token[0] == T_DOC_COMMENT) {
            if(str_contains($token[1], '2022')) {

                $selected[] = $token;
                $comments[] = $token[1] . '-' . $token[2];

            }

        }
    }
//        dd($selected);
    $comments = implode('<br>', $comments);
    // 備註方式： 辨認碼|類型|日期|執行者|內容| => // LOHASIT|A|220121|01|調整分頁設定
    // 顯示方式： | 類型 | 日期        | 執行者 |    內容    |                檔案                 | 行數
    // 顯示方式： | 壓板 | 2022-01-21 | JJ    | 調整分頁設定 | lohasit-woocommerce-customized.php | Line 170
    // 顯示方式： | 專案 | 2022-01-21 | JJ    | 調整分頁設定 | lohasit-woocommerce-customized.php | Line 170

    return $comments ;

    // =================
//        ob_start();
//        echo 'demo 2';
//        return ob_get_clean();

}

add_action( 'admin_bar_menu', 'clear_node_title', 999 );

function clear_node_title($wp_admin_bar)
{
    $wp_admin_bar->remove_node('WPML_ALS-default');
//    $wp_admin_bar->remove_node('rank-math');
}
