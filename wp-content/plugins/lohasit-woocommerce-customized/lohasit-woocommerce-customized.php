<?php

/**
 * Plugin Name: 樂活購物車客製化
 * Description: 樂活壓板系統 - WooCommerce - 購物車客製化
 * Version: 1.0.0
 * Author: Lohas IT (alanchang15)
 * Author URI: http://www.lohaslife.cc
 * Text Domain: lohasit-woocommerce-customized
 * Domain Path: /languages
 */

if (!defined('ABSPATH')) {
    die;
}

define('LOHASIT_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('LOHASIT_PLUGIN_URL', plugin_dir_url(__FILE__));

class Lohasit_WooCommerce_Customized
{
    protected static $_instance = null;

    protected $loader = [];

    protected $require_files;

    private function __construct()
    {
        /**
         * alan:需要引入的檔案記得加入此陣列
         * 注意檔名命名必須為class-lohasit開頭
         * 類名Lohasit_開頭
         * 檔案底部記得 return new {該類名}
         * 例如 return new Lohasit_Checkout;
         * 參考 includes/class-lohasit-checkout.php
         * @var array
         */
        $this->require_files = [
            'includes/helper-lohasit.php',
            'includes/class-lohasit-checkout.php',
            'includes/class-lohasit-order.php',
            'includes/api/class-lohasit-ajax-zipcode.php',
            'includes/class-lohasit-backend.php',
            'includes/class-lohasit-backend-yith.php',
            'includes/class-lohasit-wishlist.php',
            'includes/class-lohasit-myaccount.php',
            'includes/class-lohasit-coupon.php',
            'includes/class-lohasit-product.php',
            'includes/class-lohasit-cart.php',
            'includes/class-lohasit-quickview.php',
        ];

        // Alan: 載入語系檔
        load_plugin_textdomain(
            'lohasit-woocommerce-customized',
            false,
            dirname(plugin_basename(__FILE__)) . '/languages'
        );

        $this->init_hooks();

        $this->cron_schedules();
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->loader)) {
            return $this->loader[$key];
        }
    }

    public function __set($key, $value)
    {
        $this->loader[$key] = $value;
    }

    protected function init_hooks()
    {
        $this->require_files();

        if (is_admin()) {
            $this->init_admin_hooks();
        } else {
            $this->init_public_hooks();
        }

        // 地址格式化
        add_filter('woocommerce_localisation_address_formats', [$this->checkout, 'custom_address_format'], 20, 1);

    }

    public function cron_schedules()
    {
        /**
         * 新訂單產生時，排程檢查是否未付款(alan:需要確認,暫時註解掉)
         */
        // add_action('woocommerce_new_order', [$this->admin, 'schedule_cancel_expired_order']);
        /**
         * 取消未付款訂單(alan:需要確認,暫時註解掉)
         */
        // add_action('lohasit_cancel_expired_order', [$this->admin, 'cancel_expired_order'], 10, 2);
        /**
         * 訂單取消恢復庫存(alan:需要確認,暫時註解掉)
         */
        // add_action('woocommerce_order_status_changed', [$this->admin, 'restore_order_stock_after_cancelling'], 20, 3);
    }

    protected function require_files()
    {

        foreach ($this->require_files as $file) {
            if (file_exists($php_file = LOHASIT_PLUGIN_PATH . $file)) {
                $name          = str_replace(['class-lohasit-', '-' ,'.php'], ['', '_' ,''], basename($file));
                $this->{$name} = require_once $php_file;
            }
        }
    }

    protected function init_public_hooks()
    {
        // 縣市資料
        add_filter('woocommerce_states', [$this->checkout, 'load_states'], 10);
        add_filter('wc_city_select_cities', [$this->checkout, 'load_cities'], 10);

        // 載入js
        add_action('wp_enqueue_scripts', [$this->checkout, 'load_scripts'], 100);
        // 3+2郵遞區號API
        add_action('wc_ajax_zipcode', [$this->ajax_zipcode, 'handle']);

        // 我的訂單移除付款按鈕
        add_filter('woocommerce_my_account_my_orders_actions', [$this->order, 'remove_pay_button'], 10, 2);

        /**
         * 檢查手機號碼 added by alan 2020-02-03
         */
        add_action('woocommerce_after_checkout_validation', [$this->checkout, 'checkout_validation_tw_mobile'], 10, 1);

        /**
         * 在購物車頁面的結帳按鈕旁增加一按鈕-繼續購物
         */
        add_action('woocommerce_proceed_to_checkout', [$this->cart, 'add_return_to_shop_btn']);

        /**
         * 購物車自動更新
         */
        add_action('wp_footer', [$this->cart, 'woocommerce_auto_update_cart']);

        // === 從 init_hooks 轉移過來的鉤子 - 開端 ===

        // 20211225 Danny: Include security defend class
//        add_filter('plugins_loaded', [$this->defend, 'filter_sql_injection_hook'], 0); // ADDED

        /**
         * 20210602 調整會員中心賬單地址的欄位，新增聯絡電話 by JJ
         */
        add_filter('woocommerce_shipping_fields', [$this->myaccount, 'adjust_shipping_fields'], 10);

        /**
         * 20210602 調整會員中心運送地址的欄位，調整會員中心賬單地址欄位顯示的位置 by JJ
         */
        add_filter('woocommerce_billing_fields', [$this->myaccount, 'adjust_billing_fields'], 10);


        /**
         * 20210603 結帳頁 - 超商取貨的訂單無需重複顯示運送資訊 by JJ
         */
//        add_action('woocommerce_order_details_after_order_table', [$this->myaccount, 'hide_shipping_details'], 10);

        /**
         * 20210604 會員中心 - 超商取貨的訂單無需重複顯示運送資訊 by JJ
         */
//        add_action('woocommerce_view_order', [$this->myaccount, 'hide_shipping_details'], 10);

        /**
         * 20210603 顯示優惠商品（簡單商品）的折扣百分比以及剩下的金額 by JJ
         */
//        add_action('woocommerce_before_add_to_cart_form', [$this->product, 'add_discount_details_to_simple_product'], 10);

        /**
         * 20210603 單個商品頁單個商品頁（多變商品）新增折扣與省下的金額提示 by JJ
         */
//        add_action('woocommerce_before_add_to_cart_form', [$this->product, 'add_discount_details_to_variable_product'], 10);


        // [20220121] my-account頁面，根據登入狀況新增class "login"
        add_action('wp_head', [$this->myaccount,'add_login_class_to_body'], 99);

        /*
         * 20210618 虛擬商品和實體商品需分開結帳 by JJ
         */
        add_filter('woocommerce_add_to_cart_validation', [$this->cart, 'cart_validation'], 10, 3);

        //  [20220121] 會員中心顯示折價券列表 by Seni
        add_shortcode('display-coupon', [$this->coupon,'display_coupon_list']);

        // [20220128 新增顯示限制：非屬於使用者，非折價券的記錄將隱藏]
        add_shortcode('show_customer_coupons', [$this->coupon, 'show_customer_coupons']);

        //  [20220121] 新增客制鉤子，放入生日欄位在註冊頁與會員中心頁 - JJ
        add_action( 'woocommerce_edit_account_after_email', [$this->myaccount,'registration_add_birthdate_field'], 99 );

        // [20220125] 生日欄位JS  - JJ
        add_action( 'woocommerce_edit_account_form_end', [$this->myaccount, 'add_birhtdate_custom_calendar'], 99 );

        // [20220125] 生日欄位必填  - JJ
        add_action( 'woocommerce_register_post', [$this->myaccount,'registration_validate_birthdate_field'], 10, 3 );

        // [20220125] 儲存生日資料  - JJ
        add_action( 'woocommerce_save_account_details', [$this->myaccount,'registration_save_birthdate_field'] );

        // [20220125] 設定生日資料必填  - JJ
        add_action( 'woocommerce_save_account_details_required_fields', [$this->myaccount,'make_birthdate_field_required'] );

        // [20220127] 超商取貨回傳Session遺失，用以下函式可修復
        add_action('init', [$this->checkout, 'start_ecpay_store_session'], 1);

        // [20220127] 判斷區分折價來源：詢價優惠 / 點數優惠 / 折價券優惠
        add_filter('woocommerce_cart_totals_coupon_label', [$this->coupon, 'cart_coupon_discount_text'], 900, 2);


        // === 從 init_hooks 轉移過來的鉤子 - 結束 ===

        // 設定姓氏欄位非必填
        add_filter('woocommerce_save_account_details_required_fields', [$this->myaccount, 'remove_required_fields']);

        // 訂單詳細頁面再次購買
        add_filter('woocommerce_valid_order_statuses_for_order_again', [$this->order, 'valid_order_statuses_for_order_again']);

        // 訂單列表再次購買
        add_filter('woocommerce_my_account_my_orders_actions', [$this->order, 'my_order_actions'], 10, 2);

        // [4.1.1 帳戶資訊頁 - 新增多語系提示]
        add_action('woocommerce_edit_account_form_start', [$this->myaccount, 'account_details_reminder'], 10);

        // alan: 隱藏 YITH POINT 商品頁單一商品點數(待處理)
        // add_filter('ywpar_single_product_message_in_loop', function ($style) {
        // return str_replace('<div style="', '<div style="display:none;', $style);
        // });
    }

    protected function init_admin_hooks()
    {
        /**
         * 修改商店管理員可設定的帳號角色
         */
        add_filter('woocommerce_shop_manager_editable_roles', [$this->backend, 'set_shop_manager_editable_roles'], 999);

        /**
         * 調整YITH三個外掛的後台選單位置
         * 20210611 導致最高權限無法進入YITH設定，所以註解 by JJ
         */
         add_filter('yit_plugin_fw_wc_panel_option_args', [$this->backend, 'adjust_yith_submenu'], 999);


        // === 從 init_hooks 轉移過來的鉤子 - 開端 ===

        /**
         * 去掉商品的「外部」類型
         * 20210603 恢復外部商品選項 by JJ
         */
        add_filter('product_type_selector', [$this->backend, 'modify_pd_type'], 1, 10);


        // [20220118] 調整Woocommerce後台設定的可視範圍 - JJ
        add_filter( 'woocommerce_settings_tabs_array', [$this->backend,'remove_woocommerce_setting_tabs'], 200, 1 );

        // wp-content/plugins/yith-woocommerce-dynamic-pricing-and-discounts-premium/plugin-options/metabox/ywdpd_pricing_discount.php
        // [20220119] 調整YITH動態定價的規則設定欄位 - JJ
         add_filter('ywdpd_pricing_discount_metabox', [$this->backend, 'modify_yith_dynamic_pricing_rule_metaboxes'], 99);

        // wp-content/plugins/yith-woocommerce-dynamic-pricing-and-discounts-premium/plugin-options/pricing-rules-options.php
        // [20220119] 調整YITH動態定價規則類型 - JJ
         add_filter('yit_ywdpd_pricing_rules_options', [$this->backend, 'modify_yith_dynamic_pricing_rule_options'], 99);

         // [20220120] 調整YITH動態定價一般設定欄位 - JJ
         add_filter('yith_ywdpd_panel_settings_options', [$this->backend, 'modify_yith_dynamic_pricing_general_options'], 99);

        // [20220120] 調整YITH動態定價購物車規則欄位 - JJ
         add_filter('ywdpd_cart_discount_metabox', [$this->backend, 'modify_yith_cart_rule_metaboxes'], 99);

        // [20220120] 調整YITH積分與點數分頁 - JJ
         add_filter( 'ywpar_show_admin_tabs' , [$this->backend, 'modify_yith_reward_points_tabs'], 99 );

        // [20220120] 調整YITH積分與點數設定欄位 - JJ
         add_filter('ywpar_points_settings', [$this->backend, 'modify_yith_reward_points_standard_options'], 99);

        // [20220120] 調整YITH願望清單的分頁 - JJ
         add_filter('yith_wcwl_available_admin_tabs_premium', [$this->backend, 'modify_yith_wishlist_tabs'], 99);

         // [20220120] 調整YITH詢價外掛的分頁 - JJ
         add_filter('ywraq_admin_tabs', [$this->backend, 'modify_yith_request_a_quote_tabs'], 99);

        // [20220120] 調整YITH詢價訂單欄位 - JJ
         add_filter('ywraq_order_metabox', [$this->backend, 'modify_yith_request_a_quote_order_metaboxes'], 99);

        // [20220128] 詢價PDF匯出讀取中文字
        if ( defined( 'YITH_YWRAQ_PREMIUM' ) ) {

            add_filter('pdf_font_family', [$this->backend, 'pdf_font_family']);

        }

        // [20220128] 調整詢價[一般設定]的欄位
        add_filter('ywraq_generals_settings_options', [$this->backend, 'modify_yith_request_a_quote_generals_settings_options'], 99);


        // === 從 init_hooks 轉移過來的鉤子 - 結束 ===


    }

    public static function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
}

if (!function_exists('lohasit_logger')) {
    function lohasit_logger($message, $source, $type = 'error')
    {
        $logger  = wc_get_logger();
        $context = [
            'source'  => $source,
            '_legacy' => true,
        ];

        call_user_func_array([$logger, $type], [$message, $context]);
    }
}

if (!function_exists('lohasit_woocommerce')) {
    function lohasit_woocommerce()
    {
        return Lohasit_WooCommerce_Customized::get_instance();
    }
}

add_action('plugin_loaded', 'lohasit_woocommerce');
add_action('init', [lohasit_woocommerce(), 'cron_schedules']);
