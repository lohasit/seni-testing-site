<?php

class WC_Member_Level_Admin
{
    private static $_instance = null;

    public static function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function lohasit_use_bootstrap()
    {
        wp_register_style('lohas-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css');
        wp_register_script('lohas-bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js');
        wp_register_style('localcss', LWLM_PLUGIN_URL . 'assets/css/admin/style.css');
    }

    public function lohasit_create_menu()
    {
        $capability = apply_filters('lohasit_member_level_capability', 'manage_woocommerce');

        add_submenu_page(
            'woocommerce-marketing',
            __('Member Level Setting', 'lwlm'),
            __('Member Level Setting', 'lwlm'),
            $capability,
            'member-level-setting',
            [$this, 'setting_page'],
        );
    }

    public function change_role($order_id)
    {
        global $lastyearstart, $lastyearend, $thisyearstart, $thisyearend;

        $order                = new WC_Order($order_id);
        $user                 = new WP_User($order->get_user_id());
        $lastyear_total_spent = lohas_get_customer_total_spents($order->get_user_id(), $lastyearstart, $lastyearend);
        $thisyear_total_spent = lohas_get_customer_total_spents($order->get_user_id(), $thisyearstart, $thisyearend);

        $lastyear_total_spent = intval($lastyear_total_spent);

        $current_role = implode(",", $user->roles);
        $current_role = $current_role == '' ? 'customer' : $current_role;
        if ($current_role == 'customer') {
            $user->set_role('customer');
        }
        $role_cap = get_role($current_role)->capabilities;

        $Authority = count($role_cap);

        if ($Authority <= 2) {

            $results = get_level_rules();

            $user_key = 'customer';
            foreach ($results as $key => $row) {
                if ($lastyear_total_spent >= $row->target_price) {
                    $user_key = $row->role_key;
                } elseif ($thisyear_total_spent >= $row->target_price) {
                    $user_key = $row->role_key;
                }
            }

            $user->set_role($user_key);
        }
    }

    public function setting_page()
    {
        // DD#0136 移動宣告$wpdb的位置
        global $wpdb, $table;

        wp_enqueue_style('lohas-bootstrap-css');
        wp_enqueue_script('lohas-bootstrap-js');
        wp_enqueue_style('localcss');

        $results = get_level_rules();

        if (!empty($_POST)) {

            if (isset($_POST['addrole'])) {

                $data = array(
                    'role_key'     => $_POST['role_key'],
                    'target_price' => $_POST['target_price'],
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                );

                $format = array(
                    '%s',
                    '%s',
                );
                $success = $wpdb->insert($table, $data, $format);

                wp_safe_redirect($_SERVER['HTTP_REFERER']);
                die;

            } elseif (isset($_POST['uptrole'])) {
                foreach ($_POST['role_name'] as $key => $val) {
                    $result = $wpdb->update(
                        $table,
                        array(
                            // 'role_name'    => $_POST['role_name'][$key],
                            'target_price' => $_POST['target_price'][$key],
                            // DD#0136 新增資料欄位：各個級別的自訂文字訊息
                            'custom_message' => $_POST['custom_message'][$key],
                        ),
                        array(
                            "id" => $_POST['id'][$key],
                        )
                    );
                }
                wp_safe_redirect($_SERVER['HTTP_REFERER']);
                die;
            }
        }

        if (isset($_GET['act']) && $_GET['act'] == 'del') {
            $result = $wpdb->delete($table, array('id' => $_GET['id']));
            wp_safe_redirect($_SERVER['HTTP_REFERER']);
            die;
        }

        include LWLM_PLUGIN_PATH . 'templates/admin/setting-page.php';
    }
}
