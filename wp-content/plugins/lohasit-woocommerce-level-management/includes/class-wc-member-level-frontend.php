<?php

class WC_Member_Level_Frontend
{
    private static $_instance = null;

    public static $new_items = [
        'my-level',
    ];

    public static function get_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function enqueue_scripts()
    {
        $css_path = apply_filters('lwlm_css_path', LWLM_PLUGIN_URL . 'assets/css/style.css');

        wp_register_style('my-level-css', $css_path);
    }

    public function init_endpoint()
    {
        foreach (static::$new_items as $slug) {
            add_rewrite_endpoint($slug, EP_ROOT | EP_PAGES);
        }
    }

    public function query_vars($vars)
    {
        foreach (static::$new_items as $slug) {
            $vars[] = $slug;
        }

        return $vars;
    }

    public function rewrite_rules()
    {
        flush_rewrite_rules();
    }

    public function init_menu_items($items)
    {
        if (defined('YITH_WCMAP')) {
            $items['my-level'] = [
                'slug'             => 'my-level',
                'active'           => true,
                'label'            => __('Member Level', 'lwlm'),
                'icon_type'        => 'default',
                'icon'             => apply_filters('lwlm_icon', 'users'),
                'custom_icon'      => '',
                'class'            => 'lohasit-member-level',
                'content'          => '',
                'visibility'       => 'all',
                'content_position' => 'override',
            ];
        } else {
            $items['my-level'] = __('Member Level', 'lwlm');
        }

        return $items;
    }

    public function locate_template($template, $template_name, $template_path)
    {
        $template_path = LWLM_PLUGIN_PATH . 'templates/';

        if (file_exists($template_path . $template_name)) {
            $template = $template_path . $template_name;
            return $template;
        }

        return $template;
    }

    protected function get_template($template, $data = [])
    {
        $current_data['user'] = $this->user;

        $data = array_merge($current_data, $data);

        $template = $template . '.php';

        wc_get_template(
            $template,
            $data
        );
    }

    public function my_level_page()
    {
        global $lastyearstart, $lastyearend, $thisyearstart, $thisyearend;

        $current_user_id = get_current_user_id();
        $current_user    = new WP_User($current_user_id);
        $total_spend     = (int) lohas_get_customer_total_spents($current_user_id, $lastyearstart, $lastyearend);
        $thisyearspents  = (int) lohas_get_customer_total_spents($current_user_id, $thisyearstart, $thisyearend);

        $results = get_level_rules();

        $current_role = implode(',', $current_user->roles);
        $role_cap     = get_role($current_role)->capabilities;
        $Authority    = count($role_cap);

        if ($Authority == 0) {
            $type = __('Customer', 'lwlm');
        } else {
            $type = __('Admin', 'lwlm');
        }

        $title = __('Member Level', 'lwlm');

        wp_enqueue_style('my-level-css');

        $this->get_template(
            'myaccount/my-level',
            compact(
                'title', 'type',
                'current_user', 'total_spend',
                'thisyearspents', 'results'
            )
        );
    }

    public function render_dashboard_widget()
    {
        global $lastyearstart, $lastyearend, $thisyearstart, $thisyearend;

        $current_user = new WP_User(get_current_user_id());

        $user_role       = '';
        $current_user_id = get_current_user_id();
        $total_spend     = (int) lohas_get_customer_total_spents($current_user_id, $lastyearstart, $lastyearend);
        $thisyearspents  = (int) lohas_get_customer_total_spents($current_user_id, $thisyearstart, $thisyearend);

        $results = get_level_rules();

        $current_role = implode(',', $current_user->roles);
        $role_cap     = get_role($current_role)->capabilities;
        $Authority    = count($role_cap);

        if ($Authority == 0) {
            // $title = '您的角色適用升級折扣';
            $title = __('Upgrade member level apply to your role', 'lwlm');
        } else {
            $title = __('Upgrade member level are not applicable to your role', 'lwlm');
        }

        $title = apply_filters('lwlm_dashboard_title', $title);

        $this->get_template(
            'myaccount/dashboard-widget',
            compact(
                'title', 'total_spend',
                'Authority', 'current_role',
                'current_user', 'role_cap',
                'results', 'lastyearstart',
                'lastyearend', 'thisyearspents'
            )
        );
    }
}
