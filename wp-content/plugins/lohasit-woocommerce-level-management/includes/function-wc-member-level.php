<?php

function lohas_get_customer_total_spents($user_id, $date_from, $date_to)
{
    global $wpdb;

    $spent = $wpdb->get_var("SELECT SUM(meta2.meta_value)
            FROM $wpdb->posts as posts
            LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
            LEFT JOIN {$wpdb->postmeta} AS meta2 ON posts.ID = meta2.post_id
            WHERE   meta.meta_key       = '_customer_user'
            AND     meta.meta_value     = $user_id
            AND     posts.post_type     IN ('shop_order')
            AND     posts.post_status   IN ( 'wc-completed', 'wc-processing' )
            AND     meta2.meta_key      = '_order_total'
            AND post_date BETWEEN '" . $date_from . " 00:00:00' AND '" . $date_to . " 23:59:59'
        ");

    return $spent;
}

function get_user_role($user_id)
{
    global $wp_roles;

    $user_data = get_userdata($user_id);

//    if (is_multisite()) {

        $user_role_slug = array_keys($user_data->caps)[0];

//    } else {
//        $user_role_slug = $user_data->roles[0];
//    }

    return translate_user_role(__($wp_roles->roles[$user_role_slug]['name'], 'lwlm'));

}

function get_current_admin_url()
{
    return admin_url(basename($_SERVER['REQUEST_URI']));
}

function get_all_roles()
{
    require_once ABSPATH . 'wp-admin/includes/user.php';

    $roles = [];

    $editable_roles = get_editable_roles();

    foreach ($editable_roles as $name => $role) {
        $role_cap  = get_role($name)->capabilities;
        $Authority = count($role_cap);
        if ($Authority <= 2) {
            $roles[$name] = $role['name'];
        } else {
            unset($editable_roles[$name]);
        }
    }

    return $roles;
}

function get_level_rules()
{
    global $wpdb, $table;

    $results = $wpdb->get_results(
        "select * from $table order by target_price asc"
    );

    $all_roles = get_all_roles();

    foreach ($results as $k => $v) {
        $role_name              = translate_user_role(__($all_roles[$v->role_key], 'lwlm'));
        $results[$k]->role_name = $role_name;
    }

    return $results;
}
