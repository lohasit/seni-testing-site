��    "      ,      <      <     =     J     P     V     d     m  )   z     �  �   �     A     T     ]     }     �     �     �  	   �     �  
   �  '   �  4     &   9     `     d     i     o          �  9   �  T   �  B   *  '   m     �  �  �     Z  	   g     q     x  	   �     �  1   �     �  �   �     l     �     �     �     �     �     �     �     �  	   �     		  A   (	  )   j	     �	     �	     �	     �	     �	     �	  /   �	  A   
  Z   W
  $   �
     �
   Add New Rule Admin Close Current level Customer Display name From %1$s to %2$s, your total spend: %3$s How to upgrade If any of your yearly spending(last year or this year) fulfill the level criteria listed below, you will be upgraded and earn some discounts! Last year spending Lohas IT Lohasit Member Level Management Member Level Member Level Setting Remove Save Settings Super VIP This year spending Upgrade To Upgrade member level apply to your role Upgrade member level are not applicable to your role Upgrade to %1$s if you spend over %2$s VIP VVIP VVVIP Yearly Spending Yearly spending You are a : You are currently the <span style="color:red;">%1$s<span> You spent a total of %1$s this year, and there is still a gap of %2$s to reach %3$s. You will be upgraded based on the higher yearly spending achieved! Your current yearly spending(Till %1$s) http://www.lohaslife.cc Project-Id-Version: Lohasit Member Level Management
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-14 10:07+0000
PO-Revision-Date: 2022-01-27 10:51+0000
Last-Translator: 
Language-Team: 繁體中文
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.5; wp-5.9
X-Domain: lwlm 新增規則 管理者 關閉 現有會員等級 消費者 顯示名稱 您從 %1$s 到 %2$s，總共花費：%3$s 元。 升級條件 在本年度或去年度滿足以下的消費總額，您的會員身份將被升級，並獲得商家為您量身訂造的折價優惠！ 去年年度總消費 樂活 樂活會員升級外掛 會員等級 會員升級設定 刪除 儲存上列設定 超級會員 今年年度總消費 升級至 您的角色適用升級折扣 您的角色<span style="color:red;">不</span>適用升級折扣 消費達 %2$s 元以上，升級為 %1$s VIP VVIP VVVIP 年度累計金額達 年度總消費額滿 您目前的等級是 你目前是<span style="color:red;">%1$s<span> 您今年總共花費 %1$s 元，尚差 %2$s 元會達到 %3$s。 若兩個年度皆符合升級條件，您將獲得等級較高的那個會員身份喔！ 您的年度消費概況(截至%1$s) http://www.lohaslife.cc 