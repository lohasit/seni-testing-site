<?php

/**
 * Plugin Name: Lohasit Member Level Management
 * Description: Lohasit Member Level Management
 * Version: 1.0.5
 * Author: Lohas IT
 * Author URI: http://www.lohaslife.cc
 * Text Domain: lwlm
 * Domain Path: /languages
 */

define('LWLM_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('LWLM_PLUGIN_URL', plugin_dir_url(__FILE__));
require( ABSPATH . 'wp-load.php' );
global $wpdb;

$table = $wpdb->get_blog_prefix() . 'member_level_management';

$lastyearstart = (date("Y") - 1) . "-01-01";
$lastyearend   = (date("Y") - 1) . "-12-31";
$thisyearstart = (date("Y")) . "-01-01";
$thisyearend   = (date("Y")) . "-12-31";

require_once LWLM_PLUGIN_PATH . 'includes/function-wc-member-level.php';
require_once LWLM_PLUGIN_PATH . 'includes/class-wc-member-level-admin.php';
require_once LWLM_PLUGIN_PATH . 'includes/class-wc-member-level-frontend.php';

function create_lohasit_level_management()
{
    global $wpdb;

    $table = $wpdb->get_blog_prefix() . 'member_level_management';

    if ($wpdb->get_var("show tables like '$table'") != $table) {

        $charset_collate = $wpdb->get_charset_collate();

//        $sql = "CREATE TABLE $table (
//          id mediumint(9) NOT NULL AUTO_INCREMENT,
//          role_key varchar(255),
//          target_price decimal(12, 2) NOT NULL,
//          custom_message longtext NULL,
//          created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
//          updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
//          PRIMARY KEY (id)
//        ) $charset_collate;";

        $sql = "CREATE TABLE $table (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          role_key varchar(255),
          target_price decimal(12, 2) NOT NULL,
          created_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);
    }
}

register_activation_hook(__FILE__, 'create_lohasit_level_management');

function lohasit_woocommerce_level_management()
{
    // Alan: 載入語系檔
    load_plugin_textdomain('lwlm', false, dirname(plugin_basename(__FILE__)) . '/languages');

    $admin = WC_Member_Level_Admin::get_instance();
    add_action('admin_enqueue_scripts', [$admin, 'lohasit_use_bootstrap']);
    add_action('admin_menu', [$admin, 'lohasit_create_menu']);
    add_action('woocommerce_order_status_changed', [$admin, 'change_role']);

    $frontend = WC_Member_Level_Frontend::get_instance();
    add_action('wp_enqueue_scripts', [$frontend, 'enqueue_scripts']);
    add_filter('woocommerce_locate_template', [$frontend, 'locate_template'], 10, 3);
    add_action('woocommerce_account_dashboard', [$frontend, 'render_dashboard_widget']);
    add_action('init', [$frontend, 'init_endpoint']);
    add_filter('query_vars', [$frontend, 'query_vars']);
    add_action('wp_loaded', [$frontend, 'rewrite_rules']);

    foreach (WC_Member_Level_Frontend::$new_items as $slug) {
        $method = str_replace('-', '_', $slug) . '_page';
        add_action('woocommerce_account_' . $slug . '_endpoint', [$frontend, $method]);
    }
}

add_action('plugins_loaded', 'lohasit_woocommerce_level_management');

function lohasit_translate_user_roles($translations, $text, $context, $domain)
{
    $plugin_domain = 'lwlm';

    $roles = apply_filters('lohasit_translate_user_roles', [
        'VIP',
        'VVIP',
        'VVVIP',
    ]);

    if ('User role' == $context && in_array($text, $roles)) {
        return __($text, $plugin_domain);
    }

    return $translations;
}
add_filter('gettext_with_context', 'lohasit_translate_user_roles', 10, 4);
