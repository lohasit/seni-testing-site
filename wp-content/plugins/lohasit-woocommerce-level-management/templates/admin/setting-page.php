
<div class="m-4">
      <!-- Button trigger modal -->
    <div class="row">
        <div class="col"><h2><?php _e('Member Level Setting', 'lwlm');?></h2></div>
        <div class="table-responsive-sm">
            <form method="post">
                <?if (count($results) > 0): ?>
                <table class="table">
                    <thread>
                        <tr>
                            <th><?php _e('Yearly Spending', 'lwlm');?></th>
                            <th><?php _e('Upgrade To', 'lwlm');?></th>
                            <th><?php _e('Display name', 'lwlm');?></th>
<!--                            <th>--><?php //_e('Custom message', 'lwlm');?><!--</th>-->
                            <th><a href="?id=xxx"></a></th>
                        </tr>
                    </thread>
                    <tbody>
                        <?php foreach ($results as $row): ?>
                        <tr class="table-primary">
                            <td>
                                <input type="number" name="target_price[]" value="<?php echo intval($row->target_price); ?>">
                            </td>
                            <td>
                                <input type="hidden" name="id[]" value="<?php echo $row->id; ?>">
                                <input type="text" name="role_key[]" value="<?php echo $row->role_key; ?>" readonly>
                            </td>
                            <td>
                                <input type="text" name="role_name[]" value="<?php echo translate_user_role(__($row->role_name, 'lwlm')); ?>" readonly>
                            </td>
<!--                            <td>-->
<!--                                <textarea type="text" name="custom_message[]" rows="3" >--><?php //echo ($row->custom_message)? str_replace('\"', '"' , $row->custom_message):''; ?><!--</textarea>-->
<!--                            </td>-->
                            <td>
                                <a href="<?php echo get_current_admin_url(); ?>&act=del&id=<?php echo $row->id ?>" class="btn btn-danger"><?php _e('Remove', 'lwlm');?></a>
                            </td>
                       </tr>
                       <?endforeach;?>
                    </tbody>
                </table>
                <?endif;?>
            </div>
            <!-- <p>新增的角色會同步到Anywhere請勿在User Role Editor勾選任何權限，否則該角色升級自動失效。</p> -->
            <p></p>
            <div class="form-group">
                <?php if (current_user_can('administrator')): ?>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add"> <?php _e('Add New Rule', 'lwlm');?></button>
                <?php endif;?>
                <button type="submit" name="uptrole" class="btn btn-primary" value="uptrole"  <?php echo (count($results) <= 0) ? 'disabled' : ''; ?>><?php _e('Save Settings', 'lwlm');?></button>
            </div>
        </form>
    </div>
</div>


 <!-- Modal -->
 <div class="modal" id="add" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php _e('Add New Rule', 'lwlm');?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <table class="table" >
                        <tr>
                            <td><?php _e('Yearly Spending', 'lwlm');?></td>
                            <td><?php _e('Upgrade To', 'lwlm');?></td>
                        </tr>
                        <tr>
                            <td><input type="number" size="10" name="target_price" placeholder="10000" required></td>
                            <td>
                                <select class="form-control" name="role_key" required>
                                    <?php foreach (get_all_roles() as $k => $v): ?>
                                    <option value="<?php echo $k; ?>"><?php echo $k; ?> (<?php echo translate_user_role(__($v, 'lwlm')); ?>)</option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php _e('Close', 'lwlm');?></button>
                    <button  type="submit" name="addrole" class="btn btn-primary" value="addrole"><?php _e('Add New Rule', 'lwlm');?></button>
                </div>
            </form>
        </div>
    </div>
 </div>