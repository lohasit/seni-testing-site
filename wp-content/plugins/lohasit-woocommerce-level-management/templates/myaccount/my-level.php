<!--        <h2 id="mem_type">--><?php //_e('You are a :', 'lwlm');?><!--<span>--><?php //echo $type; ?><!--</span></h2>-->
<!---->
<?php //if (count($results) > 0 && $thisyearspents < $results[count($results) - 1]->target_price): ?>
<!--    --><?php //$role_name = sprintf('<span class="role-name">%1$s</span>', $results[count($results) - 1]->role_name);?>
<!--    --><?php //printf(__('You spent a total of %1$s this year, and there is still a gap of %2$s to reach %3$s.', 'lwlm'), wc_price($thisyearspents), wc_price($results[count($results) - 1]->target_price - $thisyearspents), $role_name);?>
<!--    <br>-->
<?php //endif;?>

<h2 id="mem_type"><?php _e('You are a :', 'lwlm');?><span><?php echo get_user_role($current_user->ID); ?></span></h2>

<?php
// DD#0136 糾正升級條件的顯示邏輯
    $max_level = count($results)-1;
    $current_role = $current_user->roles;

    foreach($results as $key => $value) :

        if($results[$key]->role_key == $current_role[0]):

            if($key == $max_level):

            else:
                $role_name = sprintf('<span class="role-name">%1$s</span>', $results[$key+1]->role_name);
                printf(__('You spent a total of %1$s this year, and there is still a gap of %2$s to reach %3$s.', 'lwlm'), wc_price($thisyearspents), wc_price($results[$key+1]->target_price - $thisyearspents), $role_name);
            endif;
        endif;

    endforeach;

?>

        <section class="mem_section">
            <!-- <h3>您的年度消費概況(截至2021-10-01)</h3> -->
            <!-- <p>若兩個年度皆符合升級條件，您將獲得等級較高的那個會員身份喔！</p> -->
            <h3><?php printf(__('Your current yearly spending(Till %1$s)', 'lwlm'), date('Y-m-d'));?></h3>
            <p><?php _e('You will be upgraded based on the higher yearly spending achieved!', 'lwlm');?></p>

            <table>
                <tr>
                    <!-- <td>去年年度總消費</td> -->
                    <td class="consumption"><?php _e('Last year spending', 'lwlm');?></td>
                    <td><?php echo wc_price($total_spend); ?></td>
                </tr>
                <tr>
                    <!-- <td>今年年度總消費</td> -->
                    <td class="consumption"><?php _e('This year spending', 'lwlm');?></td>
                    <td><?php echo wc_price($thisyearspents); ?></td>
                </tr>
<!--                <tr>-->
<!--                  <td>現有會員等級</td> -->
<!--                    <td class="consumption">--><?php //_e('Current level', 'lwlm');?><!--</td>-->
<!--                    <td>--><?php //echo get_user_role($current_user->ID); ?><!--</td>-->
<!--                </tr>-->
            </table>
        </section>

        <section class="mem_section">
            <div class="h3widgh">

            </div>
            <!-- <h3>升級條件</h3> -->
            <!-- <p>在本年度或去年度滿足以下的消費總額，您的會員身份將被升級，並獲得商家為您量身訂造的折價優惠！</p> -->
            <h3><?php _e('How to upgrade', 'lwlm');?></h3>
            <p><?php _e('If any of your yearly spending(last year or this year) fulfill the level criteria listed below, you will be upgraded and earn some discounts!', 'lwlm');?></p>
            <table>
                <tr>
                    <!-- <th>年度總消費額滿</th> -->
                    <!-- <th>升級至</th> -->
                    <th><?php _e('Yearly spending', 'lwlm');?></th>
                    <th><?php _e('Upgrade To', 'lwlm');?></th>

                </tr>
                <?php foreach ($results as $key => $row): ?>
                <tr>
                    <td><?php echo wc_price($row->target_price); ?></td>
<!--                    <td>--><?php //echo str_replace('\"', '' , $row->custom_message); ?><!--</td>-->
                    <td><?php echo $row->role_name; ?></td>

                </tr>
                <?php endforeach;?>
            </table>
        </section>


