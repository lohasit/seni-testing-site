/**
 * admin.js
 *
 * @author Your Inspiration Themes
 * @package YITH Infinite Scrolling Premium
 * @version 1.0.0
 */

jQuery( function($) {
    "use strict";

    var block_loader    = ( typeof yith_ywpar_admin !== 'undefined' ) ? yith_ywpar_admin.block_loader : false;

    $('#ywpar_apply_points_from_wc_points_rewards_btn').on('click', function(e) {
        e.preventDefault();
        var from = $(this).prev().val(),
            container   = $('#ywpar_apply_points_from_wc_points_rewards_btn').closest('.forminp-yith-field');

        container.find('.response').remove();

        if (block_loader) {
            container.block({
                message   : null,
                overlayCSS: {
                    background: 'transparent',
                    opacity   : 0.5,
                    cursor    : 'none'
                }
            });
        }

        $.ajax({
            type    : 'POST',
            url     : yith_ywpar_admin.ajaxurl,
            dataType: 'json',
            data    : 'action=ywpar_apply_wc_points_rewards&from=' + from + '&security=' + yith_ywpar_admin.apply_wc_points_rewards,
            success : function (response) {
                container.unblock();
                container.append('<span class="response">'+response+'</span>');
            }
        });
    });


    $('#yit_ywpar_options_fix_expiration_points-container .button').on('click', function(e) {
        e.preventDefault();
        var container   = $('#yit_ywpar_options_fix_expiration_points-container');

        container.find('.response').remove();

        if (block_loader) {
            container.block({
                message   : null,
                overlayCSS: {
                    background: 'transparent',
                    opacity   : 0.5,
                    cursor    : 'none'
                }
            });
        }

        $.ajax({
            type    : 'POST',
            url     : yith_ywpar_admin.ajaxurl,
            dataType: 'json',
            data    : 'action=ywpar_fix_expiration_points&security=' + yith_ywpar_admin.fix_expiration_points,
            success : function (response) {
                container.unblock();
                container.append('<span class="response">'+response+'</span>');
				location.reload(false);
            }
        });
    });

    $( '#ywpar-notice-is-dismissable' ).find('.notice-dismiss').on( 'click', function(){
        $.cookie('ywpar_notice', 'dismiss', { path: '/' });
    } );


    /****
     * remove a row in custom type field
     ****/
    $(document).on('click', '#yith_woocommerce_points_and_rewards_points .ywpar-remove-row', function() {
        var $t = $(this),
            current_row = $t.closest('.role-conversion-options');
        current_row.remove();
    });

    $(document).on('click', '.extrapoint-options .ywpar-remove-row', function () {
        var $t = $(this),
            current_row = $t.closest('.extrapoint-options');
        current_row.remove();
    });

    $(document).on('click', '.options-role-conversion .ywpar-remove-row, .options-role-percentage-conversion .ywpar-remove-row', function () {
        var $t = $(this),
          current_row = $t.closest('.role-conversion-options'),
					container = current_row.parent();

				current_row.remove();

        if ( container.find('.role-conversion-options').length === 0 ) {
        	container.remove();
				}

    });

    /****
     * add a row in custom type field
     ****/
    $(document).on('click', '#yith_woocommerce_points_and_rewards_points-extra .ywpar-add-row', function() {
        var $t = $(this),
            wrapper = $t.closest('.yith-plugin-fw-field-wrapper'),
            current_option = $t.prev('.extrapoint-options'),
            current_index = parseInt( current_option.data('index')),
            clone = current_option.clone(),
            options = wrapper.find('.extrapoint-options'),
            max_index = 1;

        options.each(function(){
            var index = $(this).data('index');
            if( index > max_index ){
                max_index = index;
            }
        });

        var new_index = max_index + 1;
        clone.attr( 'data-index', new_index );
        console.log(clone);
        var fields = clone.find("[name*='list']");
        fields.each(function(){
            var $t = $(this),
                name = $t.attr('name'),
                id =  $t.attr('id'),

                new_name = name.replace('[list]['+current_index+']', '[list]['+new_index+']'),
                new_id = id.replace('[list]['+current_index+']', '[list]['+new_index+']');

            $t.attr('name', new_name);
            $t.attr('id', new_id);
            $t.val('');

        });

        clone.find('.ywpar-remove-row').removeClass('hide-remove');
        clone.find('.chosen-container').remove();

        $t.before(clone);

    });

    var check_thresholds_rules = function () {
        var rules_number = $('#ywpar_checkout_threshold_exp-container[data-index]').length,
            sub_panel = $('#yith_woocommerce_points_and_rewards_points-extra [data-dep-target="ywpar_checkout_threshold_not_cumulate"]');
        if ( rules_number > 1 ) {
            sub_panel.show();
        } else {
            sub_panel.hide();
        }

    };
    check_thresholds_rules();
    
    $('#ywpar_enable_checkout_threshold_exp').on( 'change', check_thresholds_rules );

    /* extra points manage Extra Points for cart total - multiple thresholds option */
    $(document).on('click', '#yith_woocommerce_points_and_rewards_points-extra .options-extrapoints[data-dep-target="ywpar_checkout_threshold_exp"] .ywpar-add-row', function() {
        check_thresholds_rules();
    });
    $(document).on('click', '#yith_woocommerce_points_and_rewards_points-extra .options-extrapoints[data-dep-target="ywpar_checkout_threshold_exp"] .ywpar-remove-row', function() {
        check_thresholds_rules();
    });

    /* reedem role conversion */
    $(document).on('click', '.options-role-conversion .ywpar-add-row, .options-role-percentage-conversion .ywpar-add-row', function() {
        var $t = $(this),
          wrapper = $t.closest('.yith-plugin-fw-field-wrapper'),
          current_option = $t.parent().find('.role-conversion-options[data-index="1"]'),
          current_index = parseInt( current_option.data('index')),
          clone = current_option.clone(),
          options = wrapper.find('.role-conversion-options'),
					add_same_rule_button_clone = current_option.parent().find('.ywpar-add-same-row').clone(),
          max_index = 1;

        options.each(function(){
            var index = $(this).data('index');
            if( index > max_index ){
                max_index = index;
            }
        });

        var new_index = max_index + 1;
        clone.attr( 'data-index', new_index );

        var fields = clone.find("[name*='role_conversion']");
        fields.each(function(){
            var $t = $(this),
              name = $t.attr('name'),
              id =  $t.attr('id'),

              new_name = name.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']'),
              new_id = id.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']');

            $t.attr('name', new_name);
            $t.attr('id', new_id);
            $t.val('');

        });


        clone.find('.ywpar-remove-row').removeClass('hide-remove');
        clone.find('.chosen-container').remove();

        var roles= [];
        if (  $t.parent().find('.role-conversion-options').hasClass('ywpar_redeem_roles') ) {
            roles = get_used_roles('.ywpar_redeem_roles select');
        } else {
            roles = get_used_roles('.ywpar_earn_roles select');
        }

        roles.forEach(
          function (element) {
                clone.find('select.ywpar_role option[value="' + element + '"]').remove();
          }
        );

				/* this is for add same rule optoins will be added */
       /* $t.before( '<div class="role-conversion-options-container">' + clone[0].outerHTML + add_same_rule_button_clone[0].outerHTML + '<div class="clear"></div></div>' );*/
        $t.before( '<div class="role-conversion-options-container">' + clone[0].outerHTML + '<div class="clear"></div></div>' );

    });

    $(document).on('click', '.options-role-conversion .ywpar-add-same-row, .options-role-percentage-conversion .ywpar-add-same-row', function() {
        var $t = $(this),
          wrapper = $t.closest('.yith-plugin-fw-field-wrapper'),
          current_option = $t.parent().find('.role-conversion-options:first-child'),
          current_index = parseInt( current_option.data('index')),
          clone = current_option.clone(),
          options = wrapper.find('.role-conversion-options'),
          max_index = 1;

        options.each(function(){
            var index = $(this).data('index');
            if( index > max_index ){
                max_index = index;
            }
        });

        var new_index = max_index + 1;
        clone.attr( 'data-index', new_index );

        var fields = clone.find("[name*='role_conversion']");
        fields.each(function(){
            var $t = $(this),
              name = $t.attr('name'),
              id =  $t.attr('id'),

              new_name = name.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']'),
              new_id = id.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']');

            $t.attr('name', new_name);
            $t.attr('id', new_id);


        });


        clone.find('.ywpar-remove-row').removeClass('hide-remove');
        clone.find('.chosen-container').remove();

        $t.before(clone);

    });


    function get_used_roles( el ) {
        var roles = [];

        $(el).each( function(){
            var v = $(this).children("option:selected").val();
            if ( typeof v !== 'undefined' && typeof roles[v] !== v ) {
                roles.push( $(this).children("option:selected").val() );
            }
        } );


        return roles;
    }

    $(document).on('click', '#yith_woocommerce_points_and_rewards_points .ywpar-add-row', function() {
        var $t = $(this),
          wrapper = $t.closest('.yith-plugin-fw-field-wrapper'),
          current_option = $t.closest('.role-conversion-options'),
          current_index = parseInt( current_option.data('index')),
          clone = current_option.clone(),
          options = wrapper.find('.role-conversion-options'),
          max_index = 1;

        options.each(function(){
            var index = $(this).data('index');
            if( index > max_index ){
                max_index = index;
            }
        });

        var new_index = max_index + 1;
        clone.attr( 'data-index', new_index );

        var fields = clone.find("[name*='role_conversion']");
        fields.each(function(){
            var $t = $(this),
              name = $t.attr('name'),
              id =  $t.attr('id'),

              new_name = name.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']'),
              new_id = id.replace('[role_conversion]['+current_index+']', '[role_conversion]['+new_index+']');

            $t.attr('name', new_name);
            $t.attr('id', new_id);
            $t.val('');

        });

        clone.find('.ywpar-remove-row').removeClass('hide-remove');
        clone.find('.chosen-container').remove();

        wrapper.append(clone);

    });


    /**
     * Reset points to all customer
     */
    $('.ywrac_reset_points').on('click', function(e) {
        e.preventDefault();

        var conf = confirm( yith_ywpar_admin.reset_points_confirm );

        if( ! conf ){
            return false;
        }

        var container   = $(this).closest('.yith-plugin-fw-field-wrapper');

        container.find('.response').remove();

        if (block_loader) {
            container.block({
                message   : null,
                overlayCSS: {
                    background: 'transparent',
                    opacity   : 0.5,
                    cursor    : 'none'
                }
            });
        }

        $.ajax({
            type    : 'POST',
            url     : yith_ywpar_admin.ajaxurl,
            dataType: 'json',
            data    : 'action=ywpar_reset_points&security=' + yith_ywpar_admin.reset_points,
            success : function (response) {
                container.unblock();
                container.append('<span class="response">'+response+'</span>');
            }
        });

    });

    /**
     * Import Export Tab Javascript
     */
    if( $('#ywpar_import_points').length ){
        $('#ywpar_import_points').closest('form').attr('enctype',"multipart/form-data");
    }

    $('#file_import_csv_btn').on( 'click', function(e){
        e.preventDefault();
        $( '#file_import_csv').click();
    });

    $( '#file_import_csv' ).on('change', function(){
        var fname =  document.getElementById("file_import_csv").files[0].name;

        if ( fname !== '' ) {
            $('span.ywpar_file_name').html( fname );
        }
    });

    $('button#ywpar_import_points').on('click', function(e){
        e.preventDefault();
        var action = $('#type_action').val();
		    $('.ywpar_safe_submit_field').val( action + '_points');
		    $(this).closest('form').submit();
    });

    if ( $('.ywpar_import_result').length ) {
        $('.ywpar_import_result').show();
        setTimeout(function() {
            $('.ywpar_import_result').remove();
        }, 3000);


    }
    $('#type_action').on('change', function(){

        var $t = $(this),
            action = $t.val();
        if( action === 'import' ){
           $('#yith_woocommerce_points_and_rewards_import_export .upload.file_import_csv').show();
           $('#yith_woocommerce_points_and_rewards_import_export .radio.csv_import_action').show();
           $('#ywpar_import_points').text(yith_ywpar_admin.import_button_label);
        }else{
            $('#yith_woocommerce_points_and_rewards_import_export .upload.file_import_csv').hide();
            $('#yith_woocommerce_points_and_rewards_import_export .radio.csv_import_action').hide();
            $('#ywpar_import_points').text(yith_ywpar_admin.export_button_label);
        }


    });
    $('#type_action').change();



    /**
     * Reset button on Customer points view
     * @since 1.6
     */
    $(document).on('click', '.ywpar_reset_points', function(e){
        e.preventDefault();
        var $t = $(this),
            username = $t.data('username'),
            message= yith_ywpar_admin.reset_point_message + ' ' + username  + '?';
        if( confirm( message )){
          window.location.href = $t.attr('href');
        }
    });

    $(document).on('click', '.ywpar_update_points, .ywpar_filter_button', function(e) {
        window.onbeforeunload = '';
    });

    /**
     * Some deps
     * @since 1.6
     */
    $(document).on('change', '#ywpar_conversion_rate_method, #ywpar_rewards_points_for_role', function () {
        var $t = $('#ywpar_conversion_rate_method'),
        $onoff = $('#ywpar_rewards_points_for_role');

        if( 'yes' === $onoff.val()){
            if ('fixed' === $t.val()) {
                $('#ywpar_rewards_points_role_rewards_fixed_conversion_rate-container').closest('tr').show();
                $('#ywpar_rewards_points_role_rewards_percentage_conversion_rate-container').closest('tr').hide();
            } else {
                $('#ywpar_rewards_points_role_rewards_percentage_conversion_rate-container').closest('tr').show();
                $('#ywpar_rewards_points_role_rewards_fixed_conversion_rate-container').closest('tr').hide();
            }
        }else{
            $('#ywpar_rewards_points_role_rewards_fixed_conversion_rate-container').closest('tr').hide();
            $('#ywpar_rewards_points_role_rewards_percentage_conversion_rate-container').closest('tr').hide();
        }

        /* manage reward message style */
        if ('fixed' !== $t.val()) {
            $('#ywpar_enabled_rewards_cart_message_layout_style-custom').prop( 'checked', true );
            $('.textarea-editor[data-dep-target=ywpar_rewards_cart_message]').show();
            $('.textarea-editor[data-dep-target=ywpar_rewards_cart_message]').css( 'opacity', 1);
        }

        $('#ywpar_apply_redeem_restrictions').change();

    });
    $('#ywpar_conversion_rate_method').change();
    $('#ywpar_rewards_points_for_role').change();


    //tab Bulk

    /* bulk actions apply to change event */

    $('#ywpar_bulk_apply_to').on('change', function () {
        var $t = $(this),
            val = $t.val();

        if ( 'everyone' !== val || $('#ywpar_bulk_action_type').val() === 'add_to_order' ) {
            $('#ywpar_active_exclusion').next('span').css('border-color', '#d8d8d8');
            $('#ywpar_active_exclusion').prop('checked', false);
            $('#ywpar_active_exclusion').val('no');

        }

        if ( $('#ywpar_bulk_action_type').val() === 'add_to_order' ) {
            return;
        }

        $('.ywpar-sub-deps').each(function () {
            var $sb = $(this),
                dep = $sb.data('sub-deps');
            dep = dep ? dep.split(',') : [];
            if (dep.indexOf(val) !== -1) {
                $sb.show();
            } else {
                $sb.hide();
            }

        });
        $('#ywpar_active_exclusion').change();

    });


    $('#ywpar_active_exclusion').on( 'change', function(){
        var t = $(this);
        if ( 'no' === t.val() ) {
            $('.ywpar_exclude_users_type').hide();
            $('.ywpar_user_role_excluded').hide();
            $('.ywpar_customer_list_exclude').hide();
        } else {
            $('#ywpar_exclude_users_type').change();
            $('.ywpar_exclude_users_type').show();
        }

    });

    $('#ywpar_exclude_users_type').on('change', function(){
        var t = $(this);
        if ( 'by_user' === t.val() ) {
            $('.ywpar_user_role_excluded ').hide();
            $('.ywpar_customer_list_exclude ').show();
        } else {
            $('.ywpar_user_role_excluded ').show();
            $('.ywpar_customer_list_exclude ').hide();
        }
    });

    /* bulk action type change event */
    $(document).on('change', '#ywpar_bulk_action_type', function () {
        var $t = $(this),
            val = $t.val();

        $('.ywpar-deps_action').each(function () {

            var $sb = $(this),
                dep = $sb.data('deps');
            if (dep.indexOf(val) !== -1) {
                $sb.show();
            } else {
                $sb.hide();
            }

        });
        $('#ywpar_apply_points_previous_order_to').change();
        $('#ywpar_bulk_apply_to').change();
        $('#ywpar_active_exclusion').change();


    });
    $('#ywpar_bulk_action_type').change();


    /* apply points to previous orders change event */
    $('#ywpar_apply_points_previous_order_to').on( 'change', function() {
        var dfield = $('tr.ywpar_apply_points_previous_order');

       if( $(this).val() === 'all' || $('#ywpar_bulk_action_type').val() !== 'add_to_order' ) {
           dfield.hide();
       } else {
           dfield.show();
       }
    });

    $('#yith_woocommerce_points_and_rewards_bulk .bulkactions input.button').each( function(){
        $(this).on( 'click', function() { window.onbeforeunload = ''; });
    });

    /* bulk action ajax */
    $('#ywpar_bulk_action_points').on('click', function (e) {
        e.preventDefault();
        window.onbeforeunload = '';
        var form = $(this).closest('form'),
            form_values = form.serializeObject(),
            container = $('#yith_woocommerce_points_and_rewards_bulk-container');


        if ( "from" === form_values.ywpar_apply_points_previous_order_to && "" === form_values.ywpar_apply_points_previous_order ) {
            $('#ywpar_apply_points_previous_order').addClass('ywpar-error ');
            return;
        }

        if (block_loader) {
            container.block({
                message   : null,
                overlayCSS: {
                    background: 'transparent',
                    opacity   : 0.5,
                    cursor    : 'none'
                }
            });
        }

        $('.ywpar-bulk-trigger').append('<div class="ywpar-bulk-progress"><div>0%</div></div>');

        process_step(1, form.serialize(), form );

    });


    var process_step = function( step, data, form ) {

        var block_container = $('.ywpar-bulk-trigger'),
            container = $('#yith_woocommerce_points_and_rewards_bulk-container');

        $.ajax({
            type: 'POST',
            url: yith_ywpar_admin.ajaxurl,
            data: {
                form: data,
                action: 'ywpar_bulk_action',
                step: step
            },
            dataType: 'json',
            success: function( response ) {
                if( 'done' === response.step ) {
                    block_container.find('.ywpar-bulk-progress').hide('slow').remove();
                    container.append('<span class="ywpar-bulk-response">' + response.message + '</span>');
                    container.unblock();
                    setTimeout(function(){ $('span.ywpar-bulk-response').remove(); }, 2000);

                } else if( 'error' === response.step){
                    container.append('<span class="ywpar-bulk-response">' + response.error + '</span>');
                    setTimeout(function(){ $('span.ywpar-bulk-response').remove(); }, 2000);
                } else {
                    block_container.find('.ywpar-bulk-progress div').html( response.percentage + '%' );
                    block_container.find('.ywpar-bulk-progress div').animate({
                        width: response.percentage + '%',
                    }, 50, function() {
                        // Animation complete.
                    });
                    process_step( parseInt( response.step ), data, form );

                }

            }
        });

    };

    $.fn.serializefiles = function() {
        var obj = $(this);
        /* ADD FILE TO PARAM AJAX */
        var formData = new FormData();
        var params = $(obj).serializeArray();

        $.each(params, function (i, val) {
           formData.append(val.name, val.value);
        });

        return formData;
    };

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $('#ywpar_apply_redeem_restrictions').on( 'change', function(){
        if ( 'yes' === $(this).val() ) {

            if ( 'percentage' === $('#ywpar_conversion_rate_method').val() ) {
                $('#ywpar_max_percentual_discount').closest('tr').css('display', 'table-row');
            } else {
                $('#ywpar_max_points_discount').closest('tr').css('display', 'table-row');
                $('#ywpar_max_points_product_discount').closest('tr').css('display', 'table-row');
                $('#ywpar_minimum_amount_discount_to_redeem').closest('tr').css('display', 'table-row');
            }

            $('#ywpar_minimum_amount_to_redeem').closest('tr').css('display', 'table-row');

        } else {
            $('#ywpar_max_points_discount').closest('tr').css('display', 'none');
            $('#ywpar_minimum_amount_to_redeem').closest('tr').css('display', 'none');
            $('#ywpar_minimum_amount_discount_to_redeem').closest('tr').css('display', 'none');
            $('#ywpar_max_points_product_discount').closest('tr').css('display', 'none');
            $('#ywpar_max_percentual_discount').closest('tr').css('display', 'none');

        }
    });
    $('#ywpar_apply_redeem_restrictions').change();

    $('#ywpar_max_percentual_discount').after('<span class="ywpar_type_sign">%</span>')

    /* point options product category page*/
    $('div#ywpar_earning_type_product_category').on('change', function(){
        var v = $( this ).val();
        $('.form-field.ywpar_points_product_category .ywpar_points_type').hide();
        if ( 'fixed' === v ) {
            $('.form-field.ywpar_points_product_category .ywpar_points_type.points').show();
        } else if ( 'percentage' === v ) {
            $('.form-field.ywpar_points_product_category .ywpar_points_type.percentage').show();
        }
    }).change();

    $('div#ywpar_earning_schedule_product_category').on('change', function(){
        var v = $( this ).val();
        if ( 'manual' === v ) {
            $('.form-field.ywpar_start_end_date').hide();
        } else {
            $('.form-field.ywpar_start_end_date').show();
        }
    }).change();

    $('div#ywpar_redeem_type_product_category').on('change', function(){
        var v = $( this ).val();
        if ( 'fixed' === v ) {
            $('.form-field.ywpar_max_point_discount .ywpar_points_type.points').show();
            $('.form-field.ywpar_max_point_discount .ywpar_points_type.percentage').hide();
            $('.form-field.ywpar_max_point_discount').show();
        } else if ( 'percentage' === v ) {
            $('.form-field.ywpar_max_point_discount .ywpar_points_type.points').hide();
            $('.form-field.ywpar_max_point_discount .ywpar_points_type.percentage').show();
            $('.form-field.ywpar_max_point_discount').show();
        } else {
            $('.form-field.ywpar_max_point_discount').hide();
        }
    }).change();

    $('input#ywpar_override_global_product_category').on( 'change', function(){
        if ( 'yes' === $(this).val() ) {
            if ('scheduled' === $('.form-field.ywpar_earning_schedule_product_category input:checked').val() ) {
                $('.form-field.ywpar_start_end_date').show();
            }
        } else {
            $('.form-field.ywpar_start_end_date').hide();
        }
    }).change();

    $('#ywpar_enable_points_upon_sales').on( 'change', function(){
        var v = $(this).val();
        if ( 'no' === v){
            $('#yith_woocommerce_points_and_rewards_points-standard table.form-table tr.yith-plugin-fw-panel-wc-row:not(:first-child)').hide();
        } else {
            $('#yith_woocommerce_points_and_rewards_points-standard table.form-table tr.yith-plugin-fw-panel-wc-row').show();
            $('#ywpar_user_role_enabled_type').change();
            $('#ywpar_enable_conversion_rate_for_role').change();
            $('#ywpar_enable_expiration_point').change();
        }
    }).change();

    $('#ywpar_enable_rewards_points').on( 'change', function(){
        var v = $(this).val();
        if ( 'no' === v){
            $('#yith_woocommerce_points_and_rewards_points-redeem table.form-table tr.yith-plugin-fw-panel-wc-row:not(:first-child)').hide();
        } else {
            $('#yith_woocommerce_points_and_rewards_points-redeem table.form-table tr.yith-plugin-fw-panel-wc-row').show();
            $('#ywpar_user_role_redeem_type').change();
            $('#ywpar_conversion_rate_method').change();
            $('#ywpar_rewards_points_for_role').change();
            $('#ywpar_enabled_rewards_cart_message_layout_style').change();
        }
    }).change();
});

