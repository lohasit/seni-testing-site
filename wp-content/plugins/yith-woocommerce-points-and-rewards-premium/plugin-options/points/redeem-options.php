<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly.

$tab = array(
	'points-redeem' => array(
		// REDEEMING

		'rewards_point_option'                              => array(
			'name' => esc_html__( 'Points redeeming', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_rewards_point_option',
		),

		'enable_rewards_points'                             => array(
			'name'      => esc_html__( 'Allow the users to redeem points', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose if a user can redeem points automatically or if you want to manage points redeeming manually.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'yes',
			'id'        => 'ywpar_enable_rewards_points',
		),

		'conversion_rate_method'                            => array(
			'name'      => esc_html__( 'Reward Conversion Method', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose how to apply the discount. The discount can either be a percent or a fixed amount.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'fixed',
			'options'   => array(
				'fixed'      => esc_html__( 'Fixed Price Discount', 'yith-woocommerce-points-and-rewards' ),
				'percentage' => esc_html__( 'Percentage Discount', 'yith-woocommerce-points-and-rewards' ),
			),
			'id'        => 'ywpar_conversion_rate_method',
		),

		'rewards_conversion_rate'                           => array(
			'name'      => esc_html__( 'Reward Conversion Rate', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose how to calculate the discount when customers use their available points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-conversion',
			'type'      => 'yith-field',
			'class'     => 'fixed_method',
			'default'   => array(
				$currency => array(
					'points' => 100,
					'money'  => 1,
				),
			),
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'fixed',
				'type'  => 'hide',
			),
			'id'        => 'ywpar_rewards_conversion_rate',
		),

		'rewards_percentage_conversion_rate'                => array(
			'name'      => esc_html__( 'Reward Conversion Rate', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose how to calculate the discount when customers use their available points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-percentage-conversion',
			'type'      => 'yith-field',
			'default'   => array(
				$currency => array(
					'points'   => 20,
					'discount' => 5,
				),
			),
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'percentage',
				'type'  => 'hide',
			),
			'id'        => 'ywpar_rewards_percentual_conversion_rate',
		),


		'user_enabled_to_redeem'                            => array(
			'name'      => esc_html__( 'User that can redeem points', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose if all users can redeem points or only specified user roles can do that', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'id'        => 'ywpar_user_role_redeem_type',
			'options'   => array(
				'all'   => esc_html__( 'All', 'yith-woocommerce-points-and-rewards' ),
				'roles' => esc_html__( 'Only specified user roles', 'yith-woocommerce-points-and-rewards' ),
			),
			'default' => 'all'
		),

		'user_role_redeem_enabled'                          => array(
			'name'      => '',
			'desc'      => esc_html__( 'Choose which user roles can redeem points', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'class'     => 'wc-enhanced-select',
			'css'       => 'min-width:300px',
			'multiple'  => true,
			'id'        => 'ywpar_user_role_redeem_enabled',
			'options'   => yith_ywpar_get_roles(),
			'default'   => array(),
			'deps'      => array(
				'id'    => 'ywpar_user_role_redeem_type',
				'value' => 'roles',
				'type'  => 'hide',
			)
		),

		'rewards_points_for_role'                           => array(
			'name'      => esc_html__( 'Use different redeem conversion rules based on the user roles', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable if you want to set different conversion rules based on user roles', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'id'        => 'ywpar_rewards_points_for_role',
			'default'   => 'no',
		),


		'rewards_points_role_rewards_fixed_conversion_rate' => array(
			'name'                   => esc_html__('Redeem conversion rules based on user roles', 'yith-woocommerce-points-and-rewards' ),
			'desc'                   => esc_html__( '', 'yith-woocommerce-points-and-rewards' ),
			'yith-type'              => 'options-role-conversion',
			'type'                   => 'yith-field',
			'default'                => array(
				'role_conversion' => array(
					array(
						'role'    => 'administrator',
						$currency => array(
							'points' => 1,
							'money'  => 10,
						),
					),
				),
			),
			'yith-sanitize-callback' => 'ywpar_option_role_convertion_sanitize',
			'id'                     => 'ywpar_rewards_points_role_rewards_fixed_conversion_rate',
			'option' => array(
				'class'                  => 'ywpar_redeem_roles'
			)

		),

		'rewards_points_role_rewards_percentage_conversion_rate' => array(
			'name'                   => esc_html__('Redeem conversion rules based on user roles', 'yith-woocommerce-points-and-rewards' ),
			'desc'                   => esc_html__( '', 'yith-woocommerce-points-and-rewards' ),
			'yith-type'              => 'options-role-percentage-conversion',
			'type'                   => 'yith-field',
			'default'                => array(
				'role_conversion' => array(
					array(
						'role'    => 'administrator',
						$currency => array(
							'points'   => 20,
							'discount' => 5,
						),
					),
				),
			),
			'yith-sanitize-callback' => 'ywpar_option_role_convertion_sanitize',
			'id'                     => 'ywpar_rewards_points_role_rewards_percentage_conversion_rate',
			'option' => array(
				'class'                  => 'ywpar_redeem_roles'
			)

		),

		'rewards_points_level'                              => array(
			'name'      => esc_html__('For users with more than one rule', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose which redeem rule to apply to users with more than one role, when different redeem rules are applied for roles', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'radio',
			'type'      => 'yith-field',
			'options'   => array(
				'low'  => esc_html__( 'Use the rule with lowest rewards', 'yith-woocommerce-points-and-rewards' ),
				'high' => esc_html__( 'Use the rule with highest rewards', 'yith-woocommerce-points-and-rewards' ),
			),
			'default'   => 'high',
			'id'        => 'ywpar_rewards_points_level',
			'deps'      => array(
				'id'    => 'ywpar_rewards_points_for_role',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'coupon_delete_after_use'                           => array(
			'name'      => esc_html__( 'Delete used coupons', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable if you want to automatically delete the used coupons', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_coupon_delete_after_use',
			'default'   => 'yes',
		),

		'autoapply_points_cart_checkout'                    => array(
			'name'      => esc_html__( 'Automatically redeem points on Cart/Checkout Page', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to automatically apply points on the cart/checkout page', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_autoapply_points_cart_checkout',


		),

		'enabled_rewards_cart_message_layout_style'     => array(
			'name'      => esc_html__( 'Redeem box style', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose the style for the redeem points section', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'radio',
			'type'      => 'yith-field',
			'options'   => array(
				'default'  => esc_html__( 'Default', 'yith-woocommerce-points-and-rewards' ),
				'custom' => esc_html__( 'Custom', 'yith-woocommerce-points-and-rewards' ),
			),
			'id'        => 'ywpar_enabled_rewards_cart_message_layout_style',
			'default'   => 'default',
			'deps'      => array(
				'id'    => 'ywpar_enable_rewards_points',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'rewards_cart_message'             => array(
			'name'      => esc_html__( 'Redeem message in Cart and Checkout', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => _x( ' Enter the redeem message to show in cart and checkout page. <br />You can use these placeholders:<br />{points} number of points earned;<br>{points_label} of points;<br>{max_discount} maximum discount value','do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'textarea-editor',
			'type'      => 'yith-field',
			'default'   => _x( 'Use <strong>{points}</strong> {points_label} for a <strong>{max_discount}</strong> discount on this order!', 'do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
			'id'        => 'ywpar_rewards_cart_message',
			'deps'      => array(
				'id'    => 'ywpar_enabled_rewards_cart_message_layout_style',
				'value' => 'custom',
				'type'  => 'hide',
			),
			'textarea_rows' => 5
		),

		'allow_free_shipping_to_redeem'                     => array(
			'name'      => esc_html__( 'Offer free shipping when user redeem', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to offer free shipping to users that redeem their points. A free shipping method must be enabled and set up in your shipping zones to generate “a valid free shipping coupon”.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_allow_free_shipping_to_redeem',
			'default'   => 'no',
		),

		'other_coupons'                                     => array(
			'name'      => esc_html__( 'Coupons allowed', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Select if you want to allow the use of point-redemption coupons, WooCommerce coupons or both.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'radio',
			'type'      => 'yith-field',
			'options'   => array(
				'wc_coupon' => esc_html__( 'Use only WooCommerce coupons', 'yith-woocommerce-points-and-rewards' ),
				'ywpar'     => esc_html__( 'Use only points-redemption coupon', 'yith-woocommerce-points-and-rewards' ),
				'both'      => esc_html__( 'Use both coupons', 'yith-woocommerce-points-and-rewards' ),

			),
			'default'   => 'both',
			'id'        => 'ywpar_other_coupons',
		),

		'rewards_point_option_end'                          => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_rewards_point_option_end',
		),

		'min_max_option'                                    => array(
			'name' => esc_html__( 'Redeeming restrictions', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_min_max_option',
		),

		'enable_redeem_restrictions'                        => array(
			'name'      => esc_html__( 'Apply redeeming restrictions', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to setup redeeming restrictions for your users.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_apply_redeem_restrictions',
			'default'    => 'no',
		),

		// showed if conversion_rate_method == percentage.
		'max_percentual_discount'                           => array(
			'name'      => esc_html__( 'Maximum discount users can get', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( '( in %) Set maximum discount percentage allowed in cart when redeeming points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'text',
			'type'      => 'yith-field',
			'id'        => 'ywpar_max_percentual_discount',
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'percentage',
				'type'  => 'hide',
			),
			'default'   => 50,
		),


		// showed if conversion_rate_method == fixed.
		'max_points_discount'                               => array(
			'name'      => esc_html__( 'Maximum discount users can get', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set the maximum discount amount that your can get when they redeem their points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-restrictions-text-input',
			'type'      => 'yith-field',
			'id'        => 'ywpar_max_points_discount',
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'fixed',
				'type'  => 'hide',
			),
		),

		'minimum_amount_to_redeem'                          => array(
			'name'      => esc_html__( 'Minimum cart amount to redeem points', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set the minimum cart amount required to redeem points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-restrictions-text-input',
			'type'      => 'yith-field',
			'default'   => '',
			'id'        => 'ywpar_minimum_amount_to_redeem',
		),

		// showed if conversion_rate_method == fixed.
		'minimum_amount_discount_to_redeem'                 => array(
			'name'      => esc_html__( 'Minimum discount required to redeem', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set the minimum discount to redeem points.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-restrictions-text-input',
			'type'      => 'yith-field',
			'id'        => 'ywpar_minimum_amount_discount_to_redeem',
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'fixed',
				'type'  => 'hide',
			),
		),

		// showed if conversion_rate_method == fixed.
		'max_points_product_discount'                       => array(
			'name'      => esc_html__( 'Maximum discount for a single product', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set the maximum points-generated discount that can be applied per product.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'options-restrictions-text-input',
			'type'      => 'yith-field',
			'id'        => 'ywpar_max_points_product_discount',
			'deps'      => array(
				'id'    => 'ywpar_conversion_rate_method',
				'value' => 'fixed',
				'type'  => 'hide',
			),
		),


		'min_max_option_end'                                => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_min_max_option_end',
		),

	),
);

return $tab;