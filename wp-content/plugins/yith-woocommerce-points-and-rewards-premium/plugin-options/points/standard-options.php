<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly.

$currency = get_woocommerce_currency();

$tab = array(
	'points-standard' => array(

		'points_title'                                      => array(
			'name' => esc_html__( 'Points Assignments', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_points_title',
		),

		'enable_points_upon_sales'                          => array(
			'name'      => esc_html__( 'Assign points to user', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose whether to award points to users automatically or manually', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'options'   => array(
				'yes' => esc_html__( 'Automatically - Points will be assigned automatically for each purchase', 'yith-woocommerce-points-and-rewards' ),
				'no'  => esc_html__( 'Manually - You can assign points manually in \'Customer Points\' tab', 'yith-woocommerce-points-and-rewards' ),
			),
			'default'   => 'yes',
			'id'        => 'ywpar_enable_points_upon_sales',
		),

		'earn_points_conversion_rate'                       => array(
			'name'      => esc_html__( 'Points assigned for each product purchase', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__(
				"Set how many points per product will be earned based on the product value. Please, note: points are awarded on a product basis and not on the cart total.",
				'yith-woocommerce-points-and-rewards'
			),
			'yith-type' => 'options-conversion-earning',
			'type'      => 'yith-field',
			'default'   => array(
				$currency => array(
					'points' => 1,
					'money'  => 10,
				),
			),
			'id'        => 'ywpar_earn_points_conversion_rate',
		),

		'exclude_product_on_sale'                           => array(
			'name'      => esc_html__( 'Exclude on sale products', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'If enabled, sale products will not assign points to your users', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_exclude_product_on_sale',
			'default'   => 'no',
		),

		'user_role_enabled'                                 => array(
			'name'      => esc_html__( 'Assign points to', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose if assign points to all users or only to specified user roles', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'id'        => 'ywpar_user_role_enabled_type',
			'options'   => array(
				'all'   => esc_html__( 'All Users', 'yith-woocommerce-points-and-rewards' ),
				'roles' => esc_html__( 'Only specified user roles', 'yith-woocommerce-points-and-rewards' ),
			),
			'default' => 'all'
		),

		'user_roles_selected'                               => array(
			'name'      => esc_html__( 'Assign points to', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose if assign points to all users or only to specified user roles', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'class'     => 'wc-enhanced-select',
			'css'       => 'min-width:300px',
			'multiple'  => true,
			'id'        => 'ywpar_user_role_enabled',
			'options'   => yith_ywpar_get_roles(),
			'deps'      => array(
				'id'    => 'ywpar_user_role_enabled_type',
				'value' => 'roles',
				'type'  => 'hide',
			)
		),

		'enable_conversion_rate_for_role'                   => array(
			'name'      => esc_html__( 'Assign different amounts of points based on the user role', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose the rule you want to apply to assign a different number of points according to the user roles', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'id'        => 'ywpar_enable_conversion_rate_for_role',
			'default'   => 'no',
		),

		'conversion_rate_level'                             => array(
			'name'      => '',
			'desc'      => esc_html__( 'Priority Level Conversion', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'radio',
			'type'      => 'yith-field',
			'options'   => array(
				'low'  => esc_html__( 'Use the rule with the Lowest Conversion Rate', 'yith-woocommerce-points-and-rewards' ),
				'high' => esc_html__( 'Use the rule with the Highest Conversion Rate', 'yith-woocommerce-points-and-rewards' ),
			),
			'default'   => 'high',
			'id'        => 'ywpar_conversion_rate_level',
			'deps'      => array(
				'id'    => 'ywpar_enable_conversion_rate_for_role',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'earn_points_role_conversion_rate'                  => array(
			'name'                   => '',
			'desc'                   => esc_html__( '', 'yith-woocommerce-points-and-rewards' ),
			'yith-type'              => 'options-role-conversion',
			'type'                   => 'yith-field',
			'default'                => array(
				'role_conversion' => array(
					array(
						'role'    => 'administrator',
						$currency => array(
							'points' => 1,
							'money'  => 10,
						),
					),
				),
			),
			'id'                     => 'ywpar_earn_points_role_conversion_rate',
			'deps'                   => array(
				'id'    => 'ywpar_enable_conversion_rate_for_role',
				'value' => 'yes',
				'type'  => 'hide',
			),
			'yith-sanitize-callback' => 'ywpar_option_role_convertion_sanitize',
			'option' => array(
				'class'                  => 'ywpar_earn_roles'
			)
		),

		'assign_points_to_registered_guest'                 => array(
			'name'      => esc_html__( 'Assign points to a guest if his billing email is registered', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to guests if the billing email matches with a registered user.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_assign_points_to_registered_guest',
			'default'   => 'no',
		),
		'assign_older_orders_points_to_new_registered_user' => array(
			'name'      => esc_html__( 'Assign points to a newly registered user if his billing email is registered', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to newly registered users if they use the same billing email address for previous orders.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_assign_older_orders_points_to_new_registered_user',
			'default'   => 'no',
		),

		'order_status_to_earn_points'                       => array(
			'name'      => esc_html__( 'Assign points when', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose based on which order status to assign points to users.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'class'     => 'wc-enhanced-select',
			'css'       => 'min-width:300px',
			'multiple'  => true,
			'id'        => 'ywpar_order_status_to_earn_points',
			'options'   => ywpar_get_order_status_to_earn_points(),
			'default'   => array( 'woocommerce_order_status_completed', 'woocommerce_payment_complete', 'woocommerce_order_status_processing' ),
		),

		'remove_point_order_deleted'                        => array(
			'name'      => esc_html__( 'Delete points of cancelled orders', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable if you want to remove earned points when an order is cancelled', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_remove_point_order_deleted',
			'default'   => 'yes',
		),

		'reassing_redeemed_points_refund_order'             => array(
			'name'      => esc_html__( 'Reassign points when an order is refunded', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable if you want to reassign the redeemed points to a customer when an order is refunded.', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_reassing_redeemed_points_refund_order',
			'default'   => 'no',
		),

		'remove_point_refund_order'                         => array(
			'name'      => esc_html__( 'Enable removal of points for total or partial refunds', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to remove points when applying a total or partial refund of the order', 'yith-woocommerce-points-and-rewards' ),
			'yith-type' => 'onoff',
			'type'      => 'yith-field',
			'id'        => 'ywpar_remove_point_refund_order',
			'default'   => 'yes',
		),

		'remove_points_coupon'                              => array(
			'name'      => esc_html__( 'Do not assign points to the full order amount if a coupon is used.', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable this option if you do not want the user to earn points on a full order amount if they use a coupon. Instead they will only earn points on the amount minus the coupon discount. For example: order total €30 minus €10 coupon discount, so the user earns points on €20 order value only.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'id'        => 'ywpar_remove_points_coupon',
			'default'   => 'yes',
		),

		'disable_point_earning_while_reedeming'             => array(
			'name'      => esc_html__( 'Do not assign points to orders in which the user is redeeming points', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to not assign points to orders in which the user redeems points', 'yith-woocommerce-points-and-rewards' ),
			'id'        => 'ywpar_disable_earning_while_reedeming',
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'deps'      => array(
				'id'    => 'ywpar_enable_points_upon_sales',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'round_points_down_up'                              => array(
			'name'      => esc_html__( 'Points Rounding', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Select how to round the points. For example, if points are 1.5 and Round Up is selected, points will be 2. If Round Down is selected, points will be 1.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'id'        => 'ywpar_points_round_type',
			'options'   => array(
				'up'   => esc_html__( 'Round Up', 'yith-woocommerce-points-and-rewards' ),
				'down' => esc_html__( 'Round Down', 'yith-woocommerce-points-and-rewards' ),
			),
			'default'   => 'down',
			'deps'      => array(
				'id'    => 'ywpar_enable_points_upon_sales',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'enable_expiration_point'                           => array(
			'name'      => esc_html__( 'Enable points expiration', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable if you want to set an expiration time on points assigned to users', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_expiration_point',
		),


		'days_before_expiration'                            => array(
			'name'              => esc_html__( 'Points will expire after', 'yith-woocommerce-points-and-rewards' ),
			'desc'              => esc_html__( 'Set a default expiration on points earned in your shop', 'yith-woocommerce-points-and-rewards' ),
			'type'              => 'yith-field',
			'yith-type'         => 'options-expire',
			'default'           => 0,
			'id'                => 'ywpar_days_before_expiration',
			'deps'              => array(
				'id'    => 'ywpar_enable_expiration_point',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'points_title_end'                                  => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_points_title_end',
		),

	)
);

return apply_filters( 'ywpar_points_settings', $tab);
