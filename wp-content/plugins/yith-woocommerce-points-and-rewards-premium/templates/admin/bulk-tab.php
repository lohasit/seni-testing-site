<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Text Plugin Admin View
 *
 * @package    YITH
 * @author     Armando Liccardo <armando.liccardo@yithemes.com>
 * @since      1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$roles = yith_ywpar_get_roles();

$fields = array(
	'actions' => array(
		'id'    => 'ywpar_bulk_action_type',
		'name'  => 'ywpar_bulk_action_type',
		'title' => esc_html__( 'Action', 'yith-woocommerce-points-and-rewards' ),
		'type'  => 'radio',
		'options' => array(
			'reset'          => esc_html__( 'Reset points', 'yith-woocommerce-points-and-rewards' ),
			'add_to_order'   => esc_html__( 'Add points to previous orders', 'yith-woocommerce-points-and-rewards' ),
			'add_to_user'    => esc_html__( 'Add points to users', 'yith-woocommerce-points-and-rewards' ),
			'remove'         => esc_html__( 'Remove points to users', 'yith-woocommerce-points-and-rewards' ),
			'ban'            => esc_html__( 'Ban Users', 'yith-woocommerce-points-and-rewards' ),

		),
		'value' => 'reset',
		'desc' => esc_html__( 'Choose the action you want to execute', 'yith-woocommerce-points-and-rewards' )
	),
	
	'points_of' => array(
		'id'      => 'ywpar_bulk_apply_to',
		'name'    => 'ywpar_bulk_apply_to',
		'title'   => esc_html__( 'Apply action to', 'yith-woocommerce-points-and-rewards' ),
		'desc' => esc_html__( 'Choose to which users apply this action', 'yith-woocommerce-points-and-rewards' ),
		'type'    => 'radio',
		'options' => array(
			'everyone'       => esc_html__( 'All users', 'yith-woocommerce-points-and-rewards' ),
			'role_list'      => esc_html__( 'Only specified user roles', 'yith-woocommerce-points-and-rewards' ),
			'customers_list' => esc_html__( 'Only specified users', 'yith-woocommerce-points-and-rewards' ),
		),
		'value' => 'everyone',
		'data-deps' => 'reset, add_to_user, ban, remove',

	),

	'specific_user_roles' => array(
		'id' => 'ywpar_user_role',
		'name' => 'ywpar_user_role',
		'title'       => esc_html__( "Choose which roles", 'yith-woocommerce-points-and-rewards' ),
		'desc' => esc_html__( "Choose which user roles to apply the action", 'yith-woocommerce-points-and-rewards' ),
		'type'        => 'select',
		'class'       => 'wc-enhanced-select',
		'style'       => 'width:500px',
		'multiple'    => true,
		'options'     => yith_ywpar_get_roles(),
		'default'     => array( 'all' ),
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'role_list'
	),

	'specific_users' => array (
		'id' => 'ywpar_customer_list',
		'name' => 'ywpar_customer_list',
		'type' => 'ajax-customers',
		'title'       => esc_html__( "Choose which users", 'yith-woocommerce-points-and-rewards' ),
		'desc' => esc_html__( "Choose which users to apply the action", 'yith-woocommerce-points-and-rewards' ),
		'multiple' => true,
		'allow_clear' => true,
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'customers_list'

	),

	/* exclusion fields */

	'active_exclusion' => array(
		'id'      => 'ywpar_active_exclusion',
		'name'    => 'ywpar_active_exclusion',
		'title'   => esc_html__( 'Exclude users', 'yith-woocommerce-points-and-rewards' ),
		'desc'    => esc_html__( 'Enable if you want to exclude specific users or user roles', 'yith-woocommerce-points-and-rewards' ),
		'type'    => 'onoff',
		'value'   => 'no',
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'everyone'
	),

	'exclude_users_type' => array(
		'id'          => 'ywpar_exclude_users_type',
		'name'        => 'ywpar_exclude_users_type',
		'title'       => esc_html__( "Don't apply action to", 'yith-woocommerce-points-and-rewards' ),
		'description' => esc_html__( "Set which users to apply the selected action", 'yith-woocommerce-points-and-rewards' ),
		'type'    => 'radio',
		'options' => array(
			'by_user'       => esc_html__( 'Specified users', 'yith-woocommerce-points-and-rewards' ),
			'by_role'      => esc_html__( 'Specified user roles', 'yith-woocommerce-points-and-rewards' ),
		),
		'value'       => 'by_user',
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'everyone',
	),

	'select_user_roles_to_exclude' => array(
		'id'          => 'ywpar_user_role_excluded',
		'name'        => 'ywpar_user_role_excluded',
		'title'       => esc_html__( "Choose which roles to exclude", 'yith-woocommerce-points-and-rewards' ),
		'description' => esc_html__( "Choose which user roles to exclude from point resetting", 'yith-woocommerce-points-and-rewards' ),
		'type'        => 'select',
		'class'       => 'wc-enhanced-select',
		'style'       => 'width:500px',
		'multiple'    => true,
		'options'     => yith_ywpar_get_roles(),
		'default'     => array(),
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'everyone'

	),

	'select_users_to_exclude' => array (
		'id' => 'ywpar_customer_list_exclude',
		'name' => 'ywpar_customer_list_exclude',
		'type' => 'ajax-customers',
		'title'       => esc_html__( "Choose which users to exclude", 'yith-woocommerce-points-and-rewards' ),
		'description' => esc_html__( "Choose which users to exclude from point resetting", 'yith-woocommerce-points-and-rewards' ),
		'multiple' => true,
		'allow_clear' => true,
		'default'     => array(),
		'data-deps' => 'reset, add_to_user, ban, remove',
		'sub-deps' => 'everyone'

	),

	/* add points quantity field */
	'add_points_quantity_field' => array(
		'title'     => esc_html__( 'Points', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Set how many points.', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'number',
		'value'     => 0,
		'data-deps' => 'add_to_user,remove',
		'id'        => 'ywpar_bulk_add_points',
		'name'      => 'ywpar_bulk_add_points'
	),
	/* add points description field */
	'add_points_description' => array(
		'title'     => esc_html__( 'Description', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enter a description to explain to your users the reason for the points action', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'text',
		'value'     => '',
		'data-deps' => 'add_to_user,remove',
		'id'        => 'ywpar_bulk_add_description',
		'name'      => 'ywpar_bulk_add_description'
	),

	/* apply points to previous order date field */
	'apply_points_previous_order_type' => array(
		'title'     => esc_html__( 'Add points to', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Choose to assign points to all previous orders or only orders placed from a specific date. All orders from this date will be checked and points will be added to the customer\'s profile.', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'radio',
		'options'   => array(
			'all'  => esc_html__( 'All previous orders', 'yith-woocommerce-points-and-rewards' ),
			'from' => esc_html__( 'Orders placed from a specific date', 'yith-woocommerce-points-and-rewards' ),
		),
		'value'   => 'all',
		'data-deps' => 'add_to_order',
		'id'        => 'ywpar_apply_points_previous_order_to',
		'name'      => 'ywpar_apply_points_previous_order_to'
	),

	'apply_points_previous_order'                       => array(
		'title'     => esc_html__( 'Add points to orders placed from', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Choose from which date to assign points to previous orders', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'datepicker',
		'data'      => array(
				'date-format' => 'yy-mm-dd'
		),
		'style' => 'max-width: 200px;',
		'data-deps' => 'add_to_order,',
		'id'        => 'ywpar_apply_points_previous_order',
		'name'      => 'ywpar_apply_points_previous_order',

	),

);
?>

<div id="yith_woocommerce_points_and_rewards_bulk" class="yith-plugin-fw-wp-page-wrapper yith-plugin-fw  yit-admin-panel-container">
	<div class="yit-admin-panel-content-wrap wrap subnav-wrap">
		<h1 class="wp-heading-inline"><?php esc_html_e( 'Bulk Actions', 'yith-woocommerce-points-and-rewards' ); ?>
		<?php
		if ( isset( $_GET['action'] ) && isset( $link ) ) :
			?>
			<a href="<?php echo esc_url( $link ); ?>"
			class="add-new-h2"><?php esc_html_e( '< Back to list', 'yith-woocommerce-points-and-rewards' ); ?></a><?php endif ?>
	</h1>

		<div id="yith_woocommerce_points_and_rewards_bulk-container" class="yit_options rm_option rm_input rm_text">
			<form id="yith_woocommerce_points_and_rewards_bulk_form" method="post">
			<table class="form-table">
				<tbody>
				<?php foreach ( $fields as $field ) : ?>

				<?php
					$main_dep_class = isset( $field['data-deps'] ) ? 'ywpar-deps_action' : '';
					$sub_dep_class = isset( $field['sub-deps'] ) ? 'ywpar-sub-deps' : '';

					$data_deps = ( isset( $field['data-deps'] ) ) ? 'data-deps="' . $field['data-deps'] . '"' : '';
					$data_sub_deps = ( isset( $field['sub-deps'] ) ) ? 'data-sub-deps="' . $field['sub-deps'] . '"' : '';


				?>
					<tr valign="top" class="yith-plugin-fw-panel-wc-row <?php echo esc_html( $field['type'] ); ?> <?php echo $main_dep_class; ?> <?php echo $sub_dep_class; ?> <?php echo esc_html( $field['name'] ); ?> " <?php echo $data_deps  ?> <?php echo $data_sub_deps  ?>>
						<th scope="row" class="titledesc">
							<label for="<?php echo esc_html( $field['name'] ); ?>"><?php echo esc_html( $field['title'] ); ?></label>
						</th>
						<td class="forminp forminp-<?php echo esc_html( $field['type'] ); ?>">
							<?php
							yith_plugin_fw_get_field( $field, true );
							if ( isset( $field['desc'] ) && '' !== $field['desc'] ) {
								?>
								<span class="description"><?php echo esc_html( $field['desc'] ); ?></span>
								<?php
							}
							?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
				<div class="inner-option ywpar-bulk-trigger">
					<input type="hidden" class="ywpar_safe_submit_field" name="ywpar_safe_submit_field" value="" data-std="">
					<button class="button button-primary"
					        id="ywpar_bulk_action_points"><?php esc_html_e( 'Apply Action', 'yith-woocommerce-points-and-rewards' ); ?></button>
				</div>
			<div class="clear"></div>
			</form>
		</div>
	</div>
</div>
