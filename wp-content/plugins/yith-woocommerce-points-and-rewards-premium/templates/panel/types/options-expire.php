<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Custom Option Field
 *
 * @package    YITH
 * @author     Armando Liccardo <armando.liccardo@yithemes.it>
 * @since      2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


extract( $field );

$value = maybe_unserialize( $value );

$selected = $value['time'];
?>


	<div id="<?php echo esc_attr( $id ); ?>-container" class="yit_options rm_option rm_input rm_text expire-options">
		<div class="option">
			<input name="<?php echo esc_attr( $id ).'[number]'; ?>" id="<?php echo esc_attr( $id ).'[number]'; ?>" type="number" value="<?php echo esc_attr( $value['number'] ); ?>" step="1" />
			<select id="<?php echo esc_attr( $id ) .'[time]'; ?>" name="<?php echo esc_attr( $id ) .'[time]'; ?>">
				<option value="days" <?php echo ( 'days' == $selected ) ? 'selected': '' ?>><?php esc_html_e( 'Days', 'yith-woocommerce-points-and-rewards' ) ?></option>
				<option value="months" <?php echo ( 'months' == $selected ) ? 'selected': '' ?> ><?php esc_html_e( 'Months', 'yith-woocommerce-points-and-rewards' ) ?></option>
			</select>
		</div>

		<div class="clear"></div>
	</div>