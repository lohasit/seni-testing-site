<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Text Plugin Admin View
 *
 * @package    YITH
 * @author     Emanuela Castorina <emanuela.castorina@yithemes.it>
 * @since      1.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

extract( $field );
$index = 0;
$currencies       = array();
$default_currency = get_woocommerce_currency();
array_push( $currencies, $default_currency );

// filter to multi currencies integration.
$currencies = apply_filters( 'ywpar_get_active_currency_list', $currencies );
// DO_ACTION : ywpar_before_currency_loop : action triggered before the currency loop inside the option useful to multi-currency plugins
do_action( 'ywpar_before_currency_loop' );

$value = maybe_unserialize( $value );

if ( isset( $value['list'] ) ) :
	foreach ( $value['list'] as $element ) :
		$index ++;
		$current_name = $name . '[list][' . $index . ']';
		$current_id   = $id . '[list][' . $index . ']';
		$hide_remove  = 1 == $index ? 'hide-remove' : 1;
		$repeat       = ( isset( $element['repeat'] ) ) ? $element['repeat'] : 0;
		$multiple     = isset( $multiple ) ? $multiple : 1;
		$show_repeat  = isset( $show_repeat ) ? $show_repeat : 1;
		?>
		<div id="<?php echo esc_attr( $id ); ?>-container" data-index="<?php echo esc_attr( $index ); ?>"
			 class="yit_options rm_option rm_input rm_text extrapoint-options">
			<div class="option">
                <div class="left-side">
            <?php 
             foreach ( $currencies as $current_currency ) :
                $curr_name = $current_name . '[' . $current_currency . ']';
                $curr_id   = $current_id . '[' . $current_currency . ']';        
                $points       = ( isset( $element[$current_currency]['points'] ) ) ? $element[$current_currency]['points'] : '';
                $number       = ( isset( $element[$current_currency]['number'] ) ) ? $element[$current_currency]['number'] : '';     
             ?>
                <div>
                    <input type="number" name="<?php echo esc_attr( $curr_name ); ?>[number]" step="1" min="1"
                        id="<?php echo esc_attr( $curr_id ); ?>-number"
                        value="<?php echo esc_attr( $number ); ?>"/>
                    <span><?php echo sprintf( '%s (%s)', get_woocommerce_currency_symbol( $current_currency ), $current_currency ) . __( ' spent =', 'yith-woocommerce-points-and-rewards' ) ?></span>

                    <input type="number" name="<?php echo esc_attr( $curr_name ); ?>[points]" step="1" min="1"
                        id="<?php echo esc_attr( $curr_id ); ?>-points"
                        value="<?php echo esc_attr( $points ); ?>"/>
                    <span><?php
                        if ( 'ywpar_number_of_points_exp' === esc_attr( $id ) ) {
                            esc_html_e( 'points extra', 'yith-woocommerce-points-and-rewards' );
                        } else {
                            esc_html_e( 'points', 'yith-woocommerce-points-and-rewards' );
                        }
                        ?></span>
                </div>
            
            <?php endforeach; ?>
            </div>
            <div class="right_side">
				<?php if ( $show_repeat ) : ?>
					<input type="checkbox" name="<?php echo esc_attr( $current_name ); ?>[repeat]" value="1"
						   id="<?php echo esc_attr( $current_id ); ?>-repeat" <?php checked( $repeat, 1, 1 ); ?>>
					<small><?php esc_html_e( 'repeat', 'yith-woocommerce-points-and-rewards' ); ?></small>
				<?php endif ?>
				<?php if ( $multiple ) : ?>
						<span class="ywpar-remove-row yith-icon-trash <?php echo esc_attr( $hide_remove ); ?>"></span>

                <?php endif ?>
                </div>
			</div>

		</div>
	<?php if ( count( $value['list'] ) === $index && count( $value['list'] ) > 0 ) : ?>
		<span class="ywpar-add-row"><?php esc_html_e('+ Add rule') ?></span>
		<div class="clear"></div>
	<?php endif; ?>


		<?php
	endforeach;
endif;
?>
