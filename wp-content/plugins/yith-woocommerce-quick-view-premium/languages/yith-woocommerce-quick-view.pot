# Copyright (C) 2022 YITH
# This file is distributed under the same license as the YITH WooCommerce Quick View Premium package.
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Quick View Premium 1.15.0\n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin/yith-woocommerce-quick-view.premium\n"
"POT-Creation-Date: 2022-03-07 15:09:05+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2022-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#: includes/class.yith-wcqv-admin.php:119
msgid "General"
msgstr ""

#: includes/class.yith-wcqv-admin.php:120
msgid "Product"
msgstr ""

#: includes/class.yith-wcqv-admin.php:121
msgid "Style"
msgstr ""

#: init.php:52
msgid ""
"YITH WooCommerce Quick View is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: plugin-options/general-options.php:19
msgid "General Options"
msgstr ""

#: plugin-options/general-options.php:27
msgid "Enable Quick View"
msgstr ""

#: plugin-options/general-options.php:35
msgid "Enable Quick View on mobile"
msgstr ""

#: plugin-options/general-options.php:36
msgid "Enable quick view features on mobile device too"
msgstr ""

#: plugin-options/general-options.php:44
msgid "Enable Quick View on wishlist"
msgstr ""

#: plugin-options/general-options.php:46
#. translators: %s stand for YITH WooCommerce Wishlist landing url.
msgid ""
"Enable quick view on wishlist table (available only with modal window). You "
"need %s installed."
msgstr ""

#: plugin-options/general-options.php:54
msgid "Enable Lightbox"
msgstr ""

#: plugin-options/general-options.php:55
msgid "Enable lightbox. Product images will open in a lightbox."
msgstr ""

#: plugin-options/general-options.php:63
msgid "Quick View Navigation"
msgstr ""

#: plugin-options/general-options.php:64
msgid ""
"Enable product navigation on quick view. NOTE: only available on modal "
"window style."
msgstr ""

#: plugin-options/general-options.php:72
msgid "Enable navigation in the same product category"
msgstr ""

#: plugin-options/general-options.php:80
msgid "Quick View Navigation Style"
msgstr ""

#: plugin-options/general-options.php:84
msgid "Slide ( thumbnail and product name )"
msgstr ""

#: plugin-options/general-options.php:85
msgid "Rotate ( thumbnail )"
msgstr ""

#: plugin-options/general-options.php:93
msgid "Quick View Type"
msgstr ""

#: plugin-options/general-options.php:97
msgid "Modal Window"
msgstr ""

#: plugin-options/general-options.php:98
msgid "Cascading"
msgstr ""

#: plugin-options/general-options.php:106
msgid "Closing cascading quick view"
msgstr ""

#: plugin-options/general-options.php:110
msgid "Close Quick View and stay in the same row"
msgstr ""

#: plugin-options/general-options.php:111
msgid "Close quick view and scroll to the row of selected product"
msgstr ""

#: plugin-options/general-options.php:123
msgid "Select modal effect"
msgstr ""

#: plugin-options/general-options.php:127
msgid "Slide in"
msgstr ""

#: plugin-options/general-options.php:128
msgid "Fade in"
msgstr ""

#: plugin-options/general-options.php:129
msgid "Scale up"
msgstr ""

#: plugin-options/general-options.php:141
msgid "Modal window loading"
msgstr ""

#: plugin-options/general-options.php:142
msgid "Enable overlay during loading of the modal window."
msgstr ""

#: plugin-options/general-options.php:154
msgid "Modal Loading Text"
msgstr ""

#: plugin-options/general-options.php:155
msgid ""
"Enter here the text to show during quick view loading when modal overlay is "
"enabled."
msgstr ""

#: plugin-options/general-options.php:158
msgid "Loading..."
msgstr ""

#: plugin-options/general-options.php:167
msgid "Modal Width"
msgstr ""

#: plugin-options/general-options.php:168
msgid "Set width of modal window."
msgstr ""

#: plugin-options/general-options.php:181
msgid "Modal Height"
msgstr ""

#: plugin-options/general-options.php:182
msgid "Set height of modal window."
msgstr ""

#: plugin-options/general-options.php:199
msgid "Button Options"
msgstr ""

#: plugin-options/general-options.php:207
msgid "Quick View Button Type"
msgstr ""

#: plugin-options/general-options.php:208
#: plugin-options/general-options.php:235
msgid "Label for the quick view button in the WooCommerce loop."
msgstr ""

#: plugin-options/general-options.php:212
msgid "Use button"
msgstr ""

#: plugin-options/general-options.php:213
msgid "Use icon"
msgstr ""

#: plugin-options/general-options.php:221
msgid "Quick View Button Icon"
msgstr ""

#: plugin-options/general-options.php:222
msgid "Icon for the quick view button in the WooCommerce loop."
msgstr ""

#: plugin-options/general-options.php:234
msgid "Quick View Button Label"
msgstr ""

#: plugin-options/general-options.php:238
msgid "Quick View"
msgstr ""

#: plugin-options/general-options.php:247
msgid "Quick View Button Position"
msgstr ""

#: plugin-options/general-options.php:248
msgid "Position of the quick view button."
msgstr ""

#: plugin-options/general-options.php:252
msgid "After 'add to cart' button"
msgstr ""

#: plugin-options/general-options.php:253
msgid "Inside product image"
msgstr ""

#: plugin-options/product-options.php:19
msgid "Content Options"
msgstr ""

#: plugin-options/product-options.php:27
msgid "Select Element to Show"
msgstr ""

#: plugin-options/product-options.php:28
msgid "Show Product Image"
msgstr ""

#: plugin-options/product-options.php:36
msgid "Show Product Name"
msgstr ""

#: plugin-options/product-options.php:44
msgid "Show Product Rating"
msgstr ""

#: plugin-options/product-options.php:52
msgid "Show Product Price"
msgstr ""

#: plugin-options/product-options.php:60
msgid "Show Product Excerpt"
msgstr ""

#: plugin-options/product-options.php:68
msgid "Show Product Add To Cart"
msgstr ""

#: plugin-options/product-options.php:76
msgid "Show Wishlist Button"
msgstr ""

#: plugin-options/product-options.php:84
msgid "Show Compare Button"
msgstr ""

#: plugin-options/product-options.php:92
msgid "Show Request Quote Button"
msgstr ""

#: plugin-options/product-options.php:100
msgid "Show Dynamic Pricing Discount Table"
msgstr ""

#: plugin-options/product-options.php:108
msgid "Show Dynamic Pricing Discount Note"
msgstr ""

#: plugin-options/product-options.php:116
msgid "Show Product Meta"
msgstr ""

#: plugin-options/product-options.php:124
msgid "Show full description"
msgstr ""

#: plugin-options/product-options.php:125
msgid "Show full description instead of short description"
msgstr ""

#: plugin-options/product-options.php:133
msgid "Product Image Width"
msgstr ""

#: plugin-options/product-options.php:134
msgid "Set width of product image."
msgstr ""

#: plugin-options/product-options.php:143
msgid "Product Image Height"
msgstr ""

#: plugin-options/product-options.php:144
msgid "Set height of product image."
msgstr ""

#: plugin-options/product-options.php:153
msgid "Select Thumbnails Type"
msgstr ""

#: plugin-options/product-options.php:157
msgid "Don't show"
msgstr ""

#: plugin-options/product-options.php:158
msgid "Slider mode"
msgstr ""

#: plugin-options/product-options.php:159
msgid "Classic mode"
msgstr ""

#: plugin-options/product-options.php:167
msgid "Enable image zoom"
msgstr ""

#: plugin-options/product-options.php:169
#. translators: %s stand for YITH WooCommerce Zoom Magnifier landing url.
msgid ""
"Enable the plugin YITH WooCommerce Zoom Magnifier on quick view (not "
"available for slider mode). You need %s installed."
msgstr ""

#: plugin-options/product-options.php:177
msgid "Add 'View Details' Button"
msgstr ""

#: plugin-options/product-options.php:178
msgid "Check this option to add a button to go to the single product page."
msgstr ""

#: plugin-options/product-options.php:186
msgid "'View Details' Button Label"
msgstr ""

#: plugin-options/product-options.php:187
msgid "Set label for 'View Details' button"
msgstr ""

#: plugin-options/product-options.php:190
msgid "View Details"
msgstr ""

#: plugin-options/product-options.php:199
msgid "Enable Ajax Add To Cart"
msgstr ""

#: plugin-options/product-options.php:200
msgid "Check this option to enable add to cart in ajax"
msgstr ""

#: plugin-options/product-options.php:208
msgid "Close Popup after \"Add To Cart\""
msgstr ""

#: plugin-options/product-options.php:209
msgid "Check this option to auto close popup after the add to cart action in ajax"
msgstr ""

#: plugin-options/product-options.php:221
msgid "Redirect to checkout after add to cart"
msgstr ""

#: plugin-options/product-options.php:222
msgid "Check this option to redirect to checkout after add to cart"
msgstr ""

#: plugin-options/product-options.php:234
msgid "Share Options"
msgstr ""

#: plugin-options/product-options.php:242
msgid "Enable Share"
msgstr ""

#: plugin-options/product-options.php:243
msgid ""
"Check this option if you want to show the share link for products in quick "
"view"
msgstr ""

#: plugin-options/product-options.php:251
msgid "Select Socials"
msgstr ""

#: plugin-options/product-options.php:256
msgid "Facebook"
msgstr ""

#: plugin-options/product-options.php:257
msgid "Twitter"
msgstr ""

#: plugin-options/product-options.php:258
msgid "Pinterest"
msgstr ""

#: plugin-options/product-options.php:259
msgid "eMail"
msgstr ""

#: plugin-options/product-options.php:267
msgid "Facebook App ID"
msgstr ""

#: plugin-options/product-options.php:269
#. translators: %s stand for the Facebook documentation url.
msgid ""
"Facebook App ID necessary to share contents. Read more in the official "
"Facebook <a href=\"%s\">documentation</a>"
msgstr ""

#: plugin-options/style-options.php:17
msgid "General Style"
msgstr ""

#: plugin-options/style-options.php:24
msgid "Modal Window Background Color"
msgstr ""

#: plugin-options/style-options.php:34
msgid "'Quick View' Button Color"
msgstr ""

#: plugin-options/style-options.php:43
msgid "'Quick View' Button Text Color"
msgstr ""

#: plugin-options/style-options.php:52
msgid "'Quick View' Button Hover Color "
msgstr ""

#: plugin-options/style-options.php:61
msgid "'Quick View' Button Hover Text Color"
msgstr ""

#: plugin-options/style-options.php:74
msgid "Content Style"
msgstr ""

#: plugin-options/style-options.php:82
msgid "Main Text Color"
msgstr ""

#: plugin-options/style-options.php:91
msgid "Star Color"
msgstr ""

#: plugin-options/style-options.php:100
msgid "'Add to Cart' Button Color"
msgstr ""

#: plugin-options/style-options.php:109
msgid "'Add to Cart' Button Text Color"
msgstr ""

#: plugin-options/style-options.php:118
msgid "'Add to Cart' Button Hover Color "
msgstr ""

#: plugin-options/style-options.php:127
msgid "'Add to Cart' Button Hover Text Color"
msgstr ""

#: plugin-options/style-options.php:136
msgid "'View Details' Button Color"
msgstr ""

#: plugin-options/style-options.php:145
msgid "'View Details' Button Text Color"
msgstr ""

#: plugin-options/style-options.php:154
msgid "'View Details' Button Hover Color "
msgstr ""

#: plugin-options/style-options.php:163
msgid "'View Details' Button Hover Text Color"
msgstr ""

#: templates/yith-quick-view-share.php:41
msgid "May I ask you to see this product, please?"
msgstr ""

#. Description of the plugin/theme
msgid ""
"The <code><strong>YITH WooCommerce Quick View</strong></code> plugin allows "
"your customers to have a quick look about products. <a "
"href=\"https://yithemes.com/\" target=\"_blank\">Get more plugins for your "
"e-commerce shop on <strong>YITH</strong></a>."
msgstr ""