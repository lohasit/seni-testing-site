<?php
/**
 * Quick view bone.
 *
 * @author  YITH
 * @package YITH WooCommerce Quick View
 * @version 1.0.0
 */

defined( 'YITH_WCQV' ) || exit; // Exit if accessed directly.

?>

<div class="yith-quick-view <?php echo esc_attr( $type . ' ' . $effect . ' ' . $is_mobile ); ?>">

	<div class="yith-quick-view-overlay"></div>

	<div class="yith-wcqv-wrapper">

		<div class="yith-wcqv-main">

			<div class="yith-wcqv-head">
				<a href="#" class="yith-quick-view-close">X</a>
			</div>

			<div class="yith-quick-view-content woocommerce single-product"></div>

		</div>

	</div>

	<?php if ( $nav && 'yith-inline' !== $type ) : ?>
		<div class="yith-quick-view-nav <?php echo esc_attr( $nav_style ); ?>">
			<a href="#" class="yith-wcqv-prev" data-product_id="">
				<div></div>
			</a>
			<a href="#" class="yith-wcqv-next" data-product_id="">
				<div></div>
			</a>
		</div>
	<?php endif; ?>

</div>
<?php
