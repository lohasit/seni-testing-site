<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * Add to Quote button template
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @version 2.2.7
 * @author  YITH
 *
 * @var $product_id int
 * @var $wpnonce    string
 * @var $label      string
 * @var $class
 */

$id = ''; //phpcs:ignore

$product_id = yit_wpml_object_id( $product_id, 'product', true );
if ( isset( $colors ) ) {

	$css_class = str_replace( ' ', '.', $class );

	$style = "<style>#p-{$product_id}.{$css_class}{background-color: {$colors['bg_color']}!important;
    color: {$colors['color']}!important;}
    
     #p-{$product_id}.{$css_class}:hover{ background-color: {$colors['bg_color_hover']}!important;
    color: {$colors['color_hover']}!important; }</style>";

	echo $style; // phpcs:ignore

	$id = 'id=p-' . $product_id . ''; //phpcs:ignore
}
?>

<a href="#" class="<?php echo esc_attr( $class ); ?>" data-product_id="<?php echo esc_attr( $product_id ); ?>" data-wp_nonce="<?php echo esc_attr( $wpnonce ); ?>" <?php echo esc_attr( $id ); ?>>
	<?php if ( $icon ) : ?>
		<i class="ywraq-quote-icon-icon_quote"></i>
		<?php else : ?>
			<?php echo esc_html( $label ); ?>
	<?php endif; ?>
</a>
