jQuery(function ($) {

    /** LohasIT - woocommerce優惠券建立時，標題不得為空。 - 開始 **/
    $('.wp-admin.post-type-shop_coupon #publishing-action input[name="publish"]').on('click', function (e) {
        if($('input[name="post_title"]').val() != ''){
            $(this).parents('form').submit();
        }else{
            e.preventDefault();
            e.stopPropagation();
            alert('請填寫標題，謝謝配合');
        }
    });
    /** 結束 **/

    /** LohasIT - ECPay設定廠商編號的欄位的字數限制。 - 開始 **/
    $('input[name="woocommerce_ecpay_ecpay_trade_no_prefix"]').attr('maxlength', 3);
    /** 結束 **/

});