jQuery(function ($) {

    /** LohasIT - 在WooCommerce購物車或結帳頁面移除優惠卷以及重整時，會提交表單，導致無法移除優惠卷，於是加入此操作。 - 開始 **/
    $( document ).on( 'click', 'body.woocommerce-cart.woocommerce-page .woocommerce-remove-coupon, body.woocommerce-checkout.woocommerce-page .woocommerce-remove-coupon',function () {
        setTimeout(function () {
            window.location.href=location.href;
        },'1000');
    });
    /** 結束 **/

});

