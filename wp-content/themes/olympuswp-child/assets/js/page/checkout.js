jQuery(function ($) {

    /** LohasIT - 在WooCommerce結帳頁面，判斷運送區塊的連絡電話格式。 - 開始 **/
    //如要擴充其他欄位，可參考此格式。
    $(document).on('focusout' ,'#shipping_phone', function (e) {
        //如勾選運送到不同地址，且這個值為非空值才進入，因原本woocommerce就有判斷空值，且空值判斷若加入此處，會影響返回上一步驟的流程。
      if( $('#ship-to-different-address-checkbox:checked').length > 0 && $(this).val() != ''){
          var phone_format = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
          if(!phone_format.test($(this).val()) || $(this).val()[1] != '9'){
              $('input[name="checkout_next_step"]').attr('disabled', true);
              var shipping_phone_invalid_html = '<li id="phone_field_text"><strong>Shipping 連絡000電話</strong> 不是有效的號碼。</li>';
              if(!$('.woocommerce-NoticeGroup').length){
                  $('<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout" style="display:none;"><ul class="woocommerce-error" role="alert"></ul></div>').prependTo('#customer_shipping_details');
                  $('.woocommerce-NoticeGroup .woocommerce-error').prepend(shipping_phone_invalid_html);
                  $('.woocommerce-NoticeGroup').fadeIn(500);
              }else if($('.woocommerce-NoticeGroup').length && !$('#phone_field_text').length){
                  $('.woocommerce-NoticeGroup .woocommerce-error').prepend(shipping_phone_invalid_html);
              }
          }else{
              $('input[name="checkout_next_step"]').attr('disabled', false);
              $('.woocommerce-NoticeGroup').fadeOut(500);
              setTimeout(function(){
                  if($('.woocommerce-NoticeGroup').length){
                      $('.woocommerce-NoticeGroup').remove();
                  }
              }, 500);
          }
      }
   });
    //復原被上方disable的功能
    $(document).on('click', 'input[name="checkout_prev_step"]', function (e) {
        $('input[name="checkout_next_step"]').attr('disabled', false);
    });
    /** 結束 **/

});