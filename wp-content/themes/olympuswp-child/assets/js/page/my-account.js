jQuery(function ($) {

    /** LohasIT - 在WooCommerce帳號頁面(註冊)，自訂密碼強度判斷。 - 開始 **/
    $(  'body.woocommerce-account.woocommerce-page .woocommerce-form-register input[name="password"], ' +
        'body.woocommerce-account.woocommerce-page.woocommerce-edit-account input[name="password_1"]'
    ).on('keyup', function (e) {
        let strength = 0;

        if(this.value.length >= 6 && this.value.length <= 12) {
            strength++;
        }

        if(this.value.match(/[a-z]+/) || this.value.match(/[A-Z]+/)) {
            strength++;
        }

        if(this.value.match(/[0-9]+/)) {
            strength++;
        }

        if($('.woocommerce-password-strength').length){
            $('.woocommerce-password-strength').remove();
        }

        if($('.woocommerce-password-hint').length){
            $('.woocommerce-password-hint').remove();
        }

        if(strength == 3 && $('.woocommerce-form-register').length){
            $('<div class="woocommerce-password-strength strong" aria-live="polite">高</div>').appendTo($(this).parent('.password-input'));
            $('<small class="woocommerce-password-hint">提示: 密碼已符合<code> 6~12 </code>個字元，並在密碼中同時使用<code>英文字母、數字</code>，如此密碼較安全。</small>').appendTo($(this).parent('.password-input'));
        }else if(strength != 3 && $('.woocommerce-form-register').length){
            $('<div class="woocommerce-password-strength short" aria-live="polite">低</div>').appendTo($(this).parent('.password-input'));
            $('<small class="woocommerce-password-hint">提示: 建議密碼要有<code> 6~12 </code>個字元，並在密碼中同時使用<code>英文字母、數字</code>，如此密碼較安全。</small>').appendTo($(this).parent('.password-input'));
        }else if(strength == 3 && $('.woocommerce-edit-account').length){
            $('<div class="woocommerce-password-strength strong" aria-live="polite">高</div>').appendTo($(this).parent('.password-input'));
            $('<small class="woocommerce-password-hint">提示: 密碼已符合<code> 6~12 </code>個字元，並在密碼中同時使用<code>英文字母、數字</code>，如此密碼較安全。</small>').appendTo($(this).parent('.password-input'));
        }else if(strength != 3 && $('.woocommerce-edit-account').length){
            $('<div class="woocommerce-password-strength short" aria-live="polite">低</div>').appendTo($(this).parent('.password-input'));
            $('<small class="woocommerce-password-hint">提示: 建議密碼要有<code> 6~12 </code>個字元，並在密碼中同時使用<code>英文字母、數字</code>，如此密碼較安全。</small>').appendTo($(this).parent('.password-input'));
        }

    });

    /** 結束 **/


});