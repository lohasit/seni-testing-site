<?php
/*
 * [20220112] 更新WP 版本：5.7.2 > 5.7.5
 * [20220127] 更新WP 版本：5.7.5 > 5.9
 * [20220112] 更新Woocommerce 版本：5.3.0 -> 5.3.2
 * [20220113] 安裝並應用新主題：OlympusWP，新增子主題 olympuswp-child，取代OceanWP
 * [20220114] 安裝並應用RankMath，取代YoastSEO
 * [20220117] 新增默認身份帳號： lohaslife.shopmanager, lohaslife.editor, lohaslife.customer
 * [20220117] 根據默認身份帳號，綁定對應Gmail
 *
 * 移除外掛：
 * Lohasit Google OAuth
 * Lohasit Admin CSS and JS
 * Lohasit WC Member Woocommerce
 * Ocean Extra
 * Smush
 * Admin Menu Editor
 * Hide Admin Notice
 * Woocommerce Checkout Form
 *
 * 封存外掛(備用) > 雲端 ：
 * SliderRevolution
 * WP Extended Search
 * Wordfence Security
 * ManageWP-Worker
 * Classic Editor (傳統編輯器)
 * ACF QuickEdit Fields
 * Advanced Custom Fields
 * Custom Post Type UI
 * YITH WooCommerce Product Bundles Premium
 * YITH WooCommerce Points and Rewards Premium
 *
 * 新增外掛
 * ACF QuickEdit Fields(備用)
 * Advanced Custom Fields(備用)
 * Custom Post Type UI(備用)
 *
 * 更新外掛
 * AddToAny Share Buttons 1.7.44 > 1.8.3
 * Classic Editor 1.6 > 1.6.2
 * Elementor 3.2.3 > 3.5.3
 * Elementor Pro 3.0.5 > 3.5.2
 * Loco Translate 2.5.2 > 2.5.5
 * Wordfence Security 7.5.3 > 7.5.7
 *
 * 其他檔案更動：
 * wp-content/plugins/lohasit-woocommerce-customized/lohasit-woocommerce-customized.php
 * [20220118] 移除 add_uid() ，換成 modify_user_column_content()
 * [20220118] 移除 add_uid_column() ，換成 modify_user_column()
 * [20220118] 移除 Removed add_registration_date_to_user_list()
 * [20220118] 移除 Removed add_reg_date_column()
 * [20220118] 新增 Added modify_menus() - 管理後台選單
 * [20220118] 新增 Added modify_menusby_css()  - 管理後台選單
 * [20220118] 新增 Added remove_woocommerce_setting_tabs()  - 管理購物車系統的設定分頁
 * [20220119] 移除yit_plugin_fw_panel_option_args，已無作用
 * [20220119] 新增remove_all_admin_notices，隱藏後台提示信息（除最高權限外）
 * [20220119] 修改adjust_yith_submenu，移動YITH選單位置，加入到“行銷”
 * [20220119] 新增 modify_dynamic_pricing_rule_metaboxes() - 調整動態定價(Dynamic Pricing)的規則設定欄位
 * [20220119] 新增 modify_dynamic_pricing_rule_options() - 調整動態定價規則類型
 * [20220119] 新增 redirect_away_from_dynamic_pricing_general_setting() - 商店管理員轉導向到動態定價的設定規則頁
 * [20220119] 移除 modify_yith_dynamic_pricing_sections() - 調整動態定價設定分頁
 * [20220120] 新增 modify_yith_request_a_quote_tabs() 調整YITH詢價外掛的分頁
 * [20220120] 新增 modify_yith_dynamic_pricing_general_options - 調整YITH動態定價一般設定欄位
 * [20220120] 新增 modify_yith_reward_points_tabs -  調整YITH積分與點數分頁
 * [20220120] 新增 modify_yith_reward_points_standard_options -  調整YITH積分與點數設定欄位
 * [20220120] 新增 modify_yith_wishlist_tabs -  調整YITH願望清單的分頁
 * [20220120] 新增 modify_yith_request_a_quote_tabs -  調整YITH詢價外掛的分頁
 * [20220120] 新增 modify_yith_request_a_quote_order_metaboxes - 調整YITH詢價訂單欄位
 * [20220120] 移除 hide_yith_wishlist_sub_tabs()
 * [20220121] my-account頁面，根據登入狀況新增class "login"
 * [20220121] 新增鉤子woocommerce_edit_account_after_email ： wp-content/themes/olympuswp-child/woocommerce/myaccount/form-edit-account.php
 * [20220125] 移除 hide_unwanted_fields()
 * [20220125] 移除 modify_menus_by_css()
 * [20220125] 移除 load_front_page_css()
 * [20220125] 新增 set_birthdate_field_at_user_profile()
 * [20220125] 新增 save_user_profile_birthdate_fields()
 * [20220125] 新增 cart_coupon_discount_text()
 *
 * lohasit-woocommerce-customized 外掛內新增檔案
 * wp-content/plugins/lohasit-woocommerce-customized/includes/class-lohasit-cart.php
 * wp-content/plugins/lohasit-woocommerce-customized/includes/class-lohasit-coupon.php
 * wp-content/plugins/lohasit-woocommerce-customized/includes/class-lohasit-product.php
 *
 * 其他檔案夾的記錄更動
 * wp-content/plugins/wp-security-audit-log/classes/Views/ToggleAlerts.php
 * wp-content/plugins/wp-security-audit-log/classes/Views/Settings.php
 * wp-content/plugins/wp-security-audit-log/classes/Views/AuditLog.php
 * [20220119] 修改WP日誌記錄權限
 *
 * wp-content/plugins/yith-woocommerce-wishlist-premium/includes/tables/class.yith-wcwl-popular-table.php
 * [20220120] 調整YITH願望清單可用動作
 *
 * wp-content/plugins/lohasit-ecpay-gateway/lib/wc-ecpay-gateway.php
 * wp-content/plugins/lohasit-ecpay-gateway/lohasit-ecpay-gateway.php
 * [20220122] 綠界金流：預設付款選項，無需從資料庫撈取
 *
 * wp-content/plugins/revslider/admin/revslider-admin.class.php
 * wp-content/plugins/revslider/includes/functions.class.php
 * [20220125] 處理外掛衝突以及多語系失效問題
 *
 * wp-content/themes/olympuswp-child/woocommerce/myaccount/form-edit-account.php
 * [20220121] 新增鉤子woocommerce_edit_account_after_email
 *
 * wp-content/plugins/lohasit-ecpay-logistics/includes/class-alan-admin-order.php
 * [20220127] 判斷$shipping是否是空值
 *
 * wp-content/plugins/lohasit-ecpay-invoice/includes/class-alan-invoice-order.php
 * [20220127] 改用$post->ID指定order_id
 *
 * wp-content/plugins/lohasit-ecpay-home-delivery/wc2-shipping-ecpay-home.php
 * [20220127] 新增判斷$shipping是否是空值
 *
 * wp-content/plugins/lohasit-ecpay-logistics/includes/class-alan-ecpay.php
 * [20220127] 修改警示訊息顏色，附加class可彈性調整
 *
 * wp-content/plugins/yith-woocommerce-request-a-quote-premium/includes/class.yith-request-quote-admin.php
 * [20220129] 後台選單標題附上多語系
 *
 * wp-content/plugins/lohasit-ecpay-gateway/lib/wc-ecpay-gateway.php
 * 4.0.9 修改write_log() 和 check_ecpay_response()
 *
 * wp-content/plugins/yith-woocommerce-dynamic-pricing-and-discounts-premium/includes/class-yith-wc-dynamic-pricing-frontend.php
 * 4.1.0 修正運算類型為int
 *
 * wp-content/plugins/yith-woocommerce-quick-view-premium/includes/functions.yith-wcqv.php
 * wp-content/plugins/yith-woocommerce-quick-view-premium/templates/yith-quick-view-content.php
 * 4.1.2 修改YITH快速查看的彈窗排版
 *
 *
 */
