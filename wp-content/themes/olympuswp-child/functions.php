<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: olympuswp
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

// Sample of enqueuing scripts
//add_action( 'wp_enqueue_scripts', 'lohasit_add_custom_scripts_sample' );

function lohasit_add_custom_scripts_sample()
{

    wp_enqueue_script( 'olympus-lohasit-js', get_stylesheet_directory_uri() . '/assets/global.js', array(), OLY_VERSION, true );
    wp_enqueue_style( 'olympus-lohasit-style', get_stylesheet_directory_uri() . '/assets/global.css', array(), OLY_VERSION );

}

/**
 * Lohas IT 引入壓板自訂功能的php
 * alan:
 * lohasit-custom.php 之前工程師寫的或是最近加的有點雜亂,
 * 有時間請把WooCommerce相關修改拉到
 * lohasit-woocommerce-customized 這個外掛
 */
require_once __DIR__ . "/lohasit-custom.php";
require_once __DIR__ . "/lohasit-helper-functions.php";
require_once __DIR__ . "/lohasit-defend.php";

// Lohas IT 其他客戶客製化請寫入此檔案或引入其他檔案
require_once __DIR__ . '/includes/lohasit-customized.php';
require_once __DIR__ . '/includes/member/lohasit-member-custom.php';
