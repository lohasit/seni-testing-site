<?php
if (!defined('LIT_DIR')) {
    define('LIT_DIR', get_stylesheet_directory());
}

if (!defined('LIT_URL')) {
    define('LIT_URL', get_stylesheet_directory_uri());
}

class LohasItCustom
{
    private static $instance;

    public $menu_names = [];

    public function __construct()
    {
        load_child_theme_textdomain('lohasit', plugin_dir_path(__FILE__) . 'languages');

        $this->menu_names = [
            'ARForms'                            => __('Manage Forms', 'lohasit'),
            'woocommerce'                        => __('Cart System', 'lohasit'),
            'mailpoet-newsletters'               => __('Newsletters', 'lohasit'),
            'revslider'                          => __('Revslider', 'lohasit'),
            'elementor'                          => __('Elementor', 'lohasit'),
            'edit.php?post_type=elementor_cf_db' => __('Elementor DB', 'lohasit'),
            'facebook-messenger-customer-chat'   => __('Facebook Chat', 'lohasit'),
            'google_oauth'                       => __('Lohas IT SSO', 'lohasit'),
            'WP-Optimize'                        => __('WP Optimize', 'lohasit'),
            'meowapps-main-menu'                 => __('Meow Apps', 'lohasit'),
            'oceanwp-panel'                      => __('Theme Panel', 'lohasit'),

        ];

        $this->register_hooks();
    }

    public static function get_instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function register_hooks()
    {
        /**
         *移除admin bar的wordpress logo
         */
        add_action('admin_bar_menu', array($this, 'remove_admin_bar_wordpress_logo'), 999);
        /**
         * 針對非管理員移除admin bar上的項目
         */
        add_action('wp_before_admin_bar_render', array($this, 'remove_admin_bar_none_admin_nodes'), 1000);
        /**
         * 取消 Wordpress 自動更新
         */
        add_filter('pre_site_transient_update_core', array($this, 'remove_core_updates'));
        add_filter('pre_site_transient_update_plugins', array($this, 'remove_core_updates'));
        add_filter('pre_site_transient_update_themes', array($this, 'remove_core_updates'));
        /**
         * 加入版權資訊至admin頁面的footer
         */
        add_filter('admin_footer_text', array($this, 'replace_admin_footer'));
        /**
         * 預設前台頁尾Copyright區塊文字
         */
        add_filter('lohasit_default_footer_copyright_text', array($this, 'default_footer_copyright_text'));

        /**
         * 載入後台CSS檔案
         */
        add_action('admin_head', array($this, 'load_admin_css_files'));

        /**
         * 載入後台JS檔案
         */
        add_action('admin_head', array($this, 'load_admin_js_files'));

        /**
         * 增加角色的body-class至後台
         */
        add_filter('admin_body_class', array($this, 'add_user_role_to_admin_body_classes'));
        /**
         * 增加角色的body-class至前台
         */
        add_filter('body_class', array($this, 'add_user_role_to_body_classes'));
        /**
         * 修改WordPress從文章內容產生的摘要內的點點點樣式
         */
        add_filter('excerpt_more', array($this, 'override_excerpt_more'), 999);
        /**
         * 從HTML的資源連結中移除網站網址資訊
         */
        add_action('get_header', array($this, 'remove_domain_from_html'));
        /**
         * 修改選單翻譯
         */
        add_action('admin_menu', array($this, 'override_menu_names'));
        /**
         * 針對特定分類、靜態頁面、單一文章頁自動載入指定的CSS與JS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_type_css_js_files'), 102);
        /**
         * 自動載入全域CSS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_global_css_files'), 101);
        /**
         * 自動載入全域JS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_global_js_files'), 100);
        /**
         * 移除後台post列表頁不必要的欄位
         */
        add_action('admin_head', array($this, 'remove_unused_columns'));

        /**
         * 停用WordPress核心的自動更新
         */
        add_filter('auto_update_plugin', '__return_false');
        add_filter('auto_update_theme', '__return_false');

        /**
         * 停用原本WooCommerce的密碼強度判斷
         * */
        add_action('wp_print_scripts', [$this, 'remove_password_strength'], 10);

        /**
         * 限制非最高權限者取得頁面的權限。
         */
        add_action('pre_get_posts', [$this, 'exclude_page_for_customer'], 999);


        /**
         *  20222010 beta4更新功能 開端
         */
        /*
         * 20210722 使用者列表增加可調整排序的使用者ID欄位 - JJ
         */
        add_filter('manage_users_custom_column', [$this, 'modify_user_column_content'], 10, 3);
        add_filter('manage_users_columns',  [$this, 'modify_user_column'], 900);
        add_filter('manage_users_sortable_columns',   [$this, 'rudr_make_uid_column_sortable']);

        /*
         * 20210722 使用者列表增加可調整排序的會員註冊日期 - JJ
         */
        add_filter('manage_users_sortable_columns',   [$this, 'rudr_make_registered_column_sortable']);

        // [20220118] 調整後台選單可視範圍 - JJ
        add_action('admin_menu',  [$this, 'modify_menus'], 999);

        // [20220119] 隱藏後台提示信息（除最高權限外） - JJ
        add_action('in_admin_header',   [$this, 'remove_all_admin_notices'], 99);

        // [20220125] 後台編輯使用者出生日期欄位 - JJ
        add_action('show_user_profile',   [$this, 'set_birthdate_field_at_user_profile'] , 7);
        add_action('edit_user_profile',  [$this, 'set_birthdate_field_at_user_profile'], 7);

        // [20220125] 後台編輯使用者出生日期欄位，儲存邏輯 - JJ
        add_action( 'personal_options_update',  [$this, 'save_user_profile_birthdate_fields'] );
        add_action( 'edit_user_profile_update',  [$this, 'save_user_profile_birthdate_fields'] );

        // [20220210] 後台登入頁移除語言選擇器，儲存邏輯 - JJ
        add_filter( 'login_display_language_dropdown', '__return_false' );

        // [20220210] 開放上傳SVG檔案至媒體庫 - JJ
        add_filter( 'upload_mimes',  [$this, 'cc_mime_types'] );

        // [20220210] 調整[編輯]角色的後台選單可視範圍 - JJ
        add_action('admin_menu', [$this, 'editor_modify_menus'], 999);

        // [20220210] 通過CSS調整[編輯]角色的後台選單可視範圍 - JJ
        add_action('admin_head', [$this,'editor_modify_menus_by_css'], 999);

        // [20220210]  [編輯]角色在編輯使用者可指定他人的角色 - JJ
        add_filter('editable_roles', [$this, 'set_editor_assignable_roles'], 99);

        // [20220306]  啟用WP Activity Log 發生 fs_active_plugins取值為False 的替代值 - Alan
        add_filter('option_fs_active_plugins', [$this, 'set_fs_active_plugins'], 2, 999);

        // [20220319]  若啟用 WP Activity Log 出現fs_active_plugins相關的錯誤，可用domain/?delete_fs移除，然後再清除memcached - JJ
        add_action('init', [$this, 'delete_fs_active_plugins_option'], 999);


        /**
         *  20222010 beta4更新功能 尾端
         */

    }

    public function delete_fs_active_plugins_option()
    {
//        if($_GET['delete_fs'] == 'yes' && current_user_can('administrator')) :
        if($_GET['delete_fs'] == 'yes') :

                delete_option('fs_active_plugins');

            endif;
    }

    public function set_fs_active_plugins ($value, $option)
    {
//        if ('fs_active_plugins' === $option && false === $value) {
        if ('fs_active_plugins' === $option) {
            $o                = new stdClass;
            $v                = new stdClass;
            $n                = new stdClass;
            $v->version       = '2.4.2';
            $v->type          = 'plugin';
            $v->timestamp     = '1646389365';
            $v->plugin_path   = 'wp-security-audit-log/wp-security-audit-log.php';
            $n->plugin_path   = $v->plugin_path;
            $n->sdk_path      = 'wp-security-audit-log/third-party/freemius/wordpress-sdk';
            $n->version       = $v->version;
            $n->in_activation = null;
            $n->timestamp     = $v->timestamp;
            $o->plugins       = [
                'wp-security-audit-log/third-party/freemius/wordpress-sdk' => $v,
            ];
            $o->abspath = $_SERVER['DOCUMENT_ROOT'] . '/';
            $o->newest  = $n;

            return $o;
        }
        return $value;
    }

    public function exclude_page_for_customer($query)
    {
        global $pagenow, $current_user;
        if ('edit.php' == $pagenow && 'page' == get_query_var('post_type')) {
            $user_roles  = $current_user->roles;
            $hidden_page = [
                get_option('woocommerce_shop_page_id'),
                get_option('woocommerce_cart_page_id'),
                get_option('woocommerce_checkout_page_id'),
                get_option('woocommerce_myaccount_page_id'),
                get_option('yith_wcwl_wishlist_page_id'),
            ];
            if (in_array('administrator', $user_roles)) {
                return $query;
            } else {
                $query->set('post__not_in', $hidden_page);
            }
        }
        return $query;
    }

    public function remove_password_strength()
    {
        if (function_exists('WC')) {
            wp_dequeue_script('wc-password-strength-meter');
        }
    }

    public function modify_multiple_value_to_array($pricing_rules)
    {
        $rules_name = array(
            'apply_to_categories_list', 'apply_to_categories_list_excluded',
            'apply_adjustment_categories_list', 'apply_adjustment_categories_list_excluded',
            'user_rules_customers_list', 'user_rules_customers_list_excluded',
            'apply_to_products_list', 'apply_adjustment_products_list_excluded',
            'apply_adjustment_products_list', 'apply_adjustment_products_list_excluded',
            'apply_to_tags_list', 'apply_to_tags_list_excluded',
            'apply_adjustment_tags_list', 'apply_adjustment_tags_list_excluded',
        );

        foreach ($pricing_rules as $key => $rule) {
            foreach ($rule as $name => $value) {
                if ($value && in_array($name, $rules_name)) {
                    $pricing_rules[$key][$name] = explode(',', $value);
                }
            }
        }

        return $pricing_rules;
    }

    public function load_admin_js_files()
    {
        $style_uri   = get_stylesheet_directory_uri();
        $js_file     = '/assets/js/admin-js.js';
        $min_js_file = '/assets/js/admin-js.js';
        if (file_exists(get_stylesheet_directory() . $min_js_file)) {
            wp_enqueue_script("lohasit_admin_js", $style_uri . $min_js_file);
        } elseif (file_exists(get_stylesheet_directory() . $js_file)) {
            wp_enqueue_script("lohasit_admin_js", $style_uri . $js_file);
        }
    }

    public function rm_column_heading($columns)
    {
        $rm_column = array('search_weight', 'wpseo-links', 'wpseo-score', 'wpseo-score-readability', 'wpseo-title', 'wpseo-metadesc', 'wpseo-focuskw');
        foreach ($rm_column as $key => $value) {
            unset($columns[$value]);
        }
        return $columns;
    }

    public function remove_unused_columns()
    {
        foreach (get_post_types() as $key) {
            add_filter('manage_' . $key . '_posts_columns',
                array($this, 'rm_column_heading'), 999, 1);
        }
    }

    public function get_current_user_role_body_class()
    {
        return 'role-' . lohasit_get_current_user_role();
    }

    public function remove_admin_bar_wordpress_logo($wp_admin_bar)
    {
        /** @var WP_Admin_Bar $wp_admin_bar */
        $wp_admin_bar->remove_node('wp-logo');
    }

    public function remove_admin_bar_none_admin_nodes()
    {
        global $wp_admin_bar;
        /** @var WP_Admin_Bar $wp_admin_bar */
        if (!current_user_can('administrator')) {
            //W3 Total Cache
            $wp_admin_bar->remove_node('w3tc');
            //語系切換
            $wp_admin_bar->remove_node('WPML_ALS');
            //Simply Show Hooks
            $wp_admin_bar->remove_node('cxssh-main-menu');
        }
    }

    public function remove_core_updates()
    {
        global $wp_version;
        return (object) array('last_checked' => time(), 'version_checked' => $wp_version);
    }

    public function replace_admin_footer()
    {
        return 'Designed by ' . self::get_official_site_link();
    }

    public function default_footer_copyright_text()
    {
        return '&copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All Rights Reserved. Designed by ' . self::get_official_site_link();
    }

    public function load_admin_css_files()
    {
        $style_uri    = get_stylesheet_directory_uri();
        $css_file     = '/assets/css/admin-css.css';
        $min_css_file = '/assets/css/admin-css.css';
        if (file_exists(get_stylesheet_directory() . $min_css_file)) {
            wp_enqueue_style("lohasit_admin_css", $style_uri . $min_css_file);
        } elseif (file_exists(get_stylesheet_directory() . $css_file)) {
            wp_enqueue_style("lohasit_admin_css", $style_uri . $css_file);
        }

    }

    public function add_user_role_to_admin_body_classes($classes)
    {
        $classes .= ' ' . $this->get_current_user_role_body_class();
        return $classes;
    }

    public function add_user_role_to_body_classes($classes)
    {
        $classes[] = $this->get_current_user_role_body_class();
        return $classes;
    }

    public function override_excerpt_more($excerpt_more)
    {
        return '...';
    }

    public function remove_domain_from_html()
    {
        global $CONFIG;
        if (isset($CONFIG['domains_to_be_removed'])) {
            ob_start(function ($html) use ($CONFIG) {
                foreach ($CONFIG['domains_to_be_removed'] as $url) {
                    $url      = str_replace('.', '\.', $url);
                    $patterns = [
                        "/https?:\/\/$url\/*/",
                        "/\/\/$url\/*/",
                    ];
                    $html = preg_replace($patterns, '/', $html);
                }
                return $html;
            });
        }
    }

    public static function get_official_site_link()
    {
        return '<a target="_blank" rel="nofollow" href="https://www.lohaslife.cc">LOHAS IT</a>';
    }

    public function override_menu_names()
    {
        global $menu;
//        dd($menu);
        foreach ((array) $menu as $i => $_menu) {
            $menu_slug = $_menu[2];
            if (!empty($this->menu_names[$menu_slug])) {
                $menu[$i][0] = $this->menu_names[$menu_slug];
            }
        }

        return;
    }

    /**
     * 針對特定分類、靜態頁面、單一文章頁自動載入指定的CSS與JS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_type_css_js_files()
    {
        global $post;
        $css_dir    = '/assets/css';
        $js_dir     = '/assets/js';
        $file_names = [];
        if (is_page() || is_404()) {
            $type    = 'page';
            $css_dir = $css_dir . '/page/';
            $js_dir  = $js_dir . '/page/';
            if (is_front_page()) {
                $file_names[] = 'home';
            } elseif (is_404()) {
                $file_names[] = '404';
            } else {
                $file_names[] = $post->post_name;
                $temp_page    = $post;
                while ($temp_page->post_parent) {
                    $temp_page    = get_post($temp_page->post_parent);
                    $file_names[] = $temp_page->post_name;
                }
            }
        } elseif (is_archive() || (!is_front_page() && is_home())) {
            $type         = 'archive';
            $css_dir      = $css_dir . '/archive/';
            $js_dir       = $js_dir . '/archive/';
            $file_names[] = get_post_type();
            if (!$file_names[0]) {
                global $wp_taxonomies;
                $term          = get_queried_object();
                $post_types    = (isset($wp_taxonomies[$term->taxonomy])) ? $wp_taxonomies[$term->taxonomy]->object_type : [];
                $file_names[0] = $post_types[0];
            }
        } elseif (is_single()) {
            $type         = 'single';
            $css_dir      = $css_dir . '/single/';
            $js_dir       = $js_dir . '/single/';
            $file_names[] = get_post_type();
        } else {
            return;
        }

        foreach ($file_names as $file_name) {
            $min_css = $css_dir . "$file_name.min.css";
            $css     = $css_dir . "$file_name.css";
            $min_js  = $js_dir . "$file_name.min.js";
            $js      = $js_dir . "$file_name.js";

            if (file_exists(LIT_DIR . $min_css)) {
                wp_enqueue_style("lohasit-{$type}-css-{$file_name}", LIT_URL . $min_css);
            } elseif (file_exists(LIT_DIR . $css)) {
                wp_enqueue_style("lohasit-{$type}-css-{$file_name}", LIT_URL . $css);
            }

            if (file_exists(LIT_DIR . $min_js)) {
                wp_enqueue_script("lohasit-{$type}-js-{$file_name}", LIT_URL . $min_js, ['jquery'], false, true);
            } elseif (file_exists(LIT_DIR . $js)) {
                wp_enqueue_script("lohasit-{$type}-js-{$file_name}", LIT_URL . $js, ['jquery'], false, true);
            }
        }
    }

    /**
     * 載入全域CSS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_global_css_files()
    {
        $directory = '/assets/css/global/';
        $files     = glob(LIT_DIR . $directory . '*.css');
        foreach ($files as $key => $file) {
            $file_name      = pathinfo($file)['filename'];
            $file_base_name = str_replace('.min', '', $file_name);
            $files[$key]    = $file_base_name;
        }
        foreach (array_unique($files) as $css_filename) {
            $min_css = "$css_filename.min.css";
            $css     = "$css_filename.css";
            if (file_exists(LIT_DIR . $directory . $min_css)) {
                wp_enqueue_style("lohasit-global-css-{$min_css}", LIT_URL . $directory . $min_css);
            } elseif (file_exists(LIT_DIR . $directory . $css)) {
                wp_enqueue_style("lohasit-global-css-{$css}", LIT_URL . $directory . $css);
            }

        }
    }

    /**
     * 載入全域CSS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_global_js_files()
    {
        $directory = '/assets/js/global/';
        $files     = glob(LIT_DIR . $directory . '*.js');
        foreach ($files as $key => $file) {
            $file_name      = pathinfo($file)['filename'];
            $file_base_name = str_replace('.min', '', $file_name);
            $files[$key]    = $file_base_name;
        }
        foreach (array_unique($files) as $js_filename) {
            $min_js = "$js_filename.min.js";
            $js     = "$js_filename.js";
            if (file_exists(LIT_DIR . $directory . $min_js)) {
                wp_enqueue_script("lohasit-global-js-{$min_js}", LIT_URL . $directory . $min_js, ['jquery'], false, true);
            } elseif (file_exists(LIT_DIR . $directory . $js)) {
                wp_enqueue_script("lohasit-global-js-{$js}", LIT_URL . $directory . $js, ['jquery'], false, true);
            }

        }
    }

    /**
     *  20222010 beta4更新功能 開端
     */

    public function modify_user_column_content( $val, $column_name, $user_id )
    {

        $u_id = $user_id;

        switch ($column_name) {

            case 'user_id' :
                return $u_id ;
            default:

        }

//        $date_format = 'd/M/Y';
        $date_format = 'Y-m-d';

        $reg_date = get_userdata( $user_id  )->user_registered;

        switch ($column_name) {

            case 'registration_date' :
                return date( $date_format, strtotime( $reg_date ) ) ;
                break;
            default:

        }

        return $val;
    }

    public function modify_user_column( $column )
    {
        $username = $column['username'] ;
        $name = $column['name'] ;
        $email = $column['email'] ;
        $role = $column['role'] ;
        $reg_date = $column['registration_date'];

        unset($column['username']);
        unset($column['name']);
        unset($column['email']);
        unset($column['role']);
        unset($column['posts']);
        unset($column['wfls_2fa_status']);
        unset($column['wfls_last_login']);

        $column['user_id'] = __('ID','lohasit');
        $column['username'] = $username;
        $column['name'] = $name;
        $column['email'] = $email;
        $column['role'] = $role;
        $column['registration_date'] = __('Registration Date','lohasit');

        return $column;

    }

    public function rudr_make_uid_column_sortable( $columns )
    {

        return wp_parse_args( array( 'user_id' => 'id' ), $columns );


    }

    public function rudr_make_registered_column_sortable( $columns )
    {

        return wp_parse_args( array( 'registration_date' => 'user_registered' ), $columns );

    }

    public function modify_menus()
    {

        if(!current_user_can('administrator')) :


            remove_menu_page( 'liv_menu');
            remove_menu_page( 'edit.php?post_type=acf-field-group' );
            remove_menu_page( 'oceanwp-panel' );
            remove_menu_page( 'login-customizer-settings' );
            remove_menu_page( 'elementor-role-manager');

            remove_menu_page( 'tools.php' );
            remove_menu_page( 'oa_social_login_setup' );
            remove_menu_page( 'cptui_main_menu' );

            remove_menu_page( 'rank-math' );
            remove_menu_page( 'yith_plugin_panel' );
            remove_menu_page( 'wpseo_workouts' );


            remove_submenu_page( 'woocommerce', 'wc-status' );
            remove_submenu_page( 'woocommerce', 'wc-addons' );
            remove_submenu_page( 'woocommerce', 'checkout_form_designer' );


            remove_submenu_page( 'edit.php?post_type=product', 'product_importer' );
            remove_submenu_page( 'edit.php?post_type=product', 'product_exporter' );
            remove_submenu_page( 'options-general.php', 'options-general.php' );
            remove_submenu_page( 'options-general.php', 'options-writing.php' );
            remove_submenu_page( 'options-general.php', 'duplicatepost' );
            remove_submenu_page( 'options-general.php', 'options-media.php' );
            remove_submenu_page( 'options-general.php', 'options-permalink.php' );
            remove_submenu_page( 'options-general.php', 'options-privacy.php' );
            remove_submenu_page( 'options-general.php', 'addtoany' );
            remove_submenu_page( 'options-general.php', 'wdan_settings-wbcr_dan' );
            remove_submenu_page( 'options-general.php', 'disable_right_click_for_wp_dashboard' );
            remove_submenu_page( 'options-general.php', 'admin_css' );

            remove_submenu_page( 'elementor' , 'elementor');
            remove_submenu_page( 'elementor' , 'edit.php?post_type=elementor_font');
            remove_submenu_page( 'elementor' , 'edit.php?post_type=elementor_snippet');
            remove_submenu_page( 'elementor' , 'edit.php?post_type=elementor_icons');
            remove_submenu_page( 'elementor' , 'elementor-role-manager');
            remove_submenu_page( 'elementor' , 'elementor-tools');
            remove_submenu_page( 'elementor' , 'elementor-system-info');
            remove_submenu_page( 'elementor' , 'elementor-getting-started');
            remove_submenu_page( 'elementor' , 'go_knowledge_base_site');
            remove_submenu_page( 'elementor' , 'elementor-license');

            if(!current_user_can('shopmanager')) :

                remove_submenu_page( 'woocommerce', 'yith_woocommerce_request_a_quote');

                endif;

            else:

                add_submenu_page('woocommerce', __('Variation Swatches Settings', 'lohasit'),  __('Variation Swatches Settings', 'lohasit'),'manage_woocommerce', 'woo-variation-swatches-settings');

        endif;


        remove_submenu_page( 'woocommerce', 'wc-admin&path=/customers' );
        remove_submenu_page( 'woocommerce', 'coupons-moved' );
        remove_menu_page( 'woo-variation-swatches-settings' );
    }

    public function remove_all_admin_notices()
    {

//        if (!current_user_can('administrator')) {
//            remove_all_actions( 'user_admin_notices' );
        remove_all_actions( 'admin_notices' );
//        }
    }

    public function set_birthdate_field_at_user_profile( $user ) {

        $birthdate = get_user_meta($user->ID, 'user_birthdate', true);
        $birthdate = date( 'Y-m-d', strtotime( $birthdate ) );
        ?>

        <table class="form-table">
            <tr>
                <th><label for="user_birthdate"><?php esc_html_e( 'User Birthdate', 'lohasit' ); ?></label></th>
                <td>
                    <input type="date"
                           id="user_birthdate"
                           name="user_birthdate"
                           value="<?php echo $birthdate?$birthdate:''; ?>"
                           placeholder="Y-m-d"
                           class="regular-text"
                           pattern = "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
                           max="<?=date('Y-m-d'); ?>"
                    />
                </td>
            </tr>
        </table>

        <?php
    }

    public function save_user_profile_birthdate_fields($user_id)
    {
        if ( isset( $_POST['user_birthdate'] ) ) :
            update_user_meta( $user_id, 'user_birthdate', $_POST['user_birthdate'] );
        endif;
    }

    public function cc_mime_types( $mimes ){

        $mimes['svg'] = 'image/svg+xml';
        return $mimes;

    }

    public function editor_modify_menus()
    {



        if(!current_user_can('shopmanager') && !current_user_can('administrator')) :

            remove_submenu_page( 'edit.php?post_type=elementor_library' , 'tabs_group=popup');
            remove_submenu_page( 'edit.php?post_type=elementor_library' , 'e-landing-page');
            remove_submenu_page( 'edit.php?post_type=elementor_library' , 'elementor_library_category');

        endif;


    }

    public function editor_modify_menus_by_css()
    {

        $url = get_site_url();
        if(!current_user_can('shopmanager') && !current_user_can('administrator')) :
            ?>
            <style>

                a[href="<?php echo $url ?>/wp-admin/admin.php?page=elementor-app&ver=3.5.3#/kit-library"] {
                    display: none!important;
                }
                a[href="<?php echo $url ?>/wp-admin/edit.php?post_type=elementor_library#add_new"] {
                    display: none!important;
                }
                a[href="edit-tags.php?taxonomy=elementor_library_category&post_type=elementor_library"] {
                    display: none!important;
                }


            </style>

        <?php
        endif;

    }

    public function set_editor_assignable_roles($roles)
    {

        if(current_user_can('editor')) :

            unset($roles['customer']);
            unset($roles['wpseo_editor']);
            unset($roles['wpseo_editor']);
            unset($roles['wpseo_manager']);
            unset($roles['vip']);
            unset($roles['vvip']);
            unset($roles['vvvip']);
            unset($roles['shop_manager']);
            unset($roles['translator']);

        endif;

        return $roles;

    }


    /**
     *  20222010 beta4更新功能 尾端
     */

}

$GLOBALS['LohasItCustom'] = LohasItCustom::get_instance();
