<?php

/**
 * 弱點掃描修正模組
 * DANNY 20211225
 */
class Lohasit_Defend
{

    public function __construct()
    {
        $this->register_hooks();
    }

    public function register_hooks()
    {
        add_action('plugins_loaded', array($this, 'filter_sql_injection_hook') , 0);

    }

    public function filter_sql_injection_hook()
    {
        $safe = true;
        foreach(['min_price','max_price'] as $key){
            if(isset($_REQUEST[$key])){
                $val = $_REQUEST[$key];
                if (filter_var($val, FILTER_VALIDATE_FLOAT) === false) {
                    $safe = false;
                    break;
                }
            }
        }
        foreach(['wishlist_notice','wc_reset_password'] as $key){
            if(isset($_REQUEST[$key])){
                $val = $_REQUEST[$key];
                if (!in_array((string)$val, ['true', 'false'])) {
                    $safe = false;
                    break;
                }
            }
        }
        foreach(['p','page_id'] as $key){
            if(isset($_REQUEST[$key])){
                $val = $_REQUEST[$key];
                if (!preg_match("/^\d+$/", $val)) {
                    $safe = false;
                    break;
                }
            }
        }
        foreach(['register','_wpnonce'] as $key){
            if(isset($_REQUEST[$key])){
                $val = $_REQUEST['register'];
                if (is_attacks_patten($val)) {
                    $safe = false;
                }
            }
        }
        if(isset($_REQUEST['products-per-page'])){
            $val = $_REQUEST['products-per-page'];
            if ($val!='all' && !preg_match("/^\d+$/", $val)) {
                $safe = false;
            }
        }
        if(isset($_REQUEST['woocommerce-login-nonce'])){
            $val = $_REQUEST['woocommerce-login-nonce'];
            if (preg_match('/[^A-Za-z0-9]/', $val)) {
                $safe = false;
            }
        }
        if(isset($_REQUEST['comment'])){
            $val = $_REQUEST['comment'];
            if ( !((preg_match("/^\d+$/", $val)) || ( is_object( $val ) && ! empty( $val )) || ( $val instanceof YITH_WCWL_Wishlist_Item )) ) {
                $safe = false;
            }
        }


        if($safe === false){
            status_header( 404 );
            nocache_headers();
            wp_redirect('/404');
            die();
        }
    }

    private function is_attacks_patten($str)
    {
        //Regex for detection of SQL meta-characters
        if(preg_match("/(\%27)|(\')|(\-\-)|(\%23)|(#)/i", $str))
            return true;

        //Modified regex for detection of SQL meta-characters
        if(preg_match("/((\%3D)|(=))[^\n]*((\%27)|(\')|(\-\-)|(\%3B)|(;))/i", $str))
            return true;

        //Regex for typical SQL Injection attack
        if(preg_match("/\w*((\%27)|(\'))((\%6F)|o|(\%4F))((\%72)|r|(\%52))/i", $str))
            return true;

        //Regex for detecting SQL Injection with the UNION keyword
        if(preg_match("/((\%27)|(\'))union/i", $str))
            return true;

        //Regex for detecting SQL Injection attacks on a MS SQL Server
        if(preg_match("/exec(\s|\+)+(s|x)p\w+/i", $str))
            return true;

        //Regular Expressions for Cross Site Scripting (CSS)
        if(preg_match("/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/i", $str))
            return true;

        //Regex for simple CSS attack
        if(preg_match("/((\%3C)|<)((\%69)|i|(\%49))((\%6D)|m|(\%4D))((\%67)|g|(\%47))[^\n]+((\%3E)|>)/i", $str))
            return true;

        //Regex for "<img src" CSS attack
        if(preg_match("/((\%3C)|<)[^\n]+((\%3E)|>)/i", $str))
            return true;

        return false;
    }
}

return new Lohasit_Defend;