<?php

if (!function_exists('lohasit_is_string_contains_chinese')) {
    function lohasit_is_string_contains_chinese($str)
    {
        return preg_match("/\p{Han}+/u", $str);
    }
}

if (!function_exists('lohasit_get_current_user_role')) {
    function lohasit_get_current_user_role()
    {
        $user = wp_get_current_user();
        return array_shift($user->roles);
    }
}

/**
 * Dump variable.
 * Created by alan.
 */
if (!function_exists('d')) {
    function d()
    {
        call_user_func_array('dump', func_get_args());
    }
}

/**
 * Dump variables and die.
 * Created by alan.
 */
if (!function_exists('dd')) {

    function dd()
    {
        call_user_func_array('dump', func_get_args());
        die();
    }

}
