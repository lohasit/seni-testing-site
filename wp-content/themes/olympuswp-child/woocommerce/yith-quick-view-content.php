<?php
/**
 * Quick view content.
 *
 * @author  YITH
 * @package YITH WooCommerce Quick View
 * @version 1.0.0
 */

defined( 'YITH_WCQV' ) || exit; // Exit if accessed directly.

while ( have_posts() ) :
	the_post();
	?>
	<div class="product">
		<?php if ( ! post_password_required() ) { ?>

			<div id="product-<?php the_ID(); ?>" <?php post_class( 'product' ); ?>>
				<input type="hidden" id="yith_wcqv_product_id" value="<?php the_ID(); ?>"/>

<!--				--><?php //do_action( 'yith_wcqv_product_image' ); ?>

				<?php do_action( 'yith_wcqv_before_product_summary' ); ?>

				<div class="summary entry-summary" data-scrollbar>
                    <div class="yith_wcqv_custom_upper_section">
                    <?php do_action( 'yith_wcqv_product_image' ); ?>

					<div class="summary-content">
						<?php do_action( 'yith_wcqv_product_summary' ); ?>
					</div>

				</div>

				<?php do_action( 'yith_wcqv_after_product_summary' ); ?>

			</div>

			<?php

		} else {
			echo get_the_password_form(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		}
		?>
	</div>
	<?php
endwhile; // end of the loop.
